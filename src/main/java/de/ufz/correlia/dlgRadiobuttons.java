/*
Dialogue checkboxes takes an array of strings and creates a dialogue with according
checkboxes. It provides a method returning an array of Boolean representing the
states of the checkbox.

author: Matthias Schmidt
date: March 20, 2015
*/

package de.ufz.correlia;
import java.util.*;
import java.util.Vector;
import java.awt.*;
import java.io.*;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.Box;
import javax.swing.BoxLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

// DEBUG, can be removed
// import ij.*;
////////////////////////

class dlgRadiobuttons extends JDialog
{
	dlgRadiobuttons( String dlgTitle, String[] button_names, Boolean hideOKCancel )
	{
		buttons = new Vector();
	
		setup_dialogue(button_names, hideOKCancel);
		this.setTitle(dlgTitle);
		pack();
// 		setVisible(true);
		
		m_wasCancelled = true;

	}

	dlgRadiobuttons( Dialog owner, String dlgTitle, String[] button_names, Boolean hideOKCancel )
	{
		super(owner,true);	// true -> modal dialog
		buttons = new Vector();
	
		setup_dialogue(button_names, hideOKCancel);
		this.setTitle(dlgTitle);
		pack();
// 		setVisible(true);
		
		m_wasCancelled = true;
	}
	
	private void setup_dialogue(String[] button_names, Boolean hideOKCancel)
	{
		Panel p = new Panel();
		p.setLayout(new BoxLayout(p,BoxLayout.Y_AXIS));
		
		btngrp = new ButtonGroup();
				
		// create and add radiobuttons
		for ( int i=0; i<button_names.length; i++ )
		{
			JRadioButton rb = new JRadioButton(button_names[i]);
			buttons.add(rb);
			btngrp.add(rb);
			p.add(Box.createVerticalStrut( 10 ));
			p.add(rb);
		}

		p.add( Box.createVerticalStrut( 10 ) );
	
		// add and initialise buttons
		Panel pBtn = new Panel();
		pBtn.setLayout(new BoxLayout(pBtn,BoxLayout.X_AXIS));

		pBtn.add( Box.createHorizontalStrut( 30 ) );
		btnOK = new JButton("OK");
		btnOK.addActionListener(new ActionListener(){ public void actionPerformed(ActionEvent e){ userConfirm( true ); }});
		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener(){ public void actionPerformed(ActionEvent e){ userConfirm( false ); }});

		
		pBtn.add( Box.createHorizontalStrut( 20 ) );
		pBtn.add(btnOK);
		pBtn.add( Box.createHorizontalStrut( 15 ) );
		pBtn.add(btnCancel);
		pBtn.add( Box.createHorizontalStrut( 20 ) );
		
		if (!hideOKCancel){p.add(pBtn);}
		p.add( Box.createVerticalStrut( 10 ) );
		this.add(p);
	}
	
	public int get_selected_button()
	{
		if ( m_wasCancelled ){ return -1; }
	
		for ( int i=0; i<buttons.size(); i++ )
		{ if ( ((JRadioButton) buttons.elementAt(i)).isSelected() ){ return i; } } 

		return -1;
	}
	
	public Boolean setEnabled( int buttonId, Boolean enableButton )
	{
		if ( buttonId <0 || buttonId> buttons.size() ){ return false; }
		((JRadioButton) buttons.elementAt(buttonId)).setEnabled(enableButton);
		return true;
	}
	
	
	public Boolean was_cancelled(){ return m_wasCancelled; }
	
	private void userConfirm( Boolean OKCancel ) // true = OK, false = Cancel
	{ m_wasCancelled = !OKCancel; this.setVisible(false); }
	
	static public String classID(){ return "dlgRadiobuttons"; }
	static public String author(){ return "Matthias Schmidt"; }
	static public String version(){ return "November 24 2016"; }
	
	
	// widgets
	private Vector buttons;
	private ButtonGroup btngrp;
	private JButton btnOK;
	private JButton btnCancel;
	
	// flags
	private Boolean m_wasCancelled;
	
}

