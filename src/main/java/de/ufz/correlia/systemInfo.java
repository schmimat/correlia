// systemInfo class is used to get information on the operating system, screen etc.
// it is no derived from anything
// written by Matthias Schmidt
package de.ufz.correlia;
import java.util.*;
import java.awt.*;
import java.io.*;

import javax.swing.*;
import javax.swing.border.TitledBorder;

public class systemInfo
{
	public systemInfo()	// constructor
	{
		
		
	
	}

	public void show()	// show dialogue with system information
	{
		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p,BoxLayout.Y_AXIS));
		
		JTextArea TA = new JTextArea();
		TA.setBorder (BorderFactory.createTitledBorder (BorderFactory.createEtchedBorder (),
                                                           "System Information",
                                                           TitledBorder.LEFT,
                                                           TitledBorder.TOP));
                                                           
		JScrollPane sp = new JScrollPane(TA,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		sp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		sp.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 16));
		p.add(sp);
		
		JFrame dlg = new JFrame();
		dlg.setTitle("system information");
		dlg.add(p);
		dlg.show();
		return;
	
	}
	
	
	
	private static String OS = System.getProperty("os.name").toLowerCase();
	private static GraphicsDevice GD = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice(); 
	
	// machine
	
	public static int numberOfProcessors(){ return Runtime.getRuntime().availableProcessors(); }

	public static long freeMemory(){ return Runtime.getRuntime().freeMemory(); }
	public static long maxMemory(){ return Runtime.getRuntime().maxMemory(); }
	
	// operating system
	
	public static String operatingSystem(){ return OS; }
	public static String operatingSystemVersion(){ return System.getProperty("os.version"); }
	
	public static boolean isWindows()
	{ return (OS.indexOf("win") >= 0); }
 
	public static boolean isMac()
	{ return (OS.indexOf("mac") >= 0); }
 
	public static boolean isUnix()
	{ return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 ); }
 
	public static boolean isSolaris()
	{ return (OS.indexOf("sunos") >= 0); }
	
	// user
	
	public static String userName(){ return System.getProperty("user.name"); }

	// graphics device
	
	public static int screenWidth()
	{ return GD.getDisplayMode().getWidth(); }
	
	public static int screenHeight()
	{ return GD.getDisplayMode().getHeight(); }

}



