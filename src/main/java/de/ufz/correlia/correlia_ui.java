/*
ImageJ Java plug-in developed for correlative microscopy at the
Helmholtz Centre for Environmental Research Leipzig (UFZ) within
the framework of ProVIS - Centre for Chemical Microscopy.

author : Matthias Schmidt
*/
package de.ufz.correlia;

import ij.*;
import ij.io.SaveDialog;
import ij.plugin.*;
import ij.plugin.filter.GaussianBlur;
import ij.process.*;
import ij.process.ImageProcessor;

import ij.gui.*;
import ij.gui.ImageCanvas;
import ij.gui.GenericDialog;
import ij.gui.NonBlockingGenericDialog;
import ij.gui.NewImage;

import ij.io.OpenDialog;

import java.lang.Double;

import javax.swing.*;
import javax.swing.JButton;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.border.TitledBorder;

import java.awt.*;
import java.awt.Dimension;
import java.awt.geom.*;

import java.awt.event.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowListener;

import java.io.IOException;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class correlia_ui implements PlugIn, MouseListener, MouseMotionListener, KeyListener {
	private static final String DLG_IMAGES_PANE = "Imgs";
	private static final String DLG_IMAGE_PROPS_PANE = "ImgProps";
	private static final String DLG_TRANS_PANE = "Trans";
	private static final String DLG_TRANS_PROPS_PANE = "TransProps";

	private static final Dimension LEFTPANE_DIM = new Dimension(300, 400);
	private static final Dimension RIGHTPANE_DIM = new Dimension(320, 500);
	private static final int PREVIEW_PANE_WIDTH = 240;
	private static final int PREVIEW_PANE_HEIGHT = 180;
	private static final Dimension BTN_DIM = new Dimension(30, 30);
	private static final int BTN_PADDING = 5;
	private static final int MOVE_BTN_MARGIN = 5;

	final ImageIcon icoEdit = new ImageIcon(this.getClass().getResource("/icons/edit.png"));
	final ImageIcon icoDelete = new ImageIcon(this.getClass().getResource("/icons/delete.png"));
	final ImageIcon icoPlus = new ImageIcon(this.getClass().getResource("/icons/plus.png"));
	final ImageIcon icoMinus = new ImageIcon(this.getClass().getResource("/icons/minus.png"));
	final ImageIcon icoGear = new ImageIcon(this.getClass().getResource("/icons/gear.png"));
	final ImageIcon icoCalculator = new ImageIcon(this.getClass().getResource("/icons/calculator.png"));
	final ImageIcon icoCheck = new ImageIcon(this.getClass().getResource("/icons/check.png"));
	final ImageIcon icoExit = new ImageIcon(this.getClass().getResource("/icons/exit.png"));
	final ImageIcon icoUpload = new ImageIcon(this.getClass().getResource("/icons/upload.png"));
	final ImageIcon icoDownload = new ImageIcon(this.getClass().getResource("/icons/download.png"));
	final ImageIcon icoExternal = new ImageIcon(this.getClass().getResource("/icons/external.png"));
	final ImageIcon icoArrowLeft = new ImageIcon(this.getClass().getResource("/icons/arrow_left.png"));
	final ImageIcon icoArrowRight = new ImageIcon(this.getClass().getResource("/icons/arrow_right.png"));
	final ImageIcon icoArrowUp = new ImageIcon(this.getClass().getResource("/icons/arrow_up.png"));
	final ImageIcon icoArrowDown = new ImageIcon(this.getClass().getResource("/icons/arrow_down.png"));
	final ImageIcon icoRotateLeft = new ImageIcon(this.getClass().getResource("/icons/arrow_rotate_left.png"));
	final ImageIcon icoRotateRight = new ImageIcon(this.getClass().getResource("/icons/arrow_rotate_right.png"));
	final ImageIcon icoFine = new ImageIcon(this.getClass().getResource("/icons/char_f.png"));
	final ImageIcon icoCoarse = new ImageIcon(this.getClass().getResource("/icons/char_c.png"));
	final ImageIcon icoLinked = new ImageIcon(this.getClass().getResource("/icons/linked.png"));
	final ImageIcon icoUnlinked = new ImageIcon(this.getClass().getResource("/icons/unlinked.png"));
	final ImageIcon icoOverlayRG = new ImageIcon(this.getClass().getResource("/icons/overlay_rg.png"));
	final ImageIcon icoStackPrePost = new ImageIcon(this.getClass().getResource("/icons/stack_pre_post.png"));
	final ImageIcon icoFeaturePointsNo = new ImageIcon(this.getClass().getResource("/icons/featurepoints_no.png"));
	final ImageIcon icoFeaturePointsRB = new ImageIcon(this.getClass().getResource("/icons/featurepoints_rb.png"));
	final ImageIcon icoFeaturePointsR = new ImageIcon(this.getClass().getResource("/icons/featurepoints_r.png"));
	final ImageIcon icoFeaturePointsB = new ImageIcon(this.getClass().getResource("/icons/featurepoints_b.png"));
	final ImageIcon icoTransformAffineAuto = new ImageIcon(this.getClass().getResource("/icons/transform_affine.png"));
	final ImageIcon icoTransformNonLinear = new ImageIcon(this.getClass().getResource("/icons/transform_nonlinear.png"));
	final ImageIcon icoSelectReference = new ImageIcon(this.getClass().getResource("/icons/select_reference.png"));

	private JPanel leftPane;
	private JPanel rightPane;
	private JPanel imagesPane;
	private JPanel imagePropsPane;
	private JPanel imageList;
	private imagePanel imagePreview;
	private JPanel coordinatesPane;
	private JPanel transPane;
	private JPanel transPropsPane;
	private JPanel transPropsPreviewPane;
	private JPanel transPropsParamPane;
	private JPanel transList;
	private imagePanel transPreview;
	private JButton btnDelete;
	private JButton btnLink;
	private JButton btnOvl;
	private JButton btnExport;
	private JButton btnSaveTransformationResult;
	private JButton btnExit;
	private int selectedImage;
	private boolean fineMovement = false;
	private int showFeaturePoints = -1;

	public boolean new_project() {
		debug.put("entered");
	
		OpenDialog od = new OpenDialog("Open an image that serves as a base for the project");
		if (od.getPath() == null) {return false;}	// was dialogue cancelled?
		
		// check if there is microscopy image data available for the image (saved in a separate file)
		String[] pathStr = (od.getPath()).split("\\.(?=[^\\.]+$)");

		if (pathStr[pathStr.length-1].equals("mimg")) {	// microscopy image definition found, parse definition and instanciate
//			debug.put("try to open microscopyImage in path "+od.getPath());
//
//			// invoke handler for microscopyImages
//			mimgHandler mih = new mimgHandler();
//			// open mimg file, parse it, instanciate microscopyImage and instanciate correlia project
//			microscopyImage mi = mih.open(od.getDirectory(), od.getFileName());
//			if (mi == null) {return false;}
//			prj = new correlia(mi );
		} else { // just a tiff
			debug.put("correlia_ui.new_project try to construct project from tiff, path "+od.getPath());
			prj = new correlia(new ImagePlus(od.getPath()));
		}
		
		output = setup_screenWindow(1);
		opProc = output.getProcessor();

		// initialise flags for key event indication 
		initialiseFlags();

		outputOverlay = new Overlay();	// use ImageJ overlay function to indicate things like feature points

 		draw(5);	// draw and show overlay
		opCanvas = setup_canvas();
		return true;
	}

	public boolean load_project(String path) {
		debug.put("entered");

		if (path.isEmpty()) {
			OpenDialog od = new OpenDialog("Load a CORRELIA project");
			if (od.getPath() == null) {
				return false;
			}    // was dialogue cancelled?
			path = od.getDirectory() + od.getFileName();
		}

		xmlHandler xml = new xmlHandler();
		if (!xml.read(path)) {
			debug.put("trying to read oldstyle correlia project");
			if (!xml.setDoc(legacySupport.readOldProject(path))) {
				return false;
			}
		}

		if (!xml.getDoc().getDocumentElement().getTagName().equals("correlia")) {
			return false;
		}
		xml.setSavepath(path);
		prj = new correlia(xml, xml.getDoc().getDocumentElement());
		
		// check if the project was correctly loaded, if not display error messages
		if (!prj.check()) {
			debug.put("ERROR: in correlia_ui.load_project: close correlia due to previous errors");
			IJ.log("ERROR: in correlia_ui.load_project: close correlia due to previous errors");
			return false;
		}
		
		output = setup_screenWindow(1);
		opProc = output.getProcessor();

		// initialise flags for key event indication 
		initialiseFlags();

		outputOverlay = new Overlay();	// use ImageJ overlay function to indicate things like feature points

 		draw(5);	// draw and show overlay
		opCanvas = setup_canvas();

		// Update everything, if dialog is already there
		if (binningChoice != null) {
			((CardLayout) leftPane.getLayout()).show(leftPane, DLG_IMAGES_PANE);
			((CardLayout) rightPane.getLayout()).show(rightPane, DLG_IMAGE_PROPS_PANE);
			binningChoice.setSelectedIndex(estimateInitialBinning(output));
			update_gui();
		}
		return true;
	}


	public void run(String arg) {
		Boolean was_cancelled = false;

		String prjPath = "";

		if (!arg.isEmpty()) {
			prjPath = arg;
		} else if (IJ.isMacro() && Macro.getOptions() != null && !Macro.getOptions().trim().isEmpty()) {
			prjPath = Macro.getOptions().trim();
			// clear macro options as they are not intended for the genericdialog
			Macro.setOptions("");
		}

		if (prjPath.isEmpty()) {
			// wait for user input - new project or load existing project
			dlgStartCorrelia strcor = new dlgStartCorrelia();
			strcor.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
//			strcor.setSize(200,170);
			strcor.setLocationRelativeTo(null);
			strcor.setVisible(true);

			while (strcor.get_selected_button() < 0) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {}
			}	// wait for user selection

			strcor.hide();

			// switch debugging on if user has selected the debugging mode
			if (strcor.debugging()) {
				debug.debugger_on();
			} else {
				debug.debugger_off();
			}

			// start new project or load a project
			if (strcor.get_selected_button() == 0) {
				was_cancelled = !new_project();
			} else {
				was_cancelled = !load_project("");
			}
		} else {
			debug.debugger_off();
			was_cancelled = !load_project(prjPath);
		}

		if (!was_cancelled) {
			debug.put("new project created or project loaded");
		} else {
			debug.put("there was a problem creating/loading the correlia project");
		}

		if (!was_cancelled) {
			debug.put("try to setup dialogue");
			gd = setup_dialogue();
			gd.setOKLabel("Quit");
			gd.hideCancelButton();
			gd.addWindowListener(
				new java.awt.event.WindowAdapter() {
					@Override public void windowClosing(java.awt.event.WindowEvent e) {
						exitUserConfirm();
					}
				}
			);
			// ensure that first drawing is not too time consuming
			binningChoice.setSelectedIndex(estimateInitialBinning(output));

			debug.put("dialogue is setup, try to show dialogue");
			gd.showDialog();

			if (gd.wasOKed()) {
				exitUserConfirm();
			}
		} else {
			debug.put("user cancelled correlia");
			IJ.log("MSG: user cancelled correlia");
		}
	}

	private int estimateInitialBinning(correliaScreen output) {
		int binning = 0;
		//TODO Magic Number
		while (output.getWidth() / (binning+1) > 1024 && binning < 9) {
			binning++;
		}
		return binning;
	}

	private void exitUserConfirm() {
		int ans = JOptionPane.showConfirmDialog(output.getWindow(), "Do you want to save the project","", JOptionPane.YES_NO_OPTION);
		if (ans == JOptionPane.YES_OPTION) {
			save_project(false);
		}
//		} else if (ans == JOptionPane.CANCEL_OPTION) {
//			gd.setVisible(true);
//			return;
//		}
		output.getWindow().close();
		gd.dispose();
	}

	private void initialiseFlags() {
		EF_set_coordinates = false;
		EF_define_and_add_point = false;
		EF_delete_last_feature = false;
	}

	/**
	 * Create main control dialogue
	 * @return dialogue handler
	 */
	private NonBlockingGenericDialog setup_dialogue() {
		debug.put("entered");
		
		NonBlockingGenericDialog d = new NonBlockingGenericDialog("CORRELIA - Overlay Microscopy Images");

		Panel mainPane = new Panel();
		mainPane.setLayout(new BoxLayout(mainPane,BoxLayout.Y_AXIS));
		mainPane.add(setup_menuBar());
		mainPane.add(setup_twoPane());

		d.addPanel(mainPane);
		
		int dlgWidth = systemInfo.screenWidth()/3;
		int dlgHeight = systemInfo.screenHeight()*2/3;
		
		d.setMinimumSize(new Dimension(220,500));
		d.setMaximumSize(new Dimension(dlgWidth, dlgHeight));
		return d;	
	}

	private JPanel setup_twoPane() {
		JPanel twoPane = new JPanel();
		twoPane.setLayout(new BoxLayout(twoPane,BoxLayout.X_AXIS));
		twoPane.setBorder(BorderFactory.createEtchedBorder());

		imagesPane = setup_images_pane();
		imagesPane.setName(DLG_IMAGES_PANE);
		imagePropsPane = setup_image_props_pane();
		imagePropsPane.setName(DLG_IMAGE_PROPS_PANE);
		transPane = setup_trans_pane();
		transPane.setName(DLG_TRANS_PANE);
		transPropsPane = setup_trans_props_pane();
		transPropsPane.setName(DLG_TRANS_PROPS_PANE);

		leftPane = new JPanel();
		leftPane.setLayout(new CardLayout());
		leftPane.setBorder(BorderFactory.createEtchedBorder());
		leftPane.setPreferredSize(LEFTPANE_DIM);
		leftPane.setMinimumSize(LEFTPANE_DIM);
		rightPane = new JPanel();
		rightPane.setLayout(new CardLayout());
		rightPane.setBorder(BorderFactory.createEtchedBorder());
		rightPane.setPreferredSize(RIGHTPANE_DIM);
		rightPane.setMinimumSize(RIGHTPANE_DIM);

		leftPane.add(imagesPane, DLG_IMAGES_PANE);
		rightPane.add(imagePropsPane, DLG_IMAGE_PROPS_PANE);
		leftPane.add(transPane, DLG_TRANS_PANE);
		rightPane.add(transPropsPane, DLG_TRANS_PROPS_PANE);

		((CardLayout) leftPane.getLayout()).show(leftPane, DLG_IMAGES_PANE);
		((CardLayout) rightPane.getLayout()).show(rightPane, DLG_IMAGE_PROPS_PANE);

		twoPane.add(leftPane);
		twoPane.add(rightPane);
		return twoPane;
	}

	private JPanel setup_images_pane() {
		JPanel imagesPane = new JPanel();
		imagesPane.setLayout(new BoxLayout(imagesPane, BoxLayout.Y_AXIS));

		imageList = new JPanel();
		imageList.setLayout(new BoxLayout(imageList, BoxLayout.Y_AXIS));
		for (int i = 0; i < prj.size(); i++) {
			imageList.add(new imageElementPanel(this, i));
		}

		JScrollPane listScroller = new JScrollPane(imageList);
		listScroller.setPreferredSize(LEFTPANE_DIM);
		listScroller.setMinimumSize(LEFTPANE_DIM);
		listScroller.getVerticalScrollBar().setUnitIncrement(10);
		imagesPane.add(listScroller);

		JButton btnAdd = createButton(icoPlus, 0);
		btnAdd.setToolTipText("Add image to the project");
		btnAdd.addActionListener(
				e -> {
					open_image_and_add_to_project();
					update_gui();
				}
		);
		JButton btnShowFeaturePoints = createButton(icoFeaturePointsNo, 0);
		btnShowFeaturePoints.setToolTipText("<html>display defined features in:<ul><li>base image in overlay (red)</li><li>selected image in overlay (blue)</li></ul>");
		btnShowFeaturePoints.addActionListener(
				e -> {
					if (showFeaturePoints == get_selected_image()) {
						showFeaturePoints = -1;
						btnShowFeaturePoints.setIcon(icoFeaturePointsNo);
					} else {
						showFeaturePoints = get_selected_image();
						btnShowFeaturePoints.setIcon(icoFeaturePointsRB);
					}
					update_overlay();
				}
		);

		// binning
		binningChoice = new JComboBox();
		binningChoice.setToolTipText("<html>binning 2n+1 pixels in the display will:<br><li>decrease the displayed resolution</li><br><li>increase window update speed</li><br><li>not affect accuracy of exported data</li></html>");
		for (int i=0; i<10; i++) {
			binningChoice.addItem(Integer.toString(i));
		}
		binningChoice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				update_gui();
			}
		});

		binningChoice.setMinimumSize(new Dimension(64, 40));
		binningChoice.setMaximumSize(new Dimension(64, 40));

		JPanel btnPane = new JPanel();
		btnPane.setLayout(new BoxLayout(btnPane, BoxLayout.X_AXIS));
		btnPane.add(btnAdd);
		btnPane.add(Box.createRigidArea(new Dimension(BTN_PADDING, 0)));
		btnPane.add(Box.createHorizontalGlue());
		btnPane.add(btnShowFeaturePoints);
		btnPane.add(Box.createRigidArea(new Dimension(BTN_PADDING, 0)));
		btnPane.add(new JLabel("Binning: "));
		btnPane.add(binningChoice);
		btnPane.add(Box.createRigidArea(new Dimension(BTN_PADDING, 0)));
		imagesPane.add(btnPane);
		return imagesPane;
	}

	private JPanel setup_image_props_pane() {
		JPanel imagePropsPane = new JPanel();
		imagePropsPane.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;

		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1.0;
		c.weighty = 0.0;
		c.gridwidth = 3;
		imagePropsPane.add(setup_image_props_preview_panel(), c);

		coordinatesPane = setup_image_props_alignment();
		c.gridx = 0;
		c.gridy++;
		c.weightx = 1.0;
		c.weighty = 0.0;
		c.gridwidth = 3;
		imagePropsPane.add(coordinatesPane, c);
		c.gridx = 0;
		c.gridy++;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridwidth = 3;
		imagePropsPane.add(new JPanel(), c);

		return imagePropsPane;
	}

	private JPanel setup_trans_pane() {
		JPanel transPane = new JPanel();
		transPane.setLayout(new BoxLayout(transPane, BoxLayout.Y_AXIS));

		transList = new JPanel();
		transList.setLayout(new BoxLayout(transList, BoxLayout.Y_AXIS));

		JScrollPane listScroller = new JScrollPane(transList);
		listScroller.setPreferredSize(new Dimension(300, 400));
		listScroller.getVerticalScrollBar().setUnitIncrement(10);
		transPane.add(listScroller);

		JButton btnAdd = createButton(icoPlus, 0);
		btnAdd.setToolTipText("Add transformation to the image");
		btnAdd.addActionListener(
				e -> {
					Object[] possibilities = {
							transformationLandmarks.NAME,
							transformationBUnwarpJ.NAME,
							transformationTileMatching.NAME,
							transformationGlobalMI.NAME
					};
					String s = (String) JOptionPane.showInputDialog(
							gd,
							"Select a transformation!\n\n" +
									"Landmarks and bUnwarpJ use manually selected feature points and \n" +
									"are recommended for images with few but strong structures.\n" +
									"bUnwarpJ also offers a monomodal registration option.\n\n" +
									"Tile Matching splits the images into smaller parts and uses Mutual Information to align those tiles.\n" +
									"This is a pretty fast way to correct global distortions.\n\n" +
									"Global Mutual Information is slower, but more flexible.\n" +
									"It is recommended to use this type if the images are already fairly good aligned and differ only locally.\n\n",
							"Select Transformation",
							JOptionPane.PLAIN_MESSAGE,
							null,
							possibilities,
							possibilities[0]);
					if (s != null) {
						transformation t;
						transformation tPrev = null;
						int target;
						int tPrevNr = get_selected_microscopyImageWarped().transformationsSize() - 1;
						if (tPrevNr >= 0) {
							tPrev = get_selected_microscopyImageWarped().getTransformation(tPrevNr);
						}
						switch (s) {
							default:
							case transformationBUnwarpJ.NAME:
								t = new transformationBUnwarpJ(prj, get_selected_imageID(), tPrev);
								break;
							case transformationTileMatching.NAME:
								t = new transformationTileMatching(prj, get_selected_imageID(), tPrev);
								break;
							case transformationLandmarks.NAME:
								t = new transformationLandmarks(prj, get_selected_imageID(), tPrev);
								break;
							case transformationGlobalMI.NAME:
								t = new transformationGlobalMI(prj, get_selected_imageID(), tPrev);
								break;
						}
						get_selected_microscopyImageWarped().addTransformation(t);
						update_gui();
					}
				}
		);

		JButton btnUpdate = createButton(icoCalculator, 0);
		btnUpdate.setToolTipText("Transform the image");
		btnUpdate.addActionListener(
				e -> {
					get_selected_microscopyImageWarped().update();
					update_output();
					update_dialogue();
				}
		);

		btnOvl = createButton(icoOverlayRG, 0);
		btnOvl.setToolTipText("Compare the transformed image");
		btnOvl.setEnabled(false); // Will be set to true, if a transformation is added to a microscopyImageWarped
		btnOvl.addActionListener(
				e -> {
					microscopyImageWarped selMIW = get_selected_microscopyImageWarped();
					transformation selTrans = selMIW.getSelectedTransformation();

					int cmpImage = prj.get_imagePositionByID(selTrans.getRefID().get(0));
					cmpImage = MiscHelper.selectImageFromProject(prj, gd, "Select image to compare with", cmpImage, false);
					if (cmpImage < 0) return;
					ImageProcessor reference = prj.getAdaptedImage(cmpImage, get_selected_image()).getProcessor().convertToByteProcessor();
					ImageProcessor ovl;

					ImageStack ims = new ImageStack(reference.getWidth(), reference.getHeight());
					byte[] emptyPixels = new byte[reference.getWidth()*reference.getHeight()];
					for (int i = 0; i < emptyPixels.length; i++) {
						emptyPixels[i] = 0;
					}

					ColorProcessor fp;

					fp = new ColorProcessor(ims.getWidth(), ims.getHeight());
					ovl = selMIW.get_rawStack().getProcessor(selMIW.getSlice()).convertToByteProcessor();
					fp.setRGB((byte[]) ovl.getPixels(), (byte[]) reference.getPixels(), emptyPixels);
					ims.addSlice("Pre Warping (R: Overlay, G: Reference)", fp);

					int transformationsCount = selMIW.transformationsSize();
					for (int i = selMIW.transformationsSize() - 1; i >= 0; i--) {
						if (!selMIW.getTransformation(i).active()) {
							transformationsCount--;
							continue;
						}
						fp = new ColorProcessor(ims.getWidth(), ims.getHeight());
						ovl = new ImagePlus("Post", selMIW.getTransformation(i).getResult().getStack()).getProcessor().convertToByteProcessor();
						fp.setRGB((byte[]) ovl.getPixels(), (byte[]) reference.getPixels(), emptyPixels);
						ims.addSlice("Post Warping (R: Overlay, G: Reference)", fp);
					}
					if (transformationsCount > 1) {
						fp = new ColorProcessor(ims.getWidth(), ims.getHeight());
						ovl = selMIW.get_rawStack().getProcessor(selMIW.getSlice()).convertToByteProcessor();
						fp.setRGB((byte[]) ovl.getPixels(), (byte[]) reference.getPixels(), emptyPixels);
						ims.addSlice("Pre Warping (R: Overlay, G: Reference)", fp);
					}
					fp = new ColorProcessor(ims.getWidth(), ims.getHeight());
					reference = selMIW.get_rawStack().getProcessor(selMIW.getSlice()).convertToByteProcessor();
					ovl = selMIW.getProcessor().convertToByteProcessor();
					fp.setRGB((byte[]) ovl.getPixels(), (byte[]) reference.getPixels(), emptyPixels);
					ims.addSlice("Image (R: Post, G: Pre)", fp);

					new ImagePlus("Compare registration results", ims).show();
				}
		);

		btnExport = createButton(icoDownload, 0);
		btnExport.setToolTipText("Export the transformation settings as a recipe for reuse in similar images");
		btnExport.setEnabled(false); // Will be set to true, if a transformation is added to a microscopyImageWarped
		btnExport.addActionListener(e -> {
			SaveDialog dlg = new SaveDialog("Save transformation recipe", "transformationRecipe_", ".xml");
			if (dlg.getDirectory() == null) {
				return;
			}
			String dir = dlg.getDirectory();
			String fname = dlg.getFileName();

			xmlHandler xml = new xmlHandler();
			xml.setSavepath(dir + fname);
			xml.addToDoc(get_selected_microscopyImageWarped().buildRecipe(xml));
			xml.save();
		});

		JButton btnImport = createButton(icoUpload, 0);
		btnImport.setToolTipText("Import transformation recipe");
		btnImport.addActionListener(e -> {
			OpenDialog dlg = new OpenDialog("Load transformation recipe");
			if (dlg.getDirectory() == null) {
				return;
			}
			String dir = dlg.getDirectory();
			String fname = dlg.getFileName();

			xmlHandler xml = new xmlHandler();
			if (!xml.read(dir + fname)) {
				IJ.showMessage("The file is not properly formatted.");
				return;
			}

			if (!xml.getDoc().getDocumentElement().getTagName().equals("transformationRecipe")) {
				IJ.showMessage("This is not a transformation recipe.");
				return;
			}
			if (!get_selected_microscopyImageWarped().loadRecipe(xml, xml.getDoc().getDocumentElement(), get_prj())) {
				IJ.showMessage("There was an error while importing the recipe.");
				return;
			}
			update_dialogue();
		});


		btnSaveTransformationResult = createButton(icoExternal, 0);
		btnSaveTransformationResult.setToolTipText("Add the transformed image to the project");
		btnSaveTransformationResult.setEnabled(false); // Will be set to true, if a transformation is added to a microscopyImageWarped
		btnSaveTransformationResult.addActionListener(e -> {
			Object[] options = {"Add slice", "Separate image", "Cancel"};
			int ans = 1;
			if (get_selected_microscopyImageWarped().getStackSize() == 1) {
				ans = JOptionPane.showOptionDialog(gd,
						"Do you want to add the transformed image as a slice to the original image\n" +
								" or as a seperate new image?\n" +
								"Note that the first choice deletes the transformation settings permanently.",
						"Save transformation result",
						JOptionPane.YES_NO_CANCEL_OPTION,
						JOptionPane.QUESTION_MESSAGE,
						null,
						options,
						options[2]);
			}

			if (ans == JOptionPane.YES_OPTION) {
				microscopyImageWarped miw = get_selected_microscopyImageWarped();
				microscopyImage mi = new microscopyImage(get_selected_microscopyImageWarped());
				ImageStack ims = mi.getStack();
				ImageProcessor ipRaw = miw.get_rawStack().getProcessor(1);
				ims.addSlice("Original", ipRaw);
				ims.setSliceLabel("Transformed", 1);

				mi.setStack(ims);
				prj.replace(get_selected_image(), mi, null, null);

			} else if (ans == JOptionPane.NO_OPTION) {
				int i = get_selected_image();
				microscopyImage mi = new microscopyImage(get_selected_microscopyImageWarped());
				mi.setTitle(mi.getTitle() + " transformed");
				prj.importImage(mi,
						new imageColour(prj.get_imageColour(i)),
						new imageAlignment(prj.get_imageAlignment(i))
				);
				prj.moveImage(i, prj.size()-1);
				prj.get_imageColour(i+1).set_visibility(imageColour.HIDE);
			} else {
				return;
			}
			btnExit.doClick();
		});

		btnExit = createButton(icoExit, 0);
		btnExit.setToolTipText("Return to the images view");
		btnExit.addActionListener(
				e -> {
					if (prj.imageIsLinked(get_selected_image())) {
						prj.applyTransformationsToLinkedImages(get_selected_image());
					}

					((CardLayout) leftPane.getLayout()).show(leftPane, DLG_IMAGES_PANE);
					((CardLayout) rightPane.getLayout()).show(rightPane, DLG_IMAGE_PROPS_PANE);
//					output.setStack(setup_screenWindow(1, false).getStack());
//					output.getCalibration().setUnit(prj.baseImage.getCalibration().getUnit());
//					output.getCalibration().pixelHeight = prj.height()/(output.getHeight());
//					output.getCalibration().pixelWidth  = prj.width() /(output.getWidth());
//					opProc = output.getProcessor();
					update_gui();
				}
		);

		JPanel btnPane = new JPanel();
		btnPane.setLayout(new BoxLayout(btnPane, BoxLayout.X_AXIS));
		btnPane.add(btnAdd);
		btnPane.add(Box.createRigidArea(new Dimension(BTN_PADDING, 0)));
		btnPane.add(btnUpdate);
		btnPane.add(Box.createRigidArea(new Dimension(BTN_PADDING, 0)));
		btnPane.add(btnOvl);
		btnPane.add(Box.createRigidArea(new Dimension(BTN_PADDING, 0)));
		btnPane.add(btnExport);
		btnPane.add(Box.createRigidArea(new Dimension(BTN_PADDING, 0)));
		btnPane.add(btnImport);
		btnPane.add(Box.createRigidArea(new Dimension(BTN_PADDING, 0)));
		btnPane.add(Box.createHorizontalGlue());
		btnPane.add(btnSaveTransformationResult);
		btnPane.add(Box.createRigidArea(new Dimension(BTN_PADDING, 0)));
		btnPane.add(btnExit);
		btnPane.add(Box.createRigidArea(new Dimension(BTN_PADDING, 0)));

		transPane.add(btnPane);
		return transPane;
	}

	private JPanel setup_trans_props_pane() {
		JPanel transPropsPane = new JPanel();
		transPropsPane.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;

		transPropsPreviewPane = setup_trans_props_preview_panel();
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1.0;
		c.weighty = 0.0;
		c.gridwidth = 2;
		transPropsPane.add(transPropsPreviewPane, c);

		transPropsParamPane = setup_trans_props_param_panel();
		c.gridx = 0;
		c.gridy++;
		c.weightx = 1.0;
		c.weighty = 0.0;
		c.gridwidth = 2;
		transPropsPane.add(transPropsParamPane, c);

		c.gridx = 0;
		c.gridy++;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.gridwidth = 2;
		transPropsPane.add(new JPanel(), c);

		return transPropsPane;
	}

	private JPanel setup_image_props_preview_panel() {
		debug.put("entered");

		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.setAlignmentX(Component.LEFT_ALIGNMENT);
		p.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				"Image",	TitledBorder.LEFT, TitledBorder.TOP));

		imagePreview = new imagePanel(prj.get_image(get_selected_image()), PREVIEW_PANE_WIDTH, PREVIEW_PANE_HEIGHT, true);
		imagePreview.setBorder(BorderFactory.createLineBorder(new Color(200, 200, 200)));
		imagePreview.setToolTipText("<html>Display image (left click)<br>Define and edit feature points (right click)</html>");
//		imagePreview.setToolTipText("<html>Display image:<br><li>define features</li><br><li>define ROIs</li><br><li>add additional information (spectra/images)</li></html>");
		p.add(imagePreview);

		JPanel btnPane = new JPanel();
		btnPane.setLayout(new BoxLayout(btnPane, BoxLayout.Y_AXIS));
		btnPane.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));

		JButton btnEditProps = createButton(icoEdit, 0);
		btnEditProps.setToolTipText("Display and edit image properties and information");
		btnEditProps.addActionListener(e -> edit_image_properties());

		btnDelete = createButton(icoDelete, 0);
		btnDelete.setToolTipText("Remove image from the project");
		btnDelete.addActionListener(e -> remove_image_from_project());
		btnDelete.setEnabled(get_selected_image() > 0);

		btnLink = createButton(icoUnlinked, 0);
		btnLink.setToolTipText("Use the image alignment of this image also for other images");
		btnLink.addActionListener(e -> {
//			int sel = MiscHelper.selectImageFromProject(prj, gd, "Select image that is equally aligned", 1, true);
//			if (sel < 0) return;
//			prj.set_imageAlignment(get_selected_image(), prj.get_imageAlignment(sel), true);

			if (prj.imageIsLinked(get_selected_image())) {
				prj.set_imageAlignment(get_selected_image(),
						new imageAlignment(prj.get_imageAlignment(get_selected_image())), true);
			} else {
				ArrayList<Integer> imgPos = new ArrayList<>();
				microscopyImage miSel = prj.get_image(get_selected_image());
				for (int i = 1; i < prj.size(); i++) {
					if (i == get_selected_image()) continue;
					microscopyImage mi = prj.get_image(i);
					if (MiscHelper.isEqual(mi.imgWidth(), miSel.imgWidth(), 4) &&
							MiscHelper.isEqual(mi.imgHeight(), miSel.imgHeight(), 4) &&
							mi.getWidth() == miSel.getWidth() && mi.getHeight() == miSel.getHeight()) {
						imgPos.add(i);
					}
				}

				if (imgPos.size() == 0) {
					IJ.showMessage("No suitable images (same size) in the project!");
					return;
				}
				String imgNames[] = new String[imgPos.size()];
				for (int i = 0; i < imgNames.length; i++) {
					imgNames[i] = prj.get_image(imgPos.get(i)).getTitle();
				}

				dlgCheckboxes dlg = new dlgCheckboxes(gd, "Select images that have the same deformations as the current one", imgNames);
				dlg.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				dlg.setVisible(true);

				Boolean[] cpy = dlg.get_checkbox_states();

				for (int i = 0; i < cpy.length; i++) {
					if (cpy[i]) {
						prj.set_imageAlignment(imgPos.get(i), // position i
								prj.get_imageAlignment(get_selected_image()), true); // the selected overlay image coordinates
					}
				}

				prj.applyTransformationsToLinkedImages(get_selected_image());
			}
			update_gui();
		});
		btnLink.setEnabled(get_selected_image() > 0);

		btnPane.add(btnEditProps);
		btnPane.add(Box.createRigidArea(new Dimension(0, BTN_PADDING)));
		btnPane.add(btnDelete);
		btnPane.add(Box.createRigidArea(new Dimension(0, BTN_PADDING)));
		btnPane.add(btnLink);
		btnPane.add(Box.createVerticalGlue());

		p.add(btnPane);
		return p;
	}

	private JPanel setup_image_props_alignment() {
		debug.put("entered");

		// function will return this panel
		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.setAlignmentX(Component.LEFT_ALIGNMENT);
		p.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder (),
				"Image alignment", TitledBorder.LEFT, TitledBorder.TOP));

		// panel contains buttons for moving image manually
		Btn_move_right = createButton(icoArrowRight, MOVE_BTN_MARGIN);
		Btn_move_right.setToolTipText("translate overlayed image right");
		Btn_move_right.addActionListener(e -> img_moveManually(0)); // 0 - move right

		Btn_move_left = createButton(icoArrowLeft, MOVE_BTN_MARGIN);
		Btn_move_left.setToolTipText("translate overlayed image left");
		Btn_move_left.addActionListener(e -> img_moveManually(1)); // 1 - move left

		Btn_move_down = createButton(icoArrowDown, MOVE_BTN_MARGIN);
		Btn_move_down.setToolTipText("translate overlayed image down");
		Btn_move_down.addActionListener(e -> img_moveManually(2)); // 2 - move down

		Btn_move_up = createButton(icoArrowUp, MOVE_BTN_MARGIN);
		Btn_move_up.setToolTipText("translate overlayed image up");
		Btn_move_up.addActionListener(e -> img_moveManually(3)); // 3 - move up

		Btn_rot_right = createButton(icoRotateRight, MOVE_BTN_MARGIN);
		Btn_rot_right.setToolTipText("rotate overlayed image clockwise");
		Btn_rot_right.addActionListener(e -> img_moveManually(4)); // 4 - rotate clockwise

		Btn_rot_left = createButton(icoRotateLeft, MOVE_BTN_MARGIN);
		Btn_rot_left.setToolTipText("rotate overlayed image counter-clockwise");
		Btn_rot_left.addActionListener(e -> img_moveManually(5)); // 5 - rotate counter-clockwise

		Btn_scale_up = createButton(icoPlus, MOVE_BTN_MARGIN);
		Btn_scale_up.setToolTipText("upscale overlayed image");
		Btn_scale_up.addActionListener(e -> set_scale(1.01)); // multiply scale factor by 1.01

		Btn_scale_down = createButton(icoMinus, MOVE_BTN_MARGIN);
		Btn_scale_down.setToolTipText("downscale overlayed image");
		Btn_scale_down.addActionListener(e -> set_scale(1/1.01)); // divide scale factor by 1.01

		JButton Btn_FineCoarse = createButton(fineMovement? icoFine : icoCoarse, MOVE_BTN_MARGIN);
		Btn_FineCoarse.setToolTipText("Switch between fine and coarse movement");
		Btn_FineCoarse.addActionListener(
				e -> {
					fineMovement = !fineMovement;
					Btn_FineCoarse.setIcon(fineMovement? icoFine : icoCoarse);
				}
		);

		JPanel pBtnMoveManually = new JPanel();
		pBtnMoveManually.setLayout(new GridLayout(3, 3));
		pBtnMoveManually.add(Btn_rot_left);
		pBtnMoveManually.add(Btn_move_up);
		pBtnMoveManually.add(Btn_rot_right);
		pBtnMoveManually.add(Btn_move_left);
		pBtnMoveManually.add(Btn_FineCoarse);
		pBtnMoveManually.add(Btn_move_right);
		pBtnMoveManually.add(Btn_scale_down);
		pBtnMoveManually.add(Btn_move_down);
		pBtnMoveManually.add(Btn_scale_up);

		// Textfields panel
		TFX0 = new JTextField("0",6);
		TFX0.setToolTipText("x0 coordinate of selected image with respect to base coordinates");

		TFY0 = new JTextField("0",6);
		TFY0.setToolTipText("y0 coordinate of selected image with respect to base coordinates");

		TFRot = new JTextField("0",6);
		TFRot.setToolTipText("rotation angle (deg) of selected image with respect to base coordinates");

		TFScale = new JTextField("1",6);
		TFScale.setToolTipText("scaling factor of selected image");

		// panel contains the textfields to set the coordinate string
		JPanel pTextfields = new JPanel();
		pTextfields.setLayout(new GridLayout(0, 2));
		pTextfields.add(new JLabel("X0 ", SwingConstants.RIGHT));
		pTextfields.add(TFX0);
		pTextfields.add(new JLabel("Y0 ", SwingConstants.RIGHT));
		pTextfields.add(TFY0);
		pTextfields.add(new JLabel("Rot ", SwingConstants.RIGHT));
		pTextfields.add(TFRot);
		pTextfields.add(new JLabel("Scale ", SwingConstants.RIGHT));
		pTextfields.add(TFScale);

		// panel that contains set button
		Btn_set_X0Y0RotScale = createButton(icoCheck, 0);
		Btn_set_X0Y0RotScale.addActionListener(e -> set_X0Y0RotScale());
		Btn_set_X0Y0RotScale.setToolTipText("set x0,y0,rotation and scaling factor of selected image");

		JButton Btn_affineTransform = createButton(icoTransformAffineAuto, 0);
		Btn_affineTransform.setToolTipText("Automatically register the selected image");
		Btn_affineTransform.addActionListener(
				e -> {
					img_moveAutomatically();
				}
		);

		JButton Btn_switch2TransView = createButton(icoTransformNonLinear, 0);
		Btn_switch2TransView.setToolTipText("Use Non-linear registration");
		Btn_switch2TransView.addActionListener(
				e -> {
					if (prj.get_image(get_selected_image()).getClass() != microscopyImageWarped.class) {
						prj.convertMi2Miw(get_selected_image());
//						if (JOptionPane.showConfirmDialog(gd, "Selected image does not support warping. Do you want to convert the image type to change that?","", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
//							prj.convertMi2Miw(get_selected_image());
//						} else {
//							return;
//						}
					} else {
						((microscopyImageWarped)prj.get_image(get_selected_image())).update();
					}

					if (prj.get_imageColour(get_selected_image()).get_visiblity().equals(imageColour.HIDE)) {
						if (JOptionPane.showConfirmDialog(gd,
								"Selected image is not visible. Do you want to change that?",
								"Selected image not visible", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
							prj.get_imageColour(get_selected_image()).set_visibility(imageColour.SHOW);
						}
					}

					((CardLayout) leftPane.getLayout()).show(leftPane, DLG_TRANS_PANE);
					((CardLayout) rightPane.getLayout()).show(rightPane, DLG_TRANS_PROPS_PANE);
					update_gui();
				}
		);

		JPanel pBtn = new JPanel();
		pBtn.setLayout(new BoxLayout(pBtn, BoxLayout.Y_AXIS));
		pBtn.add(Btn_set_X0Y0RotScale);
		pBtn.add(Box.createRigidArea(new Dimension(0, BTN_PADDING)));
		pBtn.add(Btn_affineTransform);
		pBtn.add(Box.createRigidArea(new Dimension(0, BTN_PADDING)));
		pBtn.add(Btn_switch2TransView);
		pBtn.add(Box.createVerticalGlue());

		// add all panels to p
		p.add(pBtnMoveManually);
		p.add(Box.createVerticalStrut(5));
		p.add(Box.createHorizontalGlue());
		p.add(pTextfields);
		p.add(Box.createVerticalStrut(5));
		p.add(pBtn);
		p.add(Box.createVerticalStrut(5));

		return p;
	}

	private JPanel setup_trans_props_preview_panel() {
		debug.put("entered");

		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.setAlignmentX(Component.LEFT_ALIGNMENT);
		p.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				"Transformation",	TitledBorder.LEFT, TitledBorder.TOP));

		transPreview = new imagePanel(null, PREVIEW_PANE_WIDTH, PREVIEW_PANE_HEIGHT, true);
		transPreview.setBorder(BorderFactory.createLineBorder(new Color(200, 200, 200)));
		p.add(transPreview);

		JPanel btnPane = new JPanel();
		btnPane.setLayout(new BoxLayout(btnPane, BoxLayout.Y_AXIS));
		btnPane.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));

		JButton btnSelectRef = createButton(icoSelectReference, 0);
		btnSelectRef.setToolTipText("Select the reference image");
		btnSelectRef.addActionListener(e -> {
			transformation curT = get_selected_microscopyImageWarped().getSelectedTransformation();
			String sel = MiscHelper.selectImageIDFromProject(prj, null, "Select reference image", curT.getRefID().get(0), false);
			if (sel == null) return;
//			curT.addRefID(sel);
			curT.getRefID().set(0, sel);
			curT.createWorkingImages(true);
			curT.setChanged(true);
		});

		JButton btnEditWorkCopies = createButton(icoGear, 0);
		btnEditWorkCopies.setToolTipText("Edit the images before registration");
		btnEditWorkCopies.addActionListener(e -> {
			transformation curT = get_selected_microscopyImageWarped().getSelectedTransformation();
			curT.createWorkingImages(false);
			dlgPreprocessImage d = new dlgPreprocessImage(
					curT.srcWork, curT.refWork,
					curT.smooth,
					curT.histRange
			);
			if (d.wasCancelled()) return;
			curT.smooth = d.getSmooth();
			curT.histRange = d.getHistRange();
//			curT.createWorkingImages(true);
			curT.setChanged(true);
		});

		JButton btnComparePrePostTransformation = createButton(icoStackPrePost, 0);
		btnComparePrePostTransformation.setToolTipText("Compare source, reference and target images");
		btnComparePrePostTransformation.addActionListener(e -> {
			microscopyImageWarped selMIW = get_selected_microscopyImageWarped();
			transformation selTrans = selMIW.getSelectedTransformation();

			int cmpImage = prj.get_imagePositionByID(selTrans.getRefID().get(0));
			cmpImage = MiscHelper.selectImageFromProject(prj, gd, "Select image to compare with", cmpImage, false);
			if (cmpImage < 0) return;
			ImageProcessor reference = prj.getAdaptedImage(cmpImage, get_selected_image()).getProcessor();

			ImageProcessor ipSrc;
			if (selTrans.previousTransformation != null) {
				ipSrc = selTrans.dxyReference.getProcessor();
			} else {
				ipSrc = selMIW.get_rawStack().getProcessor(selMIW.getSlice());
			}
			ImageStack ims = new ImageStack(selMIW.getWidth(), selMIW.getHeight());
			ims.addSlice("Source", ipSrc.convertToByteProcessor());
//			ims.addSlice("Source Work", selTrans.srcWork.getProcessor().convertToByteProcessor());
			ims.addSlice("Reference", reference.convertToByteProcessor());
//			ims.addSlice("Reference Work", selTrans.refWork.getProcessor().convertToByteProcessor());
			ims.addSlice("Target", selTrans.getResult().getProcessor().convertToByteProcessor());
			(new ImagePlus("Registration result comparision", ims)).show();
		});

		JButton btnDelete = new JButton(icoDelete);
		btnDelete.setToolTipText("Delete this transformation");
		btnDelete.setBorder(null);
		btnDelete.setOpaque(false);
		btnDelete.setContentAreaFilled(false);
		btnDelete.setMinimumSize(BTN_DIM);
		btnDelete.setMaximumSize(BTN_DIM);
		btnDelete.addActionListener(e -> {
			microscopyImageWarped selectedMI = get_selected_microscopyImageWarped();
			selectedMI.removeTransformation(selectedMI.getSelectedTransformationNr());
			selectedMI.setSelectedTransformation(Math.max(selectedMI.getSelectedTransformationNr() - 1, 0));
			update_gui();
		});

		btnPane.add(Box.createRigidArea(new Dimension(0, BTN_PADDING)));
		btnPane.add(btnSelectRef);
		btnPane.add(Box.createRigidArea(new Dimension(0, BTN_PADDING)));
		btnPane.add(btnEditWorkCopies);
		btnPane.add(Box.createRigidArea(new Dimension(0, BTN_PADDING)));
		btnPane.add(btnComparePrePostTransformation);
		btnPane.add(Box.createRigidArea(new Dimension(0, BTN_PADDING)));
		btnPane.add(btnDelete);
		btnPane.add(Box.createVerticalGlue());

		p.add(btnPane);
		return p;
	}

	private JPanel setup_trans_props_param_panel() {
		debug.put("entered");

		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.setAlignmentX(Component.LEFT_ALIGNMENT);
		p.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				"Parameter", TitledBorder.LEFT, TitledBorder.TOP));
		return p;
	}

	/**
	 * Create correlia screen
	 * @param screenFitFactor =1 -> fits to screen, =2 fits to screen when 50% zoomed etc. ...
	 * @return screen handler
	 */
	protected correliaScreen setup_screenWindow(int screenFitFactor) {
		return setup_screenWindow(screenFitFactor, true);
	}
	protected correliaScreen setup_screenWindow(int screenFitFactor, boolean show) {
		debug.put("entered");
	
		// determine size of correlia screen, get aspect ration from project base image
		int corscrWidth  = systemInfo.screenWidth()*2/3;
		double ch = ((double) corscrWidth)/prj.aspectRatio();
		int corscrHeight = (int) ch;

		correliaScreen cs;
		if (output == null) {
			cs = new correliaScreen(NewImage.createRGBImage("CORRELIA",    // title
					corscrWidth * screenFitFactor,
					corscrHeight * screenFitFactor,
					1,    // one slice only
					NewImage.FILL_BLACK)
			);
		} else {
			cs = output;
			cs.setStack(NewImage.createRGBImage("CORRELIA",    // title
					corscrWidth * screenFitFactor,
					corscrHeight * screenFitFactor,
					1,    // one slice only
					NewImage.FILL_BLACK).getStack()
			);
		}
		cs.screenFitFactor = screenFitFactor;
		// re-adjust calibration to screen window
		cs.getCalibration().setUnit(prj.baseImage.getCalibration().getUnit());
		cs.getCalibration().pixelHeight = prj.height()/(corscrHeight*screenFitFactor);
		cs.getCalibration().pixelWidth  = prj.width() /(corscrWidth *screenFitFactor);

		if (!show) {
			return cs;
		}
		try{
			cs.show();
		} catch(IllegalArgumentException e) {
			IJ.log("correlia_ui.setup_correliaScreen: caught IllegalArgumentException");
			IJ.log("DEBUG:");
			IJ.log("  corscrWidth ="+corscrWidth);
			IJ.log("  corscrHeight="+corscrHeight);
			IJ.log("  prj.aspectRatio()="+prj.aspectRatio());
			IJ.log("  correliaScreen screenWidth ="+cs.getWidth());
			IJ.log("  correliaScreen screenHeight="+cs.getHeight());
			IJ.log("  correliaScreen pixelWidth ="+cs.getCalibration().pixelWidth);
			IJ.log("  correliaScreen pixelHeight="+cs.getCalibration().pixelHeight);
			IJ.log("  -------------------------");
			IJ.log("  try to set correlia screen to base image dimensions");
			corscrWidth = prj.get_image(0).getWidth();
			corscrHeight = prj.get_image(0).getHeight();
			cs = new correliaScreen(NewImage.createRGBImage("CORRELIA",	// title
				corscrWidth*screenFitFactor,
				corscrHeight*screenFitFactor,
				1,	// one slice only
				NewImage.FILL_BLACK)
			);
			cs.screenFitFactor = screenFitFactor;
			// re-adjust calibration to screen window
			cs.getCalibration().setUnit(prj.baseImage.getCalibration().getUnit());
			cs.getCalibration().pixelHeight = prj.baseImage.getCalibration().pixelHeight/screenFitFactor;
			cs.getCalibration().pixelWidth  = prj.baseImage.getCalibration().pixelWidth/screenFitFactor;
			IJ.log("  try to show correlia screen");
			cs.show();
		}
		return cs;
	}

	/**
	 * call after the correlia screen was initialised
	 * @return canvas of the screen
	 */
	private ImageCanvas setup_canvas() {
		debug.put("entered");
		
		ImageCanvas ic = output.getWindow().getCanvas();
		ic.addMouseListener(this);
		ic.addMouseMotionListener(this);
		ic.addKeyListener(this);
		output.getWindow().addWindowListener(
			new java.awt.event.WindowAdapter() {
				@Override public void windowClosing(java.awt.event.WindowEvent e) {
					exitUserConfirm();
				}
			}
		);
		return ic;
	}
	
	private JPanel setup_menuBar() {
		debug.put("entered");
	
		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p,BoxLayout.X_AXIS));
		
		// File menu
		JMenu menuFile = new JMenu("File");
		menuFile.setMnemonic(KeyEvent.VK_F);
		JMenuItem menuItemLoad = new JMenuItem("Load Project", KeyEvent.VK_L);
		menuItemLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				load_project("");
			}
		});
		JMenuItem menuItemSave = new JMenuItem("Save Project", KeyEvent.VK_S);
		menuItemSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				save_project(false);
			}
		});
		JMenuItem menuItemSaveAs = new JMenuItem("Save Project As", KeyEvent.VK_A);
		menuItemSaveAs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				save_project(true);
			}
		});
		JMenuItem menuItemQuit = new JMenuItem("Quit", KeyEvent.VK_Q);
		menuItemQuit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				exitUserConfirm();
			}
		});
		
		menuFile.add(menuItemLoad);
		menuFile.add(menuItemSave);
		menuFile.add(menuItemSaveAs);
		menuFile.add(menuItemQuit);

		// Data menu
		JMenu menuData = new JMenu("Data Processing");
		menuData.setMnemonic(KeyEvent.VK_D);
		JMenuItem menuItemExport = new JMenuItem("Export image", KeyEvent.VK_E);
		menuItemExport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				export_ROI_as_ImagePlus();
			}
		});
		menuItemExport.setToolTipText("Export image from ROI");
		menuData.add(menuItemExport);

		JMenuItem menuItemSaveWithScaleBar = new JMenuItem("Save image with scalebar");
		menuItemSaveWithScaleBar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				save_image_with_scalebar();
			}
		});
		menuItemSaveWithScaleBar.setToolTipText("Save image from ROI with scalebar as PNG");
		menuData.add(menuItemSaveWithScaleBar);

		JMenuItem menuItemCalcArithSimilarity = new JMenuItem("Calculate image similarity");
		menuItemCalcArithSimilarity.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calcArithSimilarity();
			}
		});
		menuItemCalcArithSimilarity.setToolTipText("Calculate a measure for image similarity");
		menuData.add(menuItemCalcArithSimilarity);

		JMenuItem menuItemFindEdges = new JMenuItem("Find edges", KeyEvent.VK_F);
		menuItemFindEdges.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				create_edges_image();
			}
		});
		menuItemFindEdges.setToolTipText("Find edges in selected image and add image to project");
		menuData.add(menuItemFindEdges);
		
		JMenuItem menuItemCorrelateImages = new JMenuItem("Correlate images", KeyEvent.VK_C);
		menuItemCorrelateImages.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dataProc_multiplicationCorrelationOfImages();
			}
		});
		menuItemCorrelateImages.setToolTipText("<html>Multiplication correlate images in project and add resulting image to project</html>");
		menuData.add(menuItemCorrelateImages);

		JMenuItem menuItemPearsonCorrImages = new JMenuItem("Pearson correlation", KeyEvent.VK_C);
		menuItemPearsonCorrImages.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dataProc_pearsonCorrelationOfImages();
			}
		});
		menuItemPearsonCorrImages.setToolTipText("<html>Calculate Pearson-correlation of two images in ROI</html>");
		menuData.add(menuItemPearsonCorrImages);
		
		JMenuItem menuItemAddImages = new JMenuItem("Add images", KeyEvent.VK_A);
		menuItemAddImages.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dataProc_addImages();
			}
		});
		menuItemAddImages.setToolTipText("<html>Add images in project and add resulting image to project</html>");
		menuData.add(menuItemAddImages);
		
		JMenuItem menuItemMultiplyImages = new JMenuItem("Multiply images", KeyEvent.VK_A);
		menuItemMultiplyImages.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dataProc_multiplyImages();
			}
		});
		menuItemMultiplyImages.setToolTipText("<html>Multiply images in project and add resulting image to project</html>");
		menuData.add(menuItemMultiplyImages);
		
		// View menu
		JMenu menuView = new JMenu("View");
		menuView.setMnemonic(KeyEvent.VK_D);
		JMenuItem menuItemIncreaseDisplayResolution = new JMenuItem("Display+", KeyEvent.VK_E);
		menuItemIncreaseDisplayResolution.setToolTipText("Increase display resolution");
		menuItemIncreaseDisplayResolution.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				change_correliaScreenResolution(true);
			}
		});
		menuView.add(menuItemIncreaseDisplayResolution);
		JMenuItem menuItemDecreaseDisplayResolution = new JMenuItem("Display-", KeyEvent.VK_E);
		menuItemDecreaseDisplayResolution.setToolTipText("Decrease display resolution");
		menuItemDecreaseDisplayResolution.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				change_correliaScreenResolution(false);
			}
		});
		menuView.add(menuItemDecreaseDisplayResolution);
		
		// Help menu
		JMenu menuHelp = new JMenu("Help");
		menuHelp.setMnemonic(KeyEvent.VK_M);
		JMenuItem menuItemHelp = new JMenuItem("Manual", KeyEvent.VK_H);
		menuItemHelp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					  JEditorPane jep = new JEditorPane();
					  jep.setEditable(false);
					  jep.setPage("file://"+IJ.getDirectory("plugins")+"/Correlia/correlia_manual.html");

					  JScrollPane scrollPane = new JScrollPane(jep);
					  JFrame f = new JFrame("Correlia Manual");
					  f.getContentPane().add(scrollPane);
					  f.setMinimumSize(new Dimension(400,300));
					  f.setVisible(true);
				} catch (IOException ioe) {
					IJ.log("DEBUG: caught exception");
					JOptionPane.showMessageDialog(null, "Cannot open help.html, file not found.");
				}
			}
		});
		
		JMenuItem menuItemAbout = new JMenuItem("About Correlia", KeyEvent.VK_A);
		menuItemAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				about();
			}
		});

		menuHelp.add(menuItemHelp);
		menuHelp.add(menuItemAbout);
		
		// Debug menu
		JMenu menuDebug = new JMenu("Debug");
		menuDebug.setMnemonic(KeyEvent.VK_D);
		JMenuItem menuItemDebuggingProtocol = new JMenuItem("Show Debuggin Protocol", KeyEvent.VK_A);
		menuItemDebuggingProtocol.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				IJ.log("\n\n"+debug.getProtocol());
			}
		});
		menuDebug.add(menuItemDebuggingProtocol);

		JMenuItem menuItemDebugOnOff = new JMenuItem((debug.is_debugging()?"Deactivate":"Activate") + " debug mode", KeyEvent.VK_A);
		menuItemDebugOnOff.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (debug.is_debugging()) {
					debug.debugger_off();
				} else {
					debug.debugger_on();
				}
				menuItemDebugOnOff.setText((debug.is_debugging()?"Deactivate":"Activate") + " debug log");
			}
		});
		menuDebug.add(menuItemDebugOnOff);
		
		JMenuBar mb = new JMenuBar(); //
		mb.add(menuFile);
		mb.add(menuView);
		mb.add(setup_menuBar_project());
		mb.add(setup_menuBar_image());
		mb.add(menuData);
		mb.add(menuHelp);
		mb.add(menuDebug);

		p.add(mb);
		p.add(Box.createHorizontalGlue());
		return p;
	}
	
	private JMenu setup_menuBar_project() {
		JMenu menuPrj = new JMenu("Project");
		menuPrj.setToolTipText("<html>Project menu:<br><li>add/remove data to/from project</li><br><li>edit information on project</li></html>");
		menuPrj.setMnemonic(KeyEvent.VK_P);
		
		// define menu items, tooltips and actions 
		JMenuItem menuItemAddImg = new JMenuItem("Add image to project", KeyEvent.VK_A);
		menuItemAddImg.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				open_image_and_add_to_project();
			}
		});
		menuItemAddImg.setToolTipText("Open an image (stack) and add it to the project"); 
		JMenuItem menuItemRemImg = new JMenuItem("Remove image from project", KeyEvent.VK_R);
		menuItemRemImg.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				remove_image_from_project();
			}
		});
		menuItemRemImg.setToolTipText("Delete the selected image from project");
		JMenuItem menuItemSwapImg = new JMenuItem("Swap images in project", KeyEvent.VK_R);
		menuItemSwapImg.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				swap_images_in_project();
			}
		});
		menuItemSwapImg.setToolTipText("Change the position of selected image in stack by swapping it with another image in the project");
		JMenuItem menuItemPrjProp = new JMenuItem("Project properties", KeyEvent.VK_P);
		menuItemPrjProp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				edit_project_properties();
			}
		});
		menuItemPrjProp.setToolTipText("Display and edit project properties and information");
		
		// add items
		menuPrj.add(menuItemAddImg);
		menuPrj.add(menuItemRemImg);
		menuPrj.add(menuItemSwapImg);
		menuPrj.add(menuItemPrjProp);
		
		return menuPrj;
	}
	
	private JMenu setup_menuBar_image() {
		JMenu menuImg = new JMenu("Image");
		menuImg.setToolTipText("<html>Image menu:<br><li>edit selected image</li><br><li>display image</li><br><li>define features</li><br><li>correlate image data with other data in the project</li></html>");
		menuImg.setMnemonic(KeyEvent.VK_I);

		// define menu items, tooltips and actions
		JMenuItem menuItemDisplay = new JMenuItem("Display image", KeyEvent.VK_I);
		menuItemDisplay.setToolTipText("<html>Display image:<br><li>define features</li><br><li>define ROIs</li><br><li>add additional information (spectra/images)</li></html>");
		menuItemDisplay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				prj.get_image(get_selected_image()).show();
			}
		});
		
		JMenuItem menuItemSetSlice = new JMenuItem("Set slice", KeyEvent.VK_I);
		menuItemSetSlice.setToolTipText("<html>set selected slice in image stack</html>");
		menuItemSetSlice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				set_slice();
			}
		});

		JMenuItem menuItemDelFeat = new JMenuItem("Delete features", KeyEvent.VK_I);
		menuItemDelFeat.setToolTipText("<html>delete some of the already defined features of the selected image</html>");
		menuItemDelFeat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				delete_features_in_image();
			}
		});
		
		JMenuItem menuItemFeatAlignBase = new JMenuItem("Align via features", KeyEvent.VK_F);
		menuItemFeatAlignBase.setToolTipText("<html>correlate features in selected image with features in a reference image and align image accordingly</html>");
		menuItemFeatAlignBase.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				imgAlign_correlate_features();
			}
		});
		
		JMenuItem menuItemPearsonAlign = new JMenuItem("Pearson-Alignment", KeyEvent.VK_F);
		menuItemPearsonAlign.setToolTipText("<html>align selected image with another image using the Pearson-correlation gradient</html>");
		menuItemPearsonAlign.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				imgAlign_Pearson_correlation();
			}
		});

		JMenuItem menuItemCopyCoord = new JMenuItem("Copy coordinates", KeyEvent.VK_C);
		menuItemCopyCoord.setToolTipText("<html>copy coordinates of selected image to other images in the project</html>");
		menuItemCopyCoord.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				copy_coordinates_to_images();
			}
		});

		JMenuItem menuItemCopyFeatures = new JMenuItem("Copy features", KeyEvent.VK_C);
		menuItemCopyFeatures.setToolTipText("<html>copy features of selected image to other images in the project</html>");
		menuItemCopyFeatures.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				copy_features_to_images();
			}
		});
		
		JMenuItem menuItemInfo = new JMenuItem("Image information", KeyEvent.VK_I);
		menuItemInfo.setToolTipText("Display and edit image properties and information");
		menuItemInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				edit_image_properties();
			}
		});
		
		JMenuItem menuItemCopyInfo = new JMenuItem("Copy image information", KeyEvent.VK_A);
		menuItemCopyInfo.setToolTipText("Copy properties and information of selected image to other images in the project");
		menuItemCopyInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				copy_image_properties();
			}
		});
		
		JMenuItem menuItemEditSliceLabel = new JMenuItem("Edit slice label", KeyEvent.VK_A);
		menuItemEditSliceLabel.setToolTipText("Edit slice-label of selected slice in selected image (stack)");
		menuItemEditSliceLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				edit_slice_label();
			}
		});
		
		// add items
		menuImg.add(menuItemDisplay);
		menuImg.add(menuItemSetSlice);
		menuImg.addSeparator();
		menuImg.add(menuItemDelFeat);
		menuImg.add(menuItemFeatAlignBase);
		menuImg.add(menuItemPearsonAlign);
		menuImg.add(menuItemCopyCoord);
		menuImg.add(menuItemCopyFeatures);
		menuImg.addSeparator();
		menuImg.add(menuItemInfo);
		menuImg.add(menuItemCopyInfo);
		menuImg.add(menuItemEditSliceLabel);
		return menuImg;
	}

	private JButton createButton(ImageIcon ico, int padding) {
		JButton btn = new JButton(ico);
		btn.setPreferredSize(BTN_DIM);
		if (padding > 0) {
			btn.setBorder(BorderFactory.createEmptyBorder(padding, padding, padding, padding));
		} else {
			btn.setBorder(null);
		}
		btn.setOpaque(false);
		btn.setContentAreaFilled(false);
		return btn;
	}
	public void set_selected_image(int i) {
		selectedImage = i;
	}

	public int get_selected_image() {
		if (selectedImage >= 0 && selectedImage < prj.size()) {
			return selectedImage;
		} else {
			selectedImage = 0;
			return selectedImage;
		}
	}

	public String get_selected_imageID() {
		return prj.get_image(get_selected_image()).get_ID();
	}

	public microscopyImageWarped get_selected_microscopyImageWarped() {
		microscopyImage mi = prj.get_image(get_selected_image());
		if (mi.getClass() == microscopyImageWarped.class) {
			return (microscopyImageWarped) mi;
		} else {
			debug.put("Tried to get a microscopyImage that should be a microscopyImageWarped");
			return null;
		}
	}

	public Boolean open_image_and_add_to_project() {
		debug.put("entered");

		ImagePlus imgs = null;

		int[] openID = WindowManager.getIDList();
		if (openID != null && openID.length > 1) {
			ImagePlus[] impAll = new ImagePlus[openID.length];
			int[] id = new int[openID.length];
			int j = 0;
			for(int k = 0; k < openID.length; k++) {
				impAll[j] = WindowManager.getImage(openID[k]);
				if (impAll[j] == null || impAll[j].equals(output)) {
					continue;
				}
				id[j] = openID[k];
				j++;
			}

			String[] title = new String[j+1];
			for (int i = 0; i < j; i++) {
				title[i] = impAll[i].getTitle();
				if (title[i].length() > 20) {
					title[i] = title[i].substring(0, 17) + "...";
				}
			}
			title[title.length-1] = "Select file from disk...";
			GenericDialog gd = new GenericDialog("Select image");
			gd.addChoice("Image", title, title[j-1]);
			gd.showDialog();
			if (gd.wasCanceled()) {
				return false;
			}

			int sel = gd.getNextChoiceIndex();
			if (sel < title.length - 1) {
				imgs = WindowManager.getImage(id[sel]).duplicate();
			}
		}

		if (imgs == null) {
			OpenDialog od = new OpenDialog("Open image and add it to project");
			if (od.getPath() == null) {
				return false;
			}    // was dialogue cancelled?
			imgs = new ImagePlus(od.getPath());
			imgs.show();
		}

		// check if there is microscopy image data available for the image (saved in a separate file)
//		String[] pathStr = (od.getPath()).split("\\.(?=[^\\.]+$)");

//		if (pathStr[pathStr.length-1].equals("mimg")) {	// microscopy image definition found, parse definition and instanciate
//			// invoke handler for microscopyImages
//			mimgHandler mih = new mimgHandler();
//			// open mimg file, parse it, instanciate microscopyImage and instanciate correlia project
//			microscopyImage mi = mih.open(od.getDirectory(), od.getFileName());
//
//			if (mi == null) {return false;}
//			prj.importImage(mi,
//					new imageColour(0.6, 0.6, 0.6),
//					new imageAlignment()
//			);
//
//			update_gui();
//			return true;
//		}

		// if image stack -> two options:
		//          1.) import all slices as separate images
		//          2.) import as image stack

		int importType = 0;

		if (imgs.getStackSize() > 1) {
			String[] str = {"Import stack as separate images", "Import as stack"};
			dlgRadiobuttons rbtn = new dlgRadiobuttons(gd,"Add image stack to project", str, false);
			rbtn.setSize(300,170);
			rbtn.setLocationRelativeTo(null);
			rbtn.setVisible(true);
			importType = rbtn.get_selected_button();
			rbtn.hide();
		}

		if (importType == 0) {	// import as separate images
			for (int i = 1; i <= imgs.getStackSize(); i++) {
				String title;
				if (imgs.getStackSize() > 1) {
					title = imgs.getImageStack().getSliceLabel(i);
				} else {
					title = imgs.getTitle();
				}
				ImagePlus imp = new ImagePlus(title, imgs.getImageStack().getProcessor(i));
				imp.getCalibration().setUnit(imgs.getCalibration().getUnit());
				imp.getCalibration().pixelWidth = imgs.getCalibration().pixelWidth;
				imp.getCalibration().pixelHeight = imgs.getCalibration().pixelHeight;

				// import new image, colour string and coordinates
				prj.importImage(new microscopyImage(imp),
						new imageColour(1, 1, 1),
						null
				);
			}
		} else {	// import as stack
			ImagePlus imp = new ImagePlus(imgs.getTitle(), imgs.getImageStack());
			imp.getCalibration().setUnit(imgs.getCalibration().getUnit());
			imp.getCalibration().pixelWidth = imgs.getCalibration().pixelWidth;
			imp.getCalibration().pixelHeight = imgs.getCalibration().pixelHeight;

			// import new image, colour string and coordinates
			prj.importImage(new microscopyImage(imp),
					new imageColour(1, 1, 1),
					null
			);
		}

		imgs.close();

		update_gui();
		return true;
	}


	private void outputOverlay_draw_feature_points() {
		debug.put("entered");
		
		outputOverlay.clear();
		Font fo = new Font(output.getProcessor().getFont().getName(),// name
				output.getProcessor().getFont().getStyle(),	// style
				output.getWidth()/64
		);	// size

		if (showFeaturePoints >= 0) {
			microscopyImage miCur = prj.get_image(showFeaturePoints);
			for (int i = 0; i < miCur.number_of_feature_points(); i++) {    // run through all points
				Point.Double p = miCur.get_feature_point(i);

				if (showFeaturePoints > 0) {
					p = prj.get_imageAlignment(showFeaturePoints).toBase(p);
				}
				int ix = (int) (p.x/output.getCalibration().pixelWidth);
				int iy = (int) (p.y/output.getCalibration().pixelHeight);

				Roi circle = new OvalRoi(ix - output.getWidth() / 200,
						iy - output.getWidth() / 200,
						output.getWidth() / 100,
						output.getWidth() / 100
				);
				Roi circleCentre = new OvalRoi(ix, iy, 0, 0);
				circle.setStrokeColor(Color.red);
				circle.setStrokeWidth(1);
				circleCentre.setStrokeColor(Color.red);
				circleCentre.setStrokeWidth(2);
				TextRoi txt = new TextRoi(ix + output.getWidth() / 70,
						iy - output.getWidth() / 70,
						"" + i,
						fo
				);
				txt.setStrokeColor(Color.red);
				outputOverlay.add(circle);
				outputOverlay.add(circleCentre);
				outputOverlay.add(txt);
			}

			if (get_selected_image() != showFeaturePoints) {
				miCur = prj.get_image(get_selected_image());
				for (int i = 0; i < miCur.number_of_feature_points(); i++) {	// run through all points
					Point.Double p = miCur.get_feature_point(i);

					if (get_selected_image() > 0) {
						p = prj.get_imageAlignment(get_selected_image()).toBase(p);
					}
					int ix = (int) (p.x/output.getCalibration().pixelWidth);
					int iy = (int) (p.y/output.getCalibration().pixelHeight);

					Roi circle = new OvalRoi(ix-output.getWidth()/200,
							iy-output.getWidth()/200,
							output.getWidth()/100,
							output.getWidth()/100
					);
					Roi circleCentre = new OvalRoi(ix, iy, 0, 0);
					circle.setStrokeColor(Color.blue);
					circle.setStrokeWidth(1);
					circleCentre.setStrokeColor(Color.blue);
					circleCentre.setStrokeWidth(2);
					TextRoi txt = new TextRoi(ix+output.getWidth()/70,
							iy-output.getWidth()/70,
							""+i,
							fo
					);
					txt.setStrokeColor(Color.blue);
					outputOverlay.add(circle);
					outputOverlay.add(circleCentre);
					outputOverlay.add(txt);
				}
			}
		}
		output.setOverlay(outputOverlay);
	}
	
	private void remove_image_from_project() {
		debug.put("entered");
	
		if (get_selected_image()<1) {
			IJ.showMessage("Cannot remove the base image.");
			return;
		}

		if (JOptionPane.showConfirmDialog(gd, "Are you sure, that you want to delete this image?","", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			// remove selected image
			prj.removeImage(get_selected_image());
			// update
			update_gui();
		}
	}

	private void swap_images_in_project() {
		debug.put("entered");
	
		if (get_selected_image()<1) {
			IJ.showMessage("Cannot swap base image.");
			return;
		}
	
		String[] imageNames = prj.get_imageNames();
		dlgRadiobuttons drb = new dlgRadiobuttons(gd,
			"Swap image "+prj.get_image(get_selected_image()).getTitle()+" ("+get_selected_image()+") with image",	// title dialogue
			imageNames,	// 
			false	// Boolean hideOKCancel
		);
		drb.setEnabled(0, false);	// disable base image
		drb.setEnabled(get_selected_image(), false);	// disable selected image
		drb.setVisible(true);
		
		prj.swapImages(get_selected_image(), drb.get_selected_button());
		
		update_gui();
	}
	

	private void img_change_colour_channel(double addValue, int channel) {
		debug.put("arg("+addValue+","+channel+") entered, selected image: "+get_selected_image());
	
		double r = prj.get_imageColour(get_selected_image()).R();
		double g = prj.get_imageColour(get_selected_image()).G();
		double b = prj.get_imageColour(get_selected_image()).B();

		if (channel == 0) {r += addValue;}	// red
		if (channel == 1) {g += addValue;}	// green
		if (channel == 2) {b += addValue;}	// blue
		if (channel == 3) {r *= 1+addValue; g *= 1+addValue; b *= 1+addValue;}	// white
			
		prj.get_imageColour(get_selected_image()).set_RGB(r,g,b);
		update_gui();
	}
	
	private void imageSliceChange(int incr) {
		debug.put("arg("+incr+") entered");
	
		int sizeOfStack = prj.get_image(get_selected_image()).getStack().getSize();
		int sl = prj.get_image(get_selected_image()).getCurrentSlice();

		if (sl+incr > 0 && sl+incr <= sizeOfStack) {
			prj.get_image(get_selected_image()).setZ(sl+incr);
		}
		update_gui();
	}
	
	private void set_colour_string() {
		debug.put("entered");
		
		String rgb[] = (TFcolourString.getText()).split(" ");
		
		if (rgb.length != 3) {
			IJ.showMessage("Please enter the RGB values as R G B.\n(Range: 0<RGB<2).");
			return;
		}
		double r, g, b;
		try {
			r = Double.parseDouble(rgb[0].replaceAll(",","."));
			g = Double.parseDouble(rgb[1].replaceAll(",","."));
			b = Double.parseDouble(rgb[2].replaceAll(",","."));
			
			prj.get_imageColour(get_selected_image()).set_RGB(r, g, b);
		} catch (NumberFormatException e) {
			IJ.showMessage("Please enter the RGB values as R G B.\n(Range: 0<RGB<2).");
			return;
		}
		
		update_gui();
	}
	
	public void update_gui() {
		debug.put(" entered");
		
		update_dialogue();
		update_overlay();
		update_output();
	}

	public void update_output() {
		debug.put("entered");

//		if (get_active_card_name(leftPane).equals(DLG_IMAGES_PANE)) {
			draw(binningChoice.getSelectedIndex() + 5);    // preview
			if (!stopThreads) {
				draw(binningChoice.getSelectedIndex());
			}    // proper repaint of the image
//		} else if (get_active_card_name(leftPane).equals(DLG_TRANS_PANE)) {
//			output.setStack(get_selected_microscopyImageWarped().getStack());
//			output.setCalibration(get_selected_microscopyImageWarped().getCalibration());
//		}
	}

	public void update_overlay() {
		debug.put("entered");

		if (get_active_card_name(leftPane).equals(DLG_IMAGES_PANE)) {
			// create overlay
			outputOverlay_draw_feature_points();
			output.setOverlay(outputOverlay);
		} else if (get_active_card_name(leftPane).equals(DLG_TRANS_PANE)) {
			output.setHideOverlay(true);
		}

	}

	public void update_dialogue() {
		debug.put(" entered");

		if (get_active_card_name(leftPane).equals(DLG_IMAGES_PANE)) {
//			btnUISwitch.setText("Switch to Transformation View");
			update_images_pane();
		}
		if (get_active_card_name(leftPane).equals(DLG_TRANS_PANE)) {
//			btnUISwitch.setText("Switch to Images View");
			update_trans_pane();
		}

		if (get_active_card_name(rightPane).equals(DLG_IMAGE_PROPS_PANE)) {
			update_image_props_pane();
		}

		if (get_active_card_name(rightPane).equals(DLG_TRANS_PROPS_PANE)) {
			update_trans_props_pane();
		}
	}

	public void update_images_pane() {
		imageList.removeAll();
		for (int i = 0; i < prj.size(); i++) {
			imageList.add(new imageElementPanel(this, i));
		}
		imageList.revalidate();
		imageList.repaint();
	}

	public void update_image_props_pane() {
		imagePreview.loadImage(prj.get_image(get_selected_image()));
		imagePreview.repaint();
		if (get_selected_image() == 0) {
			btnDelete.setEnabled(false);
			btnLink.setEnabled(false);
			btnLink.setIcon(icoUnlinked);
			coordinatesPane.setVisible(false);
		} else {
			btnDelete.setEnabled(true);
			btnLink.setEnabled(true);
			btnLink.setIcon(prj.imageIsLinked(get_selected_image())?icoLinked:icoUnlinked);
			coordinatesPane.setVisible(true);
		}

		TFX0.setText("" + String.format("%.3f", prj.get_imageAlignment(get_selected_image()).x0()));
		TFY0.setText("" + String.format("%.3f", prj.get_imageAlignment(get_selected_image()).y0()));
		TFRot.setText("" + String.format("%.2f", prj.get_imageAlignment(get_selected_image()).rotAngle()));
		TFScale.setText("" + String.format("%.4f", prj.get_imageAlignment(get_selected_image()).xscale()));
	}

	public void update_trans_pane() {
		transList.removeAll();
		microscopyImageWarped miw = get_selected_microscopyImageWarped();
		for (int i = 0; i < miw.transformationsSize(); i++) {
			transList.add(new transformationElementPanel(this, i));
		}
		transList.revalidate();
		transList.repaint();

		btnOvl.setEnabled(get_selected_microscopyImageWarped().transformationsSize() > 0);
		btnExport.setEnabled(get_selected_microscopyImageWarped().transformationsSize() > 0);
		btnSaveTransformationResult.setEnabled(get_selected_microscopyImageWarped().transformationsSize() > 0);
	}

	public void update_trans_props_pane() {
		microscopyImageWarped selectedMiw = get_selected_microscopyImageWarped();

		if (selectedMiw.transformationsSize() > 0) {
			transPropsPreviewPane.setVisible(true);
			transPropsParamPane.setVisible(true);
			transPreview.loadImage(selectedMiw.getSelectedTransformation().getDeformationPreview());
			transPreview.repaint();
//			selectedMiw.getSelectedTransformation().setImagePosition(prj.get_imageAlignment(get_selected_image()));
			transPropsParamPane.removeAll();
			transPropsParamPane.add(get_selected_microscopyImageWarped().getSelectedTransformation().propPanel());
			transPropsParamPane.revalidate();
			transPropsParamPane.repaint();
		} else {
			transPropsPreviewPane.setVisible(false);
			transPropsParamPane.setVisible(false);
		}
	}

	private void draw(int binning) {	// draw and show the overlay
		debug.put("arg("+binning+") entered");
		
		final long calctime = System.nanoTime();
		if (binning < 0 || binning > output.getHeight()/10 || binning > output.getWidth()/10) {
			binning = 1;
		}
		
		debug.putMicroDebug("try to get ROI");
		Rectangle roi = get_ROI();

		// start as many threads as processors are available
		int numberOfThreads = Runtime.getRuntime().availableProcessors();
		
		// stop running calculations
		debug.putMicroDebug("stop running calculations");
		stopThreads = true;
		while (currentlyRunningThreads >0) {
			try{
				Thread.sleep(100);
			} catch (InterruptedException e) {}
		}
		if (currentlyRunningThreads<0) {
			currentlyRunningThreads = 0;
		}
		stopThreads = false;

		// create a pool of threads
		debug.putMicroDebug("create threadPool");
		threadPool = Executors.newFixedThreadPool(numberOfThreads);
		final Semaphore writeLock = new Semaphore(1);
		final Semaphore readImagesLock = new Semaphore(1);
		final Semaphore repaintLock = new Semaphore(1);

		// submit jobs to threads
		debug.put("submit jobs to threads");
		for (int i = 0; i < numberOfThreads; i++) {
			final int ID = i;
			final int x0 = (2*binning+1)*i + roi.x;
			final int dx = (2*binning+1)*numberOfThreads;
			final int binningth = binning;

			final int roixmin = roi.x;
			final int roixmax = roi.x+roi.width;
			final int roiymin = roi.y;
			final int roiymax = roi.y+roi.height;
			
			final double corscrPixelWidth = output.getCalibration().pixelWidth;
			final double corscrPixelHeight = output.getCalibration().pixelHeight;
			
			final int ymax = output.getHeight()-binningth;

			debug.putMicroDebug("create thread ID="+ID);
			threadPool.submit(new Runnable() {
				public void run() {	// implement the job
					currentlyRunningThreads++;
					int[] pixVal;
					double xc, yc;
					
					for (int x = x0; x < roixmax; x += dx) {
						if (stopThreads) {break;}
						for (int y = roiymin + binningth; y < roiymax && !stopThreads; y+=2*binningth+1) {
							if (stopThreads) {break;}

							xc = ((double) x)*corscrPixelWidth;
							yc = ((double) y)*corscrPixelHeight;

							pixVal = prj.calc_pixelValue(xc,yc,readImagesLock);

							try {
								writeLock.acquire();	// protected code

								// draw binning pixel
								for (int xb = -binningth; xb<=binningth; xb++) {
									for (int yb = -binningth; yb<=binningth; yb++) {
										opProc.putPixel(x+xb, y+yb, pixVal);
									}
								}
								writeLock.release();
							}
							catch (InterruptedException e) {}
						}
					}
					
					try {	// repaint in order to show progress
						repaintLock.acquire();	// protected code
						output.repaintWindow();
						repaintLock.release();
					} catch (InterruptedException e) {}
					elapsedTime = System.nanoTime()-calctime;
					currentlyRunningThreads--;
				}
			});
		}
		
		// wait for threads to finish
		debug.put("wait for threads to finish and shut down threadPool");
		threadPool.shutdown();
		try {
			threadPool.awaitTermination(100, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {}

		debug.put("show output and repaint window");
		output.show();
		output.repaintWindow();
	}
	
	// change resolution of correlia output window
	protected void change_correliaScreenResolution(Boolean enlarge) {	// if true increase resolution, if false decrease resolution
		debug.put("arg("+enlarge+") entered");
		int screenFitFactor;
		if (enlarge) {
			screenFitFactor = (int) (output.screenFitFactor*2);
		} else {
			screenFitFactor = (int) (output.screenFitFactor/2);
			if (screenFitFactor < 1) {
				screenFitFactor = 1;
			}
		}
				
		// remove listeners from old output and close it
		debug.putMicroDebug("remove listeners from canvas and close window");
		removeListenersFromCanvas();
		output.getWindow().close();
				
		// create a new output window
		debug.putMicroDebug("create new output window");
		output = setup_screenWindow(screenFitFactor);
		opProc = output.getProcessor();
				
		opCanvas = setup_canvas();
		// change binning in order to keep computing time short
		if (enlarge && binningChoice.getSelectedIndex() < 10) {
			binningChoice.setSelectedIndex(binningChoice.getSelectedIndex()+1);
		}
		if (!enlarge && binningChoice.getSelectedIndex() > 0) {
			binningChoice.setSelectedIndex(binningChoice.getSelectedIndex()-1);
		}
		update_gui();
	}
	
	// correlate the features in the image with those defined in the base
	public void imgAlign_correlate_features_with_base() {
		debug.put("entered");
		if (get_selected_image() == 0) {
			IJ.showMessage("Please select an image to correlate!");
			return;
		}
	
		// open a dialogue in which feature points in base image and feature points in the selected image are listed and must be correlated
		debug.putMicroDebug("open correlate features dialogue");
		dlgCorrelateFeatures d = new dlgCorrelateFeatures(
				prj.get_image(0).get_feature_points(),
				prj.get_image(get_selected_image()).get_feature_points()
				);
				
		if (d.wasCanceled()) {return;}

		prj.set_imageAlignment(get_selected_image(),
				d.get_imageAlignment(prj.get_image(get_selected_image()).imgWidth()/2,
						prj.get_image(get_selected_image()).imgHeight()/2), false);	// override old coordinates

		update_gui();
	}

	// correlate the features in the image with those defined in the base
	public void imgAlign_correlate_features() {
		debug.put("entered");
		if (get_selected_image() == 0) {
			IJ.showMessage("Please select a source image that is not the base image!");
			return;
		}

		int sel = MiscHelper.selectImageFromProject(prj, gd, "Select reference image", 0, false);
		if (sel < 0) {
			return;
		}

		// open a dialogue in which feature points in base image and feature points in the selected image are listed and must be correlated
		debug.putMicroDebug("open correlate features dialogue");
		dlgMatchFeatures d = new dlgMatchFeatures(
				prj.get_image(get_selected_image()),
				prj.get_image(sel),
				prj.get_imageAlignment(get_selected_image()),
				prj.get_imageAlignment(sel),
				true,
				output,
				null
				);

		if (d.wasCancelled()) {
			output.setStack(setup_screenWindow(1, false).getStack());
			output.getCalibration().setUnit(prj.baseImage.getCalibration().getUnit());
			output.getCalibration().pixelHeight = prj.height()/(output.getHeight());
			output.getCalibration().pixelWidth  = prj.width() /(output.getWidth());
			opProc = output.getProcessor();
			update_gui();
			return;
		}

		imageAlignment iaNew = d.getImageAlignment();
		if (iaNew != null) {
			prj.set_imageAlignment(get_selected_image(), iaNew, false);
		}

		output.setStack(setup_screenWindow(1, false).getStack());
		output.getCalibration().setUnit(prj.baseImage.getCalibration().getUnit());
		output.getCalibration().pixelHeight = prj.height()/(output.getHeight());
		output.getCalibration().pixelWidth  = prj.width() /(output.getWidth());
		opProc = output.getProcessor();
		update_gui();
	}
	
	public void imgAlign_Pearson_correlation() {
		debug.put("entered");
		int img1 = get_selected_image();
		if (img1 == 0) {
			IJ.showMessage("Please select an image to correlate!\nBase image cannot be translated.");
			return;
		}
	
		// select image to which the selected image should be aligned
		String imgs[] = prj.get_imageNames();
		dlgRadiobuttons drb = new dlgRadiobuttons(gd,"To which image shall be aligned?", imgs, false);
	//	rbtn.setSize(300,170);
		drb.setLocationRelativeTo(null);
		drb.setVisible(true);
		int img2 = drb.get_selected_button();
		
		if (img1 == img2) {
			IJ.showMessage("Cannot align image with itself!");
			return;
		}
		
		// run alignment algorithm
		Point2D.Double[] pts = prj.generateRandomPointsList(get_ROIcal(), 200, -1);	// 5000 points, seed=-1 -> initialise with system time
		
		// set-up delta vector
		double[] deltaVector = new double[4];
		deltaVector[0] = prj.get_image(img1).pixelWidth()  / 1;	// dx
		deltaVector[1] = prj.get_image(img1).pixelHeight() / 1;	// dy
		deltaVector[2] = 0.01;	// drotation, 1/10 of a degree
		deltaVector[3] = 0.0001;	// dscl (1 is normal size)
		
		// calculate gradient
		double[] grad = prj.dataProc_calcPearsonCorrelationOfImages_gradient(img1, img2, deltaVector, pts);
		
		for (int i=0; i<100; i++) {
			// move image along the gradient -> set new coordinates
			prj.get_imageAlignment(img1).set_x0y0(
				prj.get_imageAlignment(img1).x0()+grad[0]*deltaVector[0],
				prj.get_imageAlignment(img1).y0()+grad[1]*deltaVector[1]
			);
			prj.get_imageAlignment(img1).set_rotAngle(
					prj.get_imageAlignment(img1).rotAngle()+grad[2]*deltaVector[2]
			);

			// calculate gradient
			grad = prj.dataProc_calcPearsonCorrelationOfImages_gradient(img1, img2, deltaVector, pts);
			grad[3]=0;	// DEBUG: scaling off
			//draw(binningChoice.getSelectedIndex());
		}
	}

	// delete features in image
	public void delete_features_in_image() {
		debug.put("entered");
	
		String[] featureList = new String[prj.get_image(get_selected_image()).number_of_feature_points()];
		for (int i=0; i<prj.get_image(get_selected_image()).number_of_feature_points(); i++) {
			featureList[i] = Integer.toString(i);
		}
		
		debug.putMicroDebug("ask user which features to delete");
		dlgCheckboxes DLG_delete_selected_chkBoxes = new dlgCheckboxes(gd, "select features to delete", featureList);
		DLG_delete_selected_chkBoxes.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		DLG_delete_selected_chkBoxes.setVisible(true);
			
		debug.putMicroDebug("delete features");
		for (int i=prj.get_image(get_selected_image()).number_of_feature_points()-1; i>=0; i--) {
			if (DLG_delete_selected_chkBoxes.get_checkbox_states()[i]) {
				prj.get_image(get_selected_image()).remove_feature_point(i);
			}
		}
		update_overlay();
	}
	
	public void copy_coordinates_to_images() {
		debug.put("entered");
	
		if (get_selected_image() == 0) {
			IJ.showMessage("Coordinates of the base image cannot be copied to overlay-images.\nPlease select an overlay image.");
			return;
		}
		if (prj.size() < 3) {
			IJ.showMessage("There is only one overlay image. Where do you want to copy its coordinates to?");
			return;
		}
		String imgNames[] = new String[prj.size()-1];
		
		for(int i=1; i<prj.size(); i++) {
			imgNames[i-1] = prj.get_image(i).getTitle();
		}
		
		debug.putMicroDebug("show checkbox dialogue and ask user to which images coordinates are to be copied");
		DLG_copy_coordinates_chkBoxes = new dlgCheckboxes(gd, "copy coordinates", imgNames);
		DLG_copy_coordinates_chkBoxes.set_checkbox_state(get_selected_image()-1, true);
		DLG_copy_coordinates_chkBoxes.set_checkbox_enable(get_selected_image()-1, false);
		DLG_copy_coordinates_chkBoxes.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		DLG_copy_coordinates_chkBoxes.setVisible(true);
		
		Boolean[] cpy = DLG_copy_coordinates_chkBoxes.get_checkbox_states();
		
		// copy the coordinates and adjustment factor
		debug.putMicroDebug("copy the coordinates");
		for (int i=0; i<cpy.length; i++) {
			if (get_selected_image()-1 != i && cpy[i]) {
				// copy coordinates
				prj.set_imageAlignment(i+1, // position i
					new imageAlignment(prj.get_imageAlignment(get_selected_image())), false); // the selected overlay image coordinates
			
				// copy adjustment factor
//				double adjf = prj.get_image(get_selected_image()).get_adjustmentFactor();
//				prj.get_image(i+1).set_adjustmentFactor(adjf);
			}
		}
		update_gui();
	}

	public void copy_features_to_images() {
		debug.put("entered");

		ArrayList<Integer> imgPos = new ArrayList<>();
		microscopyImage miSel = prj.get_image(get_selected_image());
		for (int i = 0; i < prj.size(); i++) {
			if (i == get_selected_image()) continue;
			microscopyImage mi = prj.get_image(i);
			if (MiscHelper.isEqual(mi.imgWidth(), miSel.imgWidth(), 4)
					&& MiscHelper.isEqual(mi.imgHeight(), miSel.imgHeight(), 4)) {
				imgPos.add(i);
			}
		}

		if (imgPos.size() == 0) {
			IJ.showMessage("No suitable images (same size) in the project!");
			return;
		}
		String imgNames[] = new String[imgPos.size()];
		for (int i = 0; i < imgNames.length; i++) {
			imgNames[i] = prj.get_image(imgPos.get(i)).getTitle();
		}

		dlgCheckboxes dlg = new dlgCheckboxes(gd, "Select images to copy feature to", imgNames);
		dlg.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dlg.setVisible(true);

		if (dlg.was_cancelled()) {
			return;
		}

		boolean replace = JOptionPane.showConfirmDialog(
				output.getWindow(),
				"Do you want to replace the exisiting features?",
				"", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;

		Boolean[] cpy = dlg.get_checkbox_states();

		for (int i = 0; i < cpy.length; i++) {
			if (cpy[i]) {
				microscopyImage miCur = prj.get_image(imgPos.get(i));
				if (replace) {
					miCur.set_feature_points(MiscHelper.copyFeatures(miSel.get_feature_points()));
				} else {
					for (Point.Double fp : miSel.get_feature_points()) {
						miCur.add_feature_point(fp);
					}
				}
			}
		}
		update_gui();
	}

	public void edit_project_properties() {
		debug.put("entered");
		dlgEditProjectProperties dlgProp = new dlgEditProjectProperties(prj);
		if (!dlgProp.was_cancelled()) {
			prj.set_projectTitle(dlgProp.get_title());
			prj.set_additionalInfo(dlgProp.get_additionalInfo());
		}

	}
	// edit image properties
	
	public void edit_image_properties() {
		debug.put("entered");
		
		microscopyImage curMI = prj.get_image(get_selected_image());

		DLG_editMicroscopyImageProperties = new dlgEditMicroscopyImageProperties(curMI, gd);
		if (!DLG_editMicroscopyImageProperties.was_cancelled()) {
			curMI.setTitle(DLG_editMicroscopyImageProperties.get_title());
			curMI.set_acquisitionDate(DLG_editMicroscopyImageProperties.get_acquisitionDate());
			curMI.set_setup(DLG_editMicroscopyImageProperties.get_setup());
			curMI.set_experimenter(DLG_editMicroscopyImageProperties.get_experimenter());
			curMI.set_additionalInfo(DLG_editMicroscopyImageProperties.get_additionalInfo());
		}

		//update_gui(true);
		update_dialogue();
	}

	public void set_slice() {
		debug.put("entered");
		String sl = JOptionPane.showInputDialog(null,"Please enter a slice-number.","Set slice",JOptionPane.INFORMATION_MESSAGE);
		try {
			int sidx = Integer.parseInt(sl);
			int sizeOfStack = prj.get_image(get_selected_image()).getStack().getSize();
			if (sidx <= 0) {
				IJ.showMessage("Please enter a number larger than 0.");
				return;
			}
			if (sidx > sizeOfStack) {
				IJ.showMessage("Slice doesn't exist. Stack-size is "+sizeOfStack+".");
				return;
			}
			prj.get_image(get_selected_image()).setZ(sidx);
		} catch (NumberFormatException e) {
			IJ.showMessage("Please enter a number.");
		}
		update_gui();
	}
	
	public void edit_slice_label() {
		debug.put("entered");
		String sl = JOptionPane.showInputDialog(null,"Please enter new slice label.","Slice label",JOptionPane.INFORMATION_MESSAGE);
		int sidx = prj.get_image(get_selected_image()).getZ();
		prj.get_image(get_selected_image()).getStack().setSliceLabel(sl, sidx);
		update_dialogue();
	}

	public void copy_image_properties() {
		debug.put("entered");
		String title;
		
		if (get_selected_image() == 0) {
			title = "copy properties of base image to images";
		} else {
			title = "copy properties of image \""+prj.get_image(get_selected_image()).getTitle()+"\"  to images";
		}
	
		debug.putMicroDebug("show checkbox dialogue and ask user to which images properties are to be copied");
		dlgCheckboxes DLG_copy_image_properties = new dlgCheckboxes(gd, title, prj.get_imageNames());
		DLG_copy_image_properties.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		DLG_copy_image_properties.set_checkbox_state(get_selected_image(), true);
		DLG_copy_image_properties.set_checkbox_enable(get_selected_image(), false);
		DLG_copy_image_properties.setVisible(true);

		Boolean ckb[] = DLG_copy_image_properties.get_checkbox_states();
		
		debug.putMicroDebug("copy the properties to selected images");
		for (int i=0; i<prj.size(); i++) {
			if (ckb[i]) {	// copy to this image?
				prj.get_image(i).set_acquisitionDate(prj.get_image(get_selected_image()).get_acquisitionDate());
				prj.get_image(i).set_setup(prj.get_image(get_selected_image()).get_setup());
				prj.get_image(i).set_experimenter(prj.get_image(get_selected_image()).get_experimenter());
				prj.get_image(i).set_additionalInfo(prj.get_image(get_selected_image()).get_additionalInfo());
			}
		}
	}
	
	// calculate a close-up from ROI and export it as a new ImagePlus with user-selected resolution 
	public Boolean export_ROI_as_ImagePlus() {
		debug.put("entered");
	
		Rectangle roi = get_ROI();
		
		// ask user about resolution of the resulting image
		debug.putMicroDebug("ask user about resolution of resulting image");
		GenericDialog d = new GenericDialog("Choose output resolution");
		Panel pan = new Panel();
		JComboBox sel = new JComboBox();
		sel.addItem(new String(Integer.toString(roi.width)+" x "+Integer.toString(roi.height)));
		sel.addItem(new String("256 x "+Integer.toString((int) (256.0/((double) roi.width) * ((double) roi.height)))));
		sel.addItem(new String("512 x "+Integer.toString((int) (512.0/((double) roi.width) * ((double) roi.height)))));
		sel.addItem(new String("1024 x "+Integer.toString((int) (1024.0/((double) roi.width) * ((double) roi.height)))));
		sel.addItem(new String("2048 x "+Integer.toString((int) (2048.0/((double) roi.width) * ((double) roi.height)))));
		sel.addItem(new String("4096 x "+Integer.toString((int) (4096.0/((double) roi.width) * ((double) roi.height)))));
		sel.setMinimumSize(new Dimension(250, 40));
		
		pan.add(sel);
		pan.setMinimumSize(new Dimension(250, 40));
		d.addPanel(pan);
		d.showDialog();
		
		if (d.wasCanceled()) {return false;}
		
		debug.putMicroDebug("calculate image");
		switch (sel.getSelectedIndex()) {
			case 0: calculate_close_up_from_ROI(roi, roi.width, roi.height);
				break;
			case 1: calculate_close_up_from_ROI(roi, 256, (int) (256.0/((double) roi.width) * ((double) roi.height)));
				break;
			case 2: calculate_close_up_from_ROI(roi, 512, (int) (512.0/((double) roi.width) * ((double) roi.height)));
				break;
			case 3: calculate_close_up_from_ROI(roi, 1024, (int) (1024.0/((double) roi.width) * ((double) roi.height)));
				break;
			case 4: calculate_close_up_from_ROI(roi, 2048, (int) (2048.0/((double) roi.width) * ((double) roi.height)));
				break;
			case 5: calculate_close_up_from_ROI(roi, 4096, (int) (4096.0/((double) roi.width) * ((double) roi.height)));
				break;
		}
		return true;
	}

	public boolean save_image_with_scalebar() {
		debug.put("entered");

		String s = JOptionPane.showInputDialog(gd, "Add roi string or leave empty to use current roi",
				"Roi selection", JOptionPane.QUESTION_MESSAGE);
		Rectangle roi;
		String[] parts = null;
		if (s.isEmpty()) {
			roi = get_ROI();
		} else {
			parts = s.split(";");
			roi = new Rectangle(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]),
					Integer.parseInt(parts[2]), Integer.parseInt(parts[3]));
		}
		if (roi != null) {
			IJ.log((int)roi.getX() + ";" + (int)roi.getY() + ";" + (int)roi.getWidth() + ";" + (int)roi.getHeight());
		}
		int width = 1024;
		int height = (int)(width * roi.getHeight() / roi.getWidth());

		SaveDialog sd = new SaveDialog("Save location", "", "");
		if (sd.getDirectory() == null) {
			return false;
		}
		String dir = sd.getDirectory();
		String fname = sd.getFileName().replaceAll(" ", "_");
		String projectPath = dir + fname;


		for (int i = 0; i < get_prj().size(); i++) {
			get_prj().get_imageColour(i).set_visibility(imageColour.HIDE);
		}
		String[] type = {"_ovl", "_res", "_ref"};
		imageColour icCur = get_prj().get_imageColour(get_selected_image());
		imageColour icBase = get_prj().get_imageColour(0);
		Color curCol = icCur.get_color();
		for (int t = 0; t < type.length; t++) {
			switch (type[t]) {
				case "_ovl":
					icBase.set_visibility(imageColour.SHOW);
					icCur.set_visibility(imageColour.SHOW);
					break;
				case "_res":
					icBase.set_visibility(imageColour.HIDE);
					icCur.set_visibility(imageColour.SHOW);
					icCur.set_color(Color.WHITE);
					break;
				case "_ref":
					icBase.set_visibility(imageColour.SHOW);
					icCur.set_visibility(imageColour.HIDE);
					break;
			}

			ColorProcessor cp = new ColorProcessor(width, height);

			double x0 = roi.x * output.getCalibration().pixelWidth;
			double dx = (roi.width * output.getCalibration().pixelWidth) / width;
			double y0 = roi.y * output.getCalibration().pixelHeight;
			double dy = (roi.height * output.getCalibration().pixelHeight) / height;

			double x = x0;
			debug.putMicroDebug("calculate the pixels");
			for (int i = 0; i < width; i++) {
				double y = y0;
				for (int j = 0; j < height; j++) {
					cp.putPixel(i, j, prj.calc_pixelValue(x, y, new Semaphore(1)));
					y += dy;
				}
				x += dx;
			}

			icCur.set_color(curCol);

			ImagePlus imp = new ImagePlus(prj.get_image(get_selected_image()).getTitle(), cp);
			// set calibration
			debug.putMicroDebug("set calibration");
			imp.getCalibration().setUnit(prj.get_image(0).getCalibration().getUnit());    // get calibration from base image
			imp.getCalibration().pixelWidth = dx;
			imp.getCalibration().pixelHeight = dy;

			String loc = "Lower Right";
			double barWidth = Math.ceil(dx * imp.getWidth() / 10);
			while (barWidth > dx * imp.getWidth() / 2) barWidth /= 2;
			if (parts != null && parts.length > 4) loc = parts[4];
			if (parts != null && parts.length > 5) barWidth = Double.parseDouble(parts[5]);
			IJ.run(imp, "Scale Bar...", "width=" + barWidth + " height=4 font=14 color=White background=Black location=["+loc+"] bold");

			IJ.saveAs(imp, "png", projectPath + type[t]);
		}

		return true;
	}

	public void calcArithSimilarity() {
		int src = MiscHelper.selectImageFromProject(prj, gd, "Select source image", 1, true);
		int ref = MiscHelper.selectImageFromProject(prj, gd, "Select reference image", 0, true);

		ArrayList<Point.Double> fpSrc = MiscHelper.copyFeatures(prj.get_image(src).get_feature_points());
		ArrayList<Point.Double> fpRef = prj.get_image(ref).get_feature_points();

		// match sequentially
		ArrayList<Integer[]> matches = new ArrayList<>();
		for (int i = 0; i < Math.min(fpSrc.size(), fpRef.size()); i++) {
			Integer[] m = {i, i};
			matches.add(m);
		}

//		microscopyImage miR = new microscopyImage(prj.get_image(ref));
//		miR.set_feature_points(MiscHelper.getFeaturesPointsInRefCoords(fpSrc, prj.get_imageAlignment(src)));
//		miR.show();

		StringBuilder sb = new StringBuilder();
		StringBuilder sbGold = new StringBuilder();

		double SSD = MiscHelper.calculateSSD(
				MiscHelper.getFeaturesPointsInRefCoords(fpSrc, prj.get_imageAlignment(src)),
				MiscHelper.getFeaturesPointsInRefCoords(fpRef, prj.get_imageAlignment(ref)),
				matches);
		IJ.log("Affin (FP):\t" + SSD);
		sb.append(MiscHelper.roundDouble(SSD, 6));

		ImageProcessor[] ipRef = new ImageProcessor[2];
		ImageProcessor[] ipSrc = new ImageProcessor[2];
		GaussianBlur gBlur = new GaussianBlur();
		double smooth = 2;

		int gs = MiscHelper.selectImageFromProject(prj, gd, "Select gold standard", 2, true);
		if (get_prj().get_image(gs).getStackSize() < 2) {
			IJ.log("Goldstandard has to consistent of two layers: image and mask");
			return;
		}
		ipRef[0] = get_prj().get_image(gs).getStack().getProcessor(1).duplicate();
		ipRef[1] = get_prj().get_image(gs).getStack().getProcessor(2);
		gBlur.blurGaussian(ipRef[0], smooth);
		ipSrc[0] = get_prj().get_image(src).getProcessor().duplicate();
		ipSrc[1] = MiscHelper.createMask(ipSrc[0]);
		gBlur.blurGaussian(ipSrc[0], smooth);
		SSD = MiscHelper.calculateSSD(ipSrc, ipRef);
		IJ.log("Affin (GS):\t" + SSD);
		sbGold.append(MiscHelper.roundDouble(SSD, 4));

		ArrayList<Integer> imgPos = new ArrayList<>();
		for (int i = 1; i < prj.size(); i++) {
			if (i == src) continue;
			if (i == ref) continue;
			if (prj.get_image(i) instanceof microscopyImageWarped) {
				imgPos.add(i);
			}
		}

		if (imgPos.size() == 0) {
			IJ.showMessage("No suitable images (same size) in the project!");
			return;
		}
		String imgNames[] = new String[imgPos.size()];
		for (int i = 0; i < imgNames.length; i++) {
			imgNames[i] = prj.get_image(imgPos.get(i)).getTitle();
		}

		dlgCheckboxes dlg = new dlgCheckboxes(gd, "Select images to copy feature to", imgNames);
		dlg.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dlg.setVisible(true);

		if (dlg.was_cancelled()) {
			return;
		}

		Boolean[] cpy = dlg.get_checkbox_states();

		for (int i = 0; i < cpy.length; i++) {
			if (cpy[i]) {
				microscopyImageWarped miwCur = new microscopyImageWarped((microscopyImageWarped)prj.get_image(imgPos.get(i)));
				ArrayList<Point.Double> fpTar = new ArrayList<>();
				for (Point.Double fp : fpSrc) {
					fpTar.add(miwCur.getTransformedCoords(fp));
				}
//				miR = new microscopyImage(prj.get_image(imgPos.get(i)));
//				miR.set_feature_points(fpTar);
//				miR.show();
//				miR = new microscopyImage(prj.get_image(ref));
//				miR.set_feature_points(MiscHelper.getFeaturesPointsInRefCoords(fpTar, prj.get_imageAlignment(imgPos.get(i))));
//				miR.show();
				SSD = MiscHelper.calculateSSD(
						MiscHelper.getFeaturesPointsInRefCoords(fpTar, prj.get_imageAlignment(imgPos.get(i))),
						MiscHelper.getFeaturesPointsInRefCoords(fpRef, prj.get_imageAlignment(ref)),
						matches);
				IJ.log(miwCur.getTitle() + " (FP):\t" + SSD);
				sb.append("," + MiscHelper.roundDouble(SSD, 6));

				ImageStack ims = new ImageStack(miwCur.getWidth(), miwCur.getHeight());
				ims.addSlice("image", miwCur.getProcessor());
				ims.addSlice(MiscHelper.createMask(miwCur.getProcessor()));
				miwCur.set_rawStack(ims);
				miwCur.update();
//				miwCur.show();
				ipSrc[0] = miwCur.getStack().getProcessor(1).duplicate();
				ipSrc[1] = miwCur.getStack().getProcessor(2).duplicate();
				gBlur.blurGaussian(ipSrc[0], smooth);
				SSD = MiscHelper.calculateSSD(ipSrc, ipRef);
				IJ.log(miwCur.getTitle() + " (GS):\t" + SSD);
				sbGold.append("," + MiscHelper.roundDouble(SSD, 4));
			}
		}
		IJ.log(sb.toString());
		IJ.log(sbGold.toString());
	}

	public Boolean calculate_close_up_from_ROI(Rectangle roi, int width, int height) {
		debug.put("arg(ROI,"+width+","+height+") entered");
		
		ColorProcessor cp = new ColorProcessor(width, height);

		double x0 = roi.x * output.getCalibration().pixelWidth;
		double dx = (roi.width*output.getCalibration().pixelWidth)/width;
		double y0 = roi.y * output.getCalibration().pixelHeight;
		double dy = (roi.height*output.getCalibration().pixelHeight)/height;

		double x = x0;
		debug.putMicroDebug("calculate the pixels");
		for (int i = 0; i<width; i++) {
			double y = y0;
			for (int j = 0; j<height; j++) {
				cp.putPixel(i,j,prj.calc_pixelValue(x,y,new Semaphore(1)));
				y+=dy;
			}
			x += dx;
		}
		
		ImagePlus ip = new ImagePlus("close-up",cp);
		// set calibration
		debug.putMicroDebug("set calibration");
		ip.getCalibration().setUnit(prj.get_image(0).getCalibration().getUnit());	// get calibration from base image
		ip.getCalibration().pixelWidth = dx;
		ip.getCalibration().pixelHeight = dy;
		
		ip.show();
		return true;
	}
	
	public void create_edges_image() {
		debug.put("entered");
		if (get_selected_image() == 0) {return;}	// not possible for the base
	
		microscopyImage mi = new microscopyImage(prj.get_image(get_selected_image())); // call copy constructor
		mi.getProcessor().findEdges();
		mi.setTitle(prj.get_image(get_selected_image()).getTitle()+" EDGES");
		prj.importImage(
			mi,	// microscopy image
			new imageColour(prj.get_imageColour(get_selected_image())),	// colour
			new imageAlignment(prj.get_imageAlignment(get_selected_image())));	// coordinates
		update_gui();
	}
	

	public void dataProc_multiplicationCorrelationOfImages() {
		debug.put("entered");
		// get roi if there is one and calculate only inside ROI rectangle
		Rectangle2D roi = get_ROIcal();
		int[] imgs = get_userSelectedImages("Which images shall be correlated?");
		prj.dataProc_calcMultiplicationCorrelationImage(roi, imgs, 2048);	// TODO maybe ask user which resolution he needs
		update_gui();
	}
	
	public void dataProc_pearsonCorrelationOfImages() {
		debug.put("entered");
		// get roi if there is one and calculate only inside ROI rectangle
		Rectangle2D roi = get_ROIcal();
		
		int[] imgs = get_userSelectedImages("Please, select 2 images");
		int count = 0;
		int img1 = -1;
		int img2 = -1;
		
		for (int i=0; i<imgs.length; i++) {
			if (imgs[i]>=0) {
				if (count==0) {
					img1=i;
				} else {
					img2=i;
				}
				count++;
			}
		} 
		if (count!=2) {
			IJ.showMessage("Please select EXACTLY TWO images.");
			return;
		}
		IJ.showMessage("The Pearson-correlation is "+
				prj.dataProc_calcPearsonCorrelationOfImages(roi, img1, img2, 200, 250)
		);
	}
	
	
	
	public void dataProc_addImages() {
		debug.put("entered");
		// get roi if there is one and calculate only inside ROI rectangle
		Rectangle2D roi = get_ROIcal();
		int[] imgs = get_userSelectedImages("Which images shall be added?");
		prj.dataProc_imageMath_addMul(roi, imgs, 2048, correlia.IMAGEMATH_ADD);
		update_gui();
	}
	
	public void dataProc_multiplyImages() {
		debug.put("entered");
		// get roi if there is one and calculate only inside ROI rectangle
		Rectangle2D roi = get_ROIcal();
		int[] imgs = get_userSelectedImages("Which images shall be multiplied?");
		prj.dataProc_imageMath_addMul(roi, imgs, 2048, correlia.IMAGEMATH_MULTIPLY);
		update_gui();
	}
	
	
	public Rectangle get_ROI() {	// return the ROI in pixels of Correlia screen window
		debug.put("entered");
		// get roi if there is one
		Rectangle roi;
		if (output.getRoi() != null) {
			roi = output.getRoi().getBounds();	// ROI
		} else if (!(opCanvas == null)) {
			roi = opCanvas.getSrcRect();  // Use zoomed area
		} else {
			roi = output.getProcessor().getRoi();  // full image
		}
		return roi;
	}
	
	public Rectangle2D get_ROIcal()	{  // return the ROI in base image coordinates
		debug.put("entered");
		Rectangle roi = get_ROI();
		Rectangle2D roical = new Rectangle2D.Double();
		roical.setRect(
			((double) roi.x)      * output.getCalibration().pixelWidth, // x
			((double) roi.y)      * output.getCalibration().pixelHeight,// y
			((double) roi.width) * output.getCalibration().pixelWidth, // width
			((double) roi.height) * output.getCalibration().pixelHeight // height
		);
		return roical;
	}
	
	public int[] get_userSelectedImages(String dlgTitle) {	// will open a checkbox dialog where user can select images from the project
						// function will return a list of length size() with indices of the selected images (if not selected list[i]=-1) 
		debug.put("arg("+dlgTitle+") entered");
		// ask user which images shall be correlated
		dlgCheckboxes dlg = new dlgCheckboxes(gd,dlgTitle, prj.get_imageNames());
		dlg.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dlg.setVisible(true);

		int[] imgs = new int[prj.size()];
		
		if (dlg.was_cancelled()) {
			for (int i=0; i<prj.size(); i++) {
				imgs[i]=-1;
			}
			return imgs;
		}
		for (int i=0; i<prj.size(); i++) {
			if (dlg.get_checkbox_states()[i]) {
				imgs[i]=i;
			} else {
				imgs[i] = -1;
			}
		}
		return imgs;
	}
	
	public Boolean save_project(boolean saveAs) {
		debug.put("entered"); return prj.save_project(saveAs);
	}

	public void about() {
		String s = "CORRELIA Plug-In for ImageJ\n\n";
		
		s += "author: Matthias Schmidt\n";
		s += "email : matthias.schmidt@ufz.de\n\n\n";
		s += "system: "+System.getProperty("os.name")+", "+Runtime.getRuntime().availableProcessors()+" processors available\n \n";
		s += "last calculation took "+elapsedTime/1E9+"s, "+currentlyRunningThreads+" threads are currently running\n \n";

		s += "\n \nclass versions:\n \n";
		s += correlia_ui.classID() + " " + correlia_ui.version() + "\n";
		s += correlia.classID() + " " + correlia.version() + "\n";
		s += correliaScreen.classID()  + " " + correliaScreen.version() + "\n";
		s += imageColour.classID() + " " + imageColour.version() + "\n";
		s += microscopyImage.classID() + " " + microscopyImage.version() + "\n";
		s += microscopyImageRoiHandler.classID() + " " + microscopyImageRoiHandler.version() + "\n";
		s += mimgHandler.classID() + " " + mimgHandler.version() + "\n";
		s += imageAlignment.classID() + " " + imageAlignment.version() + "\n";
		s += parseInputAndInstanciateClass.classID() + " " + parseInputAndInstanciateClass.version() + "\n";
		s += dlgCheckboxes.classID() + " " + dlgCheckboxes.version() + "\n";
		s += dlgRadiobuttons.classID() + " " + dlgRadiobuttons.version() + "\n";
		s += dlgCorrelateFeatures.classID() + " " + dlgCorrelateFeatures.version() + "\n";
		s += dlgMicroImageTitleAndCalibrate.classID() + " " + dlgMicroImageTitleAndCalibrate.version() + "\n";
		s += dlgEditMicroscopyImageProperties.classID() + " " + dlgEditMicroscopyImageProperties.version() + "\n";
		s += dlgEditProjectProperties.classID() + " " + dlgEditProjectProperties.version() + "\n";

		IJ.showMessage(s);
	}
	
	public String get_colourString() {
		debug.put("entered");
		return prj.get_colourString(get_selected_image());
	}

	public correlia get_prj() {
		return prj;
	}

	public imageElementPanel get_imageListElement(int i) {
		return (imageElementPanel) imageList.getComponent(i);
	}
	public transformationElementPanel get_transListElement(int i) {
		return (transformationElementPanel) transList.getComponent(i);
	}

	private void img_moveManually(int direction) { // direction: 0 = right, 1 = left, 2 = down, 3 = up, 4 = rotate clockwise, 5 = rotate counter-clockwise
		debug.put("entered");
		
		if (get_selected_image() == 0) {
			IJ.showMessage("Base image cannot be moved.");
			return;
		}
	
		debug.putMicroDebug("get image and coordinates");
		microscopyImage img = prj.get_image(get_selected_image());
		imageAlignment imgCoord = prj.get_imageAlignment(get_selected_image());
		
		double x0 = imgCoord.x0();
		double y0 = imgCoord.y0();

		// vector from (0,0) of the image to its centre point
		double R = Math.sqrt(img.imgWidth()*img.imgWidth() + img.imgHeight()*img.imgHeight())/2;
		double alpha = Math.acos(img.imgWidth() / (2*R));

		// shift of image centre when rotating
		double dx=0;
		double dy=0;
		
		// get shift vector
		double shift = 0.0005 * prj.width();	// move in steps of 0.05% of the base image width image width
		double rot = 0.25;	// 1/4degree rotation
		
		if (!fineMovement) { // move in coarse mode
			shift = 0.02 * prj.width();	// move in steps of 2% of the base image width image width
			rot = 5;	// rotate by 5 degrees
		}	

		debug.putMicroDebug("move image");
		switch (direction) {
			case 0: // right
				imgCoord.set_x0y0(x0+shift, y0);
				break;
			case 1: // left
				imgCoord.set_x0y0(x0-shift, y0);
				break;
			case 2: // down
				imgCoord.set_x0y0(x0, y0+shift);
				break;
			case 3: // up
				imgCoord.set_x0y0(x0, y0-shift);
				break;
			case 4: // rotate clockwise around image centre -> rotate around upper left corner and shift image afterwards		
//				dx = R*(Math.cos(Math.toRadians(imgCoord.rotAngle()-rot) + alpha) - Math.cos(Math.toRadians(imgCoord.rotAngle()) + alpha));
//				dy = R*(Math.sin(Math.toRadians(imgCoord.rotAngle()-rot) + alpha) - Math.sin(Math.toRadians(imgCoord.rotAngle()) + alpha));
//				imgCoord.set_x0y0(x0-dx, y0-dy);
//				imgCoord.set_rotAngle(imgCoord.rotAngle()-rot);
				imgCoord.set_rotAngle(imgCoord.rotAngle() + rot);
				break;
			case 5: // rotate counter-clockwise
//				dx = R*(Math.cos(Math.toRadians(imgCoord.rotAngle()+rot) + alpha) - Math.cos(Math.toRadians(imgCoord.rotAngle()) + alpha));
//				dy = R*(Math.sin(Math.toRadians(imgCoord.rotAngle()+rot) + alpha) - Math.sin(Math.toRadians(imgCoord.rotAngle()) + alpha));
//				imgCoord.set_x0y0(x0-dx, y0-dy);
//				imgCoord.set_rotAngle(imgCoord.rotAngle()+rot);
				imgCoord.set_rotAngle(imgCoord.rotAngle() - rot);
				break;
		}
		update_gui();
	}

	private void img_moveAutomatically() {
		int reference;
		imageAlignment iaTmp = get_prj().get_imageAlignment(get_selected_image());

		reference = MiscHelper.selectImageFromProject(prj, gd, "Select reference image", 0, false);
		if (reference < 0) return;
		dlgAutoAlignment dlg = new dlgAutoAlignment();
		if (dlg.was_cancelled()) {return;}

		int verbosity = debug.is_debugging()? dlg.get_verbosity() : 0;
		switch (dlg.get_searchType()) {
			default:
			case dlgAutoAlignment.SEARCH_GLOBAL:
				prj.affineRegistration(
						reference, get_selected_image(),
						0, dlg.get_multiScale(),
						dlg.get_translation(), dlg.get_rotation(),
						dlg.get_scale(), dlg.get_shear(),
						verbosity
				);
				break;
			case dlgAutoAlignment.SEARCH_HILL:
				prj.affineRegistration(
						reference, get_selected_image(),
						1, dlg.get_multiScale(),
						dlg.get_translation(), dlg.get_rotation(),
						dlg.get_scale(), dlg.get_shear(),
						verbosity
				);
				break;
		}

		if (dlg.get_dryRun()) {
			get_prj().set_imageAlignment(get_selected_image(), iaTmp, false);
		}

		update_gui();
	}

	private void set_scale(double factor) {
		debug.put("arg("+factor+") entered");
		if (get_selected_image() == 0) {
			IJ.showMessage("Base image cannot be scaled.");
			return;
		}
		prj.get_imageAlignment(get_selected_image()).set_scale(factor * prj.get_imageAlignment(get_selected_image()).xscale());
//		prj.get_imageAlignment(get_selected_image()).apply_scale(factor - 1, factor - 1);
		update_gui();
	}

	private void set_X0Y0RotScale() {
		debug.put("entered");
	
		if (get_selected_image() == 0) {
			IJ.showMessage("Base image cannot be rotated.");
			return;
		}
	
		String X0String = TFX0.getText();
		double x0 = Double.parseDouble(X0String.replaceAll(",","."));
	
		String Y0String = TFY0.getText();
		double y0 = Double.parseDouble(Y0String.replaceAll(",","."));
	
		String rotString = TFRot.getText();
		double rot = Double.parseDouble(rotString.replaceAll(",","."));
		
		String scString = TFScale.getText();
		double sc = Double.parseDouble(scString.replaceAll(",","."));

		if (sc < 0.1) {
			JOptionPane.showMessageDialog(gd,
					"Scale has to be greater than 0.1",
					"Bad input",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		prj.get_imageAlignment(get_selected_image()).set_x0y0(x0,y0);
		prj.get_imageAlignment(get_selected_image()).set_scale(sc);
		microscopyImage mi = prj.get_image(get_selected_image());
		prj.get_imageAlignment(get_selected_image()).set_rotAngle(rot);
		update_gui();
	}

	private String get_active_card_name(Container c) {
		for (Component comp : c.getComponents()) {
			if (comp.isVisible()) {
				return comp.getName();
			}
		}
		return null;
	}

	static public String classID() {return "correlia_ui";}
	static public String author() {return "Matthias Schmidt";}
	static public String version() {return "June 09 2017";}
	
	// remove listeners from canvas
	void removeListenersFromCanvas() {
		debug.put("entered");
		
		for (MouseListener l : opCanvas.getMouseListeners()) {
			opCanvas.removeMouseListener(l);
		}
		for (MouseMotionListener l : opCanvas.getMouseMotionListeners()) {
			opCanvas.removeMouseMotionListener(l);
		}
		for (WindowListener l : output.getWindow().getWindowListeners()) {
			output.getWindow().removeWindowListener(l);
		}
	}

	// mouse events
	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}
	public void mouseDragged(MouseEvent e) {
		debug.putMicroDebug("entered");
	
		// get coordinate in image (respects zoom)
		int dxpix = (int) (opCanvas.offScreenXD(e.getX()) - mouseClickedX);
		int dypix = (int) (opCanvas.offScreenYD(e.getY()) - mouseClickedY);
	
		mouseClickedX = opCanvas.offScreenXD(e.getX());
		mouseClickedY = opCanvas.offScreenXD(e.getY());
	
		if (EF_set_coordinates)	{  // true if "space" is pressed
			microscopyImage img = prj.get_image(get_selected_image());
			imageAlignment imgCoord = prj.get_imageAlignment(get_selected_image());
			
			// calculate shift in order to click in the middle of the image and not at the top left corner
			double dx = dxpix * output.getCalibration().pixelWidth;
			double dy = dypix * output.getCalibration().pixelHeight;
			
			// calculate the coordinates
			double x = imgCoord.x0() + dx;
			double y = imgCoord.y0() + dy;

			if (get_selected_image() == 0) {return;} // base image coordinates can't be changes
			imgCoord.set_x0y0(x,y);
			update_gui();
		}
	}
	public void mouseExited(MouseEvent e) {}
	public void mouseClicked(MouseEvent e) {
		debug.putMicroDebug("entered");

		// get coordinate in image (respects zoom)
		mouseClickedX = opCanvas.offScreenXD(e.getX());
		mouseClickedY = opCanvas.offScreenYD(e.getY());

		int img = showFeaturePoints;
		if (img < 0) return;

		if (e.isControlDown()) {
			double x = mouseClickedX;
			double y = mouseClickedY;

			if (output.getRoi() != null) {
				Roi r = output.getRoi();
				if (r.isArea()) {
					x = r.getXBase() + r.getFloatWidth() / 2;
					y = r.getYBase() + r.getFloatHeight() / 2;
				}
				output.deleteRoi();
			}

			x *=  output.getCalibration().pixelWidth;
			y *=  output.getCalibration().pixelHeight;

			if (EF_set_coordinates) {
//			if (e.getClickCount() == 2) {
//			if (e.getButton() != MouseEvent.BUTTON1) {
				img = get_selected_image();
				Point.Double p = new Point.Double(x, y);
				p = prj.get_imageAlignment(img).fromBase(p);
				x = p.getX();
				y = p.getY();
			}
			prj.get_image(img).add_feature_point(x, y);	// add this point to the list of features in base image which can be used for correlative microscopy
		} else if (e.isShiftDown()) {
			img = (EF_set_coordinates)?get_selected_image():showFeaturePoints;
//			int img = (e.getClickCount() == 2)?get_selected_image():0;
//			int img = (e.getButton() == MouseEvent.BUTTON1)?0:get_selected_image();
			prj.get_image(img).remove_feature_point(prj.get_image(img).number_of_feature_points()-1);
		}
		update_overlay();
	}
	public void mouseEntered(MouseEvent e) {}
	public void mouseMoved(MouseEvent e) {}

	// key events
	public void keyPressed(KeyEvent e) {
		debug.putMicroDebug("entered");
		if (e.getKeyCode() == e.VK_SPACE) {
			EF_set_coordinates = true;
			return;
		}	// "space" allows to set coordinates
	}
	public void keyReleased(KeyEvent e) {
		debug.putMicroDebug("entered");
		EF_set_coordinates = false;
//		EF_define_and_add_point = false;
//		EF_delete_last_feature = false;
	}
	public void keyTyped(KeyEvent e) {}

	// event flags
	Boolean EF_set_coordinates;
	Boolean EF_define_and_add_point;
	Boolean EF_delete_last_feature;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// flags and monitoring
	long elapsedTime;
	
	// calculation, multi-threading
	ExecutorService threadPool;
	int currentlyRunningThreads;
	Boolean stopThreads;
	
	// data
	correlia prj;	// correlia project data
	
	// output and overlay
	correliaScreen output;	// this is the ImagePlus we're working on
	ImageProcessor opProc;
	ImageCanvas opCanvas;
// 	ImageWindow opWin;
	Overlay outputOverlay;	// use ImageJ overlay function to indicate feature points etc.

	double mouseClickedX, mouseClickedY;	// reference points defined by mouse click
	
	// dialogue
	GenericDialog gd;

	// buttons
	Button Btn_exit;

	// image and slice selector buttons
	JButton Btn_stack_slice_down;
	JButton Btn_stack_slice_up;
	JButton Btn_activate_images;
	
	// Buttons colours
	JButton Btn_red_brighten;
	JButton Btn_red_faint;
	JButton Btn_green_brighten;
	JButton Btn_green_faint;
	JButton Btn_blue_brighten;
	JButton Btn_blue_faint;
	JButton Btn_brighten;
	JButton Btn_faint;
	JButton Btn_colour_string;

	// Buttons move and scale
	JButton Btn_move_right;
	JButton Btn_move_left;
	JButton Btn_move_up;
	JButton Btn_move_down;
	JButton Btn_rot_right;
	JButton Btn_rot_left;
	JButton Btn_scale_down;
	JButton Btn_scale_up;
	JButton Btn_set_X0Y0RotScale;

//	text labels
	JLabel LblSliceLabel;
	
	// ComboBoxes
	JComboBox binningChoice;	// binning
	
	// Textfields
	JTextField TFcolourString;
	JTextField TFX0;
	JTextField TFY0;	
	JTextField TFRot;
	JTextField TFScale;
	
	// Dialogues
	dlgCheckboxes DLG_copy_coordinates_chkBoxes; // copy coordinates from one image to others dialogue
	dlgEditMicroscopyImageProperties DLG_editMicroscopyImageProperties;	// show and edit the properties of this image
}