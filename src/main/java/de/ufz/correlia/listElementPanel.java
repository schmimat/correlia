package de.ufz.correlia;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import static javax.swing.SwingUtilities.isLeftMouseButton;

public abstract class listElementPanel extends JPanel {
	protected static final int PANEL_HEIGHT = 30;
	protected static final int DEFAULT_MARGIN = 5;
	protected static final Dimension MOVE_DIM = new Dimension(15, PANEL_HEIGHT);
	protected static final Dimension STATE_DIM = new Dimension(30, PANEL_HEIGHT);
	protected static final Color COLOR_ACTIVE = Color.BLACK;
	protected static final Color COLOR_INACTIVE = Color.GRAY;

	protected state state;
	protected int number;
	protected correlia_ui ui;
	protected String title;
	protected boolean selected;
	protected JSpinner spMove;
	protected JButton btnState;
	protected JLabel lbTitle;
	protected JButton btnAdd;

	protected JPanel upperPan;

	public listElementPanel(correlia_ui cUI, int no, boolean sel, String[] states) {
		this.ui = cUI;
		this.number = no;
		this.selected = sel;
		this.state = new state(states);

		spMove = new JSpinner();
		spMove.setModel(new SpinnerNumberModel(0, -1, 1, 1));
		JLabel spacer = new JLabel("");
		spacer.setPreferredSize(new Dimension(0, PANEL_HEIGHT));
		spMove.setEditor(spacer);
		spMove.setBorder(null);
		spMove.setMinimumSize(MOVE_DIM);
		spMove.setPreferredSize(MOVE_DIM);
		spMove.setMaximumSize(MOVE_DIM);

		btnState = new JButton();
		btnState.setPreferredSize(STATE_DIM);
		btnState.setBorder(null);
		btnState.setOpaque(false);
		btnState.setContentAreaFilled(false);
		btnState.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (isLeftMouseButton(e)) {
					state.next();
				} else {
					state.prev();
				}
				btnState.setIcon(get_state_icon());
				state_changed();
			}
		});

		lbTitle = new JLabel();

		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		upperPan = new JPanel();
		upperPan.setLayout(new BoxLayout(upperPan, BoxLayout.X_AXIS));
		upperPan.add(spMove);
		upperPan.add(Box.createRigidArea(new Dimension(DEFAULT_MARGIN, 0)));
		upperPan.add(btnState);
		upperPan.add(Box.createRigidArea(new Dimension(DEFAULT_MARGIN, 0)));
		upperPan.add(lbTitle);
		upperPan.add(Box.createRigidArea(new Dimension(DEFAULT_MARGIN, 0)));
		upperPan.add(Box.createHorizontalGlue());
		this.add(upperPan);

		this.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(!selected) {
					select();
				}
			}
		});

		if (selected) {
			this.setBorder(BorderFactory.createLoweredBevelBorder());
		} else {
			this.setBorder(BorderFactory.createRaisedBevelBorder());
		}
	}

	public void select() {
		selected = true;
		this.setBorder(BorderFactory.createLoweredBevelBorder());
	}

	public void deselect() {
		selected = false;
		this.setBorder(BorderFactory.createRaisedBevelBorder());
	}

	protected abstract ImageIcon get_state_icon();

	protected abstract void state_changed();
}