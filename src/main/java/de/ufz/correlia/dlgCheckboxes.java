/*
Dialogue checkboxes takes an array of strings and creates a dialogue with according
checkboxes. It provides a method returning an array of Boolean representing the
states of the checkbox.

author: Matthias Schmidt
date: March 20, 2015
*/
package de.ufz.correlia;

import java.util.*;
import java.util.Vector;
import java.awt.*;
import java.io.*;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.Box;
import javax.swing.BoxLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

// DEBUG, can be removed
 import ij.*;
////////////////////////

class dlgCheckboxes extends JDialog
{
	dlgCheckboxes( String dlgTitle, String[] checkbox_names )
	{
		chkBoxes = new Vector();
	
		setup_dialogue(checkbox_names);
		this.setTitle(dlgTitle);
		pack();
		
		m_wasCancelled = true;

	}

	dlgCheckboxes( Dialog owner, String dlgTitle, String[] checkbox_names )
	{
		super(owner,true);	// true -> modal dialog
		chkBoxes = new Vector();
	
		setup_dialogue(checkbox_names);
		this.setTitle(dlgTitle);
		pack();
		
		m_wasCancelled = true;
	}
	
	private void setup_dialogue(String[] checkbox_names)
	{
		Panel p = new Panel();
		p.setLayout(new BoxLayout(p,BoxLayout.Y_AXIS));
		
		Panel pckbx = new Panel();
		pckbx.setLayout(new BoxLayout(pckbx,BoxLayout.X_AXIS));
		
		Panel pckby = new Panel();
		pckby.setLayout(new BoxLayout(pckby,BoxLayout.Y_AXIS));
		
		// create and add checkboxes
		for ( int i=0; i<checkbox_names.length; i++ )
		{
			if ( (i % 5) == 0 )	// every 5 boxes start new column
			{
				pckbx.add(pckby);
				pckbx.add( Box.createHorizontalStrut( 10 ) );
				pckby = new Panel();
				pckby.setLayout(new BoxLayout(pckby,BoxLayout.Y_AXIS));
			}
			JCheckBox cb = new JCheckBox(checkbox_names[i]);
			chkBoxes.add(cb);
			pckby.add(cb);
			pckby.add( Box.createVerticalStrut( 10 ) );
		}
		pckbx.add(pckby);
		p.add(pckbx);
		p.add( Box.createVerticalStrut( 10 ) );
	
		// add and initialise buttons
		Panel pBtn = new Panel();
		pBtn.setLayout(new BoxLayout(pBtn,BoxLayout.X_AXIS));

		pBtn.add( Box.createHorizontalStrut( 30 ) );
		btnOK = new JButton("OK");
		btnOK.addActionListener(new ActionListener(){ public void actionPerformed(ActionEvent e){ userConfirm( true ); }});
		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener(){ public void actionPerformed(ActionEvent e){ userConfirm( false ); }});

		pBtn.add( Box.createHorizontalStrut( 20 ) );
		pBtn.add(btnOK);
		pBtn.add( Box.createHorizontalStrut( 15 ) );
		pBtn.add(btnCancel);
		pBtn.add( Box.createHorizontalStrut( 20 ) );
		
		p.add(pBtn);
		p.add( Box.createVerticalStrut( 10 ) );
		this.add(p);
	}
	
	public Boolean set_checkbox_state( int chkBoxNumber, Boolean selected )
	{
		if ( chkBoxNumber<0 || chkBoxNumber >= chkBoxes.size() ){ return false; }
		((JCheckBox) chkBoxes.elementAt(chkBoxNumber) ).setSelected( selected );
		return true;
	}
	
	public Boolean set_checkbox_enable( int chkBoxNumber, Boolean enabled )
	{
		if ( chkBoxNumber<0 || chkBoxNumber >= chkBoxes.size() ){ return false; }
		((JCheckBox) chkBoxes.elementAt(chkBoxNumber) ).setEnabled( enabled );
		return true;
	}
	
	public Boolean[] get_checkbox_states()
	{
		Boolean chkStat[] = new Boolean[chkBoxes.size()];
	
		for ( int i=0; i<chkBoxes.size(); i++ )
		{ chkStat[i] = ( (JCheckBox) chkBoxes.elementAt(i) ).isSelected(); }
	
		return chkStat;
	}
	
	public Boolean was_cancelled(){ return m_wasCancelled; }
	
	private void userConfirm( Boolean OKCancel ) // true = OK, false = Cancel
	{ m_wasCancelled = !OKCancel; this.setVisible(false); }
	
	static public String classID(){ return "dlgCheckboxes"; }
	static public String author(){ return "Matthias Schmidt"; }
	static public String version(){ return "July 21 2015"; }
	
	
	// widgets
	private Vector chkBoxes;
	private JButton btnOK;
	private JButton btnCancel;
	
	// flags
	private Boolean m_wasCancelled;
	
}

