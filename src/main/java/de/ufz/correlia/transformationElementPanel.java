package de.ufz.correlia;
import ij.IJ;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class transformationElementPanel extends listElementPanel {
	final ImageIcon icoDo = new ImageIcon(this.getClass().getResource("/icons/check.png"));
	final ImageIcon icoSkip = new ImageIcon(this.getClass().getResource("/icons/denied.png"));
	private static final String DO = "Do";
	private static final String SKIP = "Skip";

	private microscopyImageWarped miw;

	public transformationElementPanel(correlia_ui cUI, int transNo) {
		super(cUI, transNo,
				(transNo == cUI.get_selected_microscopyImageWarped().getSelectedTransformationNr()),
				new String[] {SKIP, DO}
				);

		miw = ui.get_selected_microscopyImageWarped();
		if (miw.getTransformation(number).active()) {
			state.set(DO);
		} else {
			state.set(SKIP);
		}

		spMove.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				debug.put("spMove");
				if (((SpinnerNumberModel) spMove.getModel()).getNumber().intValue() > 0) {
					if (number > 0) {
						miw.swapTransformations(number, number - 1);
						if (selected) {
							miw.setSelectedTransformation(number - 1);
						} else if (miw.getSelectedTransformationNr() == number - 1) {
							miw.setSelectedTransformation(number);
						}
						ui.update_gui();
					}
				} else {
					if (number < miw.transformationsSize() - 1) {
						miw.swapTransformations(number, number + 1);
						if (selected) {
							miw.setSelectedTransformation(number + 1);
						} else if (miw.getSelectedTransformationNr() == number + 1) {
							miw.setSelectedTransformation(number);
						}
						ui.update_gui();
					}
				}
			}
		});

		btnState.setIcon(get_state_icon());

		lbTitle.setText(miw.getTransformation(number).getName());
		lbTitle.setForeground(state.is(SKIP)? COLOR_INACTIVE : COLOR_ACTIVE);
	}

	public void select() {
		super.select();
		ui.get_transListElement(miw.getSelectedTransformationNr()).deselect();
		miw.setSelectedTransformation(number);
		ui.update_trans_props_pane();
	}

	protected ImageIcon get_state_icon() {
		ImageIcon ico;
		switch (state.get()) {
			default:
			case SKIP:
				ico = icoSkip;
				break;
			case DO:
				ico = icoDo;
				break;
		}
		return ico;
//		return icoImage.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH)
	}

	protected void state_changed() {
		switch (state.get()) {
			default:
			case SKIP:
				miw.deactivateTransformation(number);
				lbTitle.setForeground(COLOR_INACTIVE);
				break;
			case DO:
				miw.activateTransformation(number);
				lbTitle.setForeground(COLOR_ACTIVE);
				break;
		}
	}
}