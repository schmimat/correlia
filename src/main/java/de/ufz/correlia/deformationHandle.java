package de.ufz.correlia;

public class deformationHandle {
	private double x;
	private double y;
	private double dx;
	private double dy;
	private double weight;

	deformationHandle(double x, double y, double dx, double dy, double weight) {
		this.x = x;
		this.y = y;
		this.dx = dx;
		this.dy = dy;
		this.weight = weight;
	}

	deformationHandle(deformationHandle dH) {
		this.x = dH.getX();
		this.y = dH.getY();
		this.dx = dH.getDx();
		this.dy = dH.getDy();
		this.weight = dH.getWeight();
	}

	public double getX() {
		return x;
	}

	public void setX(double d) {
		x = d;
	}

	public double getY() {
		return y;
	}

	public void setY(double d) {
		y = d;
	}

	public double getDx() {
		return dx;
	}

	public void setDx(double d) {
		dx = d;
	}

	public double getDy() {
		return dy;
	}

	public void setDy(double d) {
		dy = d;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double d) {
		weight = d;
	}
}
