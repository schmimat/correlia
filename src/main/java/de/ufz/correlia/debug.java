package de.ufz.correlia;

import ij.*;

public class debug
{
	static private Boolean debugger = true;
	static private String dbgString = new String(
			"DEBUGGER PROTOCOL\n\n"+
			"operating_system "+System.getProperty("os.name").toLowerCase()+"\n"+
			"number_of_processors "+Runtime.getRuntime().availableProcessors()+"\n"+
			"64bit "+IJ.is64Bit()+"\n"+
			"ImageJ_version "+IJ.getVersion()+"\n"+
			"ImageJ_max_memory_Mb "+((int) (IJ.maxMemory()/1E6))+"\n"+
			"\n\n"
			);
	static private String microDbgString = new String("");	// keeps micro debugging information since last "put" was called 
	static private long calculationStartTime = System.nanoTime();
	
	static public void debugger_on(){ debugger=true;   } 
	static public void debugger_off(){ debugger=false; }
	static public Boolean is_debugging(){ return debugger; }
	
	
	static public void put( String s )
	{
		// display message in debugging mode
		if ( debugger )
		{
			IJ.log("DEBUG: "+
				Thread.currentThread().getStackTrace()[2].getClassName()+"."+
				Thread.currentThread().getStackTrace()[2].getMethodName()+" "+
				s); }
		// write to debugging protocol
		dbgString += "at "+(int) ((System.nanoTime()-calculationStartTime)/1E6)+"ms: "+
			"in "+Thread.currentThread().getStackTrace()[2].getClassName()+"."+
			Thread.currentThread().getStackTrace()[2].getMethodName()+" L"+
			Thread.currentThread().getStackTrace()[2].getLineNumber()+ ": "+
			s+"\n";
		microDbgString = new String("");
	}
	
	static public void putMicroDebug( String s )
	{
		microDbgString += "at "+(int) ((System.nanoTime()-calculationStartTime)/1E6)+"ms: "+
			"in "+Thread.currentThread().getStackTrace()[2].getClassName()+"."+
			Thread.currentThread().getStackTrace()[2].getMethodName()+" L"+
			Thread.currentThread().getStackTrace()[2].getLineNumber()+ ": "+s+"\n";
	}
	
	static public String getProtocol()
	{ 
		String s = new String(dbgString);
		
		s += "\nMICRO DEBUG\n";
		s += microDbgString;
		s += "\n-----------\n\ncurrently_used_memory_Mb "+((int) (IJ.currentMemory()/1E6))+"\n";
		
		
		return s;
	}
	
	
}