/*
coordinate system transformation: offset shift and rotation


| x' |  =  |  cos(rotAng)   sin(rotAng) | * | x-x0 |
| y' |     | -sin(rotAng)   cos(rotAng) |   | y-y0 |


The class was developped at the Helmholtz Centre for Environmental Research Leipzig
(UFZ) within the framework of ProVIS - Centre for Chemical Microscopy.

author : Matthias Schmidt
*/
package de.ufz.correlia;

import org.w3c.dom.Element;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.util.ArrayList;

class imageAlignment {
	protected double m_x0, m_y0;
	protected double m_rotAngle;
	protected double m_xshear, m_yshear;
	protected double m_xscale, m_yscale;
	protected double m_xCenter, m_yCenter;
	protected AffineTransform m_affine;

	imageAlignment(xmlHandler xml, Element root) {
		Element tmp = xml.getElementByName(root, "translation");
		m_x0 = xml.getDoubleAttribute(tmp, "x");
		m_y0 = xml.getDoubleAttribute(tmp, "y");
		tmp = xml.getElementByName(root, "shear");
		m_xshear = xml.getDoubleAttribute(tmp, "x");
		m_yshear = xml.getDoubleAttribute(tmp, "y");
		tmp = xml.getElementByName(root, "scale");
		m_xscale = xml.getDoubleAttribute(tmp, "x");
		if (m_xscale == 0) {
			m_xscale = 1;
		}
		m_yscale = xml.getDoubleAttribute(tmp, "y");
		if (m_yscale == 0) {
			m_yscale = 1;
		}

		m_rotAngle = xml.getDoublebyElementName(root, "rotation");

		tmp = xml.getElementByAttribute(
				(Element) root.getParentNode(),
				"microscopyImage",
				"pos", xml.getIntAttribute(root, "pos")
		);
		m_xCenter = xml.getDoubleAttributeByElementName(tmp, "size", "width") / 2;
		m_yCenter = xml.getDoubleAttributeByElementName(tmp, "size",  "height") / 2;

		refreshAffine();
	}

	imageAlignment() {
		this(0, 0);
	}

	imageAlignment(double x0_, double y0_) {
		this(x0_, y0_, 0, 0, 0, 1);
	}

	imageAlignment(double x0_, double y0_, double rotAng_, double rotCenterX_, double rotCenterY_) {
		this(x0_, y0_, rotAng_, rotCenterX_, rotCenterY_, 1);
	}

	imageAlignment(double x0_, double y0_, double rotAng_, double rotCenterX_, double rotCenterY_, double scale_) {
		this(x0_, y0_, rotAng_, rotCenterX_, rotCenterY_, scale_, 0, 0);
	}

	imageAlignment(double x0_, double y0_, double rotAng_, double rotCenterX_, double rotCenterY_, double scale_, double xshear_, double yshear_) {
		m_x0 = x0_;
		m_y0 = y0_;
		m_rotAngle = rotAng_;
		m_xscale = scale_;
		m_yscale = scale_;
		m_xshear = xshear_;
		m_yshear = yshear_;
		m_xCenter = rotCenterX_;
		m_yCenter = rotCenterY_;
		refreshAffine();
	}

	imageAlignment(imageAlignment tr) {
		m_x0 = tr.x0();
		m_y0 = tr.y0();
		m_rotAngle = tr.rotAngle();
		m_xscale = tr.xscale();
		m_yscale = tr.yscale();
		m_xshear = tr.xshear();
		m_yshear = tr.yshear();
		m_xCenter = tr.xcenter();
		m_yCenter = tr.ycenter();
		m_affine = (AffineTransform) tr.get_affine().clone();
	}

	imageAlignment(AffineTransform affineMatrix) {
		m_affine = affineMatrix;
	}

	public void refreshAffine() {
		m_affine = new AffineTransform();
		m_affine.translate(m_x0, m_y0);
		m_affine.translate(m_xCenter, m_yCenter);
		m_affine.scale(m_xscale, m_yscale);
		m_affine.rotate(Math.toRadians(m_rotAngle));
		m_affine.shear(m_xshear, m_yshear);
		m_affine.translate(-m_xCenter, -m_yCenter);

	}

	public double xprime(double x, double y) {
		Point2D.Double p = new Point2D.Double(x, y);
		try {
			return m_affine.inverseTransform(p, null).getX();
		} catch (NoninvertibleTransformException e) {
			debug.put("NoninvertibleTransformException");
			return 0;
		}
//		return Math.cos(Math.toRadians(rotAngle())) * (x - x0()) + xshear() * Math.sin(Math.toRadians(rotAngle())) * (y - y0());
	}

	public double yprime(double x, double y) {
		Point2D.Double p = new Point2D.Double(x, y);
		try {
			return m_affine.inverseTransform(p, null).getY();
		} catch (NoninvertibleTransformException e) {
			debug.put("NoninvertibleTransformException");
			return 0;
		}
//		return -Math.sin(Math.toRadians(rotAngle())) * (x - x0()) * yshear() + Math.cos(Math.toRadians(rotAngle())) * (y - y0());
	}

	public Point.Double fromBase(Point.Double p) {
		Point.Double pPrime = new Point.Double();
		try {
			m_affine.inverseTransform(p, pPrime);
			return pPrime;
		} catch (NoninvertibleTransformException e) {
			debug.put("NoninvertibleTransformException");
			return null;
		}
	}

	/**
	 * Transform coordinates from target to source
	 * @param xprime x-coord in target
	 * @param yprime y-coord in target
	 * @return x-coord in source
	 */
	public double x(double xprime, double yprime) {
		Point2D.Double p = new Point2D.Double(xprime, yprime);
		return m_affine.transform(p, null).getX();
//		return (Math.cos(Math.toRadians(rotAngle())) * xprime - Math.sin(Math.toRadians(rotAngle())) * yprime) + x0();
	}

	public double y(double xprime, double yprime) {
		Point2D.Double p = new Point2D.Double(xprime, yprime);
		return m_affine.transform(p, null).getY();
//		return (Math.sin(Math.toRadians(rotAngle())) * xprime + Math.cos(Math.toRadians(rotAngle())) * yprime) + y0();
	}

	public Point.Double toBase(Point.Double pPrime) {
		Point.Double p = new Point.Double();
		m_affine.transform(pPrime, p);
		return p;
	}


	public void set_x0y0(double x0_, double y0_) {
		m_x0 = x0_;
		m_y0 = y0_;
		refreshAffine();
	}

	public double x0() {
		return m_x0;
	}

	public double y0() {
		return m_y0;
	}

	public void set_rotAngle(double deg) {
		m_rotAngle = deg;
		refreshAffine();
	}

	public double rotAngle() {
		return m_rotAngle;
	}    // degrees

	public void set_center(double xcenter, double ycenter) {
		m_xCenter = xcenter;
		m_yCenter = ycenter;
	}

	public double xcenter() {
		return m_xCenter;
	}

	public double ycenter() {
		return m_yCenter;
	}

	public void set_xyshear(double sh) {
		set_xyshear(sh, sh);
	}

	public void set_xyshear(double xsh, double ysh) {
//		m_affine.shear(xsh - m_xshear, ysh - m_yshear);
		m_xshear = xsh;
		m_yshear = ysh;
		refreshAffine();
	}

	public double xshear() {
		return m_xshear;
	}

	public double yshear() {
		return m_yshear;
	}

	public void set_scale(double s) {
		set_scale(s, s);
	}

	public void set_scale(double sx, double sy) {
//		m_affine.scale(1 + sx - m_xscale, 1 + sy - m_yscale);
		m_xscale = sx;
		m_yscale = sy;
		refreshAffine();
	}

	public void set_xscale(double s) {
		set_scale(s, yscale());
	}
	public void set_yscale(double s) {
		set_scale(xscale(), s);
	}

	public double xscale() {
		return m_xscale;
	}

	public double yscale() {
		return m_yscale;
	}

	public AffineTransform get_affine() {
		return m_affine;
	}

	public Element buildXML(xmlHandler xml) {
		Element root = xml.createElement("imageAlignment");
		ArrayList<String> params;

		params = new ArrayList<>();
		params.add("x");
		params.add(Double.toString(m_x0));
		params.add("y");
		params.add(Double.toString(m_y0));
		xml.addTextElementWithAttributes(root, "translation", "", params);

		xml.addTextElement(root,"rotation", m_rotAngle);

		params = new ArrayList<>();
		params.add("x");
		params.add(Double.toString(m_xshear));
		params.add("y");
		params.add(Double.toString(m_yshear));
		xml.addTextElementWithAttributes(root, "shear", "", params);

		params = new ArrayList<>();
		params.add("x");
		params.add(Double.toString(m_xscale));
		params.add("y");
		params.add(Double.toString(m_yscale));
		xml.addTextElementWithAttributes(root, "scale", "", params);
		return root;
	}

	static public String classID() {
		return "imageAlignment";
	}

	static public String author() {
		return "Matthias Schmidt";
	}

	static public String version() {
		return "December 1 2015";
	}

}