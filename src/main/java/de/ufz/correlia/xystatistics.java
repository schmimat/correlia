package de.ufz.correlia;
import java.util.ArrayList;

public class xystatistics
{
	static final int X = 1;
	static final int Y = 2;
	
	static final int X2 = 3;
	static final int Y2 = 4;
	static final int XY = 5;
	
	
	public xystatistics( ArrayList<Double> set1, ArrayList<Double> set2 )
	{
		debug.put("entered");
	
		m_x = set1;
		m_y = set2;
		
		// ensure these lists exist
		xdevmean = new ArrayList<Double>();
		ydevmean = new ArrayList<Double>();
		
		mean_calculated = false;
	}
	
	public double x_mean()
	{
		if ( !mean_calculated ){ calc_mean(); }
		return m_xmean;
	}
	
	public double y_mean()
	{
		if ( !mean_calculated ){ calc_mean(); }
		return m_ymean;
	}
	
	public double calc_pearsonCorrelationCoefficiant()
	{
		debug.put("entered");
		// make sure xmean and ymean are calculated
		if ( !mean_calculated ){ calc_mean(); }
	
		// calculate lists of (x_i - xmean) and (y_i - ymean) in case they don't exist
		calc_devmean_lists();
	
		//       sum [ ( xi-xmean) * (yi-ymean) ]
		// r = --------------------------------------------------------------------
		//       sum [ sqrt( sum( (xi-xmean)^2 ) ) * sqrt( sum( (xi-xmean)^2 ) ) ]
		
		return sumlist( XY ) / ( Math.sqrt( sumlist( X2 ) ) * Math.sqrt( sumlist( Y2 ) ) );
	}
	
	
	
	
	public void calc_mean()
	{
		debug.put("entered");
		
		m_xmean = calc_mean( X, 0, m_x.size()-1 ); 
		m_ymean = calc_mean( Y, 0, m_y.size()-1 );
				
		mean_calculated = true;
	}
	
	// called by calc_mean, recursive function
	private double calc_mean(int xy, int idx_start, int idx_end)	// [sum_i( x_i )]/N , N is size of the list
	{
		debug.putMicroDebug("entered");
	
		int length = idx_end+1 - idx_start;
		
		if ( length > 16 )
		{ debug.putMicroDebug("final calculation"); return ( calc_mean( xy, idx_start, idx_end-length/2  ) + calc_mean( xy, idx_end-length/2+1, idx_end ) ) / 2; }
		else
		{
			double sum = 0.0;
			
			for ( int i=idx_start; i<idx_end; i++ )
			{ 
				if ( xy == X ) { sum += ((double) m_x.get(i)); }	// decide if mean should be calculated for X or Y data set
				if ( xy == Y ) { sum += ((double) m_y.get(i)); }
			}
			
			return sum /= ( idx_end-idx_start );
		}
	}

	private void calc_devmean_lists()	// calculate lists of (x_i - xmean) and (y_i - ymean) in case they don't exist
	{
		debug.put("entered");
	
		if ( xdevmean.size() != m_x.size() || ydevmean.size() != m_y.size() )
		{
			debug.putMicroDebug("add x-values");
			for ( double xi : m_x )
			{ xdevmean.add( xi-m_xmean ); }

			debug.putMicroDebug("add y-values");
			for ( double yi : m_y )
			{ ydevmean.add( yi-m_ymean ); }
		}

		return;
	}
	
	private double sumlist( int list ) // list = { X2, Y2, XY }
	{ 
		debug.put("entered");
		return sumlist( list, 0, m_x.size()-1 ); }

	
	
	private double sumlist( int list, int idx_start, int idx_end )
	{
		debug.putMicroDebug("entered");
		int length = idx_end+1 - idx_start;
		
		double sum = 0.0;
		
		if ( length > 128 )
		{ return ( sumlist( list, idx_start, idx_end-length/2  ) + sumlist( list, idx_end-length/2+1, idx_end ) ) / 2; }
		else
		{
			for ( int i=idx_start; i<idx_end; i++ )
			{ 
				switch(list)
				{
					case X2:	// ( x_i-xmean)^2
						sum += ((double) xdevmean.get(i))*((double) xdevmean.get(i));
						break;
        
					case Y2:	// ( y_i-ymean)^2
						sum += ((double) ydevmean.get(i))*((double) ydevmean.get(i));
						break; 
						
					case XY:	// ( x_i-xmean) * ( y_i-ymean)
						sum += ((double) xdevmean.get(i))*((double) ydevmean.get(i));
						break; 
				}
			}
		}
		return sum;
	}

	static public String classID(){ return "xystatistics"; }
	static public String author(){ return "Matthias Schmidt"; }
	static public String version(){ return "March 27 2017"; }
	
//-------- Members ------------------//
	
	protected ArrayList<Double> m_x;	// first data set
	protected ArrayList<Double> m_y;	// second data set
	protected ArrayList<Double> xdevmean;	// deviation of elements in first data set from the average of first data set (x_i - xmean)
	protected ArrayList<Double> ydevmean;	// deviation of elements in second data set from the average of second data set (y_i - ymean)
	
	protected Boolean mean_calculated;
	
	protected double m_xmean;
	protected double m_ymean;



}