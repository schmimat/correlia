/*
 * Transformation type that applies nonlinear registration by aligning corresponding landmarks without regularisation
 * Author: Florens Rohde
 *
 * Part of:
 * Correlia plugin for ImageJ and FIJI
 */

package de.ufz.correlia;
import de.ufz.correlia.slider.RangeSlider;
import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import org.w3c.dom.Element;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.Hashtable;

public class transformationLandmarks extends transformation {
	public static final String NAME = "Landmarks";
	public static final double REF_SIZE_LIMIT = 1.2;
	public static final int SPLINEGRID_LOWEND = 0;
	public static final int SPLINEGRID_HIEND = 5;

	private double[][] cx;
	private double[][] cy;

	private int minSplineGrid;
	private int maxSplineGrid;

	private ArrayList<Integer[]> matchedFP = null;
	private RangeSlider rslSplineGrid;
	private JCheckBox cbUseDeformationField;

	private Instant start;

	transformationLandmarks(xmlHandler xml, Element root, correlia prj, transformation prevT, microscopyImageWarped src) {
		super(xml, root, prj, prevT, src);

		minSplineGrid = xml.getIntAttributeByElementName(root, "approximationGrid", "min");
		maxSplineGrid = xml.getIntAttributeByElementName(root, "approximationGrid", "max");

		refSizeLimit = REF_SIZE_LIMIT;

		Element edx = xml.getElementByName(root, "dx");
		if (edx != null) {
			cx = readCoefficientsFromXML(xml, edx);
		}

		Element edy = xml.getElementByName(root, "dy");
		if (edy != null) {
			cy = readCoefficientsFromXML(xml, edy);
		}

		if (edx != null && edy != null) {
			dx = MiscHelper.interpolateMatrixFromCoefficients(cx, src.getWidth(), src.getHeight());
			dy = MiscHelper.interpolateMatrixFromCoefficients(cy, src.getWidth(), src.getHeight());
		}
	}

	transformationLandmarks(correlia prj, String originalID, transformation prevTrans) {
		super(NAME, prj, originalID, prevTrans);
		minSplineGrid = 0;
		maxSplineGrid = 4;
		refSizeLimit = REF_SIZE_LIMIT;
	}

	transformationLandmarks(transformationLandmarks t) {
		super(t);

		minSplineGrid = t.get_minSplineGrid();
		maxSplineGrid = t.get_maxSplineGrid();

		refSizeLimit = t.refSizeLimit;

		if (t.matchedFP != null) {
			matchedFP = new ArrayList<>();
			for (Integer[] m : t.matchedFP) {
				Integer[] mCopy = {new Integer(m[0]), new Integer(m[1])};
				matchedFP.add(mCopy);
			}
		}
		if (t.cx != null) {
			cx = new double[t.cx.length][t.cx[0].length];
			for (int i = 0; i < cx.length; i++) {
				cx[i] = Arrays.copyOf(t.cx[i], t.cx[i].length);
			}
		}
		if (t.cy != null) {
			cy = new double[t.cy.length][t.cy[0].length];
			for (int i = 0; i < cy.length; i++) {
				cy[i] = Arrays.copyOf(t.cy[i], t.cy[i].length);
			}
		}
	}

	@Override
	public Element buildXML(xmlHandler xml, boolean recipe) {
		ArrayList<String> params;
		Element root = super.buildXML(xml, recipe);

		params = new ArrayList<>();
		params.add("min");
		params.add(Integer.toString(minSplineGrid));
		params.add("max");
		params.add(Integer.toString(maxSplineGrid));
		xml.addTextElementWithAttributes(root, "approximationGrid", "", params);

		if (!recipe) {
			writeCoefficientsToXML(xml, root, "dx", cx);
			writeCoefficientsToXML(xml, root, "dy", cy);
		}

		return root;
	}

	public boolean calc() {
		debug.put(" entered ("+getName()+")");
		if (matchedFP == null) {
			IJ.showMessage("Please match landmarks first!");
			return false;
		}
		microscopyImage[] mi = {srcWork, refWork};

		double[][] fpsS = MiscHelper.getFeaturePointsInPixelUnits(mi[0], offset);
		double[][] fpsR = MiscHelper.getFeaturePointsInPixelUnits(mi[1], null);

		if (debug.is_debugging()) {
//			mi[0].show(microscopyImage.MODE_OVL);
//			mi[1].show(microscopyImage.MODE_OVL);
		}

		deformations.clear();
		if (useDeformationField) {
			deformations.addAll(previousTransformation.deformations);
		}

		for (Integer[] m : matchedFP) {
			if (fpsR[m[1]][0] < 0 || fpsR[m[1]][1] < 0) continue;
			deformations.add(new deformationHandle(
					fpsR[m[1]][0],
					fpsR[m[1]][1],
					fpsS[m[0]][0] - fpsR[m[1]][0],
					fpsS[m[0]][1] - fpsR[m[1]][1],
					1)
			);
		}

		int[] max = {refWork.getWidth(), refWork.getHeight()};
		start = Instant.now();
		double[][][] c = MiscHelper.getBSplineCoefficientsFromScatteredPoints(deformations, max, (int)Math.pow(2, minSplineGrid), (int)Math.pow(2, maxSplineGrid));
		cx = c[0];
		cy = c[1];
		debug.put("getBSplineCoefficientsFromScatteredPoints (2x): " + Duration.between(start, Instant.now()).toString());

		start = Instant.now();
		dx = MiscHelper.interpolateMatrixFromCoefficients(cx, refWork.getWidth(), refWork.getHeight());
		dy = MiscHelper.interpolateMatrixFromCoefficients(cy, refWork.getWidth(), refWork.getHeight());
		debug.put("interpolateMatrixFromCoefficients (2x): " + Duration.between(start, Instant.now()).toString());

		dx = MiscHelper.extractArea(dx, offset[0], offset[1], dxyReference.getWidth(), dxyReference.getHeight());
		dy = MiscHelper.extractArea(dy, offset[0], offset[1], dxyReference.getWidth(), dxyReference.getHeight());
		for (deformationHandle dH : deformations) {
			dH.setX(dH.getX() - offset[0]);
			dH.setY(dH.getY() - offset[1]);
		}

		if (debug.is_debugging()) {
			StringBuilder sb = new StringBuilder("fpSrc={");
			for (int i = 0; i < deformations.size(); i++) {
				deformationHandle dH = deformations.get(i);
				sb.append("{" + (dH.getX()+dH.getDx()) + "," + (dH.getY()+dH.getDy()) + "}");
				if (i < deformations.size() - 1) sb.append(",");
			}
			sb.append("};");
			IJ.log(sb.toString());
			sb = new StringBuilder("fpRef={");
			for (int i = 0; i < deformations.size(); i++) {
				deformationHandle dH = deformations.get(i);
				sb.append("{" + dH.getX() + "," + dH.getY() + "}");
				if (i < deformations.size() - 1) sb.append(",");
			}
			sb.append("};");
			IJ.log(sb.toString());

			double[][] cmp = new double[deformations.size()][6];
			for (int i = 0; i < cmp.length; i++) {
				cmp[i][0] = deformations.get(i).getX();
				cmp[i][1] = deformations.get(i).getY();
				cmp[i][2] = deformations.get(i).getDx();
				cmp[i][4] = deformations.get(i).getDy();
				if (cmp[i][0] >= 0 && cmp[i][1] >= 0) {
					cmp[i][3] = dx[(int) cmp[i][1]][(int) cmp[i][0]];
					cmp[i][5] = dy[(int) cmp[i][1]][(int) cmp[i][0]];
				} else {
					cmp[i][3] = 0;
					cmp[i][5] = 0;
				}
			}
			MiscHelper.printdoubleArray(cmp);

			ImageStack imsDeformation;
			imsDeformation = new ImageStack(dx[0].length, dx.length);
			imsDeformation.addSlice("Landmark deformations", MiscHelper.deformationArrowField(deformations, dx[0].length, dx.length, 1, 0));
			imsDeformation.addSlice("Interpolated deformations", MiscHelper.deformationArrowField(dx, dy, 1, 0));
			new ImagePlus("Deformation fields", imsDeformation).show();
		}

		return true;
	}

	public JPanel propPanel() {
		JPanel propPane = new JPanel();
		propPane.setLayout(new BoxLayout(propPane, BoxLayout.PAGE_AXIS));

		JPanel paneMatch = new JPanel();
		paneMatch.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				"Landmark matching", TitledBorder.LEFT, TitledBorder.TOP));
		paneMatch.setLayout(new GridLayout(0, 2, INNER_PADDING, INNER_PADDING));


		JButton btnEditFeatures = new JButton("Add & Edit");
		btnEditFeatures.addActionListener(e -> {
			editLandmarks();
		});
		paneMatch.add(btnEditFeatures);

		JButton btnMatch = new JButton("Match");
		btnMatch.addActionListener(e -> {
			ArrayList<Integer[]> res = matchLandmarks(matchedFP);
			if (res != null) {
				matchedFP = res;
				setChanged(true);
			}
		});
		paneMatch.add(btnMatch);

		// Pane with two columns for labels and textfields
		JPanel twoColPane = new JPanel();
		twoColPane.setLayout(new GridLayout(0, 2, INNER_PADDING, INNER_PADDING));

		cbUseDeformationField = new JCheckBox("Use prev. Def.");
		cbUseDeformationField.setSelected(useDeformationField);
		cbUseDeformationField.addChangeListener(
				e -> set_useDeformationField(cbUseDeformationField.isSelected())
		);
		twoColPane.add(cbUseDeformationField);
		twoColPane.add(Box.createHorizontalGlue());

		twoColPane.add(new JLabel("Deformation scale"));
		rslSplineGrid = new RangeSlider(SPLINEGRID_LOWEND, SPLINEGRID_HIEND, minSplineGrid, maxSplineGrid);
		rslSplineGrid.setMajorTickSpacing(1);
		rslSplineGrid.setPaintTicks(true);
		rslSplineGrid.setPaintLabels(true);
		rslSplineGrid.setSnapToTicks(true);
		Dictionary dict = new Hashtable();
		for (int i = SPLINEGRID_LOWEND; i <= SPLINEGRID_HIEND; i++) {
			String s = "";
			if (i == SPLINEGRID_LOWEND) s = "Coarse";
			if (i == SPLINEGRID_HIEND) s = "Fine";
			dict.put(i, new JLabel(s));
		}
		rslSplineGrid.setLabelTable(dict);
		rslSplineGrid.addChangeListener(
				e -> {
					set_minSplineGrid(rslSplineGrid.getValue());
					set_maxSplineGrid(rslSplineGrid.getUpperValue());
				}
		);
		twoColPane.add(rslSplineGrid);

		propPane.add(paneMatch);
		propPane.add(twoColPane);

		return propPane;
	}

	public void set_matches(ArrayList<Integer[]> matches) {
		matchedFP = matches;
	}

	public boolean get_useDeformationField() {
		return useDeformationField;
	}

	public void set_useDeformationField(boolean b) {
		if (b != useDeformationField) {
			createWorkingImages(true);
			useDeformationField = b;
			setChanged(true);
		}
	}

	public int get_minSplineGrid() {
		return minSplineGrid;
	}

	public void set_minSplineGrid(int i) {
		minSplineGrid = i;
		setChanged(true);
	}

	public int get_maxSplineGrid() {
		return maxSplineGrid;
	}

	public void set_maxSplineGrid(int i) {
		maxSplineGrid = i;
		setChanged(true);
	}
}
