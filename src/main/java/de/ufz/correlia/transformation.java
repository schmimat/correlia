/*
 * Abstract class for a transformation type in the cascade of a microscopyImageWarped
 * Author: Florens Rohde
 *
 * Part of:
 * Correlia plugin for ImageJ and FIJI
 */
package de.ufz.correlia;

import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.PointRoi;
import ij.gui.Roi;
import ij.plugin.LutLoader;
import ij.process.FloatPolygon;
import ij.process.ImageProcessor;
import ij.process.LUT;
import org.w3c.dom.Element;

import javax.swing.*;
import java.awt.*;
import java.awt.image.IndexColorModel;
import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;

public abstract class transformation {
	protected IndexColorModel blueToRedColorModel;
	protected static final Integer TEXTFIELD_WIDTH = 40;
	protected static final Integer TEXTAREA_HEIGHT = 6;
	protected static final Integer INNER_PADDING = 5;
	protected static final Integer OUTER_PADDING = 20;

	private String name;
	private boolean active = true;
	private boolean changedSinceLastUpdate = true;
	private boolean resultChanged = true;

	protected transformation previousTransformation = null;
	protected boolean useDeformationField;
	protected ArrayList<deformationHandle> deformations = new ArrayList<>();
	protected double dx[][] = null;
	protected double dy[][] = null;
	protected microscopyImage dxyReference = null;
	protected microscopyImage result = null;

	protected ArrayList<String> srcID = new ArrayList<>();
	protected ArrayList<String> refID = new ArrayList<>();
	protected microscopyImage srcWork = null;
	protected microscopyImage refWork = null;
	protected double refSizeLimit = 1;
	protected int[] offset;
	protected correlia prj;

	protected double[] smooth = new double[2];
	protected double[][] histRange = new double[2][2];

	private ImagePlus deformationPreview;

	private Instant start;

	/**
	 * Construct from XML element
	 * @param xml xmlHandler instance
	 * @param root xml root element
	 * @param project correlia project
	 * @param previousTransformation previous transformation in the cascade
	 * @param src image that the transformation belongs to
	 */
	transformation(xmlHandler xml, Element root, correlia project, transformation previousTransformation, microscopyImageWarped src) {
		this.name = root.getAttribute("type");
		prj = project;
		this.previousTransformation = previousTransformation;

		this.active = Boolean.parseBoolean(root.getAttribute("active"));
		this.useDeformationField = Boolean.parseBoolean(root.getAttribute("useDeformationField"));

		int pos = 0;
		Element el;
		do {
			el = xml.getElementByAttribute(root, "srcID", "pos", pos);
			if (el == null) break;
			srcID.add(el.getTextContent());
			pos++;
		} while (pos > 0);
		pos = 0;
		do {
			el = xml.getElementByAttribute(root, "refID", "pos", pos);
			if (el == null) break;
			refID.add(el.getTextContent());
			pos++;
		} while (pos > 0);

		boolean recipe = false;
		if (srcID.size() == 0 || refID.size() == 0) {
			recipe = true;
			srcID.add(src.get_ID());

			String preSel = prj.get_image(0).get_ID();
			String sel = MiscHelper.selectImageIDFromProject(prj, null,
					"Select the reference image for transformation " + getName(), preSel, false);
			if (sel == null) sel = preSel;
			refID.add(sel);
		}

		smooth[0] = Double.parseDouble(xml.getElementByAttribute(root, "smooth", "id", recipe?"SRC0":srcID.get(0)).getTextContent());
		smooth[1] = Double.parseDouble(xml.getElementByAttribute(root, "smooth", "id", recipe?"REF0":refID.get(0)).getTextContent());

		el = xml.getElementByAttribute(root, "histRange", "id", recipe?"SRC0":srcID.get(0));
		if (el != null) {
			histRange[0][0] = xml.getDoubleAttribute(el, "min");
			histRange[0][1] = xml.getDoubleAttribute(el, "max");
		} else {
			histRange[0] = null;
		}
		el = xml.getElementByAttribute(root, "histRange", "id", recipe?"REF0":refID.get(0));
		if (el != null) {
			histRange[1][0] = xml.getDoubleAttribute(el, "min");
			histRange[1][1] = xml.getDoubleAttribute(el, "max");
		} else {
			histRange[1] = null;
		}

		if (!recipe) {
			setChanged(false);
			setResultChanged(false);
		}

		try {
			InputStream is = getClass().getResourceAsStream("/diff.lut");
			blueToRedColorModel = LutLoader.open(is);
		} catch(IOException e) {
			IJ.log("IOException" + e);
			blueToRedColorModel = null;
		}
	}

	/**
	 * Constructor
	 * @param name displayed name of this transformation
	 * @param project correlia project
	 * @param originalID ID of the image this transformation belongs to
	 * @param previousTransformation previous transformation in the cascade
	 */
	transformation(String name, correlia project, String originalID, transformation previousTransformation) {
		this.name = name;
		prj = project;
		this.previousTransformation = previousTransformation;

		if (previousTransformation != null) {
			srcID.addAll(previousTransformation.srcID);
			refID.addAll(previousTransformation.refID);
			for (int i = 0; i < smooth.length; i++) {
				smooth[i] = previousTransformation.smooth[i];
			}
			for (int i = 0; i < histRange.length; i++) {
				if (previousTransformation.histRange[i] == null) {
					histRange[i] = null;
				} else {
					histRange[i] = Arrays.copyOf(previousTransformation.histRange[i], previousTransformation.histRange[i].length);
				}
			}
		} else {
			srcID.add(originalID);
			refID.add(prj.get_image(0).get_ID());
		}

		useDeformationField = true;
		updateDxyReference();

		for (int i = 0; i < smooth.length; i++) {
			smooth[i] = 0;
		}
		for (int i = 0; i < histRange.length; i++) {
			histRange[i] = null;
		}

		try {
			InputStream is = getClass().getResourceAsStream("/diff.lut");
			blueToRedColorModel = LutLoader.open(is);
		} catch(IOException e ) {
			IJ.log("IOException" + e);
			blueToRedColorModel = null;
		}
	}

	/**
	 * Copy constructor
	 * @param t orignal transformation
	 */
	transformation(transformation t) {
		this.name = t.getName();
		this.prj = t.prj;
		this.previousTransformation = t.previousTransformation;

		srcID.addAll(t.getSrcID());
		refID.addAll(t.getRefID());

		useDeformationField = t.useDeformationField;
		updateDxyReference();

		if (t.smooth == null) {
			smooth = null;
		} else {
			smooth = new double[t.smooth.length];
			smooth = Arrays.copyOf(t.smooth, t.smooth.length);
		}
		if (t.histRange == null) {
			histRange = null;
		} else {
			for (int i = 0; i < histRange.length; i++) {
				if (t.histRange[i] == null) {
					histRange[i] = null;
				} else {
					histRange[i] = Arrays.copyOf(t.histRange[i], t.histRange[i].length);
				}
			}
		}

		active = t.active();
		setChanged(t.changed());
		setResultChanged(t.resultChanged());

		if (t.dx != null) {
			dx = new double[t.dx.length][t.dx[0].length];
			for (int i = 0; i < dx.length; i++) {
				dx[i] = Arrays.copyOf(t.dx[i], t.dx[i].length);
			}
		}
		if (t.dy != null) {
			dy = new double[t.dy.length][t.dy[0].length];
			for (int i = 0; i < dy.length; i++) {
				dy[i] = Arrays.copyOf(t.dy[i], t.dy[i].length);
			}
		}

		try {
			InputStream is = getClass().getResourceAsStream("/diff.lut");
			blueToRedColorModel = LutLoader.open(is);
		} catch(IOException e ) {
			IJ.log("IOException" + e);
			blueToRedColorModel = null;
		}
	}

	public Element buildXML(xmlHandler xml) {
		return buildXML(xml, false);
	}

	public Element buildXML(xmlHandler xml, boolean recipe) {
		ArrayList<String> params;

		Element root = xml.createElement("transformation");
		xml.addAttribute(root, "type", name);
		xml.addAttribute(root, "active", Boolean.toString(active));
		xml.addAttribute(root, "useDeformationField", Boolean.toString(useDeformationField));

		if (!recipe) {
			for (int i = 0; i < srcID.size(); i++) {
				params = new ArrayList<>();
				params.add("pos");
				params.add(Integer.toString(i));
				xml.addTextElementWithAttributes(root, "srcID", srcID.get(i), params);
			}
			for (int i = 0; i < refID.size(); i++) {
				params = new ArrayList<>();
				params.add("pos");
				params.add(Integer.toString(i));
				xml.addTextElementWithAttributes(root, "refID", refID.get(i), params);
			}
		}

		for (int i = 0; i < srcID.size(); i++) {
			params = new ArrayList<>();
			params.add("id");
			params.add(recipe?"SRC"+Integer.toString(i):srcID.get(i));
			params.add("type");
			params.add("gaussian");
			xml.addTextElementWithAttributes(root, "smooth", Double.toString(smooth[0]), params);
		}
		for (int i = 0; i < refID.size(); i++) {
			params = new ArrayList<>();
			params.add("id");
			params.add(recipe?"REF"+Integer.toString(i):refID.get(i));
			params.add("type");
			params.add("gaussian");
			xml.addTextElementWithAttributes(root, "smooth", Double.toString(smooth[1]), params);
		}

		for (int i = 0; i < srcID.size(); i++) {
			if (histRange[0] == null) break;
			params = new ArrayList<>();
			params.add("id");
			params.add(recipe?"SRC"+Integer.toString(i):srcID.get(i));
			params.add("min");
			params.add(Double.toString(histRange[0][0]));
			params.add("max");
			params.add(Double.toString(histRange[0][1]));
			xml.addTextElementWithAttributes(root, "histRange", "", params);
		}
		for (int i = 0; i < refID.size(); i++) {
			if (histRange[1] == null) break;
			params = new ArrayList<>();
			params.add("id");
			params.add(recipe?"REF"+Integer.toString(i):refID.get(i));
			params.add("min");
			params.add(Double.toString(histRange[1][0]));
			params.add("max");
			params.add(Double.toString(histRange[1][1]));
			xml.addTextElementWithAttributes(root, "histRange", "", params);
		}

		return root;
	}

	/**
	 * Add a XML element with the contents of a coefficient matrix
	 * @param xml xmlHandler instance
	 * @param root root xml element
	 * @param tag name of the coefficients xml element
	 * @param coeff matrix containing the data
	 */
	protected void writeCoefficientsToXML(xmlHandler xml, Element root, String tag, double[][] coeff) {
		ArrayList<String> params;
		StringBuilder sb = new StringBuilder();
		for (int v = 0; v < coeff.length; v++) {
			for (int u = 0; u < coeff[0].length; u++) {
				if (u + v != 0) sb.append(" ");
				sb.append(coeff[v][u]);
			}
		}
		params = new ArrayList<>();
		params.add("w");
		params.add(Integer.toString(coeff[0].length));
		params.add("h");
		params.add(Integer.toString(coeff.length));

		xml.addTextElementWithAttributes(root, tag, sb.toString(), params);
	}

	/**
	 * Parse a xml element containing coefficients of a matrix
	 * @param xml xmlHandler instance
	 * @param e xml element
	 * @return coefficients matrix
	 */
	protected double[][] readCoefficientsFromXML(xmlHandler xml, Element e) {
		int h = xml.getIntAttribute(e, "h");
		int w = xml.getIntAttribute(e, "w");
		double[] coeff = new double[w * h];
		String[] parts = e.getTextContent().split(" ");
		for (int i = 0; i < coeff.length; i++) {
			coeff[i] = Double.parseDouble(parts[i]);
		}
		return MiscHelper.doubleArray1DTo2D(coeff, w, h);
	}

	/**
	 * Check whether the previous transformation exists and if so,
	 * set the source image pointer for this transformation according to the useDeformation setting
	 */
	public void updateDxyReference() {
		if (previousTransformation != null) {
			if (previousTransformation.dx == null || previousTransformation.dy == null) {
				useDeformationField = false;
			}
			if (useDeformationField) {
				dxyReference = previousTransformation.dxyReference;
			} else {
				dxyReference = previousTransformation.getResult();
			}
		} else {
			useDeformationField = false;
			microscopyImageWarped miwRef = (microscopyImageWarped)prj.get_imageByID(srcID.get(0));
			dxyReference = new microscopyImage(miwRef);
			dxyReference.setStack(miwRef.get_rawStack());
			dxyReference.set_feature_points(MiscHelper.copyFeatures(miwRef.get_rawFeaturePoints()));
			dxyReference.setTitle("-1");
		}
	}

	protected void createWorkingImages(boolean preprocess) {
		createWorkingImages(0, 0, preprocess);
	}

	/**
	 * Create source and reference working images (each with a mask as second slice)
	 * and apply preprocessing (depending on the parameter)
	 * @param src source image number
	 * @param ref reference image number
	 * @param preprocess apply preprocessing?
	 */
	protected void createWorkingImages(int src, int ref, boolean preprocess) {
		debug.put(" entered");
		int srcPos = prj.get_imagePositionByID(srcID.get(src));
		int refPos = prj.get_imagePositionByID(refID.get(ref));

		int currentSlice;
		ImageStack newIms;
		ImageProcessor ipImg;
		ImageProcessor ipMask;

		if (dxyReference == null) { // Occurs when this transformation is loaded from xml
			updateDxyReference();
		}
		srcWork = new microscopyImage(dxyReference);
		newIms = new ImageStack(srcWork.getWidth(), srcWork.getHeight());
		currentSlice = srcWork.getCurrentSlice();
		ipImg = srcWork.getStack().getProcessor(currentSlice);
		ipImg = ipImg.convertToFloat();
		newIms.addSlice(srcWork.getStack().getSliceLabel(currentSlice), ipImg);
		ipMask = MiscHelper.createMask(ipImg);
		newIms.addSlice("Mask", ipMask);
		srcWork.setStack(newIms);

		refWork = new microscopyImage(prj.get_image(refPos));
		newIms = new ImageStack(refWork.getWidth(), refWork.getHeight());
		currentSlice = refWork.getCurrentSlice();
		ipImg = refWork.getStack().getProcessor(currentSlice);
		ipImg = ipImg.convertToFloat();
		newIms.addSlice(refWork.getStack().getSliceLabel(currentSlice), ipImg);
		ipMask = MiscHelper.createMask(ipImg);
		newIms.addSlice("Mask", ipMask);
		refWork.setStack(newIms);

		offset = MiscHelper.adaptImage(
				refWork,
				dxyReference,
				prj.get_imageAlignment(refPos),
				prj.get_imageAlignment(srcPos),
				refSizeLimit
		);

		if (preprocess) {
			dlgPreprocessImage.applyPreprocessing(srcWork, 1, smooth[0], histRange[0]);
			dlgPreprocessImage.applyPreprocessing(refWork, 1, smooth[1], histRange[1]);
		}

		if (previousTransformation != null) previousTransformation.setResultChanged(false);
	}

	/**
	 * Wrapper for guessInitialStepSize, that rounds the result to integer
	 * @param x x coordinate (in pixel units)
	 * @param y y coordinate (in pixel units)
	 * @return stepsize for these coordinates
	 */
	protected int[] guessInitialStepSizeInteger(double x, double y) {
		double[] stepSizeD = guessInitialStepSize(x, y);
		int[] stepSizeI = new int[2];
		for (int i = 0; i < stepSizeI.length; i++) {
			stepSizeI[i] = (int)Math.ceil(stepSizeD[i]);
		}
		return stepSizeI;
	}

	/**
	 * Guess stepsize either from previous transformations deformationo fields
	 * or by fraction of the image size
	 * @param x x coordinate (in pixel units)
	 * @param y y coordinate (in pixel units)
	 * @return stepsize for these coordinates
	 */
	protected double[] guessInitialStepSize(double x, double y) {
		double[] stepSize = new double[2];
		if (useDeformationField) {
			stepSize[0] = previousTransformation.dx[(int)y][(int)x]/8;
			stepSize[1] = previousTransformation.dy[(int)y][(int)x]/8;
			if (stepSize[0] == 0) {
				stepSize[0] = 0.005 * srcWork.getWidth();
			}
			if (stepSize[1] == 0) {
				stepSize[1] = 0.005 * srcWork.getHeight();
			}
		} else {
			stepSize[0] = 0.01 * srcWork.getWidth();
			stepSize[1] = 0.01 * srcWork.getHeight();
		}
		return stepSize;
	}

	public boolean update() {
		return update(false);
	}

	/**
	 * Check whether the transformation has to be (re)calculated and call calc() in this case
	 * @param forceCalculation force calc() calling
	 * @return success
	 */
	public boolean update(boolean forceCalculation) {
		debug.put(" entered");

		if (dxyReference == null) updateDxyReference();

		if (!forceCalculation && !changed()) {
			if (previousTransformation == null || !previousTransformation.resultChanged()) {
				debug.put(" quit (nothing changed)");
				return true;
			}
		}

		microscopyImage dxyPrev = dxyReference;
		updateDxyReference();

		if ((previousTransformation != null && dxyPrev != dxyReference) || (previousTransformation != null && previousTransformation.resultChanged())) {
			debug.put(" create working images (previous Transformation changed)");
			createWorkingImages(true);
		}
		if (srcWork == null || refWork == null) {
			debug.put(" create working images (not available)");
			createWorkingImages(true);
		}

		result = null;
		boolean ret = calc();
		if (ret) {
			setChanged(false);
			setResultChanged(true);
		}
		debug.put("return: "+ret);
		return ret;
	}

	/**
	 * Abstract method for transformation calculation, has to be implemented by the subclasses
	 * @return success
	 */
	public abstract boolean calc();

	/**
	 * Convert point from target coordinates to source coordinates
	 * @param p point in target coordinates
	 * @return point in source coordinates
	 */
	public Point.Double coordTransformationT2O(Point.Double p) {
		if (dx == null || dy == null) {
			return p;
		}
		return new Point.Double(p.getX() + MiscHelper.interpolateValueFromArray(dx, p.getX(), p.getY()),
				p.getY() + MiscHelper.interpolateValueFromArray(dy, p.getX(), p.getY()));
	}

	/**
	 * Convert point from target coordinates to source coordinates without interpolation (use next lower integer value)
	 * @param x x coordinate in target
	 * @param y y coordinate in target
	 * @return point in source coordinates
	 */
	private Point.Double coordTransformationT2O_coarse(int x, int y) {
		return new Point.Double(x + dx[y][x], y + dy[y][x]);
	}

	/**
	 * Convert point from source coordinates to target coordinates
	 * @param p point in source coordinates
	 * @return point in target coordinates
	 */
	public Point.Double coordTransformationO2T(Point.Double p) {
		if (dx == null || dy == null) {
			return p;
		}
		Point.Double pT = new Point.Double();
		double minDistance = Double.MAX_VALUE;
		double distance;
		for (int u = 0; u < dx[0].length; u++) {
			for (int v = 0; v < dx.length; v++) {
				distance = p.distance(coordTransformationT2O_coarse(u, v));
				if (distance < minDistance) {
					minDistance = distance;
					pT.setLocation(u, v);
				}
			}
		}
		return pT;
	}

	/**
	 * Abstract method for GUI panel for transformation properties, has to be implemented by subclasses
	 * @return properties panel
	 */
	public abstract JPanel propPanel();

	/**
	 * Get a source image of this transformation
	 * @param i number of the source image
	 * @return source image
	 */
	protected microscopyImage getSrcImage(int i) {
		if (i == 0) {
			return dxyReference;
		} else {
			return prj.get_imageByID(srcID.get(i));
		}
	}

	/**
	 * Get a reference image of this transformation
	 * @param i number of the reference image
	 * @return reference image
	 */
	protected microscopyImage getRefImage(int i) {
		int src = prj.get_imagePositionByID(srcID.get(0));
		int ref = prj.get_imagePositionByID(refID.get(i));
		microscopyImage out = new microscopyImage(prj.get_image(ref));
		MiscHelper.adaptImage(
				out,
				dxyReference,
				prj.get_imageAlignment(ref),
				prj.get_imageAlignment(src),
				1.5
		);
		return out;
	}

	/**
	 * Transform a microscopyImage with its feature points using the deformation fields
	 * @param source source image
	 * @return target image
	 */
	public microscopyImage applyDeformationField(microscopyImage source) {
		microscopyImage out = new microscopyImage(source);
		out.setStack(applyDeformationField(source.getStack()));
		for (int i = 0; i < source.number_of_feature_points(); i++) {
			Point.Double fp = source.len2pix(source.get_feature_point(i));
			Point.Double nfp = source.pix2len(coordTransformationO2T(fp));
			out.set_feature_point(i, nfp.getX(), nfp.getY());
		}
		return out;
	}

	/**
	 * Transform an ImageStack using the deformation fields
	 * @param source source stack
	 * @return target stack
	 */
	public ImageStack applyDeformationField(ImageStack source) {
		ImageStack res = ImageStack.create(source.getWidth(), source.getHeight(), source.getSize(), source.getBitDepth());
		for (int s = 0; s < res.getSize(); s++) {
			ImageProcessor srcIP = source.getProcessor(s + 1);
			res.setProcessor(applyDeformationField(srcIP), s + 1);
			res.setSliceLabel(source.getSliceLabel(s + 1), s + 1);
		}
		return res;
	}

	public ImageProcessor applyDeformationField(ImageProcessor source) {
		return applyDeformationField(source, dx, dy);
	}

	/**
	 * Transform an ImageProcessor using the deformation fields
	 * @param source source processor
	 * @param dx deformation fields for x coordinates
	 * @param dy deformation fields for y coordinates
	 * @return target processor
	 */
	public static ImageProcessor applyDeformationField(ImageProcessor source, double[][] dx, double[][] dy) {
		assert source.getWidth() == dx[0].length;
		assert source.getHeight() == dx.length;
		assert source.getWidth() == dy[0].length;
		assert source.getHeight() == dy.length;

		int valC;
		ImageProcessor result = source.duplicate();
		source.setInterpolate(true);
		source.setInterpolationMethod(ImageProcessor.BILINEAR);
		for (int x = 0; x < result.getWidth(); x++) {
			for (int y = 0; y < result.getHeight(); y++) {
				valC = source.getPixelInterpolated(x + dx[y][x], y + dy[y][x]);
				try {
					result.set(x, y, valC);
				} catch (ArrayIndexOutOfBoundsException e) {
					debug.put("OutOfBounds: y=" + y + ", x=" + x);
				}
			}
		}
		return result;
	}

	/**
	 * Open source and reference working images for editing
	 */
	protected void editLandmarks() {
		if (srcWork == null || refWork == null) createWorkingImages(true);
		srcWork.show(microscopyImage.MODE_EDIT_FP);
		refWork.show(microscopyImage.MODE_EDIT_FP);
	}

	protected ArrayList<Integer[]> matchLandmarks(ArrayList<Integer[]> oldMatches) {
		if (srcWork == null || refWork == null) createWorkingImages(true);
		return matchLandmarks(prj, srcWork, refWork, offset, oldMatches);
	}

	/**
	 * Match landmarks
	 * 	if working copies are open, read the multipoint-rois and use all their points as matched landmarks
	 * 	if they are closed, open the feature matching utily and rearrange the landmark order afterwards,
	 * 		so matched ones precede
	 * @param prj correlia project
	 * @param srcWork source working image
	 * @param refWork reference working image
	 * @param offset offset distance in pixel units, >0 if the reference image is larger
	 * @param oldMatches previous matches
	 * @return ArrayList containing pairs of corresponding landmarks
	 */
	public static ArrayList<Integer[]> matchLandmarks(correlia prj, microscopyImage srcWork, microscopyImage refWork, int[] offset, ArrayList<Integer[]> oldMatches) {
		ArrayList<Integer[]> matches = new ArrayList<>();

		if (srcWork.isVisible() && refWork.isVisible()) {
			Roi roiSrc = srcWork.getRoi();
			Roi roiRef = refWork.getRoi();
			if (roiSrc != null && roiSrc instanceof PointRoi && roiRef != null && roiRef instanceof PointRoi) {
				FloatPolygon ptsSrc = roiSrc.getFloatPolygon();
				FloatPolygon ptsRef = roiRef.getFloatPolygon();
				int minLen = Math.min(ptsSrc.npoints, ptsRef.npoints);
				srcWork.get_feature_points().clear();
				refWork.get_feature_points().clear();
				for (int i = 0; i < minLen; i++) {
					Point.Double pt = new Point.Double(ptsSrc.xpoints[i], ptsSrc.ypoints[i]);
					srcWork.add_feature_point(srcWork.pix2len(pt));
					pt = new Point.Double(ptsRef.xpoints[i], ptsRef.ypoints[i]);
					refWork.add_feature_point(refWork.pix2len(pt));
					Integer[] m = {i, i};
					matches.add(m);
				}
				srcWork.deleteRoi();
				refWork.deleteRoi();
				srcWork.close();
				refWork.close();
			}
		} else {
			ImagePlus screen = prj.get_image(0).duplicate();
			screen.setTitle("Match features");
			screen.show();

			dlgMatchFeatures d = new dlgMatchFeatures(
					srcWork,
					refWork,
					new imageAlignment(offset[0]*srcWork.pixelWidth(), offset[1]*srcWork.pixelHeight()),
					new imageAlignment(),
					true,
					screen,
					oldMatches
			);

			if (d.wasCancelled()) {
				screen.close();
				return null;
			}

			screen.close();

			// Sort features as matched and append unmatched ones
			ArrayList<Point.Double> srcFP = new ArrayList<>();
			ArrayList<Point.Double> refFP = new ArrayList<>();
			for (int m = 0; m < d.getMatches().size(); m++) {
				srcFP.add(srcWork.get_feature_point(d.getMatches().get(m)[0]));
				refFP.add(refWork.get_feature_point(d.getMatches().get(m)[1]));
				Integer[] newM = {m, m};
				matches.add(newM);
			}
			for (Point.Double fp : srcWork.get_feature_points()) {
				if (!srcFP.contains(fp)) {
					srcFP.add(fp);
				}
			}
			srcWork.set_feature_points(srcFP);
			for (Point.Double fp : refWork.get_feature_points()) {
				if (!refFP.contains(fp)) {
					refFP.add(fp);
				}
			}
			refWork.set_feature_points(refFP);
		}
		return matches;
	}

	/**
	 * Create a deformation preview image consisting of the following slices
	 * 	- deformations as checkboard
	 * 	- deformations as arrow field
	 * 	- colored deformation field for x coordinates
	 * 	- colored deformation field for y coordinates
	 * @return deformation preview image
	 */
	public ImagePlus getDeformationPreview() {
//		if (!changedSinceLastUpdate && deformationPreview != null) {
//			return deformationPreview;
//		} else if (dx != null && dy != null) {
		if (dx != null && dy != null) {
			Dimension dim = new Dimension(dx[0].length, dx.length);
			double min = Math.min(MiscHelper.findMin(dx), MiscHelper.findMin(dy));
			double max = Math.max(MiscHelper.findMax(dx), MiscHelper.findMax(dy));
			double border = Math.max(Math.abs(Math.min(min, 0.0)), Math.abs(Math.max(max, 0.0)));
			LUT diffLUT = null;
			if (blueToRedColorModel != null) {
				diffLUT = new LUT(blueToRedColorModel, -border, border);
			}

			ImageStack ims = new ImageStack(dim.width, dim.height);
			ims.addSlice("Deformation grid", MiscHelper.deformationCheckboard(dx, dy, 0, border));
			ims.addSlice("Deformation field", MiscHelper.deformationArrowField(dx, dy, 0, border));
			ims.addSlice("dx", MiscHelper.doubleArray2DToFP(dx));
			ims.addSlice("dy", MiscHelper.doubleArray2DToFP(dy));
			deformationPreview = new ImagePlus("Deformation preview", ims);
			deformationPreview.setLut(diffLUT);
			return deformationPreview;
		} else {
			return null;
		}
	}

	/**
	 * Get the target image of the transformation
	 * @return target image
	 */
	public microscopyImage getResult() {
		debug.put(" entered ("+getName()+")");
		if (!update()) return null;
		if (result == null) {
			result = applyDeformationField(dxyReference);
			setTag(result);
			result.getProcessor().setMinAndMax(dxyReference.getProcessor().getMin(), dxyReference.getProcessor().getMax());
		}
		return result;
	}

	/**
	 * Tag the image with the number of this transformation in the cascade
	 * @param mi image
	 */
	public void setTag(microscopyImage mi) {
		microscopyImageWarped miwRef = (microscopyImageWarped)prj.get_imageByID(srcID.get(0));
		mi.setTitle(Integer.toString(miwRef.getTransformationNr(this)));
	}

	/**
	 * Read the tag/transformation number of the image
	 * @param mi image
	 * @return transformation number
	 */
	public static int getTag(microscopyImage mi) {
		return Integer.parseInt(mi.getTitle());
	}

	/**
	 * Change previous transformation; needed when transformations are rearranged, (de-)activated, added or deleted
	 * @param prevT previous transformation
	 */
	public void setPreviousTransformation(transformation prevT) {
		if (prevT != previousTransformation) {
			previousTransformation = prevT;
			updateDxyReference();
			changedSinceLastUpdate = true;
		}
	}

	public void activate() {
		active = true;
		changedSinceLastUpdate = true;
	}

	public void deactivate() {
		active = false;
		resultChanged = true;
	}

	public boolean active() {
		return active;
	}

	public boolean changed() {
		return changedSinceLastUpdate;
	}

	protected void setChanged(boolean c) {
		changedSinceLastUpdate = c;
	}

	protected boolean resultChanged() {
		return resultChanged;
	}

	protected void setResultChanged(boolean b) {
		resultChanged = b;
	}

	public void addRefID(String ID) {
		if (refID.contains(ID)) return;
		refID.clear();	// Currently only one refence image is allowed
		refID.add(ID);
	}

	public void removeRefID(String ID) {
		refID.remove(ID);
	}

	public ArrayList<String> getRefID() {
		return refID;
	}

	public void addSrcID(String ID) {
		if (srcID.contains(ID)) return;
		srcID.clear();	// Currently only one source image is allowed
		srcID.add(ID);
	}

	public void removeSrcID(String ID) {
		srcID.remove(ID);
	}

	public ArrayList<String> getSrcID() {
		return srcID;
	}

	public void setName(String n) {
		name = n;
	}

	public String getName() {
		if (name == null || name.isEmpty()) {
			setName("noName");
		}
		return name;
	}
}
