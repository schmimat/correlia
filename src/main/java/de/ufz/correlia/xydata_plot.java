// xydata_gui uses jfreechart to display the x-y data
package de.ufz.correlia;

import java.util.*;
import java.awt.*;
import java.awt.Color;	// line colours
import java.awt.geom.*;	// marker shapes
import java.awt.Shape;	// marker shapes
import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.renderer.xy.XYShapeRenderer;
import org.jfree.chart.renderer.xy.XYAreaRenderer;
import org.jfree.chart.annotations.XYTextAnnotation;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;


import org.jfree.ui.RefineryUtilities;


class xydata_plot extends xydata
{
  public xydata_plot()
  {
	m_xlabel = new String("X");
	m_ylabel = new String("Y");
	m_title = new String("");
	
	m_annotations = new Vector();
	m_annotations_x = new Vector();
	m_annotations_y = new Vector();
	
	m_plotFrame = null;
  }
  
  public xydata_plot(xydata xyd)
  {
	m_x = xyd.m_x;
	m_y = xyd.m_y;
	
	m_is_sorted = xyd.m_is_sorted;	// is the data sorted in ascending x order
	m_linreg_slope = xyd.m_linreg_slope;
	m_linreg_offset = xyd.m_linreg_offset;
	
	m_xlabel = new String("X");
	m_ylabel = new String("Y");
	m_title = new String("");
	
	m_annotations = new Vector();
	m_annotations_x = new Vector();
	m_annotations_y = new Vector();

	m_plotFrame = null;
  }
 
  public JFrame plot(){ return plot_selection( 0,size()-1 ); } 
 
 
  public JFrame plot_selection( int sel_imin, int sel_imax )
  {
	if ( m_plotFrame != null ){ m_plotFrame.dispose(); }	// close previous window in case of replotting

	
	if ( size() < 2 ){ return null; }
  
	final JFreeChart chart = create_chart( create_chart_dataset( 0, size()-1 ) );	// full data
	XYPlot plot = chart.getXYPlot();
	
	final ChartPanel chartPanel = new ChartPanel(chart);
	chartPanel.setPreferredSize(new java.awt.Dimension(500, 400));


	// set colours and linestyles
	XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
	XYAreaRenderer renderer_selection = new XYAreaRenderer();
	plot.setRenderer(0, renderer);
	plot.setRenderer(1, renderer_selection); 

	plot.getRendererForDataset(plot.getDataset(0)).setSeriesPaint(0, new Color(0, 0, 180));

	if ( sel_imin == 0 && sel_imax == size()-1 ){}	// if a subset was selected mark area under the curve
	else
	{
		plot.setDataset( 1, create_chart_dataset( sel_imin, sel_imax ) );
		plot.getRendererForDataset(plot.getDataset(1)).setSeriesPaint(0, new Color(180, 180, 255));
	}
	
	// markers
	Shape markerShape = new Ellipse2D.Double(0, 0, 3, 3); // data
	renderer.setSeriesShape(0, markerShape);
	
	plot.setBackgroundPaint(new Color(255, 255, 255));
	chart.setBackgroundPaint(new Color(255, 255, 255));
	
	// add annotations
	
	for ( int i=0; i<m_annotations.size(); i++ )
	{
		final XYTextAnnotation annotation = new XYTextAnnotation( (String) m_annotations.elementAt(i),
								((Double) m_annotations_x.elementAt(i)).doubleValue(),
								((Double) m_annotations_y.elementAt(i)).doubleValue());
		annotation.setFont(new Font("SansSerif", Font.PLAIN, 9));
		plot.addAnnotation(annotation);
	}
	
	// end add annotations
	
	
	// create a window and show plot
	JFrame plotFrame = new JFrame();
	plotFrame.setContentPane(chartPanel);
	RefineryUtilities.centerFrameOnScreen(plotFrame);
	plotFrame.setSize(600, 480);
	
	plotFrame.setVisible(true);
	plotFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	// when plot window is closed memory is freed
	
	return m_plotFrame = plotFrame;
  }
  
  public JFrame plot_indicate_points( Vector points )
  {
	if ( m_plotFrame != null ){ m_plotFrame.dispose(); }	// close previous window in case of replotting
  
	if ( size() < 2 ){ return null; }
  
	final JFreeChart chart = create_chart( create_chart_dataset_indicated_points( points ) );	// full data
	XYPlot plot = chart.getXYPlot();
	
	final ChartPanel chartPanel = new ChartPanel(chart);
	chartPanel.setPreferredSize(new java.awt.Dimension(500, 400));

	// set colours and linestyles
	XYLineAndShapeRenderer renderer_data = new XYLineAndShapeRenderer();
	plot.setRenderer(1, renderer_data);
	XYShapeRenderer renderer_indicate = new XYShapeRenderer();
	plot.setRenderer(0, renderer_indicate);

	plot.getRendererForDataset(plot.getDataset(0)).setSeriesPaint(0, new Color(220, 0, 0));

	plot.setDataset( 1, create_chart_dataset( 0, size()-1 ) );
	plot.getRendererForDataset(plot.getDataset(1)).setSeriesPaint(0, new Color(0, 0, 180));	//indicated points
	
	// markers
	Shape markerShape_data = new Ellipse2D.Double(0, 0, 3, 3); // data
	renderer_data.setSeriesShape(0, markerShape_data);
	Shape markerShape_indicate = new Rectangle(6, 6); // data
	renderer_indicate.setSeriesShape(0, markerShape_indicate);
	
	plot.setBackgroundPaint(new Color(255, 255, 255));
	chart.setBackgroundPaint(new Color(255, 255, 255));
	
	
	// create a window and show plot
	JFrame plotFrame = new JFrame();
	plotFrame.setContentPane(chartPanel);
	RefineryUtilities.centerFrameOnScreen(plotFrame);
	plotFrame.setSize(600, 480);
	
	plotFrame.setVisible(true);
	plotFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	// when plot window is closed memory is freed
	
	return m_plotFrame = plotFrame;
  }
  
  public JFrame plot_staight_line( double slope, double offset )
  {
	if ( m_plotFrame != null ){ m_plotFrame.dispose(); }	// close previous window in case of replotting
  
	if ( size() < 2 ){ return null; }


	final JFreeChart chart = create_chart( create_chart_dataset_straight_line( slope, offset ) );	// full data
	XYPlot plot = chart.getXYPlot();
	
	final ChartPanel chartPanel = new ChartPanel(chart);
	chartPanel.setPreferredSize(new java.awt.Dimension(500, 400));

	// set colours and linestyles
	XYLineAndShapeRenderer renderer_data = new XYLineAndShapeRenderer();
	plot.setRenderer(1, renderer_data);
	XYLineAndShapeRenderer renderer_line = new XYLineAndShapeRenderer();
	plot.setRenderer(0, renderer_line);

	plot.getRendererForDataset(plot.getDataset(0)).setSeriesPaint(0, new Color(220, 0, 0));

	plot.setDataset( 1, create_chart_dataset( 0, size()-1 ) );
	plot.getRendererForDataset(plot.getDataset(1)).setSeriesPaint(0, new Color(0, 0, 180));	//indicated points
	
	// markers
	Shape markerShape_data = new Ellipse2D.Double(0, 0, 3, 3); // data
	renderer_data.setSeriesShape(0, markerShape_data);
	Shape markerShape_line = new Ellipse2D.Double(0, 0, 1, 1); // line
	renderer_line.setSeriesShape(0, markerShape_line);
	
	plot.setBackgroundPaint(new Color(255, 255, 255));
	chart.setBackgroundPaint(new Color(255, 255, 255));
	
	// create a window and show plot
	JFrame plotFrame = new JFrame();
	plotFrame.setContentPane(chartPanel);
	RefineryUtilities.centerFrameOnScreen(plotFrame);
	plotFrame.setSize(600, 480);
	
	plotFrame.setVisible(true);
	plotFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	// when plot window is closed memory is freed
	
	return m_plotFrame = plotFrame;
  }
  
  
  public void set_xlabel( String xl ){ m_xlabel = xl; }
  public void set_ylabel( String yl ){ m_ylabel = yl; }  
  public String get_xlabel(){ return m_xlabel; }
  public String get_ylabel(){ return m_ylabel; }
  public void set_title( String s ){ m_title = s; }
  public String get_title(){ return m_title; }
  
  public void add_annotation( String ann, double x, double y )
  {
	if ( ann == null ){return;}
  
	m_annotations.add(ann);
	m_annotations_x.add(x);
	m_annotations_y.add(y);
  }
  
  public void clear_annotations()
  {
	m_annotations.clear();
	m_annotations_x.clear();
	m_annotations_y.clear();
  }
  
  
  private XYSeriesCollection create_chart_dataset( int imin, int imax )
  {
	final XYSeriesCollection dataset = new XYSeriesCollection();
	final XYSeries series = new XYSeries("");
	
	// add data points
	for ( int i=0; i<size(); i++ )
	{ 
		if ( i>=imin && i<=imax )
		{ series.add(get_x(i), get_y(i)); }
	}
	dataset.addSeries(series);
	
	return dataset;
  }
  
  private XYSeriesCollection create_chart_dataset_indicated_points( Vector points ) 
  {
	final XYSeriesCollection dataset = new XYSeriesCollection();
	final XYSeries series = new XYSeries("");
	
	int j = 0;
	
	// add data points
	for ( int i=0; i<points.size(); i++ )
	{ 
		j = ((Integer) points.elementAt(i)).intValue();
		if ( j >= 0 && j < size() )
		{ series.add(get_x(j), get_y(j)); }
	}
	dataset.addSeries(series);
	
	return dataset;
  }
  
  private XYSeriesCollection create_chart_dataset_straight_line( double slope, double offset )
  {
	final XYSeriesCollection dataset = new XYSeriesCollection();
	final XYSeries series = new XYSeries("");
  
	// calculate first data point
	double x0 = xmin();
	double y0 = slope*x0 + offset;
	
	if ( y0>ymax() ){ y0 = ymax(); x0 = (y0-offset)/slope; }
	if ( y0<ymin() ){ y0 = ymin(); x0 = (y0-offset)/slope; }
	
	// calculate second data point
	double x1 = xmax();
	double y1 = slope*x1+offset;

	if ( y1>ymax() ){ y1 = ymax(); x1 = (y1-offset)/slope; }
	if ( y1<ymin() ){ y1 = ymin(); x1 = (y1-offset)/slope; }
	
	series.add(x0,y0);
	series.add(x1,y1);
	dataset.addSeries(series);
	return dataset;
  }
  
  
  private JFreeChart create_chart(final XYDataset dataset)
  {
	final JFreeChart chart = ChartFactory.createXYLineChart(
		m_title,      // chart title
		m_xlabel,  // x axis label
		m_ylabel,  // y axis label
		dataset,                  // data
		PlotOrientation.VERTICAL,
		false,                     // include legend
		true,                     // tooltips
		false                     // urls
		);
	return chart;
  }
  


 
  protected String m_xlabel;
  protected String m_ylabel;
  protected String m_title;
  
  protected Vector m_annotations;	// strings for annotations
  protected Vector m_annotations_x;	// double values for x coordinate of annotations
  protected Vector m_annotations_y;	// double values for y coordinate of annotations
  
  
  protected JFrame m_plotFrame;
  
}