package de.ufz.correlia;

import ij.IJ;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.awt.*;
import java.io.*;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Locale;

public class legacySupport {

	public static Document readOldProject(String path) {
		File f = new File(path);
		xmlHandler xml = new xmlHandler();
		BufferedReader reader = null;

		String definitionString = new String("");
		try {
			reader = new BufferedReader(new FileReader(f));
			String line;
			while ((line = reader.readLine()) != null) {	// read file
				definitionString += line+"\n";
			}
			reader.close();
		} catch (FileNotFoundException e) {
			debug.put("ERROR: in readOldProject: caught FileNotFoundException when trying to open file "+path);
			IJ.log("ERROR: in readOldProject: caught FileNotFoundException when trying to open file "+path);
			e.printStackTrace();
		} catch (IOException e) {
			debug.put("ERROR: in readOldProject: caught IOException when trying to open file "+path);
			IJ.log("ERROR: in readOldProject: caught IOException when trying to open file "+path);
			e.printStackTrace();
		} finally {
			try	{
				if (reader != null) {
					reader.close();
				}
			} catch (IOException e) {}
		}

		// set up parser
		parseInputAndInstanciateClass parser = new parseInputAndInstanciateClass();
		parser.parseString(definitionString);
//		parser.parseString(definitionString, f.getParent() + File.separator);

		if (!parser.getClassName().equals("overlay_images") && !parser.getClassName().equals("correlia")) {
			IJ.showMessage("class " + parser.getClassName() + " not found");
			return null;
		}

		definitionString = parser.getSubstring();

		xml.addToDoc(parse_correlia(definitionString, xml));

		return xml.getDoc();
	}

	/**
	 * Evaluate the project definition string containing project metadata and included images
	 * @param definitionString project definition string
	 * @param xml xml Handler
	 * @return false, if an error occurred
	 */
	private static Element parse_correlia(String definitionString, xmlHandler xml) {
		Element root = xml.createElement("correlia");

		// project title is the first line in a project definition
		String prjString = definitionString.split("begin")[0];
		String words[] = prjString.split("[ \n\t]");

		for (int i=0; i<words.length; i++) {
			if (words[i].equals("projectTitle")) {
				xml.addTextElement(root, "projectTitle", words[++i]);
//				set_projectTitle(words[++i]);
			} else if (words[i].equals("additionalInfo")) {
				StringBuilder t = new StringBuilder();
				while (i<words.length-1 && !words[i+1].equals("endAdditionalInfo")) {
					i++;
					t.append(words[i]);
					t.append(" ");
				}
				xml.addTextElement(root, "additionalInfo", t.toString().replaceAll("<br>","\n"));
//				set_additionalInfo(t.toString().replaceAll("<br>","\n"));
			}
		}

		// set up parser
		parseInputAndInstanciateClass parser = new parseInputAndInstanciateClass();
		parser.parseString(definitionString);  // assuming path was already set

		// set up flag indicating first image is the base
		int count = 0;
		int pos = -1;

		//TODO Why magic number 5? --> length of "begin"
		while (!(definitionString.length() < 5)) {  // equals(""))
			parser.parseString(definitionString);
			if (count < 2) {  // first microscopyImage is the base, first imageColour is colour definition for the base
				if (parser.getClassName().equals("microscopyImage")) {
					pos++;
					xml.addElement(root, xml.addAttribute(xml.createElement("microscopyImage"), "pos", pos));
					xml.addElement(root, xml.addAttribute(xml.createElement("imageAlignment"), "pos", pos));
					xml.addElement(root, xml.addAttribute(xml.createElement("imageColour"), "pos", pos));
					parse_microscopyImage(parser.getSubstring(), xml, root, pos);
//					baseImage = new microscopyImage(parser.getSubstring());
					count++;
				}
				if (parser.getClassName().equals("imageColour")) {
					parse_imageColour(parser.getSubstring(), xml, root, pos);
//					baseImage_colour = new imageColour(parser.getSubstring());
					count++;
				}
			} else {
				if (parser.getClassName().equals("microscopyImage")) {	// an overlying image
					pos++;
					xml.addElement(root, xml.addAttribute(xml.createElement("microscopyImage"), "pos", pos));
					xml.addElement(root, xml.addAttribute(xml.createElement("imageColour"), "pos", pos));
					xml.addElement(root, xml.addAttribute(xml.createElement("imageAlignment"), "pos", pos));
					parse_microscopyImage(parser.getSubstring(), xml, root, pos);
//					microscopyImage mi = new microscopyImage(parser.getSubstring());
//					images.add(mi);
				}
				if (parser.getClassName().equals("translateAndRotate2D")) {	// coordinates of that image
					parse_translateAndRotate2D(parser.getSubstring(), xml, root, pos);
//					imageAlignment coord = new imageAlignment(parser.getSubstring());
//					imageAlignments.add(coord);
				}
				if (parser.getClassName().equals("imageColour")) {	// colour of the image
					parse_imageColour(parser.getSubstring(), xml, root, pos);
//					imageColour ic = new imageColour(parser.getSubstring());
//					imageColourChannels.add(ic);
				}
			}
			definitionString = parser.getRemainderString();
		}

		//return check();
		//check if as many coordinates and colour definitions as images were imported
//		if (images.size() != imageColourChannels.size() ||
//				images.size() != imageAlignments.size())
//		{ return false; }
		return root;
	}

	private static boolean parse_microscopyImage(String s, xmlHandler xml, Element correlia, int pos) {
		debug.put(" (" + pos +  ") entered");
		Element root = xml.getElementByAttribute(correlia, "microscopyImage", "pos", pos);
		Element size = xml.createElement("size");
		xml.addAttribute(size, "unit", "um");
		xml.addElement(root, size);

		String defStr = s;	// cpoy is needed for the parser following the for loop
		String words[] = s.split("[ \n\t]");
		for (int i = 0; i < words.length; i++) {	// get parameters for this image
			if (words[i].equals("path")) { // read title
				xml.addTextElement(root, "path", words[i+1]);
			}

			if (words[i].equals("title")) { // read title
				String t = "";
				while (i<words.length-1 && !words[i+1].equals("endTitle")) {
					i++;
					t += words[i] + " ";
				}
				xml.addTextElement(root, "title", t);
//				setTitle(t);
			}

			if (words[i].equals("currentSlice")) { // set current slice
				try {
					xml.addTextElement(root, "currentSlice", Integer.parseInt(words[i+1]));
//					setZ(Integer.parseInt(words[i+1]));
				} catch (NumberFormatException e) {
					IJ.log("ERROR: caught NumberFormatException in microscopyImage.parse_parameterString when parsing currentSlice");
					return false;
				}
			}
			if (words[i].equals("setup")) { // read title
				String t = "";
				while (i<words.length-1 && !words[i+1].equals("endSetup")) {
					i++;
					t += words[i] + " ";
				}
				xml.addTextElement(root, "setup", t);
//				set_setup(t);
			}
			if (words[i].equals("experimenter")) { // read experimenter
				String t = "";
				while (i<words.length-1 && !words[i+1].equals("endExperimenter")) {
					i++;
					t += words[i] + " ";
				}
				xml.addTextElement(root, "experimenter", t);
//				set_experimenter(t);
			}

			if (words[i].equals("acquisition")) { // read acquisition date
				String t = "";
				while (i<words.length-1 && !words[i+1].equals("endAcquisition")) {
					i++;
					t += words[i] + " ";
				}
				xml.addTextElement(root, "acquisitionDate", t);
//				set_acquisitionDate(t);
			}

			if (words[i].equals("additionalInfo")) { // read additional info
				String t = "";
				while (i<words.length-1 && !words[i+1].equals("endAdditionalInfo")) {
					i++;
					t += words[i] + " ";
				}
				t = t.replaceAll("<br>","\n");
				xml.addTextElement(root, "additionalInfo", t);
//				set_additionalInfo(t);
			}

			if (words[i].equals("width") && i<words.length-1) {	// dimensions
				NumberFormat format = NumberFormat.getInstance(Locale.ENGLISH);
				try {
					xml.addAttribute(xml.getElementByName(root, "size"), "width", format.parse(words[i+1].replaceAll(",",".")).doubleValue());
//					m_width = format.parse(words[i+1].replaceAll(",",".")).doubleValue();
				} catch (ParseException e) {
					IJ.log("ERROR: caught ParseException in microscopyImage.parse_parameterString when parsing image width");
					return false;
				}
			}
			if (words[i].equals("height") && i<words.length-1) {	// dimensions
				NumberFormat format = NumberFormat.getInstance(Locale.ENGLISH);
				try {
					xml.addAttribute(xml.getElementByName(root, "size"), "height", format.parse(words[i+1].replaceAll(",",".")).doubleValue());
//					m_height = format.parse(words[i+1].replaceAll(",",".")).doubleValue();
				} catch (ParseException e) {
					IJ.log("ERROR: caught ParseException in microscopyImage.parse_parameterString when parsing image height");
					return false;
				}
			}

			if (words[i].equals("adjFac") && i<words.length-1) {	// stretch image
				NumberFormat format = NumberFormat.getInstance(Locale.ENGLISH);
				try {
					if (pos > 0) {
						Element ia = xml.getElementByAttribute(correlia, "imageAlignment", "pos", pos);
						double scale = format.parse(words[i + 1].replaceAll(",", ".")).doubleValue();
						ArrayList<String> params = new ArrayList<>();
						params.add("x");
						params.add(Double.toString(scale));
						params.add("y");
						params.add(Double.toString(scale));
						xml.addTextElementWithAttributes(root, "scale", "", params);
					}
//					m_adjustmentFactor = format.parse(words[i+1].replaceAll(",",".")).doubleValue();
				} catch(ParseException e) {
					IJ.log("ERROR: caught ParseException in microscopyImage.parse_parameterString when parsing adjFac");
					return false;
				}
			}

			if (words[i].equals("feature") && i<words.length-2) {	// features
				NumberFormat format = NumberFormat.getInstance(Locale.ENGLISH);
				try {
					Point.Double p = new Point.Double(
							format.parse(words[i + 1].replaceAll(",", ".")).doubleValue(),
							format.parse(words[i + 2].replaceAll(",", ".")).doubleValue()
					);
					Element fp = xml.createElement("feature");
					xml.addAttribute(fp, "type", "point");
					xml.addAttribute(fp, "unit", "um");
					xml.addAttribute(fp, "x", p.getX());
					xml.addAttribute(fp, "y", p.getY());
					xml.addElement(root, fp);
//				feature_points.add(p);
				} catch (ParseException e) {
					IJ.log("ERROR: caught ParseException in microscopyImage.parse_parameterString when parsing feature");
					return false;
				}
			}
//			int ret = parse_microscopyImageWord(words, i, xml, root);
//			if (ret < 0) {
//				return null;
//			} else {
//				i = ret;
//			}
		}

		// set up parser to read ROIs and spectra + additional infos
//		parseInputAndInstanciateClass parser = new parseInputAndInstanciateClass();

		// check for all microscopyImageRoiHandler
//		parser.parseString(defStr);
//		while (parser.getClassName().equals("microscopyImageRoiHandler")) {
//			microscopyImageRoiHandler mrh = new microscopyImageRoiHandler(parser.getSubstring());
//			microImg_rois.add(mrh);
//			defStr = parser.getRemainderString();
//			parser.parseString(defStr);
//		}
//
//		m_selectedRoi = microImg_rois.size()-1;
		return true;
	}
//
//	private static int parse_microscopyImageWord(String words[], int i, xmlHandler xml, Element root, int pos) {
//		if (words[i].equals("title")) { // read title
//			String t = "";
//			while (i<words.length-1 && !words[i+1].equals("endTitle")) {
//				i++;
//				t += words[i] + " ";
//			}
//			xml.addTextElement(root, "title", t);
////			setTitle(t);
//		}
//
//		if (words[i].equals("currentSlice")) { // set current slice
//			try {
//				xml.addTextElement(root, "currentSlice", Integer.parseInt(words[i+1]));
////				setZ(Integer.parseInt(words[i+1]));
//			} catch (NumberFormatException e) {
////				IJ.log("ERROR: caught NumberFormatException in microscopyImage.parse_parameterString when parsing currentSlice, image title: "+getTitle());
//				return -1;
//			}
//		}
//		if (words[i].equals("setup")) { // read title
//			String t = "";
//			while (i<words.length-1 && !words[i+1].equals("endSetup")) {
//				i++;
//				t += words[i] + " ";
//			}
//			xml.addTextElement(root, "setup", t);
////			set_setup(t);
//		}
//		if (words[i].equals("experimenter")) { // read experimenter
//			String t = "";
//			while (i<words.length-1 && !words[i+1].equals("endExperimenter")) {
//				i++;
//				t += words[i] + " ";
//			}
//			xml.addTextElement(root, "experimenter", t);
////			set_experimenter(t);
//		}
//
//		if (words[i].equals("acquisition")) { // read acquisition date
//			String t = "";
//			while (i<words.length-1 && !words[i+1].equals("endAcquisition")) {
//				i++;
//				t += words[i] + " ";
//			}
//			xml.addTextElement(root, "acquisitionDate", t);
////			set_acquisitionDate(t);
//		}
//
//		if (words[i].equals("additionalInfo")) { // read additional info
//			String t = "";
//			while (i<words.length-1 && !words[i+1].equals("endAdditionalInfo")) {
//				i++;
//				t += words[i] + " ";
//			}
//			t = t.replaceAll("<br>","\n");
//			xml.addTextElement(root, "additionalInfo", t);
////			set_additionalInfo(t);
//		}
//
//		if (words[i].equals("width") && i<words.length-1) {	// dimensions
//			NumberFormat format = NumberFormat.getInstance(Locale.ENGLISH);
//			try {
//				xml.addAttribute(xml.getElementByName(root, "size"), "width", format.parse(words[i+1].replaceAll(",",".")).doubleValue());
////				m_width = format.parse(words[i+1].replaceAll(",",".")).doubleValue();
//			} catch (ParseException e) {
//				IJ.log("ERROR: caught ParseException in microscopyImage.parse_parameterString when parsing image width");
//				return -1;
//			}
//		}
//		if (words[i].equals("height") && i<words.length-1) {	// dimensions
//			NumberFormat format = NumberFormat.getInstance(Locale.ENGLISH);
//			try {
//				xml.addAttribute(xml.getElementByName(root, "size"), "height", format.parse(words[i+1].replaceAll(",",".")).doubleValue());
////				m_height = format.parse(words[i+1].replaceAll(",",".")).doubleValue();
//			} catch (ParseException e) {
//				IJ.log("ERROR: caught ParseException in microscopyImage.parse_parameterString when parsing image height");
//				return -1;
//			}
//		}
//
//		if (words[i].equals("adjFac") && i<words.length-1) {	// stretch image
//			NumberFormat format = NumberFormat.getInstance(Locale.ENGLISH);
//			try {
////				m_adjustmentFactor = format.parse(words[i+1].replaceAll(",",".")).doubleValue();
//			} catch(ParseException e) {
//				IJ.log("ERROR: caught ParseException in microscopyImage.parse_parameterString when parsing adjFac");
//				return -1;
//			}
//		}
//
//		if (words[i].equals("feature") && i<words.length-2) {	// features
//			NumberFormat format = NumberFormat.getInstance(Locale.ENGLISH);
//			try {
//				Point.Double p = new Point.Double(
//						format.parse(words[i + 1].replaceAll(",", ".")).doubleValue(),
//						format.parse(words[i + 2].replaceAll(",", ".")).doubleValue()
//				);
//				Element fp = xml.createElement("feature");
//				xml.addAttribute(fp, "unit", "um");
//				xml.addAttribute(fp, "x", p.getX());
//				xml.addAttribute(fp, "y", p.getY());
////				feature_points.add(p);
//			} catch (ParseException e) {
//				IJ.log("ERROR: caught ParseException in microscopyImage.parse_parameterString when parsing feature");
//				return -1;
//			}
//		}
//		return i;
//	}
//
	private static boolean parse_imageColour(String s, xmlHandler xml, Element correlia, int pos) {
		debug.put(" (" + pos +  ") entered");
		Element root = xml.getElementByAttribute(correlia, "imageColour", "pos", pos);
		Element show = xml.addTextElement(root, "show", imageColour.HIDE);

		int foundDefinition = 0;
		String words[] = s.split("[ \t\n]");
		for (int i = 0; i < words.length; i++)    // find keywords
		{
			if (words[i].equals("RGB") && i + 3 < words.length) // colours (deprecated, used for downward compatibility)
			{
				// try to read colour values (double numbers)
				NumberFormat format = NumberFormat.getInstance(Locale.ENGLISH);

				ArrayList<String> params;
				params = new ArrayList<>();
				try {
					params.add("r");
					params.add(Double.toString(format.parse(words[i + 1].replaceAll(",", ".")).doubleValue()).replaceAll(",", "."));
					params.add("g");
					params.add(Double.toString(format.parse(words[i + 2].replaceAll(",", ".")).doubleValue()).replaceAll(",", "."));
					params.add("b");
					params.add(Double.toString(format.parse(words[i + 3].replaceAll(",", ".")).doubleValue()).replaceAll(",", "."));
					xml.addTextElementWithAttributes(root, "rgb", "", params);

//					set_red(format.parse(words[i + 1].replaceAll(",", ".")).doubleValue());
//					set_green(format.parse(words[i + 2].replaceAll(",", ".")).doubleValue());
//					set_blue(format.parse(words[i + 3].replaceAll(",", ".")).doubleValue());
					foundDefinition++;
				} catch (ParseException e) {
					IJ.log("ERROR: caught ParseException in when parsing rgb color");
					return false;
				}
			}

			if (words[i].equals("Red") && i + 2 < words.length) // colours
			{
				// try to read colour values (double numbers)
				Element rgb = xml.getElementByName(root, "rgb");
				if (rgb == null) {
					rgb = xml.createElement("rgb");
				}
				Element rgbOff = xml.getElementByName(root, "rgbOff");
				if (rgbOff == null) {
					rgbOff = xml.createElement("rgbOff");
				}

				NumberFormat format = NumberFormat.getInstance(Locale.ENGLISH);
				try {
					xml.addAttribute(rgb, "r", format.parse(words[i + 1].replaceAll(",", ".")).doubleValue());
					xml.addAttribute(rgbOff, "ro", format.parse(words[i + 2].replaceAll(",", ".")).doubleValue());
//					set_red(format.parse(words[i + 1].replaceAll(",", ".")).doubleValue(), format.parse(words[i + 2].replaceAll(",", ".")).doubleValue());
					foundDefinition++;
				} catch (ParseException e) {
					IJ.log("ERROR: caught ParseException in when parsing red color");
					return false;
				}
			}

			if (words[i].equals("Green") && i + 2 < words.length) // colours
			{
				// try to read colour values (double numbers)
				Element rgb = xml.getElementByName(root, "rgb");
				if (rgb == null) {
					rgb = xml.createElement("rgb");
				}
				Element rgbOff = xml.getElementByName(root, "rgbOff");
				if (rgbOff == null) {
					rgbOff = xml.createElement("rgbOff");
				}

				NumberFormat format = NumberFormat.getInstance(Locale.ENGLISH);
				try {
					xml.addAttribute(rgb, "g", format.parse(words[i + 1].replaceAll(",", ".")).doubleValue());
					xml.addAttribute(rgbOff, "go", format.parse(words[i + 2].replaceAll(",", ".")).doubleValue());
//					set_green(format.parse(words[i + 1].replaceAll(",", ".")).doubleValue(), format.parse(words[i + 2].replaceAll(",", ".")).doubleValue());
					foundDefinition++;
				} catch (ParseException e) {
					IJ.log("ERROR: caught ParseException in when parsing green color");
					return false;
				}
			}

			if (words[i].equals("Blue") && i + 2 < words.length) // colours
			{
				// try to read colour values (double numbers)
				Element rgb = xml.getElementByName(root, "rgb");
				if (rgb == null) {
					rgb = xml.createElement("rgb");
				}
				Element rgbOff = xml.getElementByName(root, "rgbOff");
				if (rgbOff == null) {
					rgbOff = xml.createElement("rgbOff");
				}

				NumberFormat format = NumberFormat.getInstance(Locale.ENGLISH);
				try {
					xml.addAttribute(rgb, "b", format.parse(words[i + 1].replaceAll(",", ".")).doubleValue());
					xml.addAttribute(rgbOff, "bo", format.parse(words[i + 2].replaceAll(",", ".")).doubleValue());
//					set_blue(format.parse(words[i + 1].replaceAll(",", ".")).doubleValue(), format.parse(words[i + 2].replaceAll(",", ".")).doubleValue());
					foundDefinition++;
				} catch (ParseException e) {
					IJ.log("ERROR: caught ParseException in when parsing blue color");
					return false;
				}
			}


			if (words[i].equals("active") && i + 1 < words.length) // is image active or inactive
			{
				if (Boolean.parseBoolean(words[i+1])) {
					show.setTextContent(imageColour.SHOW);
//					visibility = imageColour.SHOW;
					foundDefinition++;
				}
			}

			if (words[i].equals("exclusive") && i + 1 < words.length) // is this image shown exclusively?
			{
				if (Boolean.parseBoolean(words[i+1])) {
					show.setTextContent(imageColour.EXCLUSIVE);
//					visibility = imageColour.EXCLUSIVE;
					foundDefinition++;
				}
			}

			if (words[i].equals("colourEnhanced") && i + 1 < words.length) // is this image shown enhanced?
			{
				if (Boolean.parseBoolean(words[i+1])) {
					show.setTextContent(imageColour.ENHANCED);
//					visibility = imageColour.ENHANCED;
					foundDefinition++;
				}
			}
		}
		return true;
	}

	private static boolean parse_translateAndRotate2D(String s, xmlHandler xml, Element correlia, int pos) {
		debug.put(" (" + pos +  ") entered");
		Element root = xml.getElementByAttribute(correlia, "imageAlignment", "pos", pos);

		String words[] = s.split(" ");
		Boolean foundDefinition = false;
		for (int i = 0; i < words.length; i++) {
			if (words[i].equals("x0:y0:rot") && i + 3 < words.length)    //coordinate definition found
			{
				NumberFormat format = NumberFormat.getInstance(Locale.ENGLISH);
				try {
					ArrayList<String> params;

					params = new ArrayList<>();
					params.add("x");
					params.add(Double.toString(format.parse(words[i + 1].replaceAll(",", ".")).doubleValue()));
					params.add("y");
					params.add(Double.toString(format.parse(words[i + 2].replaceAll(",", ".")).doubleValue()));
					xml.addTextElementWithAttributes(root, "translation", "", params);

					xml.addTextElement(root,"rotation", format.parse(words[i + 3].replaceAll(",", ".")).doubleValue());

//					xml.addTextElement(root, "scale", m_scale);
//					set_x0y0(format.parse(words[i + 1].replaceAll(",", ".")).doubleValue(), format.parse(words[i + 2].replaceAll(",", ".")).doubleValue());
//					set_rotAngle(format.parse(words[i + 3].replaceAll(",", ".")).doubleValue());
					foundDefinition = true;
				} catch (ParseException e) {
					return false;
				}
			}
		}
		return true;
	}

}
