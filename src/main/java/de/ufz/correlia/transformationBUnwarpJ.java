/*
 * Transformation type that integrates the ImageJ plugin bUnwarpJ for nonlinear image registration
 * Author: Florens Rohde
 *
 * Part of:
 * Correlia plugin for ImageJ and FIJI
 */
package de.ufz.correlia;

import de.ufz.correlia.slider.RangeSlider;
import ij.IJ;
import bunwarpj.*;
import ij.gui.PointRoi;
import org.w3c.dom.Element;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.time.Instant;
import java.util.*;

public class transformationBUnwarpJ extends transformation {
	public static final String NAME = "bUnwarpJ";

	private static final double IMAGEWEIGHT_DEFAULT = 0.0;
	private static final double LANDMARK_DEFAULT = 1.0;
	private static final int MINDEFORMATIONSCALE_DEFAULT = 0;
	private static final int MAXDEFORMATIONSCALE_DEFAULT = 2;
	private static final double DIVWEIGHT_DEFAULT = 0.1;
	private static final double CURLWEIGHT_DEFAULT = 0.1;

	private double[][] cx;
	private double[][] cy;

	private ArrayList<Integer[]> matchedFP = null;
	private double imageWeight = IMAGEWEIGHT_DEFAULT;
	private double landmarkWeight = LANDMARK_DEFAULT;
	private double divWeight = DIVWEIGHT_DEFAULT;
	private double curlWeight = CURLWEIGHT_DEFAULT;
	private int minDeformationScale = MINDEFORMATIONSCALE_DEFAULT;
	private int maxDeformationScale = MAXDEFORMATIONSCALE_DEFAULT;

	private JSlider slImageWeight;
	private JSlider slLandmarkWeight;
	private JSlider slDivWeight;
	private JSlider slCurlWeight;
	private RangeSlider rslDeformationScale;

	private Instant start;

	transformationBUnwarpJ(xmlHandler xml, Element root, correlia prj, transformation prevT, microscopyImageWarped src) {
		super(xml, root, prj, prevT, src);
		useDeformationField = false;

		imageWeight = xml.getDoublebyElementName(root, "imageWeight");
		landmarkWeight = xml.getDoublebyElementName(root, "landmarkWeight");

		minDeformationScale = xml.getIntAttributeByElementName(root, "deformationScale", "min");
		maxDeformationScale = xml.getIntAttributeByElementName(root, "deformationScale", "max");

		Element edx = xml.getElementByName(root, "dx");
		if (edx != null) {
			cx = readCoefficientsFromXML(xml, edx);
		}

		Element edy = xml.getElementByName(root, "dy");
		if (edy != null) {
			cy = readCoefficientsFromXML(xml, edy);
		}

		if (edx != null && edy != null) {
			dx = MiscHelper.interpolateMatrixFromCoefficients(cx, src.getWidth(), src.getHeight());
			dy = MiscHelper.interpolateMatrixFromCoefficients(cy, src.getWidth(), src.getHeight());
			for (int v = 0; v < dx.length; v++) {
				for (int u = 0; u < dx[0].length; u++) {
					dx[v][u] -= u;
					dy[v][u] -= v;
				}
			}
		}
	}

	transformationBUnwarpJ(correlia prj, String originalID, transformation prevTrans) {
		super(NAME, prj, originalID, prevTrans);
		useDeformationField = false;
	}

	transformationBUnwarpJ(transformationBUnwarpJ t) {
		super(t);
		useDeformationField = false;

		imageWeight = t.get_imageWeight();
		landmarkWeight = t.get_landmarkWeight();
		minDeformationScale = t.get_minDeformationScale();
		maxDeformationScale = t.get_maxDeformationScale();

		if (t.matchedFP != null) {
			matchedFP = new ArrayList<>();
			for (Integer[] m : t.matchedFP) {
				Integer[] mCopy = {new Integer(m[0]), new Integer(m[1])};
				matchedFP.add(mCopy);
			}
		}

		if (t.cx != null) {
			cx = new double[t.cx.length][t.cx[0].length];
			for (int i = 0; i < cx.length; i++) {
				cx[i] = Arrays.copyOf(t.cx[i], t.cx[i].length);
			}
		}
		if (t.cy != null) {
			cy = new double[t.cy.length][t.cy[0].length];
			for (int i = 0; i < cy.length; i++) {
				cy[i] = Arrays.copyOf(t.cy[i], t.cy[i].length);
			}
		}
	}

	@Override
	public Element buildXML(xmlHandler xml, boolean recipe) {
		ArrayList<String> params;
		Element root = super.buildXML(xml, recipe);

		xml.addTextElement(root, "imageWeight", Double.toString(imageWeight));
		xml.addTextElement(root, "landmarkWeight", Double.toString(landmarkWeight));

		params = new ArrayList<>();
		params.add("min");
		params.add(Integer.toString(minDeformationScale));
		params.add("max");
		params.add(Integer.toString(maxDeformationScale));
		xml.addTextElementWithAttributes(root, "deformationScale", "", params);

		if (!recipe) {
			writeCoefficientsToXML(xml, root, "dx", cx);
			writeCoefficientsToXML(xml, root, "dy", cy);
		}

		return root;
	}

	public boolean calc() {
		debug.put(" entered ("+getName()+")");

		// Convert feature to point roi
		// Only do that if landmarkWeight is not null, because UnWarpJ always uses landmarks, when it finds some
		if (landmarkWeight > 0.0) {
			if (matchedFP != null) {
				ArrayList<Point.Double> srcNewFp = new ArrayList<>();
				ArrayList<Point.Double> refNewFp = new ArrayList<>();
				for (Integer[] m : matchedFP) {
					if (m[0] < 0 | m[0] >= srcWork.number_of_feature_points() | m[1] < 0 | m[1] >= refWork.number_of_feature_points()) {
						continue;
					} else {
						srcNewFp.add(srcWork.get_feature_point(m[0]));
						refNewFp.add(refWork.get_feature_point(m[1]));
					}
				}

				PointRoi roiSrc = MiscHelper.convertFeaturePointsToPointRoi(srcNewFp, srcWork.pixelWidth(), srcWork.pixelHeight(), null);
				srcWork.setRoi(roiSrc);
				PointRoi roiRef = MiscHelper.convertFeaturePointsToPointRoi(refNewFp, refWork.pixelWidth(), refWork.pixelHeight(), null);
				refWork.setRoi(roiRef);
			} else {
				IJ.showMessage("Please match landmarks first!");
				return false;
			}
		} else {
			srcWork.killRoi();
			refWork.killRoi();
		}

		Param param = new Param(
				MainDialog.MONO_MODE,
				0,
				minDeformationScale,
				maxDeformationScale,
				divWeight,
				curlWeight,
				landmarkWeight,
				imageWeight,
				10.0,
				0.1
		);

		Transformation warp = bUnwarpJ_.computeTransformationBatch(
				refWork, srcWork,
				null, null,
				param
		);
		if (warp == null) return false;

		cx = warp.getDirectDeformationCoefficientsX();
		cy = warp.getDirectDeformationCoefficientsY();
		dx = MiscHelper.interpolateMatrixFromCoefficients(cx, dxyReference.getWidth(), dxyReference.getHeight());
		dy = MiscHelper.interpolateMatrixFromCoefficients(cy, dxyReference.getWidth(), dxyReference.getHeight());

		// bUnwarpJ uses deformationfields with absolute coordinates whereas Correlia works with relative coords
		// This is corrected with the following loops
		for (int v = 0; v < dxyReference.getHeight(); v++) {
			for (int u = 0; u < dxyReference.getWidth(); u++) {
				dx[v][u] -= u;
				dy[v][u] -= v;
			}
		}

		return true;
	}

	public JPanel propPanel() {
		JPanel propPane = new JPanel();
		propPane.setLayout(new BoxLayout(propPane, BoxLayout.PAGE_AXIS));

		JPanel paneMatch = new JPanel();
		paneMatch.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				"Landmark matching", TitledBorder.LEFT, TitledBorder.TOP));
		paneMatch.setLayout(new GridLayout(0, 2, INNER_PADDING, INNER_PADDING));


		JButton btnEditFeatures = new JButton("Add & Edit");
		btnEditFeatures.addActionListener(e -> {
			editLandmarks();
		});
		paneMatch.add(btnEditFeatures);

		JButton btnMatch = new JButton("Match");
		btnMatch.addActionListener(e -> {
			ArrayList<Integer[]> res = matchLandmarks(matchedFP);
			if (res != null) {
				matchedFP = res;
				setChanged(true);
			}
		});
		paneMatch.add(btnMatch);

		// Pane with two columns for labels and textfields
		JPanel twoColPane = new JPanel();
		twoColPane.setLayout(new GridLayout(0, 2, INNER_PADDING, INNER_PADDING));

		twoColPane.add(new JLabel("Image weight"));
		slImageWeight = new JSlider(JSlider.HORIZONTAL, 0, 100, (int)(imageWeight * 100));
		slImageWeight.setEnabled(true);
		slImageWeight.addChangeListener(
				e -> set_imageWeight(slImageWeight.getValue() / 100.0)
		);
		twoColPane.add(slImageWeight);

		twoColPane.add(new JLabel("Landmark weight"));
		slLandmarkWeight = new JSlider(JSlider.HORIZONTAL, 0, 100, (int)(landmarkWeight * 100));
		slLandmarkWeight.setEnabled(true);
		slLandmarkWeight.addChangeListener(
				e -> set_landmarkWeight(slLandmarkWeight.getValue() / 100.0)
		);
		twoColPane.add(slLandmarkWeight);

//		twoColPane.add(new JLabel("Divergence weight"));
//		slDivWeight = new JSlider(JSlider.HORIZONTAL, 0, 100, (int)(divWeight * 100));
//		slDivWeight.setEnabled(true);
//		slDivWeight.addChangeListener(
//				e -> set_divWeight(slDivWeight.getValue() / 100.0)
//		);
//		twoColPane.add(slDivWeight);
//
//		twoColPane.add(new JLabel("Curl weight"));
//		slCurlWeight = new JSlider(JSlider.HORIZONTAL, 0, 100, (int)(divWeight * 100));
//		slCurlWeight.setEnabled(true);
//		slCurlWeight.addChangeListener(
//				e -> set_curlWeight(slCurlWeight.getValue() / 100.0)
//		);
//		twoColPane.add(slCurlWeight);

		twoColPane.add(new JLabel("Deformation scale"));
		rslDeformationScale = new RangeSlider(0, 4, minDeformationScale, maxDeformationScale);
		rslDeformationScale.setMajorTickSpacing(1);
		rslDeformationScale.setPaintTicks(true);
		rslDeformationScale.setSnapToTicks(true);
		rslDeformationScale.setPaintLabels(true);
		Dictionary dict = new Hashtable();
		for (int i = 0; i <= 4; i++) {
			String s = "";
			if (i == 0) s = "Coarse";
			if (i == 4) s = "Fine";
			dict.put(i, new JLabel(s));
		}
		rslDeformationScale.setLabelTable(dict);
		rslDeformationScale.addChangeListener(
				e -> {
					set_minDeformationScale(rslDeformationScale.getValue());
					set_maxDeformationScale(rslDeformationScale.getUpperValue());
				}
		);
		twoColPane.add(rslDeformationScale);

		propPane.add(paneMatch);
		propPane.add(twoColPane);

		return propPane;
	}

	public double get_imageWeight() {
		return imageWeight;
	}

	public void set_imageWeight(double d) {
		imageWeight = d;
		setChanged(true);
	}

	public double get_landmarkWeight() {
		return landmarkWeight;
	}

	public void set_landmarkWeight(double d) {
		landmarkWeight = d;
		setChanged(true);
	}

	public double get_divWeight() {
		return divWeight;
	}

	public void set_divWeight(double d) {
		divWeight = d;
		setChanged(true);
	}

	public double get_curlWeight() {
		return curlWeight;
	}

	public void set_curlWeight(double d) {
		curlWeight = d;
		setChanged(true);
	}

	public int get_minDeformationScale() {
		return minDeformationScale;
	}

	public void set_minDeformationScale(int i) {
		minDeformationScale = i;
		setChanged(true);
	}

	public int get_maxDeformationScale() {
		return maxDeformationScale;
	}

	public void set_maxDeformationScale(int i) {
		maxDeformationScale = i;
		setChanged(true);
	}

}
