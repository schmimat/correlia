/*
 * Transformation type that applies nonlinear registration by iteratively modifying the target image and
 * measuring the similarity with the reference with mutual information
 * Author: Florens Rohde
 *
 * Part of:
 * Correlia plugin for ImageJ and FIJI
 */
package de.ufz.correlia;
import de.ufz.correlia.slider.RangeSlider;
import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.Roi;
import ij.process.ImageProcessor;
import org.w3c.dom.Element;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.Hashtable;

public class transformationGlobalMI extends transformation {
	public static final String NAME = "Global Mutual Information";
	public static final int MAX_INTERVALS = 4;
	public static final int MAX_ITERATIONS = 21;
	public static final int SPLINEGRID_LOWEND = 0;
	public static final int SPLINEGRID_HIEND = 5;
	public static final int DOWNSCALE_WIDTH = 512;
	public static final double STEP_SIZE_REDUCTION = 0.67;
	public static final int TYPE_ALL = 0;
	public static final int TYPE_MARGIN = 1;
	public static final int TYPE_ROI = 2;

	private double[][] cx;
	private double[][] cy;

	private int intervals;
	private int iterations;
	private int minSplineGrid;
	private int maxSplineGrid;
	private int type;
	private Roi roi = null;

	private microscopyImage extImage;
	private boolean hide_roi_msg = false;

	private JSlider slIntervals;
	private JSlider slIterations;
	private RangeSlider rslSplineGrid;
	private JCheckBox cbUseDeformationField;
	private JComboBox cmbType;
	private JButton btnSelectRoi;
	private Roi roiSel;

	private Instant start;

	transformationGlobalMI(xmlHandler xml, Element root, correlia prj, transformation prevT, microscopyImageWarped src) {
		super(xml, root, prj, prevT, src);

		intervals = xml.getIntByElementName(root, "intervals");
		iterations = xml.getIntByElementName(root, "iterations");

		minSplineGrid = xml.getIntAttributeByElementName(root, "approximationGrid", "min");
		maxSplineGrid = xml.getIntAttributeByElementName(root, "approximationGrid", "max");

		type = xml.getIntByElementName(root, "type");

		if (xml.getTextByElementName(root, "roi", 0) != null) {
			roi = new Roi(xml.getDoubleAttributeByElementName(root, "roi", 0, "x"),
					xml.getDoubleAttributeByElementName(root, "roi", 0, "y"),
					xml.getDoubleAttributeByElementName(root, "roi", 0, "w"),
					xml.getDoubleAttributeByElementName(root, "roi", 0, "h")
			);
		}

		Element edx = xml.getElementByName(root, "dx");
		if (edx != null) {
			cx = readCoefficientsFromXML(xml, edx);
		}

		Element edy = xml.getElementByName(root, "dy");
		if (edy != null) {
			cy = readCoefficientsFromXML(xml, edy);
		}

		if (edx != null && edy != null) {
			dx = MiscHelper.interpolateMatrixFromCoefficients(cx, src.getWidth(), src.getHeight());
			dy = MiscHelper.interpolateMatrixFromCoefficients(cy, src.getWidth(), src.getHeight());
		}
	}

	transformationGlobalMI(correlia prj, String originalID, transformation prevTrans) {
		super(NAME, prj, originalID, prevTrans);
		intervals = 1;
		iterations = 5;
		type = TYPE_ALL;

		for (int i = 0; i < smooth.length; i++) {
			smooth[i] = 2;
		}

		if (previousTransformation != null) {
			if (previousTransformation instanceof transformationGlobalMI) {
				transformationGlobalMI t = (transformationGlobalMI) previousTransformation;
				intervals = Math.min(t.get_intervals() + 1, MAX_INTERVALS);
			}
		}

		minSplineGrid = 0;
		maxSplineGrid = Math.min(Math.max(2, intervals + 1), SPLINEGRID_HIEND);
	}

	transformationGlobalMI(transformationGlobalMI t) {
		super(t);

		intervals = t.get_intervals();
		iterations = t.get_iterations();
		minSplineGrid = t.get_minSplineGrid();
		maxSplineGrid = t.get_maxSplineGrid();
		type = t.get_type();
		if (t.get_roi() != null) {
			roi = new Roi(t.get_roi().getBounds());
		}

		if (t.cx != null) {
			cx = new double[t.cx.length][t.cx[0].length];
			for (int i = 0; i < cx.length; i++) {
				cx[i] = Arrays.copyOf(t.cx[i], t.cx[i].length);
			}
		}
		if (t.cy != null) {
			cy = new double[t.cy.length][t.cy[0].length];
			for (int i = 0; i < cy.length; i++) {
				cy[i] = Arrays.copyOf(t.cy[i], t.cy[i].length);
			}
		}
	}

	@Override
	public Element buildXML(xmlHandler xml, boolean recipe) {
		ArrayList<String> params;
		Element root = super.buildXML(xml, recipe);

		xml.addTextElement(root, "intervals", Integer.toString(intervals));
		xml.addTextElement(root, "iterations", Integer.toString(iterations));

		params = new ArrayList<>();
		params.add("min");
		params.add(Integer.toString(minSplineGrid));
		params.add("max");
		params.add(Integer.toString(maxSplineGrid));
		xml.addTextElementWithAttributes(root, "approximationGrid", "", params);

		xml.addTextElement(root, "type", type);

		if (type == TYPE_ROI && roi != null) {
			params = new ArrayList<>();
			params.add("x");
			params.add(Double.toString(roi.getXBase()));
			params.add("y");
			params.add(Double.toString(roi.getYBase()));
			params.add("w");
			params.add(Double.toString(roi.getFloatWidth()));
			params.add("h");
			params.add(Double.toString(roi.getFloatHeight()));
			xml.addTextElementWithAttributes(root, "roi", "", params);
		}

		if (!recipe) {
			writeCoefficientsToXML(xml, root, "dx", cx);
			writeCoefficientsToXML(xml, root, "dy", cy);
		}

		return root;
	}

	public boolean calc() {
		debug.put(" entered ("+getName()+")");

		microscopyImage[] mi = {new microscopyImage(srcWork), new microscopyImage(refWork)};
		ImageProcessor[][] ipOrig = new ImageProcessor[mi.length][2];
		ImageProcessor[][] ipOrigScaled = new ImageProcessor[mi.length][2];
		ImageProcessor[][] ip = new ImageProcessor[mi.length][2];
		for (int i = 0; i < mi.length; i++) {
			for (int s = 0; s < 2; s++) {
				ipOrig[i][s] = mi[i].getStack().getProcessor(s + 1);
				ipOrigScaled[i][s] = ipOrig[i][s].resize(DOWNSCALE_WIDTH);
				ip[i][s] = ipOrigScaled[i][s].duplicate();
			}
		}

		deformations.clear();
		double w = (double)dxyReference.getWidth();
		double h = (double)dxyReference.getHeight();
		double x0 = 0;
		double y0 = 0;
		if (type == TYPE_ROI) {
			if (roi == null) {
				IJ.showMessage("Please select a roi first!");
				return false;
			}
			w = roi.getFloatWidth();
			h = roi.getFloatHeight();
			x0 = roi.getXBase();
			y0 = roi.getYBase();
		}
		for (int v = 0; v <= intervals; v++) {
			for (int u = 0; u <= intervals; u++) {
				if ((type == TYPE_MARGIN) && !(u == 0 || u == intervals || v == 0 || v == intervals)) {
					continue;
				}
				double xTmp = x0 + u * w / (double)intervals;
				double yTmp = y0 + v * h / (double)intervals;
				int x = Math.min((int)Math.round(x0 + u * w / (double)intervals), dxyReference.getWidth() - 1);
				int y = Math.min((int)Math.round(y0 + v * h / (double)intervals), dxyReference.getHeight() - 1);
				double dx = 0;
				double dy = 0;
				if (useDeformationField) {
					dx = MiscHelper.interpolateValueFromArray(previousTransformation.dx, x, y);
					dy = MiscHelper.interpolateValueFromArray(previousTransformation.dy, x, y);
				}
				double weight = 0.2;
				if (type== TYPE_ROI) {
					weight = 0.5;
				}
				deformations.add(new deformationHandle(x, y, dx, dy, 0.2));
			}
		}

		double[] min = {0, 0};
		double[] max = {ipOrigScaled[0][0].getMax(), ipOrigScaled[1][0].getMax()};
		int nbins = 256;
		int type = mutualInformationFast.PLAIMI;

		double[][] stepSize = new double[deformations.size()][2];
		for (int i = 0; i < stepSize.length; i++) {
			stepSize[i] = guessInitialStepSize(deformations.get(i).getX(), deformations.get(i).getY());
		}

		ip[0] = applyDeformations(deformations, ipOrigScaled[0]);
		mutualInformationFast MI = new mutualInformationFast(ip, type, nbins, min, max);
		double bestMI = MI.lastResult;

		ImageStack ims = new ImageStack(ipOrigScaled[0][0].getWidth(), ipOrigScaled[1][0].getHeight());
		ims.addSlice("Orig Src", ipOrigScaled[0][0]);
		ims.addSlice("Orig Ref", ipOrigScaled[1][0]);
		ImageStack imsStep = new ImageStack(ipOrigScaled[0][0].getWidth(), ipOrigScaled[1][0].getHeight());
		imsStep.addSlice("Orig Src", ipOrigScaled[0][0]);
		imsStep.addSlice("Orig Ref", ipOrigScaled[1][0]);

		ArrayList<deformationHandle> bestDeformations = new ArrayList<>();
		for (deformationHandle dH : deformations) {
			bestDeformations.add(new deformationHandle(dH));
		}
		for (int step = 0; step < iterations; step++) {
			int foundBetterCounter = bestDeformations.size();
			for (int d = 0; d < bestDeformations.size(); d++) {
				boolean foundBetter = false;
				double[] dxyCurBest = {bestDeformations.get(d).getDx(), bestDeformations.get(d).getDy()};
				double[][] grad = new double[3][3];
				ArrayList<deformationHandle> curDeformations = new ArrayList<>();
				for (deformationHandle dH : bestDeformations) {
					curDeformations.add(new deformationHandle(dH));
				}
				for (int ty = 0; ty < grad.length; ty++) {
					for (int tx = 0; tx < grad[0].length; tx++) {
						double newDx = bestDeformations.get(d).getDx();
						newDx += (tx-1) * stepSize[d][0];
						double newDy = bestDeformations.get(d).getDy();
						newDy += (ty-1) * stepSize[d][1];
						curDeformations.get(d).setDx(newDx);
						curDeformations.get(d).setDy(newDy);

						ip[0] = applyDeformations(curDeformations, ipOrigScaled[0]);
						mutualInformationFast curMI = new mutualInformationFast(ip, type, nbins, min, max);
						if (curMI.lastResult > bestMI) {
							dxyCurBest[0] = curDeformations.get(d).getDx();
							dxyCurBest[1] = curDeformations.get(d).getDy();
							debug.put("("+d+") "+curMI.lastResult + ">" + bestMI + ": " + tx + "-" + ty);
							bestMI = curMI.lastResult;
							foundBetter = true;
						}
					}
				}
				if (foundBetter) {
					bestDeformations.get(d).setDx(dxyCurBest[0]);
					bestDeformations.get(d).setDy(dxyCurBest[1]);
				} else {
					if (dxyCurBest[0] == bestDeformations.get(d).getDx()) {
						stepSize[d][0] = Math.max(stepSize[d][0] * STEP_SIZE_REDUCTION, 1);
					}
					if (dxyCurBest[1] == bestDeformations.get(d).getDy()) {
						stepSize[d][1] = Math.max(stepSize[d][1] * STEP_SIZE_REDUCTION, 1);
					}
					if (stepSize[d][0] == 1 && stepSize[d][1] == 1) foundBetterCounter--;
					debug.put("new stepsize ("+d+"): " + stepSize[d][0] + "-" + stepSize[d][1]);
				}
				if (debug.is_debugging()) {
					ip[0] = applyDeformations(bestDeformations, ipOrigScaled[0]);
					imsStep.addSlice("Src final (step=" + step + ", dH=" + d + ")", ip[0][0]);
				}
			}
			if (debug.is_debugging()) {
				ip[0] = applyDeformations(bestDeformations, ipOrigScaled[0]);
				ims.addSlice("Src (step=" + step + ", final)", ip[0][0]);
			}
			applyDeformations(bestDeformations, ipOrig[0]);
			for (deformationHandle dH : bestDeformations) {
				debug.put("pre: " + dH.getDx() + ":" + dH.getDy());
				dH.setDx(dx[(int)dH.getY()][(int)dH.getX()]);
				dH.setDy(dy[(int)dH.getY()][(int)dH.getX()]);
				debug.put("post: " + dH.getDx() + ":" + dH.getDy());
			}
			if (debug.is_debugging()) {
				ip[0] = applyDeformations(bestDeformations, ipOrigScaled[0]);
				ims.addSlice("Src (step=" + step + ", final)", ip[0][0]);
			}
			if (foundBetterCounter == 0) break;
		}
		if (debug.is_debugging()) {
			(new ImagePlus("Deformed images", ims)).show();
			(new ImagePlus("Deformed images (fine steps)", imsStep)).show();
		}

		ip[0] = applyDeformations(bestDeformations, ipOrig[0]);

		if (debug.is_debugging()) {
			ImageStack imsDeformation;
			imsDeformation = new ImageStack(dx[0].length, dx.length);

			double[][] spXYold = new double[deformations.size()][4];
			for (int i = 0; i < spXYold.length; i++) {
				spXYold[i][0] = deformations.get(i).getX();
				spXYold[i][1] = deformations.get(i).getY();
				spXYold[i][2] = deformations.get(i).getDx();
				spXYold[i][3] = deformations.get(i).getDy();
			}
			double[][] spXY = new double[deformations.size()][4];
			for (int i = 0; i < spXY.length; i++) {
				spXY[i][0] = bestDeformations.get(i).getX();
				spXY[i][1] = bestDeformations.get(i).getY();
				spXY[i][2] = bestDeformations.get(i).getDx();
				spXY[i][3] = bestDeformations.get(i).getDy();
			}
			int[] size = {dxyReference.getWidth(), dxyReference.getHeight()};
			double[][][] cOld = MiscHelper.getBSplineCoefficientsFromScatteredPoints(deformations, size, (int)Math.pow(2, minSplineGrid), (int)Math.pow(2, maxSplineGrid));
			double[][] dxOld = MiscHelper.interpolateMatrixFromCoefficients(cOld[0], dxyReference.getWidth(), dxyReference.getHeight());
			double[][] dyOld = MiscHelper.interpolateMatrixFromCoefficients(cOld[1], dxyReference.getWidth(), dxyReference.getHeight());
			imsDeformation.addSlice("Control point deformations (previous)", MiscHelper.deformationArrowField(spXYold, dx[0].length, dx.length, 1, 0));
			imsDeformation.addSlice("Interpolated deformations (previous)", MiscHelper.deformationArrowField(dxOld, dyOld, 1, 0));
			imsDeformation.addSlice("Control point deformations", MiscHelper.deformationArrowField(spXY, dx[0].length, dx.length, 1, 0));
			imsDeformation.addSlice("Interpolated deformations", MiscHelper.deformationArrowField(dx, dy, 1, 0));
			new ImagePlus("Deformation fields", imsDeformation).show();
		}

		deformations.clear();
		if (useDeformationField) {
			for (deformationHandle dH : previousTransformation.deformations) {
				deformations.add(new deformationHandle(dH));
			}
		}
		deformations.addAll(bestDeformations);

		return true;
	}

	/**
	 * Transform the image processor by using a list of deformationHandles
	 * List can be extended with deformationHandles of the previous transformation
	 * @param defHandles list of deformationHandles
	 * @param ipIn input image processor (slice 0: image, slice 1: mask)
	 * @return output image processor (slice 0: image, slice 1: mask)
	 */
	private ImageProcessor[] applyDeformations(ArrayList<deformationHandle> defHandles, ImageProcessor[] ipIn) {
		ImageProcessor[] ipOut = new ImageProcessor[2];
		int[] size = {ipIn[0].getWidth(), ipIn[0].getHeight()};

		ArrayList<deformationHandle> defHandlesWithPrev = new ArrayList<>();
		defHandlesWithPrev.addAll(defHandles);
		if (useDeformationField) {
			defHandlesWithPrev.addAll(previousTransformation.deformations);
		}

		ArrayList<deformationHandle> defHandlesScale = new ArrayList<>();
		for (deformationHandle dH : defHandlesWithPrev) {
			deformationHandle dHScale = new deformationHandle(dH);
			dHScale.setX(dH.getX() * size[0] / dxyReference.getWidth());
			dHScale.setY(dH.getY() * size[1] / dxyReference.getHeight());
			dHScale.setDx(dH.getDx() * size[0] / dxyReference.getWidth());
			dHScale.setDy(dH.getDy() * size[1] / dxyReference.getHeight());
			defHandlesScale.add(dHScale);
		}
		double[][][] c = MiscHelper.getBSplineCoefficientsFromScatteredPoints(defHandlesScale, size, (int)Math.pow(2, minSplineGrid), (int)Math.pow(2, maxSplineGrid));
		cx = c[0];
		cy = c[1];

		dx = MiscHelper.interpolateMatrixFromCoefficients(cx, ipIn[0].getWidth(), ipIn[0].getHeight());
		dy = MiscHelper.interpolateMatrixFromCoefficients(cy, ipIn[0].getWidth(), ipIn[0].getHeight());

		for (int i = 0; i < 2; i++) {
			ipOut[i] = applyDeformationField(ipIn[i]);
		}
		return ipOut;
	}

	public JPanel propPanel() {
		JPanel propPane = new JPanel();
		propPane.setLayout(new BoxLayout(propPane, BoxLayout.PAGE_AXIS));

		JPanel paneDefHandles = new JPanel();
		paneDefHandles.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				"Placement of deformationhandles", TitledBorder.LEFT, TitledBorder.TOP));
		paneDefHandles.setLayout(new GridLayout(0, 2, INNER_PADDING, INNER_PADDING));

		String[] options = {"Full image", "Along margins", "Only in a ROI"};
		cmbType = new JComboBox<>(options);
		cmbType.setSelectedIndex(type);
		cmbType.addItemListener(e -> {
			if (cmbType.getSelectedIndex() == TYPE_ROI) {
				btnSelectRoi.setEnabled(true);
			} else {
				btnSelectRoi.setEnabled(false);
			}
			set_type(cmbType.getSelectedIndex());
		});
		paneDefHandles.add(cmbType);

		btnSelectRoi = new JButton("Select ROI");
		btnSelectRoi.addActionListener(e -> {
			if (extImage != null && extImage.isVisible()) {
				roiSel = extImage.getRoi();
				set_roi(roiSel);
				extImage.close();
				if (roiSel != null)	updateSplineGridParameter();
			} else {
				if (!hide_roi_msg) {
					IJ.showMessage("Draw the roi and confirm it by clicking again on the button");
					hide_roi_msg = true;
				}
				if (srcWork == null) createWorkingImages(true);
				extImage = new microscopyImage(srcWork);
				extImage.setTitle("Use the rectangle tool to select the roi");
				if (roiSel != null) {
					extImage.setRoi(new Roi(roiSel.getBounds()));
				}
				extImage.show(microscopyImage.MODE_SHOW);
			}
		});
		if (type == TYPE_ROI) {
			btnSelectRoi.setEnabled(true);
			roiSel = new Roi(roi.getBounds());
		} else {
			btnSelectRoi.setEnabled(false);
			roiSel = null;
		}
		paneDefHandles.add(btnSelectRoi);

		paneDefHandles.add(new JLabel("<html>Number per<br />width / height</html>"));
		slIntervals = new JSlider(JSlider.HORIZONTAL, 1, MAX_INTERVALS, intervals);
		slIntervals.setMajorTickSpacing(1);
		slIntervals.setPaintTicks(true);
		slIntervals.setPaintLabels(true);
		slIntervals.setSnapToTicks(true);
		slIntervals.setEnabled(true);
		slIntervals.addChangeListener(e -> {
			set_intervals(slIntervals.getValue());
			if (type == TYPE_ROI && roiSel != null) updateSplineGridParameter();
		});
		paneDefHandles.add(slIntervals);


		JPanel twoColPane = new JPanel(new GridLayout(0, 2, INNER_PADDING, INNER_PADDING));

		cbUseDeformationField = new JCheckBox("Use prev. Def.");
		cbUseDeformationField.setSelected(useDeformationField);
		cbUseDeformationField.addChangeListener(
				e -> set_useDeformationField(cbUseDeformationField.isSelected())
		);
		twoColPane.add(cbUseDeformationField);
		twoColPane.add(Box.createHorizontalGlue());

		twoColPane.add(new JLabel("Max. iterations"));
		slIterations = new JSlider(JSlider.HORIZONTAL, 1, MAX_ITERATIONS, iterations);
		slIterations.setMajorTickSpacing(4);
		slIterations.setMinorTickSpacing(1);
		slIterations.setPaintTicks(true);
		slIterations.setPaintLabels(true);
		slIterations.setSnapToTicks(false);
		slIterations.setEnabled(true);
		slIterations.addChangeListener(
				e -> set_iterations(slIterations.getValue())
		);
		twoColPane.add(slIterations);

		twoColPane.add(new JLabel("Deformation scale"));
		rslSplineGrid = new RangeSlider(SPLINEGRID_LOWEND, SPLINEGRID_HIEND, minSplineGrid, maxSplineGrid);
		rslSplineGrid.setMajorTickSpacing(1);
		rslSplineGrid.setPaintTicks(true);
		rslSplineGrid.setPaintLabels(true);
		rslSplineGrid.setSnapToTicks(true);
		Dictionary dict = new Hashtable();
		for (int i = 0; i <= SPLINEGRID_HIEND - SPLINEGRID_LOWEND; i ++) {
			String s = "";
			if (i == 0) s = "Coarse";
			if (i == SPLINEGRID_HIEND - SPLINEGRID_LOWEND) s = "Fine";
			dict.put(i, new JLabel(s));
		}
		rslSplineGrid.setLabelTable(dict);
		rslSplineGrid.addChangeListener(
				e -> {
					set_minSplineGrid(rslSplineGrid.getValue());
					set_maxSplineGrid(rslSplineGrid.getUpperValue());
				}
		);
		twoColPane.add(rslSplineGrid);

		propPane.add(paneDefHandles);
		propPane.add(twoColPane);
		return propPane;
	}

	/**
	 * Guess lower and higher spline grid density from the size of the roi
	 */
	private void updateSplineGridParameter() {
		int intervalsTmp = slIntervals.getValue();
		int minSplineGridTmp = SPLINEGRID_LOWEND;
		while (Math.pow(2, minSplineGridTmp) <= dxyReference.getWidth() / roiSel.getFloatWidth() ||
				Math.pow(2, minSplineGridTmp) <= dxyReference.getHeight() / roiSel.getFloatHeight()) {
			minSplineGridTmp++;
		}
		minSplineGridTmp = Math.max(SPLINEGRID_LOWEND, minSplineGridTmp - 1);
		debug.put(Math.pow(2, minSplineGridTmp) + " < (" + (int)Math.round(dxyReference.getWidth() / roiSel.getFloatWidth()) + " || " +
				(int)Math.round(dxyReference.getHeight() / roiSel.getFloatHeight()) + ")");
		int maxSplineGridTmp = SPLINEGRID_HIEND;
		while (Math.pow(2, maxSplineGridTmp) >= (int)Math.round(dxyReference.getWidth() * intervalsTmp / roiSel.getFloatWidth()) ||
				Math.pow(2, maxSplineGridTmp) >= (int)Math.round(dxyReference.getHeight() * intervalsTmp / roiSel.getFloatHeight())) {
			maxSplineGridTmp--;
		}
		maxSplineGridTmp = Math.min(SPLINEGRID_HIEND, maxSplineGridTmp + 1);
		debug.put(Math.pow(2, maxSplineGridTmp) + " > (" + dxyReference.getWidth() * intervalsTmp / roiSel.getFloatWidth() + " || " +
				dxyReference.getHeight() * intervalsTmp / roiSel.getFloatHeight() + ")");

		rslSplineGrid.setLowerAndUpperValue(minSplineGridTmp, maxSplineGridTmp);
	}

	public boolean get_useDeformationField() {
		return useDeformationField;
	}

	public void set_useDeformationField(boolean b) {
		if (b == useDeformationField) return;
		useDeformationField = b;
		setChanged(true);
	}

	public int get_type() {
		return type;
	}

	public void set_type(int d) {
		if (d == type) return;
		type = d;
		setChanged(true);
	}

	public Roi get_roi() {
		return roi;
	}

	public void set_roi(Roi r) {
		if (r == null) return;
		if (roi != null) {
			if (r.getFloatWidth() == roi.getFloatWidth() &&
					r.getFloatHeight() == roi.getFloatHeight() &&
					r.getXBase() == roi.getXBase() &&
					r.getYBase() == roi.getYBase()) return;
		}
		roi = new Roi(r.getBounds());
		setChanged(true);
	}

	public int get_intervals() {
		return intervals;
	}

	public void set_intervals(int d) {
		if (d == intervals) return;
		intervals = d;
		setChanged(true);
	}
	
	public int get_iterations() {
		return iterations;
	}

	public void set_iterations(int d) {
		if (d == iterations) return;
		iterations = d;
		setChanged(true);
	}

	public int get_minSplineGrid() {
		return minSplineGrid;
	}

	public void set_minSplineGrid(int d) {
		if (d == minSplineGrid) return;
		minSplineGrid = d;
		setChanged(true);
	}

	public int get_maxSplineGrid() {
		return maxSplineGrid;
	}

	public void set_maxSplineGrid(int d) {
		if (d == maxSplineGrid) return;
		maxSplineGrid = d;
		setChanged(true);
	}
}
