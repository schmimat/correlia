/*
 * Dialog for editing the properties of a correlia project
 * Author: Florens Rohde
 *
 * Part of:
 * Correlia plugin for ImageJ and FIJI
 */

package de.ufz.correlia;

import javax.swing.*;
import java.awt.*;

class dlgEditProjectProperties extends JDialog {
	private static final Integer TEXTFIELD_WIDTH = 40;
	private static final Integer TEXTAREA_HEIGHT = 6;
	private static final Integer DLG_MINIMUM_SIZE = 400;
	private static final Integer DLG_INNER_PADDING = 20;
	private static final Integer DLG_OUTER_PADDING = 20;
	private static final Integer DLG_FONT_SIZE = 14;
	private Boolean wasCancelled;
	private JTextField tfTitle;
	private JTextArea taAdditionalInfo;
	private JTextArea taExperimenter;
	private String title;
	private String experimenter = "";
	private String additionalInfo = "";

	dlgEditProjectProperties(correlia prj) {
		debug.put(" entered");
		title = prj.get_projectTitle();
		additionalInfo = prj.get_additionalInfo();
		String tmp;
		StringBuilder builder = new StringBuilder();

		// Iterate over baseImage and overlays
		for (int i = 0; i <= prj.images.size(); i++) {
			tmp = prj.get_image(i).get_experimenter();
			if (!tmp.equals("") && !builder.toString().contains(tmp)) {
				builder.append(tmp);
				builder.append(System.getProperty("line.separator"));
			}
		}
		experimenter = builder.toString();
		setup_dialog();

	}

	private boolean setup_dialog() {
		this.setTitle("Edit Project Properties");
		this.setModalityType(ModalityType.APPLICATION_MODAL);
		this.setMinimumSize(new Dimension(DLG_MINIMUM_SIZE, DLG_MINIMUM_SIZE));

		JPanel centerPane = new JPanel();
		centerPane.setLayout(new BoxLayout(centerPane, BoxLayout.PAGE_AXIS));
		centerPane.setBorder(BorderFactory.createEmptyBorder(0, 0, DLG_INNER_PADDING, 0));

		// Pane with two columns for labels and textfields
		JPanel twoColPane = new JPanel();
		twoColPane.setLayout(new GridLayout(0, 2));

		twoColPane.add(new JLabel("Title:"));
		tfTitle = new JTextField(title, TEXTFIELD_WIDTH);
		twoColPane.add(tfTitle);
		centerPane.add(twoColPane);

		// Additional info
		JPanel taPane = new JPanel();
		taPane.setLayout(new BorderLayout());
		taPane.setBorder(BorderFactory.createEmptyBorder(DLG_INNER_PADDING, 0, 0, 0));
		taPane.add(new JLabel("Additional information:", SwingConstants.LEFT), BorderLayout.PAGE_START);

		taAdditionalInfo = new JTextArea(additionalInfo);
		JScrollPane spAdditionalInfo = new JScrollPane(taAdditionalInfo);
		spAdditionalInfo.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		spAdditionalInfo.setFont(new Font(Font.MONOSPACED, Font.PLAIN, DLG_FONT_SIZE));
		taPane.add(spAdditionalInfo, BorderLayout.CENTER);

		centerPane.add(taPane);

		// Experimenter
		taPane = new JPanel();
		taPane.setLayout(new BorderLayout());
		taPane.setBorder(BorderFactory.createEmptyBorder(DLG_INNER_PADDING, 0, 0, 0));
		taPane.add(new JLabel("Involved experimenter:", SwingConstants.LEFT), BorderLayout.PAGE_START);

		JTextArea taExperimenter = new JTextArea(experimenter);
		taExperimenter.setEditable(false);
		JScrollPane spExperimenter = new JScrollPane(taExperimenter);
		spExperimenter.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		spExperimenter.setFont(new Font(Font.MONOSPACED, Font.PLAIN, DLG_FONT_SIZE));
		taPane.add(spExperimenter, BorderLayout.CENTER);

		centerPane.add(taPane);

		// Button Pane
		JPanel buttomPane = new JPanel();
		buttomPane.setLayout(new BoxLayout(buttomPane, BoxLayout.X_AXIS));

		JButton btnOK = new JButton("OK");
		btnOK.addActionListener(
				e -> userConfirm(true)
		);
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(
				e -> userConfirm(false)
		);

		buttomPane.add(Box.createHorizontalGlue());
		buttomPane.add(btnOK);
		buttomPane.add(Box.createRigidArea(new Dimension(DLG_INNER_PADDING,0)));
		buttomPane.add(btnCancel);
		buttomPane.add(Box.createHorizontalGlue());

		JPanel container = new JPanel();
		container.setBorder(BorderFactory.createEmptyBorder(DLG_OUTER_PADDING, DLG_OUTER_PADDING, DLG_OUTER_PADDING, DLG_OUTER_PADDING));
		container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
		container.add(centerPane);
		container.add(buttomPane);

		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(container, BorderLayout.CENTER);

		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setVisible(true);
		return true;
	}

	public String get_title() {
		return tfTitle.getText();
	}

	public String get_additionalInfo() {
		return taAdditionalInfo.getText();
	}

	private void userConfirm(Boolean OKCancel) { // true = OK, false = Cancel
		wasCancelled = !OKCancel;
		this.setVisible(false);
	}

	public Boolean was_cancelled() {
		return wasCancelled;
	}

	static public String classID(){ return "dlgEditProjectProperties"; }
	static public String author(){ return "Florens Rohde"; }
	static public String version(){ return "June 10 2017"; }
}
