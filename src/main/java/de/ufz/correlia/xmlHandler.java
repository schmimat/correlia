package de.ufz.correlia;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import ij.IJ;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class xmlHandler {
	private DocumentBuilder docBuilder;
	private Document doc;
	private String savepath;

	/**
	 * Writer
	 */
	xmlHandler() {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		try {
			docBuilder = docFactory.newDocumentBuilder();
			doc = docBuilder.newDocument();
		} catch (ParserConfigurationException e) {
			IJ.log("XML creation failed");
		}

	}

	public boolean read(String path) {
		File f = new File(path);
		try {
			doc = docBuilder.parse(f);
		} catch (IOException | SAXException e) {
			IJ.log("Parsing xml input failed: " + path);
			return false;
		}
		doc.getDocumentElement().normalize();
		return true;
	}

	public boolean save() {
		if (savepath == null || savepath.isEmpty()) {
			return false;
		}

		try {
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(savepath));
			transformer.transform(source, result);
		} catch (TransformerException e) {
			IJ.log("Saving to xml file failed");
			return false;
		}
		return true;
	}

	public Document getDoc() {
		return doc;
	}

	public boolean setDoc(Document d) {
		if (d == null) {
			return false;
		}
		d.getDocumentElement().normalize();
		doc = d;
		return true;
	}

	public void addToDoc(Element e) {
		doc.appendChild(e);
	}

	public Element createElement(String s) {
		return doc.createElement(s);
	}

	public void addElement(Element root, Element e) {
		root.appendChild(e);
	}

	public Element addTextElement(Element root, String tag, int value) {
		return addTextElement(root, tag, Integer.toString(value));
	}

	public Element addTextElement(Element root, String tag, double value) {
		return addTextElement(root, tag, Double.toString(value));
	}

	public Element addTextElement(Element root, String tag, String value) {
		Element e = doc.createElement(tag);
		e.appendChild(doc.createTextNode(value));
		root.appendChild(e);
		return e;
	}

	public Element addTextElementWithAttributes(Element root, String tag, String value, ArrayList<String> attrPairs) {
		Element e = addTextElement(root, tag, value);
		return addAttributes(e, attrPairs);
	}

	public Element addAttributes(Element e, ArrayList<String> attrPairs) {
		for (int i = 0; i < attrPairs.size(); i = i + 2) {
			addAttribute(e, attrPairs.get(i), attrPairs.get(i+1));
		}
		return e;
	}

	public Element addAttribute(Element e, String key, int val) {
		return addAttribute(e, key, Integer.toString(val));
	}

	public Element addAttribute(Element e, String key, double val) {
		return addAttribute(e, key, Double.toString(val));
	}

	public Element addAttribute(Element e, String key, String val) {
		e.setAttribute(key, val);
		return e;
	}

	public Element getElementByAttribute(Element root, String tag, String key, int val) {
		return getElementByAttribute(root, tag, key, Integer.toString(val));
	}

	public Element getElementByAttribute(Element root, String tag, String key, String val) {
		NodeList nList = root.getElementsByTagName(tag);
		if (nList.getLength() == 0) {
			return null;
		}
		for (int i = 0; i < nList.getLength(); i++) {
			Element e = (Element) nList.item(i);
			if (e.getAttribute(key).equals(val)) {
				return e;
			}
		}
		return null;
	}

	public List<Element> getElementsByName(Element root, String tag) {
		NodeList nList = root.getElementsByTagName(tag);
		return asList(nList);
	}

	public Element getElementByName(Element root, String tag) {
		return getElementByName(root, tag, 0);
	}

	public Element getElementByName(Element root, String tag, int i) {
		NodeList nList = root.getElementsByTagName(tag);
		if (nList.getLength() == 0) {
			return null;
		}
		return (Element) nList.item(i);
	}

	public String getTextByElementName(Element root, String tag) {
		return getTextByElementName(root, tag, 0);
	}

	public String getTextByElementName(Element root, String tag, int i) {
		Element e = getElementByName(root, tag, i);
		if (e != null) {
			return e.getTextContent();
		} else {
			return null;
		}
	}

	public int getIntByElementName(Element root, String tag) {
		return getIntByElementName(root, tag, 0);
	}

	public int getIntByElementName(Element root, String tag, int i) {
		String s = getTextByElementName(root, tag, i);
		if (s == null) {
			return 0;
		} else {
			return Integer.parseInt(s);
		}
	}

	public double getDoublebyElementName(Element root, String tag) {
		return getDoublebyElementName(root, tag, 0);
	}

	public double getDoublebyElementName(Element root, String tag, int i) {
		String s = getTextByElementName(root, tag, i);
		if (s == null) {
			return 0;
		} else {
			return Double.parseDouble(s);
		}
	}

	public String getStringAttributeByElementName(Element root, String tag, String attr) {
		return getStringAttributeByElementName(root, tag, 0, attr);
	}

	public String getStringAttributeByElementName(Element root, String tag, int i, String attr) {
		Element e = getElementByName(root, tag, i);
		if (e != null) {
			return e.getAttribute(attr);
		} else {
			return null;
		}
	}

	public double getDoubleAttributeByElementName(Element root, String tag, String attr) {
		return getDoubleAttributeByElementName(root, tag, 0, attr);
	}

	public double getDoubleAttributeByElementName(Element root, String tag, int i, String attr) {
		Element e = getElementByName(root, tag, i);
		return getDoubleAttribute(e, attr);
	}

	public int getIntAttributeByElementName(Element root, String tag, String attr) {
		return getIntAttributeByElementName(root, tag, 0, attr);
	}

	public int getIntAttributeByElementName(Element root, String tag, int i, String attr) {
		Element e = getElementByName(root, tag, i);
		return getIntAttribute(e, attr);
	}

	public double getDoubleAttribute(Element e,String attr) {
		if (e != null) {
			return Double.parseDouble(e.getAttribute(attr));
		} else {
			return 0;
		}
	}

	public int getIntAttribute(Element e,String attr) {
		if (e != null) {
			return Integer.parseInt(e.getAttribute(attr));
		} else {
			return 0;
		}
	}

	public void setSavepath(String p) {
		savepath = p;
	}

	public String getSavepath() {
		return savepath;
	}

	// Make NodeList iterable via asList(NodeList)
	// https://stackoverflow.com/questions/19589231/can-i-iterate-through-a-nodelist-using-for-each-in-java
	public static List<Element> asList(NodeList n) {
		return n.getLength()==0?
				Collections.<Element>emptyList(): new NodeListWrapper(n);
	}

	static final class NodeListWrapper extends AbstractList<Element> implements RandomAccess {
		private final NodeList list;

		NodeListWrapper(NodeList l) {
			list = l;
		}

		public Element get(int index) {
			return (Element) list.item(index);
		}

		public int size() {
			return list.getLength();
		}
	}
}
