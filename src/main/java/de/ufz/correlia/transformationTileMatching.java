/*
 * Transformation type that applies nonlinear registration by splitting the iamges in tiles and aligning those
 * Author: Florens Rohde
 *
 * Part of:
 * Correlia plugin for ImageJ and FIJI
 */
package de.ufz.correlia;

import de.ufz.correlia.slider.RangeSlider;
import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.process.ImageProcessor;
import org.scijava.vecmath.Vector2d;
import org.w3c.dom.Element;

import javax.swing.*;
import java.awt.*;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.Hashtable;

public class transformationTileMatching extends transformation {
	public static final String NAME = "Tile matching";
	public static final int MAX_INTERVAL = 6;
	public static final int DEFAULT_INTERVALS = 2;
	public static final int SPLINEGRID_LOWEND = 0;
	public static final int SPLINEGRID_HIEND = 4;
	public static final int GINI_LOWEND = 5;
	public static final int GINI_HIEND = 40;
	public static final int GINI_STEP = 5;
	public static final double DEFAULT_GINITHRESHOLD = 0.15;
	public static final double REF_SIZE_LIMIT = 1.2;
	public static final double DEFAULT_SMOOTH = 2;
	public static final double EMPTY_TILES_LIMIT = 0.5;
	public static final double STEP_SIZE_REDUCTION = 0.67;

	private double[][] cx;
	private double[][] cy;

	private int intervals;
	private double giniThreshold;
	private boolean useOverlappingTiles;
	private int minSplineGrid;
	private int maxSplineGrid;


	private JSlider slIntervals;
	private JSlider slGiniThreshold;
	private JCheckBox cbUseDeformationField;
	private JCheckBox cbUseOverlappingTiles;
	private RangeSlider rslSplineGrid;

	private Instant start;

	transformationTileMatching(xmlHandler xml, Element root, correlia prj, transformation prevT, microscopyImageWarped src) {
		super(xml, root, prj, prevT, src);

		useOverlappingTiles = Boolean.parseBoolean(root.getAttribute("useOverlappingTiles"));
		giniThreshold = xml.getDoublebyElementName(root, "giniThreshold");
		intervals = xml.getIntByElementName(root, "intervals");

		minSplineGrid = xml.getIntAttributeByElementName(root, "approximationGrid", "min");
		maxSplineGrid = xml.getIntAttributeByElementName(root, "approximationGrid", "max");

		refSizeLimit = REF_SIZE_LIMIT;

		Element edx = xml.getElementByName(root, "dx");
		if (edx != null) {
			cx = readCoefficientsFromXML(xml, edx);
		}

		Element edy = xml.getElementByName(root, "dy");
		if (edy != null) {
			cy = readCoefficientsFromXML(xml, edy);
		}

		if (edx != null && edy != null) {
			dx = MiscHelper.interpolateMatrixFromCoefficients(cx, src.getWidth(), src.getHeight());
			dy = MiscHelper.interpolateMatrixFromCoefficients(cy, src.getWidth(), src.getHeight());
		}
	}

	transformationTileMatching(correlia prj, String originalID, transformation prevTrans) {
		super(NAME, prj, originalID, prevTrans);

		intervals = DEFAULT_INTERVALS;
		for (int i = 0; i < smooth.length; i++) {
			smooth[i] = DEFAULT_SMOOTH;
		}

		if (previousTransformation != null) {
			if (previousTransformation instanceof transformationTileMatching) {
				transformationTileMatching t = (transformationTileMatching) previousTransformation;
				intervals = Math.min(t.get_intervals() + 1, MAX_INTERVAL);
			}
		}

		minSplineGrid = 0;
		maxSplineGrid = Math.min(Math.max(2, intervals + 1), SPLINEGRID_HIEND);
		giniThreshold = DEFAULT_GINITHRESHOLD;
		useOverlappingTiles = true;

		refSizeLimit = REF_SIZE_LIMIT;
	}

	transformationTileMatching(transformationTileMatching t) {
		super(t);

		intervals = t.get_intervals();
		minSplineGrid = t.get_minSplineGrid();
		maxSplineGrid = t.get_maxSplineGrid();
		giniThreshold = t.get_giniThreshold();
		useOverlappingTiles = t.get_useOverlappingTiles();

		refSizeLimit = t.refSizeLimit;

		if (t.cx != null) {
			cx = new double[t.cx.length][t.cx[0].length];
			for (int i = 0; i < cx.length; i++) {
				cx[i] = Arrays.copyOf(t.cx[i], t.cx[i].length);
			}
		}
		if (t.cy != null) {
			cy = new double[t.cy.length][t.cy[0].length];
			for (int i = 0; i < cy.length; i++) {
				cy[i] = Arrays.copyOf(t.cy[i], t.cy[i].length);
			}
		}
	}

	@Override
	public Element buildXML(xmlHandler xml, boolean recipe) {
		ArrayList<String> params;
		Element root = super.buildXML(xml, recipe);

		xml.addAttribute(root, "useOverlappingTiles", Boolean.toString(useOverlappingTiles));
		xml.addTextElement(root, "giniThreshold", Double.toString(giniThreshold));
		xml.addTextElement(root, "intervals", Integer.toString(intervals));

		params = new ArrayList<>();
		params.add("min");
		params.add(Integer.toString(minSplineGrid));
		params.add("max");
		params.add(Integer.toString(maxSplineGrid));
		xml.addTextElementWithAttributes(root, "approximationGrid", "", params);

		if (!recipe) {
			writeCoefficientsToXML(xml, root, "dx", cx);
			writeCoefficientsToXML(xml, root, "dy", cy);
		}

		return root;
	}

	public boolean calc() {
		debug.put(" entered ("+getName()+")");
		microscopyImage[] mi = {srcWork, refWork};

		ImageStack ims;

		double q = (double) dxyReference.getWidth() / (double) dxyReference.getHeight();
		int w, h;
		if (q >= 1) {
			w = (int) Math.floor(intervals * q);
			h = intervals;
		} else {
			w = intervals;
			h = (int) Math.floor(intervals / q);
		}

		int[] bestT;
		int[][] tile0 = new int[2][2];
		int[] tileSize = {(int) Math.floor(mi[0].getWidth() / w), (int) Math.floor(mi[0].getHeight() / h)};

		ImageProcessor[][] ipFull = new ImageProcessor[2][2];

		for (int i = 0; i < 2; i++) {
			ipFull[0][i] = mi[0].getStack().getProcessor(i+1);
			ipFull[1][i] = mi[1].getStack().getProcessor(i+1);
		}

		ims = new ImageStack(tileSize[0], tileSize[1]);

		double dv = 1;
		double du = 1;
		if (useOverlappingTiles) {
			w = 2 * w - 1;
			h = 2 * h - 1;
			dv /= 2;
			du /= 2;
		}
		ArrayList<TilePair> tiles = new ArrayList<>();
		for (int v = 0; v < h; v ++) {
			for (int u = 0; u < w; u ++) {
				// Source
				tile0[0][0] = (int)Math.round(u * du * tileSize[0]);
				tile0[0][1] = (int)Math.round(v * dv * tileSize[1]);
				// Reference
				tile0[1][0] = tile0[0][0];
				tile0[1][1] = tile0[0][1];
				if (useDeformationField) {
					int[] center = {tile0[1][0] + tileSize[0] / 2, tile0[1][1] + tileSize[1] / 2};
					tile0[1][0] = (int)(center[0] - previousTransformation.dx[center[1]][center[0]]) - tileSize[0] / 2;
					tile0[1][1] = (int)(center[1] - previousTransformation.dy[center[1]][center[0]]) - tileSize[1] / 2;
				}
				tiles.add(new TilePair(ipFull, tile0, tileSize, offset));
				ims.addSlice(v * w + u + "(" + (int)u + ":" + (int)v + ") R: " + tiles.get(tiles.size() - 1).entropy[1], tiles.get(tiles.size() - 1).ip[1][0]);
				ims.addSlice(v * w + u + "(" + (int)u + ":" + (int)v + ") S: " + tiles.get(tiles.size() - 1).entropy[0], tiles.get(tiles.size() - 1).ip[0][0]);
			}
		}
		if (debug.is_debugging()) (new ImagePlus("Tiles", ims)).show();

		disableSecondaryTiles(tiles);

		ims = new ImageStack(tileSize[0], tileSize[1]);

		deformations.clear();
		if (useDeformationField) {
			deformations.addAll(previousTransformation.deformations);
		}

		boolean showMILandscapes = false;
		if (debug.is_debugging()) {
			showMILandscapes = JOptionPane.showConfirmDialog(
					null,
					"Generate Mutual Information Landscapes?\n",
					"Visual debugging?",
					JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
		}
		for (TilePair tile : tiles) {
			if (tile.ignore) continue;
			ims.addSlice(tile.tile0[0][0] + ":" + tile.tile0[0][1] + "R pre", tile.ip[1][0]);
			ims.addSlice(tile.tile0[0][0] + ":" + tile.tile0[0][1] + "S", tile.ip[0][0]);

			microscopyImage miImage = null;
			if (debug.is_debugging()) {
				if (showMILandscapes) {
					if (JOptionPane.showConfirmDialog(
							null,
							"Generate Mutual Information Landscape?\n"
							+ "tile " + tile.tile0[0][0] + ":" + tile.tile0[0][1],
							"Visual debugging?",
							JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
						int steps = 24;
						start = Instant.now();
						double[][] miMatrix = MiscHelper.generateMutualInformationLandscape(tile.ipFull, tile.tile0, tile.tileSize, tile.offset, steps);
						IJ.log("generateMutualInformationLandscape (" + steps + "): " + Duration.between(start, Instant.now()).toString());

						double[] stepSize = {1, 1};
						miImage = MiscHelper.interpolateImageFromSampleGrid(miMatrix, stepSize, 30);
						miImage.setTitle("MI-Matrix(" + tile.tile0[0][0] + ":" + tile.tile0[0][1]+ ")");
						miImage.add_feature_point(miImage.imgWidth() / 2, miImage.imgHeight() / 2);
					}
				}
			}

			int[][] stepRes = new int[20][2];
			start = Instant.now();
			bestT = findBestTranslation(tile, stepRes);
			debug.put("findBestTranslation ("+tile.tile0[0][0]+":"+tile.tile0[0][1]+"): " + Duration.between(start, Instant.now()).toString());

			for (int s = 0; s < stepRes.length; s++) {
				debug.put("s("+s+") "+stepRes[s][0] + ":" + stepRes[s][1]);
				if (stepRes[s][0] == 0 && stepRes[s][1] == 0) {
					continue;
				}
				if (debug.is_debugging() && miImage != null) {
					miImage.add_feature_point(stepRes[s][0] + miImage.imgWidth() / 2, stepRes[s][1] + miImage.imgHeight() / 2);
				}
			}
			if (debug.is_debugging() && miImage != null) miImage.show();

			deformations.add(new deformationHandle(
					tile.tile0[0][0] + tile.tileSize[0]/2,
					tile.tile0[0][1] + tile.tileSize[1]/2,
					tile.tile0[0][0] - tile.tile0[1][0] - bestT[0],
					tile.tile0[0][1] - tile.tile0[1][1] - bestT[1],
					0.5)
			);
			ims.addSlice(tile.tile0[0][0] + ":" + tile.tile0[0][1] + "R post", tile.getTile(1, bestT[0], bestT[1])[0]);
		}
		if (debug.is_debugging()) new ImagePlus("Tilecomparision", ims).show();

		double[][] spXYall = new double[deformations.size()][4];
		int[] max = {mi[0].getWidth(), mi[0].getHeight()};
		for (int i = 0; i < spXYall.length; i++) {
			spXYall[i][0] = deformations.get(i).getX();
			spXYall[i][1] = deformations.get(i).getY();
			spXYall[i][2] = deformations.get(i).getDx();
			spXYall[i][3] = deformations.get(i).getDy();
		}

		// Drop unlikely deformation handles
		double dxTmp[][];
		double dyTmp[][];
		if (useDeformationField) {
			dxTmp = previousTransformation.dx;
			dyTmp = previousTransformation.dy;
		} else {
			double[][][] c = MiscHelper.getBSplineCoefficientsFromScatteredPoints(deformations, max, (int)Math.pow(2, minSplineGrid), (int)Math.pow(2, maxSplineGrid / 2));
			dxTmp = MiscHelper.interpolateMatrixFromCoefficients(c[0], dxyReference.getWidth(), dxyReference.getHeight());
			dyTmp = MiscHelper.interpolateMatrixFromCoefficients(c[1], dxyReference.getWidth(), dxyReference.getHeight());
		}
		int d = 0;
		while (d < deformations.size()) {
			deformationHandle dH = deformations.get(d);
			double dxi = dxTmp[(int)dH.getY()][(int)dH.getX()];
			double dyi = dyTmp[(int)dH.getY()][(int)dH.getX()];

			Vector2d di = new Vector2d(dxi, dyi);
			Vector2d dh = new Vector2d((int)dH.getDx(), (int)dH.getDy());
			double diLen = di.length();
			double dhLen = dh.length();
			double diAng = Math.toDegrees(di.angle(dh));

			double limLenFac = 2;
			double limLenLo = diLen - limLenFac * diLen;
			double limLenHi = diLen + limLenFac * diLen;
			double limAng = 45;

			if (dhLen > limLenHi || dhLen < limLenLo) {
//				IJ.log("len: " + diLen + " vs" + dhLen);
				deformations.remove(d);
			} else if (diAng > limAng || diAng < -limAng) {
//				IJ.log("ang: " + diAng);
				deformations.remove(d);
			} else {
				d++;
			}
		}

		double[][] spXYclean = new double[deformations.size()][4];
		for (int i = 0; i < spXYclean.length; i++) {
			spXYclean[i][0] = deformations.get(i).getX();
			spXYclean[i][1] = deformations.get(i).getY();
			spXYclean[i][2] = deformations.get(i).getDx();
			spXYclean[i][3] = deformations.get(i).getDy();
		}

		double[][][] c = MiscHelper.getBSplineCoefficientsFromScatteredPoints(deformations, max, (int)Math.pow(2, minSplineGrid), (int)Math.pow(2, maxSplineGrid));
		cx = c[0];
		cy = c[1];

		dx = MiscHelper.interpolateMatrixFromCoefficients(cx, dxyReference.getWidth(), dxyReference.getHeight());
		dy = MiscHelper.interpolateMatrixFromCoefficients(cy, dxyReference.getWidth(), dxyReference.getHeight());

		if (debug.is_debugging()) {
			double[][] cmp = new double[spXYall.length][6];
			for (int i = 0; i < spXYall.length; i++) {
				cmp[i][0] = spXYall[i][0];
				cmp[i][1] = spXYall[i][1];
				cmp[i][2] = spXYall[i][2];
				cmp[i][3] = dx[(int) spXYall[i][1]][(int) spXYall[i][0]];
				cmp[i][4] = spXYall[i][3];
				cmp[i][5] = dy[(int) spXYall[i][1]][(int) spXYall[i][0]];
			}
			MiscHelper.printdoubleArray(cmp);
			ImageStack imsDeformation;
			imsDeformation = new ImageStack(dx[0].length, dx.length);
			imsDeformation.addSlice("Landmark deformations (all)", MiscHelper.deformationArrowField(spXYall, dx[0].length, dx.length, 1, 0));
			imsDeformation.addSlice("Interpolated deformations (all)", MiscHelper.deformationArrowField(dxTmp, dyTmp, 1, 0));
			imsDeformation.addSlice("Landmark deformations (clean)", MiscHelper.deformationArrowField(spXYclean, dx[0].length, dx.length, 1, 0));
			imsDeformation.addSlice("Interpolated deformations (clean)", MiscHelper.deformationArrowField(dx, dy, 1, 0));
			new ImagePlus("Deformation fields", imsDeformation).show();
		}

		return true;
	}

	/**
	 * Disable those tiles, that are either (partly) empty
	 * or are very likely unsuited for mutual information based registration
	 * @param tiles list of tiles
	 */
	private void disableSecondaryTiles(ArrayList<TilePair> tiles) {
		double[][] xDist = new double[2][tiles.size()];
		double[][] yDist = new double[2][tiles.size()];
		int[][] order = new int[2][tiles.size()];
		double[] gini = {0, 0};
		int quitCounter = 2;
		int remainingTiles;
		ArrayList<Integer> tileList = new ArrayList<>();

		for (int i = 0; i < tiles.size(); i++) {
			tileList.add(i);
		}

		// Disable empty tiles
		int tl = 0;
		for (int i = 0; i < tiles.size(); i++) {
			TilePair tp = tiles.get(i);
			int[] emptyPixels = {0, 0};
			for (int v = 0; v < tp.tileSize[1]; v++) {
				for (int u = 0; u < tp.tileSize[0]; u++) {
					for (int j = 0; j < tp.ip.length; j++) {
						if (tp.ip[j][1].get(u, v) == 0) {
							emptyPixels[j]++;
						}
					}
				}
			}
			double pixelCount = (double) (tp.tileSize[0] * tp.tileSize[1]);
			boolean disableTile = false;
			for (int j = 0; j < tp.ip.length; j++) {
				if ((double) emptyPixels[j] / pixelCount > EMPTY_TILES_LIMIT) {
					disableTile = true;
					break;
				}
			}
			if (disableTile) {
				debug.put("empty tile: " + i);
				tiles.get(i).ignore = true;
				tileList.remove(tl);
			} else {
				tl++;
			}
		}

		// Disable tiles until gini coefficient is sufficiently small
		remainingTiles = tileList.size();
		while (quitCounter > 0 && remainingTiles > 0) {
			for (int i = 0; i < 2; i++) {
				debug.put("remainingTiles="+remainingTiles);
				xDist[i] = new double[remainingTiles];
				yDist[i] = new double[remainingTiles];
				order[i] = new int[remainingTiles];
				int j = 0;
				for (int t = 0; t < tiles.size(); t++) {
					if (!tiles.get(t).ignore) {
						xDist[i][j] = 1;
						yDist[i][j] = tiles.get(t).getWeight()[i];
						j++;
					}
				}
				gini[i] = MiscHelper.calculateGiniCoefficient(xDist[i], yDist[i], order[i]);
				debug.put("gini("+i+")="+gini[i]);
				if (gini[i] > giniThreshold) {
					debug.put("worst tile:"+tileList.get(order[i][0]) + "(" + order[i][0] + ")");
					tiles.get(tileList.get(order[i][0])).ignore = true;
					tileList.remove(order[i][0]);
					remainingTiles--;
					quitCounter = 2;
				} else {
					quitCounter--;
				}
			}
		}
	}

	/**
	 * Find the best corresponding tile in the source image for a tile of the reference image
	 * @param tile TilePair
	 * @param stepRes contains the translation parameters for all steps in the iterative process
	 * @return translation parameters
	 */
	private int[] findBestTranslation(TilePair tile, int[][] stepRes) {
		int nbins = 256;
		int stepSizeFactor = 32;
		int[] bestT = {0, 0};
		double entropySum;
		double[][] gradMI = new double[3][3];
		double bestMI;
		int[] stepSize;
		ImageProcessor[][] ip = new ImageProcessor[2][2];
		boolean foundBetterMI;
		int stepLimit = stepRes.length;
		double[] min = {0, 0};
		double[] max = {tile.ipFull[0][0].getMax(), tile.ipFull[1][0].getMax()};

		ImageStack ims = new ImageStack(tile.tileSize[0], tile.tileSize[1]);
		ip[0] = tile.ip[0];
		ip[1] = tile.ip[1];
		ims.addSlice("R-Orig", ip[1]);
		ims.addSlice("S", ip[0]);
		bestMI = new mutualInformationFast(ip, mutualInformationFast.PLAIMI, nbins, min, max).lastResult;

		stepSize = guessInitialStepSizeInteger(tile.tile0[1][0] + tile.tileSize[0] / 2,
							tile.tile0[1][1] + tile.tileSize[1] / 2);
		debug.put("stepSize: " + stepSize[0] + ":" + stepSize[1]);

		for (int s = 0; s < stepLimit; s++) {
			debug.put(s + " bestMI=" + bestMI + " stepSize=" + stepSize[0]);
			for (int dv = -1; dv < 2; dv++) {
				for (int du = -1; du < 2; du++) {
					ip[1] = tile.getTile(1, bestT[0] + du*stepSize[0], bestT[1] + dv*stepSize[1]);
					gradMI[dv+1][du+1] = new mutualInformationFast(ip, mutualInformationFast.PLAIMI, nbins, min, max).lastResult;
					if (s == 0) {
						debug.put("MI("+du+","+dv+") = " + gradMI[dv+1][du+1]);
					}
				}
			}
			foundBetterMI = false;
			int[] tmpT = new int[2];
			for (int dv = -1; dv < 2; dv++) {
				for (int du = -1; du < 2; du++) {
					if (gradMI[dv+1][du+1] > bestMI) {
						bestMI = gradMI[dv+1][du+1];
						tmpT[0] = du;
						tmpT[1] = dv;
						foundBetterMI = true;
					}
				}
			}

			if (!foundBetterMI) {
				if (stepSize[0] > 1 || stepSize[1] > 1) {
					for (int i = 0; i < stepSize.length; i++) {
						if (stepSize[i] > 1) {
							stepSize[i] *= STEP_SIZE_REDUCTION;
						} else {
							stepSize[i] = 1;
						}
					}
					s -= 1;
				} else {
					break;
				}
			} else {
				bestT[0] += tmpT[0] * stepSize[0];
				bestT[1] += tmpT[1] * stepSize[1];
				stepRes[s][0] = bestT[0];
				stepRes[s][1] = bestT[1];
			}
		}
		ip[1] = tile.getTile(1, bestT[0], bestT[1]);
		ims.addSlice("T("+bestT[0]+":"+bestT[1]+")", ip[1][0]);
//		if (debug.is_debugging()) new ImagePlus("Tiles(" + tile.tile0[0] + "," + tile.tile0[1] + ")", ims).show();
		return bestT;
	}

	public JPanel propPanel() {
		// Pane with two columns for labels and textfields
		JPanel twoColPane = new JPanel();
		twoColPane.setLayout(new GridLayout(0, 2, INNER_PADDING, INNER_PADDING));

		cbUseDeformationField = new JCheckBox("Use prev. Def.");
		cbUseDeformationField.setSelected(useDeformationField);
		cbUseDeformationField.addChangeListener(
				e -> set_useDeformationField(cbUseDeformationField.isSelected())
		);
		twoColPane.add(cbUseDeformationField);
		cbUseOverlappingTiles = new JCheckBox("Overlapping Tiles");
		cbUseOverlappingTiles.setSelected(useOverlappingTiles);
		cbUseOverlappingTiles.addChangeListener(
				e -> set_useOverlappingTiles(cbUseOverlappingTiles.isSelected())
		);
		twoColPane.add(cbUseOverlappingTiles);

		twoColPane.add(new JLabel("<html>Number of tiles per width/height</html>"));
		slIntervals = new JSlider(JSlider.HORIZONTAL, 1, MAX_INTERVAL, intervals);
		slIntervals.setMajorTickSpacing(1);
		slIntervals.setPaintTicks(true);
		slIntervals.setPaintLabels(true);
		slIntervals.setSnapToTicks(true);
		slIntervals.setEnabled(true);
		slIntervals.addChangeListener(
				e -> set_intervals(slIntervals.getValue())
		);
		twoColPane.add(slIntervals);

		twoColPane.add(new JLabel("<html>Discard unfit tiles</html>"));
		slGiniThreshold = new JSlider(JSlider.HORIZONTAL, GINI_LOWEND, GINI_HIEND, (int)(giniThreshold*100));
		slGiniThreshold.setMajorTickSpacing(5);
		slGiniThreshold.setPaintTicks(true);
		slGiniThreshold.setPaintLabels(true);
		slGiniThreshold.setSnapToTicks(true);
		slGiniThreshold.setEnabled(true);
		Dictionary dict = new Hashtable();
		for (int i = GINI_LOWEND; i <= GINI_HIEND; i += GINI_STEP) {
			String s = "";
			if (i == GINI_LOWEND) s = "Strict";
			if (i == GINI_HIEND) s = "Mild";
			dict.put(i, new JLabel(s));
		}
		slGiniThreshold.setLabelTable(dict);
		slGiniThreshold.addChangeListener(
				e -> set_giniThreshold((double)slGiniThreshold.getValue()/100)
		);
		twoColPane.add(slGiniThreshold);

		twoColPane.add(new JLabel("Deformation scale"));
		rslSplineGrid = new RangeSlider(SPLINEGRID_LOWEND, SPLINEGRID_HIEND, minSplineGrid, maxSplineGrid);
		rslSplineGrid.setMajorTickSpacing(1);
		rslSplineGrid.setPaintTicks(true);
		rslSplineGrid.setPaintLabels(true);
		rslSplineGrid.setSnapToTicks(true);
		dict = new Hashtable();
		for (int i = SPLINEGRID_LOWEND; i <= SPLINEGRID_HIEND; i++) {
			String s = "";
			if (i == SPLINEGRID_LOWEND) s = "Coarse";
			if (i == SPLINEGRID_HIEND) s = "Fine";
			dict.put(i, new JLabel(s));
		}
		rslSplineGrid.setLabelTable(dict);
		rslSplineGrid.addChangeListener(
				e -> {
					set_minSplineGrid(rslSplineGrid.getValue());
					set_maxSplineGrid(rslSplineGrid.getUpperValue());
				}
		);
		twoColPane.add(rslSplineGrid);

		return twoColPane;
	}

	public int get_intervals() {
		return intervals;
	}

	public void set_intervals(int d) {
		intervals = d;
		setChanged(true);
	}

	public double get_giniThreshold() {
		return giniThreshold;
	}

	public void set_giniThreshold(double d) {
		giniThreshold = d;
		setChanged(true);
	}

	public boolean get_useDeformationField() {
		return useDeformationField;
	}

	public void set_useDeformationField(boolean b) {
		useDeformationField = b;
		setChanged(true);
	}
	public boolean get_useOverlappingTiles() {
		return useOverlappingTiles;
	}

	public void set_useOverlappingTiles(boolean b) {
		useOverlappingTiles = b;
		setChanged(true);
	}

	public int get_minSplineGrid() {
		return minSplineGrid;
	}

	public void set_minSplineGrid(int i) {
		minSplineGrid = i;
		setChanged(true);
	}

	public int get_maxSplineGrid() {
		return maxSplineGrid;
	}

	public void set_maxSplineGrid(int i) {
		maxSplineGrid = i;
		setChanged(true);
	}

	class TilePair {
		ImageProcessor[][] ip = new ImageProcessor[2][2];
		ImageProcessor[][] ipFull = new ImageProcessor[2][2];
		int[][] tile0 = new int[2][2];
		int[] tileSize = new int[2];
		int[] offset = new int[2];
		double[] entropy = new double[2];
		double[] contrast = new double[2];
		boolean ignore;

		TilePair(ImageProcessor[][] ipFull, int[][] tile0, int[] tileSize, int[] offset) {

			this.tile0[0][0] = tile0[0][0];
			this.tile0[0][1] = tile0[0][1];
			this.tile0[1][0] = tile0[1][0];
			this.tile0[1][1] = tile0[1][1];
			this.tileSize[0] = tileSize[0];
			this.tileSize[1] = tileSize[1];
			this.offset[0] = offset[0];
			this.offset[1] = offset[1];
			this.ignore = false;
			this.ipFull = ipFull;

			for (int i = 0; i < 2 ; i++) {
				ip[0][i] = MiscHelper.extractTile(ipFull[0][i], tile0[0][0], tile0[0][1], tileSize[0], tileSize[1]);
				ip[1][i] = MiscHelper.extractTile(ipFull[1][i], tile0[1][0] + offset[0], tile0[1][1] + offset[1], tileSize[0], tileSize[1]);
			}
		}

		public ImageProcessor[] getTile(int number, int tx, int ty) {
			ImageProcessor[] ip = new ImageProcessor[2];
			for (int i = 0; i < 2 ; i++) {
				ip[i] = MiscHelper.extractTile(ipFull[number][i], tile0[number][0] + offset[0] + tx, tile0[number][1] + offset[1] + ty, tileSize[0], tileSize[1]);
			}
			return ip;
		}

		public double[] calcEntropy() {
			entropy[0] = mutualInformationFast.calcEntropy(ip[0][0], 256, ipFull[0][0].getMin(), ipFull[0][0].getMax(), 2);
			entropy[1] = mutualInformationFast.calcEntropy(ip[1][0], 256, ipFull[1][0].getMin(), ipFull[1][0].getMax(), 2);
			return entropy;
		}

		public double[] getWeight() {
			calcEntropy();
			return entropy;
		}

		public boolean ignore() {
			return ignore;
		}
	}
}
