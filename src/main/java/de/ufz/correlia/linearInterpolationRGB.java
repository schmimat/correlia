/*
This class interpolates the colour-transition between two given RGB tuples
linearily at a given number of steps.

author: Matthias Schmidt
*/
package de.ufz.correlia;

public class linearInterpolationRGB
{
	static int RED = 0;
	static int GREEN = 1;
	static int BLUE = 2;
	
	
	linearInterpolationRGB()
	{
		defaultInitialisation();
	}
	
	linearInterpolationRGB( int[] RGBstart, int[] RGBend, int steps )
	{
		if ( RGBstart.length != 3 || RGBend.length != 3 || steps < 2 )
		{ defaultInitialisation(); return; }
		
		m_Rs = (double) RGBstart[RED];
		m_Re = (double) RGBend[RED];
		m_Gs = (double) RGBstart[GREEN];
		m_Ge = (double) RGBend[GREEN];
		m_Bs = (double) RGBstart[BLUE];
		m_Be = (double) RGBend[BLUE];
		m_steps = steps;
	}

	linearInterpolationRGB( double[] RGBstart, double[] RGBend, int steps )
	{
		if ( RGBstart.length != 3 || RGBend.length != 3 || steps < 2 )
		{ defaultInitialisation(); return; }
		
		m_Rs = RGBstart[RED];
		m_Re = RGBend[RED];
		m_Gs = RGBstart[GREEN];
		m_Ge = RGBend[GREEN];
		m_Bs = RGBstart[BLUE];
		m_Be = RGBend[BLUE];
		m_steps = steps;
	}

	
	private void defaultInitialisation()
	{
		m_Rs = 0.0;
		m_Re = 255.0;
		m_Gs = 0.0;
		m_Ge = 255.0;
		m_Bs = 0.0;
		m_Be = 255.0;
		m_steps = 3;
	}
	
	public Boolean setSteps( int steps )
	{
		if ( steps<2 ){ return false; }
		else{ m_steps = steps; return true; }
	}
	
	public int getSteps(){ return m_steps; }
	
	public double[] get( int step )
	{
		double[] v = { m_Rs, m_Gs, m_Bs };	
		
		if ( step <= 0 ) { return v; }
		if ( step >= m_steps ) { v[RED] = m_Re; v[GREEN] = m_Ge; v[BLUE] = m_Be; return v; }
		
		v[RED] = m_Rs + ((double) step) * (m_Re-m_Rs) / (m_steps-1);
		v[GREEN] = m_Gs + ((double) step) * (m_Ge-m_Gs) / (m_steps-1);
		v[BLUE] = m_Bs + ((double) step) * (m_Be-m_Bs) / (m_steps-1);		
		
		return v;
	}
	
	public int[] getInteger( int step )
	{
		double[] vd = get(step);
		int[] v = { (int) vd[RED], (int) vd[GREEN], (int) vd[BLUE] };
		return v;
	}
	
	
	
	
	double m_Rs;	// red value at start of transition
	double m_Re;	// reg value at end of transition
	double m_Gs;	// green value at start of transition
	double m_Ge;	// green value at end of transition
	double m_Bs;	// blue value at start of transition
	double m_Be;	// blue value at end of transition
	
	int m_steps;	// number of steps for the transition
	
	static public String classID(){ return "linearInterpolationRGB"; }
	static public String author(){ return "Matthias Schmidt"; }
	static public String version(){ return "September 11 2015"; }
}