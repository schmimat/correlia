/*
 * Class containing miscellaneous static methods for image processing in Correlia
 * Author: Florens Rohde
 *
 * Part of:
 * Correlia plugin for ImageJ and FIJI
 */
package de.ufz.correlia;

import bunwarpj.BSplineModel;
import bunwarpj.MiscTools;
import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.PointRoi;
import ij.process.*;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.UUID;

public class MiscHelper {

	/**
	 * Primitive type operations
	 */

	public static double[] addArrays(double[] a, double[] b) {
		double[] sm = a.length > b.length ? b : a;
		double[] res = new double[sm.length];
		for (int i = 0; i < sm.length; i++) {
			res[i] = a[i] + b[i];
		}
		return res;
	}

	public static double[][] addArrays(double[][] a, double[][] b) {
		int[] size = new int[2];
		size[0] = Math.min(a[0].length, b[0].length);
		size[1] = Math.min(a.length, b.length);
		double[][] res = new double[size[1]][size[0]];
		for (int v = 0; v < res.length; v++) {
			for (int u = 0; u < res[0].length; u++) {
				res[v][u] = a[v][u] + b[v][u];
			}
		}
		return res;
	}

	public static double[] multiplyArrays(double[] a, double[] b) {
		double[] sm = a.length > b.length ? b : a;
		double[] res = new double[sm.length];
		for (int i = 0; i < sm.length; i++) {
			res[i] = a[i] * b[i];
		}
		return res;
	}

	public static double[] scaleArray(double[] in, double s) {
		double[] res = new double[in.length];
		for (int i = 0; i < res.length; i++) {
			res[i] = in[i] * s;
		}
		return res;
	}

	public static double findMax(double[][] arr) {
		return findMax(doubleArray2DTo1D(arr));
	}

	public static double findMax(double[] arr) {
		double max = Double.MIN_VALUE;
		for(double cur: arr) {
			max = Math.max(max, cur);
		}
		return max;
	}

	public static int findMaxPos(double[] arr) {
		int maxPos = 0;
		double curMax = Double.MIN_VALUE;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] > curMax) {
				curMax = arr[i];
				maxPos = i;
			}
		}
		return maxPos;
	}

	public static double findMin(double[][] arr) {
		return findMin(doubleArray2DTo1D(arr));
	}

	public static double findMin(double[] arr) {
		double min = Double.MAX_VALUE;
		for(double cur: arr) {
			min = Math.min(min, cur);
		}
		return min;
	}

	public static int findMinPos(double[] arr) {
		int minPos = 0;
		double curMin = Double.MAX_VALUE;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] < curMin) {
				curMin = arr[i];
				minPos = i;
			}
		}
		return minPos;
	}

	public static double[][] sortNaturally(double[][] in, int sortColumn, int[] order) {
		double[][] out = new double[in.length][in[0].length];
		ArrayList<Integer> usedIndices = new ArrayList<>();
		double lastMin = 0;
		double curMin;
		int nextRow = 0;

		for (int o = 0; o < out.length; o++) {
			curMin = Double.POSITIVE_INFINITY;
			for (int i = 0; i < in.length; i++) {
				if (!usedIndices.contains(i)) {
					if (in[i][sortColumn] >= lastMin) {
						if (in[i][sortColumn] <= curMin) {
							curMin = in[i][sortColumn];
							nextRow = i;
						}
					}
				}
			}
			lastMin = curMin;
			usedIndices.add(nextRow);
			if (order != null && order.length == in.length) {
				order[o] = nextRow;
			}
//			IJ.log(nextRow + ":" + in[nextRow][sortColumn]);
			for (int c = 0; c < out[o].length; c++) {
				out[o][c] = in[nextRow][c];
			}
		}
		return out;
	}

	public static float[] toFloat(double[] dArr) {
		if (dArr == null) return null;
		float[] fArr = new float[dArr.length];
		for (int i = 0; i < fArr.length; i++) {
			fArr[i] = (float)dArr[i];
		}
		return fArr;
	}

	public static float[][] toFloat(double[][] dArr) {
		if (dArr == null) return null;
		double[] dArr1d = doubleArray2DTo1D(dArr);
		float[] fArr1d = toFloat(dArr1d);
		return floatArray1DTo2D(fArr1d, dArr[0].length, dArr.length);
	}

	public static double[] toDouble(float[] fArr) {
		if (fArr == null) return null;
		double[] dArr = new double[fArr.length];
		for (int i = 0; i < dArr.length; i++) {
			dArr[i] = (double)fArr[i];
		}
		return dArr;
	}

	public static double[][] toDouble(float[][] fArr) {
		if (fArr == null) return null;
		float[] fArr1d = floatArray2DTo1D(fArr);
		double[] dArr1d = toDouble(fArr1d);
		return doubleArray1DTo2D(dArr1d, fArr[0].length, fArr.length);
	}

	public static void printdoubleArray(double[][] m) {
		printdoubleArray(m, 2);
	}

	public static void printdoubleArray(double[][] m, int decimals) {
		int w = m[0].length;
		int h = m.length;

		for (double[] row : m) {
			StringBuilder s = new StringBuilder();
			for (double cell : row) {
				s.append(String.format("%10s", String.format("%."+decimals+"f", cell)));
			}
			IJ.log(s.toString());
		}
	}

	public static double[][] doubleArray1DTo2D(double[] d1, int width, int height) {
		double[][] d2 = new double[height][width];
		for (int r = 0; r < height; r++) {
			System.arraycopy(d1, r*width, d2[r], 0, width);
		}
		return d2;
	}

	public static double[] doubleArray2DTo1D(double[][] d2) {
		double[] d1 = new double[d2.length*d2[0].length];
		for (int r = 0; r < d2.length; r++) {
			System.arraycopy(d2[r], 0, d1, r * d2[r].length, d2[r].length);
		}
		return d1;
	}

	public static float[][] floatArray1DTo2D(float[] f1, int width, int height) {
		float[][] f2 = new float[height][width];
		for (int r = 0; r < height; r++) {
			System.arraycopy(f1, r*width, f2[r], 0, width);
		}
		return f2;
	}

	public static float[] floatArray2DTo1D(float[][] f2) {
		float[] f1 = new float[f2.length*f2[0].length];
		for (int r = 0; r < f2.length; r++) {
			System.arraycopy(f2[r], 0, f1, r * f2[r].length, f2[r].length);
		}
		return f1;
	}

	public static byte[] doubleArrayToByteArray(double[] dArray) {
		byte[] b = new byte[dArray.length];

		// Find min and max values
		double oldMin = findMin(dArray);
		double oldMax = findMax(dArray);

		// Populate byte array with
		for (int i = 0; i < dArray.length; i++) {
			b[i] = (byte) normalizeValue(dArray[i], oldMin, oldMax, 0, Byte.MAX_VALUE - Byte.MIN_VALUE);
		}
		return b;
	}

	public static double[] normalizeArray(double[] in, double oldMin, double oldMax, double newMin, double newMax) {
		double[] out = new double[in.length];

		for (int i = 0; i < out.length; i++) {
			out[i] = normalizeValue(in[i], oldMin, oldMax, newMin, newMax);
		}
		return out;
	}

	public static double normalizeValue(double val, double oldMin, double oldMax, double newMin, double newMax) {
		if (val <= oldMin) return newMin;
		if (val >= oldMax) return newMax;
		return newMin + ((val - oldMin) * (newMax - newMin) / (oldMax - oldMin));
	}

	public static int[] doubleArrayToInt(double[] in) {
		int[] out = new int[in.length];
		for (int i = 0; i < out.length; i++) {
			out[i] = (int) Math.round(in[i]);
		}
		return out;
	}

	public static double roundDouble(double in, int decimals) {
		double h = Math.pow(10, decimals);
		return Math.round(in * h) / h;
	}

	public static boolean isEqual(double val1, double val2, int decimals) {
		return MiscHelper.roundDouble(val1, decimals) == MiscHelper.roundDouble(val2, decimals);
	}

	public static double calculateSSD(ArrayList<Point.Double> fpSrc, ArrayList<Point.Double> fpRef, ArrayList<Integer[]> matches) {
		double result = 0;
		for (Integer[] match : matches) {
			Point.Double pSrc = fpSrc.get(match[0]);
			result += Math.pow(pSrc.distance(fpRef.get(match[1])), 2);
		}
		return result / matches.size();
	}

	public static double calculateSSD(ImageProcessor[] ipSrc, ImageProcessor[] ipRef) {
		ImageStack ims = new ImageStack(ipRef[0].getWidth(), ipRef[0].getHeight());
		ims.addSlice("ref0", ipRef[0]);
		ims.addSlice("ref1", ipRef[1]);
		ims.addSlice("src0", ipSrc[0]);
		ims.addSlice("src1", ipSrc[1]);
		(new ImagePlus("SSD", ims)).show();
		double result = 0;
		int w = Math.min(ipSrc[0].getWidth(), ipRef[0].getWidth());
		int h = Math.min(ipSrc[0].getHeight(), ipRef[0].getHeight());
		for (int v = 0; v < h; v++) {
			for (int u = 0; u < w; u++) {
				if (ipSrc[1].get(u, v) == 0 || ipRef[1].get(u, v) == 0) continue;
				result += Math.pow(ipSrc[0].get(u, v) - ipRef[0].get(u, v), 2);
			}
		}
		return result / (w*h);
	}

	public static double[][] switchRowsAndColumns(double[][] in) {
		assert in.length == in[0].length;
		double[][] out = new double[in.length][in.length];
		for (int v = 0; v < out.length; v++) {
			for (int u = 0; u < out.length; u++) {
				out[u][v] = in[v][u];
			}
		}
		return out;
	}

	public static double[][] extractArea(double[][] in, int x0, int y0, int w, int h) {
		double[][] out = new double[h][w];

		for (int v = 0; v < h; v++) {
			for (int u = 0; u < w; u++) {
				out[v][u] = in[y0 + v][x0 + u];
			}
		}
		return out;
	}

	public static String generateID() {
		String ID;
		ID = UUID.randomUUID().toString();
		return ID;
	}

	public static double interpolateValueFromArray(double[][] arr, double x, double y) {
		int width = arr[0].length;
		int height = arr.length;
		int xbase = (int)x;
		int ybase = (int)y;
		int xbasePlus = xbase + 1;
		int ybasePlus = ybase + 1;
		double xFraction = x - xbase;
		double yFraction = y - ybase;
		if (xbase + 1 > width - 1) {
			xbasePlus = width - 1;
			xFraction = 0;
		}
		if (ybase + 1 > height - 1) {
			ybasePlus = height - 1;
			yFraction = 0;
		}
		int offset = ybase * width + xbase;
		double lowerLeft = arr[ybase][xbase];
		double lowerRight = arr[ybase][xbasePlus];
		double upperRight = arr[ybasePlus][xbasePlus];
		double upperLeft = arr[ybasePlus][xbase];
		double upperAverage = upperLeft + xFraction * (upperRight-upperLeft);
		double lowerAverage = lowerLeft + xFraction * (lowerRight-lowerLeft);
		return lowerAverage + yFraction * (upperAverage-lowerAverage);
	}

	/**
	 * Basic ImageJ operations
	 */

	public static FloatProcessor doubleArray2DToFP(double[][] dD) {
		double[] d1 = doubleArray2DTo1D(dD);
		return new FloatProcessor(dD[0].length, dD.length, d1);
	}

	public static ByteProcessor doubleArray2DToBP(double[][] dD) {
		return doubleArray2DToBP(doubleArray2DTo1D(dD), dD[0].length, dD.length);
	}

	public static ByteProcessor doubleArray2DToBP(double[] dD, int width, int height) {
		byte[] dB1 = doubleArrayToByteArray(dD);
		return new ByteProcessor(width, height, dB1);
	}

	public static ImageProcessor extractTile(ImageProcessor in, int x0, int y0, int w, int h) {
		ImageProcessor out;
		switch (in.getClass().getSimpleName()) {
			case "ByteProcessor":
				out = new ByteProcessor(w, h);
				break;
			case "ShortProcessor":
				out = new ShortProcessor(w, h);
				break;
			case "FloatProcessor":
				out = new FloatProcessor(w, h);
				break;
			default:
			case "ColorProcessor":
				out = new ColorProcessor(w, h);
				break;
		}

		for (int v = 0; v < h; v++) {
			for (int u = 0; u < w; u++) {
				out.setf(u, v, in.getPixelValue(x0 + u, y0 + v));
			}
		}
		return out;
	}

	public static ImageProcessor createMask(ImageProcessor ip) {
		ImageProcessor ipOut = ip.duplicate();
		float val = (float)ip.getMax();
		if (val <= 0) val = 1;
		for (int y = 0; y < ip.getHeight(); y++) {
			for (int x = 0; x < ip.getWidth(); x++) {
				if (ip instanceof ColorProcessor) {
					ipOut.putPixelValue(x, y, val);
				} else {
					ipOut.setf(x, y, val);
				}
			}
		}
		return ipOut;
	}

	/**
	 * Basic Correlia operations
	 */

	public static ArrayList<Point.Double> copyFeatures(ArrayList<Point.Double> fpIn) {
		ArrayList<Point.Double> fpOut = new ArrayList<>();
		for (Point.Double fp : fpIn) {
			fpOut.add(new Point.Double(fp.getX(), fp.getY()));
		}
		return fpOut;
	}

	public static boolean unmodifiedImageAlignment(imageAlignment ia) {
		boolean ret = true;
		if (ia.m_x0 != 0 || ia.m_y0 != 0) {
			ret = false;
		} else if (ia.m_rotAngle != 0 || ia.m_xscale != 1) {
			ret = false;
		}
		return ret;
	}

	public static String selectImageIDFromProject(correlia prj, Component parentDialog, String title, String preSelection, boolean excludeBase) {
		int sel = prj.get_imagePositionByID(preSelection);
		sel = selectImageFromProject(prj, parentDialog, title, sel, excludeBase);
		if (sel < 0) {
			return null;
		} else {
			return prj.get_image(sel).get_ID();
		}
	}

	public static int selectImageFromProject(correlia prj, Component parentDialog, String title, int preSelection, boolean excludeBase) {
		int size = prj.size();
		if (excludeBase) size--;
		Object[] possibilities = new String[size];
		String s;
		int res = -1;

		for (int i = 0; i < possibilities.length; i++) {
			possibilities[i] = prj.get_image(excludeBase?i+1:i).getTitle();
		}
		if (preSelection < 0 || preSelection > possibilities.length - 1) {
			preSelection = 0;
		}

		s = (String) JOptionPane.showInputDialog(
				parentDialog,
				title,
				title,
				JOptionPane.PLAIN_MESSAGE,
				null,
				possibilities,
				possibilities[preSelection]);
		for (int i = 0; i < possibilities.length; i++) {
			if (possibilities[i].equals(s)) {
				res = i;
			}
		}
		if (excludeBase) res++;
		return res;
	}

	public static microscopyImage getMicroscopyImageFromPixelData(double[][] data, double width, double height) {
		FloatProcessor fp = doubleArray2DToFP(data);
		ImagePlus imp = new ImagePlus("New microscopyImage", fp);

		microscopyImage mi = new microscopyImage(imp, true);
		mi.calibrateFromSize(width, height);
		return mi;
	}

	public static PointRoi convertFeaturePointsToPointRoi(ArrayList<Point.Double> featurepoints, double pxWidth, double pxHeight, int[] offset) {
		double[][] fps = MiscHelper.getFeaturePointsInPixelUnits(featurepoints, pxWidth, pxHeight, offset);

		int[] px = new int[fps.length];
		int[] py = new int[fps.length];
		Point.Double p;
		for (int i = 0; i < fps.length; i++) {
			px[i] = (int) fps[i][0];
			py[i] = (int) fps[i][1];
		}
		PointRoi roi = new PointRoi(px, py, fps.length);
		roi.setPointType(1); // HYBRID=0, CROSSHAIR=1, DOT=2, CIRCLE=3
		roi.setSize(3); //TINY=1, SMALL=1, MEDIUM=2, LARGE=3, EXTRA_LARGE=4
		roi.setStrokeColor(Color.RED);
		roi.setShowLabels(true);
		return roi;
	}

	public static double[][] getFeaturePointsInPixelUnits(microscopyImage mi, int[] offset) {
		return getFeaturePointsInPixelUnits(mi.get_feature_points(), mi.pixelWidth(), mi.pixelHeight(), offset);
	}

	public static double[][] getFeaturePointsInPixelUnits(ArrayList<Point.Double> featurepoints, double pxWidth, double pxHeight, int[] offset) {
		double[][] fps = new double[featurepoints.size()][2];
		if (offset == null) {
			offset = new int[2];
			offset[0] = 0;
			offset[1] = 0;
		}
		for (int i = 0; i < fps.length; i++) {
			Point.Double fp = featurepoints.get(i);
			fps[i][0] = fp.getX() / pxWidth + offset[0];
			fps[i][1] = fp.getY() / pxHeight + offset[1];
		}
		return fps;
	}

	public static ArrayList<Point.Double> getFeaturesPointsInRefCoords(ArrayList<Point.Double> fp, imageAlignment refIA) {
		ArrayList<Point.Double> fpRef = new ArrayList<>();
		for (Point.Double pRef : fp) {
			double xRef = refIA.x(pRef.getX(), pRef.getY());
			double yRef = refIA.y(pRef.getX(), pRef.getY());
			fpRef.add(new Point.Double(xRef, yRef));
		}
		return fpRef;
	}

	// Using code from bUnwarpJ
	public static ImageProcessor deformationArrowField(double[][] dx, double[][] dy, double bg_color, double fg_color) {
		int width = dx[0].length;
		int height = dx.length;
		double dwidth = Math.min(Math.max(10, width/15), 50);
		double dheight = Math.min(Math.max(10, height/15), 50);
		ArrayList<Double[]> pointList = new ArrayList<>();

		for (double y = 0; y < height; y += dheight) {
			for (double x = 0; x < width; x += dwidth) {
				Double[] d = new Double[4];
				d[0] = x;
				d[1] = y;
				d[2] = dx[(int)y][(int)x];
				d[3] = dy[(int)y][(int)x];
				pointList.add(d);
			}
		}
		double[][] points = new double[pointList.size()][pointList.get(0).length];
		for (int i = 0; i < points.length; i++) {
			for (int j = 0; j < points[0].length; j++) {
				points[i][j] = pointList.get(i)[j];
			}
		}
		return deformationArrowField(points, width, height, bg_color, fg_color);
	}

	public static ImageProcessor deformationArrowField(ArrayList<deformationHandle> deformations, int width, int height, double bg_color, double fg_color) {
		double[][] points = new double[deformations.size()][4];
		for (int i = 0; i < deformations.size(); i++) {
			deformationHandle dH = deformations.get(i);
			points[i][0] = dH.getX();
			points[i][1] = dH.getY();
			points[i][2] = dH.getDx();
			points[i][3] = dH.getDy();
		}
		return deformationArrowField(points, width, height, bg_color, fg_color);
	}

	// Using code from bUnwarpJ
	public static ImageProcessor deformationArrowField(double[][] points, int width, int height, double bg_color, double fg_color) {
		final double arrowSlice[][] = new double[height][width];

		// Fill background
		for (int v = 0; v < height; v++) {
			for (int u = 0; u < width; u++) {
				arrowSlice[v][u] = bg_color;
			}
		}

		// Draw arrows
		for (double[] p : points) {
			MiscTools.drawArrow(arrowSlice,
					(int)Math.round(p[0]), (int)Math.round(p[1]), (int)Math.round(p[0] + p[2]), (int)Math.round(p[1] + p[3]), fg_color, 2);
		}

		// Create ImageProcessor
		FloatProcessor fp = new FloatProcessor(width, height);
		for (int v = 0; v < height; v++) {
			for (int u = 0; u < width; u++) {
				fp.setf(u, v, (float) arrowSlice[v][u]);
			}
		}
		return fp;
	}

	// Using code from bUnwarpJ
	public static ImageProcessor deformationCheckboard(double[][] dx, double[][] dy, double bg_color, double fg_color) {
		int width = dx[0].length;
		int height = dx.length;
		int dwidth = (int)Math.round(Math.min(Math.max(20, (double)(width - 1)/30), 50));
		int dheight = (int)Math.round(Math.min(Math.max(20, (double)(height - 1)/30), 50));
		dwidth = dheight = Math.min(dwidth, dheight);

		final double slice[][] = new double[height][width];
		for (int v = 0; v < height; v++) {
			for (int u = 0; u < width; u++) {
				slice[v][u] = bg_color;
			}
		}

		boolean rowBeginFg = true;
		boolean fillFg = true;

		for (int v = 0; v < height; v += dheight) {
			fillFg = rowBeginFg;
			for (int u = 0; u < width; u += dwidth) {
				double uh = Math.min(u + dwidth, width - 1);
				double vv = Math.min(v + dheight, height - 1);

				for (int v0 = v; v0 < vv; v0++) {
					for (int v1 = v; v1 < vv; v1++) {
						int x0 = (int)Math.floor(u + dx[(int)Math.floor(v0)][(int)Math.floor(u)]);
						int y0 = (int)Math.floor(v0 + dy[(int)Math.floor(v0)][(int)Math.floor(u)]);
						int x1 = (int)Math.floor(uh + dx[(int)Math.floor(v1)][(int)Math.floor(uh)]);
						int y1 = (int)Math.floor(v1 + dy[(int)Math.floor(v1)][(int)Math.floor(uh)]);
						MiscTools.drawLine(
								slice,
								x0, y0, x1, y1,
								fillFg? fg_color : bg_color);
					}
				}

				fillFg = !fillFg;
			}
			rowBeginFg = !rowBeginFg;
		}

		// Create ImageProcessor
		FloatProcessor fp = new FloatProcessor(width, height);
		for (int v = 0; v < height; v++) {
			for (int u = 0; u < width; u++) {
				fp.setf(u, v, (float)slice[v][u]);
			}
		}
		return fp;
	}

	// Using code from bUnwarpJ
	public static ImageProcessor deformationGrid(double[][] dx, double[][] dy, double bg_color, double fg_color) {
		int width = dx[0].length;
		int height = dx.length;
		double dwidth = Math.min(Math.max(10, (double)(width - 1)/30), 30);
		double dheight = Math.min(Math.max(10, (double)(height - 1)/30), 50);

		final double slice[][] = new double[height][width];
		for (int v = 0; v < height; v++) {
			for (int u = 0; u < width; u++) {
				slice[v][u] = bg_color;
			}
		}

		for (double v = 0; v < height; v += dheight) {
			for (double u = 0; u < width; u += dwidth) {
				final double x = u + dx[(int)Math.floor(v)][(int)Math.floor(u)];
				final double y = v + dy[(int)Math.floor(v)][(int)Math.floor(u)];
				// Draw horizontal line
				double uh = (u+dwidth < width)? u+dwidth : width - 1;
				if (uh<width) {
					final double xh = uh + dx[(int)Math.floor(v)][(int)Math.floor(uh)];
					final double yh = v + dy[(int)Math.floor(v)][(int)Math.floor(uh)];
					MiscTools.drawLine(
							slice,
							(int)Math.round(x) ,(int)Math.round(y),
							(int)Math.round(xh),(int)Math.round(yh), fg_color);
				}

				// Draw vertical line
				double vv = (v+dheight < height)? v+dheight : height - 1;
				if (vv<height) {
					final double xv = u + dx[(int)Math.floor(vv)][(int)Math.floor(u)];
					final double yv = vv + dy[(int)Math.floor(vv)][(int)Math.floor(u)];
					MiscTools.drawLine(
							slice,
							(int)Math.round(x) ,(int)Math.round(y),
							(int)Math.round(xv),(int)Math.round(yv), fg_color);
				}
			}
		}

		// Create ImageProcessor
		FloatProcessor fp = new FloatProcessor(width, height);
		for (int v = 0; v < height; v++) {
			for (int u = 0; u < width; u++) {
				fp.setf(u, v, (float)slice[v][u]);
			}
		}
		return fp;
	}

	public static ImageProcessor deformationLengthField(double[][] dx, double[][] dy, double minGray, double maxGray) {
		int width = dx[0].length;
		int height = dx.length;

		final double slice[][] = new double[height][width];

		for (int v = 0; v < height; v ++) {
			for (int u = 0; u < width; u ++) {
				slice[v][u] = Math.sqrt(dx[v][u] * dx[v][u] + dy[v][u] * dy[v][u]);
			}
		}

		double max = findMax(slice);
		for (int v = 0; v < height; v ++) {
			for (int u = 0; u < width; u ++) {
				slice[v][u] = normalizeValue(slice[v][u], 0, max, minGray, maxGray);
			}
		}

		// Create ImageProcessor
		FloatProcessor fp = new FloatProcessor(width, height);
		for (int v = 0; v < height; v++) {
			for (int u = 0; u < width; u++) {
				fp.setf(u, v, (float)slice[v][u]);
			}
		}
		return fp;
	}

	public static double[][] generateMutualInformationLandscape(ImageProcessor[][] ipFull, int[][] tile0, int[] tileSize, int[] offset, int steps) {
		double[] min = {0, 0};
		double[] max = {ipFull[0][0].getMax(), ipFull[1][0].getMax()};
		int nbins = 256;
		int type = mutualInformationFast.PLAIMI;
		double logbase = 10;
		int[] steps2 = {steps, steps};
		return generateMutualInformationLandscape(ipFull, tile0, tileSize, offset, steps2, type, nbins, min, max, logbase);
	}

	public static double[][] generateMutualInformationLandscape(
			ImageProcessor[][] ipFull,
			int[][] tile0,
			int[] tileSize,
			int[] offset,
			int[] steps,
			int type,
			int nbins,
			double[] min,
			double[] max,
			double logbase) {
		ImageProcessor[][] ip = new ImageProcessor[2][2];
		double[][] miMatrix = new double[2*steps[1]+1][2*steps[0]+1];
		int[] stepLimit = {steps[0], steps[1]};

		for (int i = 0; i < 2; i++) {
			ip[0][i] = MiscHelper.extractTile(ipFull[0][i], tile0[0][0], tile0[0][1], tileSize[0], tileSize[1]);
		}
		for (int dv = 0; dv < miMatrix.length; dv++) {
			for (int du = 0; du < miMatrix[0].length; du++) {
				for (int i = 0; i < 2; i++) {
					ip[1][i] = MiscHelper.extractTile(
							ipFull[1][i],
							tile0[1][0] + offset[0] + du - stepLimit[0],
							tile0[1][1] + offset[1] + dv - stepLimit[1],
							tileSize[0], tileSize[1]
					);
				}
				mutualInformationFast MI = new mutualInformationFast(ip, type, nbins, min, max, logbase);
				miMatrix[dv][du] = MI.lastResult;
			}
		}
		return miMatrix;
	}

	/**
	 * Image adaptation and resize methods
	 */

	public static microscopyImage[] resizeImagesToSamePixelWidth(microscopyImage[] miArrayOrig, int minWidth) {
		microscopyImage[] miArrayOut = new microscopyImage[2];
		for (int i = 0; i < miArrayOrig.length; i++) {
			miArrayOut[i] = new microscopyImage(miArrayOrig[i]);
		}

		double minPxWidth = Double.POSITIVE_INFINITY;
		double width[] = new double[miArrayOrig.length];
		double normWidth = Double.POSITIVE_INFINITY;

		for (microscopyImage mi : miArrayOrig) {
			minPxWidth = Math.min(mi.pixelWidth(), minPxWidth);
		}

		for (int i = 0; i < miArrayOrig.length; i++) {
			width[i] = miArrayOrig[i].imgWidth() * minPxWidth;
			normWidth = Math.min(width[i], normWidth);
		}

		for (int i = 0; i < miArrayOrig.length; i++) {
			width[i] = minWidth * width[i] / normWidth;
			ImageStack ims = null;
			for (int s = 1; s <= miArrayOrig[i].getStackSize(); s++) {
				ImageProcessor ipResized = miArrayOrig[i].getStack().getProcessor(s);
				ipResized.setInterpolationMethod(ImageProcessor.BILINEAR);
				ipResized = ipResized.resize((int) Math.round(width[i]));
				if (ims == null) {
					ims = new ImageStack(ipResized.getWidth(), ipResized.getHeight());
				}
				ims.addSlice(miArrayOrig[i].getSliceLabels()[s-1], ipResized);
			}
			miArrayOut[i].setStack(ims);
			miArrayOut[i].calibrateFromSize();
		}

		return miArrayOut;
	}

	public static ImageProcessor[][] getAdaptedIPs(microscopyImage[] mi, imageAlignment[] ia) {
		ImageProcessor[][] ip = new ImageProcessor[2][2];

		int imgAdapt;
		int imgCopy;
		if (mi[0].getWidth() * mi[0].getHeight() > mi[1].getWidth() * mi[1].getHeight()) {
			imgAdapt = 0;
			imgCopy = 1;
		} else {
			imgAdapt = 1;
			imgCopy = 0;
		}
		microscopyImage miAdapted = getAdaptedImage(mi[imgAdapt], mi[imgCopy], ia[imgAdapt], ia[imgCopy]);
		ip[imgAdapt][0] = miAdapted.getStack().getProcessor(1);
		ip[imgAdapt][1] = miAdapted.getStack().getProcessor(2);
		ip[imgCopy][0] = mi[imgCopy].getStack().getProcessor(1);
		ip[imgCopy][1] = mi[imgCopy].getStack().getProcessor(2);

		return ip;
	}

	public static microscopyImage getAdaptedImage(microscopyImage miS, microscopyImage miR, imageAlignment iaS, imageAlignment iaR) {
		microscopyImage out = new microscopyImage(miS);
		adaptImage(out, miR, iaS, iaR, 1);
		return out;
	}

	public static int[] adaptImage(microscopyImage miS, microscopyImage miR, imageAlignment iaS, imageAlignment iaR, double sizeLimit) {
		double xRef, yRef;
		double xBase, yBase;
		double xSrc, ySrc;
		int[] translation = new int[2];

		double[] aSize = getAdaptedSize(miS, miR, iaS, iaR, sizeLimit);

		int wPx = (int) Math.round(Math.max(aSize[0] - aSize[2], miR.imgWidth()) / miR.pixelWidth());
		int hPx = (int) Math.round(Math.max(aSize[1] - aSize[3], miR.imgHeight()) / miR.pixelHeight());
		ImageStack ims = new ImageStack(wPx, hPx, miS.getStackSize());

		int oldSlice = miS.getCurrentSlice();
		for (int s = 0; s < ims.getSize(); s++) {
			miS.setSlice(s + 1);
			ImageProcessor ip;
			switch (miS.getType()) {
				case ImagePlus.GRAY8:
					ip = new ByteProcessor(ims.getWidth(), ims.getHeight());
					break;
				case ImagePlus.GRAY16:
					ip = new ShortProcessor(ims.getWidth(), ims.getHeight());
					break;
				case ImagePlus.GRAY32:
					ip = new FloatProcessor(ims.getWidth(), ims.getHeight());
					break;
				default:
				case ImagePlus.COLOR_RGB:
					ip = new ColorProcessor(ims.getWidth(), ims.getHeight());
					break;
			}
			for (int u = 0; u < ims.getWidth(); u++) {
				for (int v = 0; v < ims.getHeight(); v++) {
					xRef = ((double) u)*miR.pixelWidth();
					yRef = ((double) v)*miR.pixelHeight();
					xBase = iaR.x(xRef + aSize[2], yRef + aSize[3]);
					yBase = iaR.y(xRef + aSize[2], yRef + aSize[3]);
					xSrc = iaS.xprime(xBase, yBase);
					ySrc = iaS.yprime(xBase, yBase);
					if (ip.isGrayscale()) {
						ip.setf(u, v, (float) miS.get_value(xSrc, ySrc, 0));
					} else {
						ip.putPixel(u, v, doubleArrayToInt(miS.get_valueTuple(xSrc, ySrc)));
					}
				}
			}
			ip.resetMinAndMax();
			ims.setProcessor(ip, s+1);
		}

		miS.setStack(ims);
		miS.setSlice(oldSlice);
		miS.setCalibration(miR.getCalibration());
		miS.updateImgSizeFromCalibration();

		for (int fp = 0; fp < miS.number_of_feature_points(); fp++) {
			xBase = iaS.x(miS.get_feature_point(fp).getX(), miS.get_feature_point(fp).getY());
			yBase = iaS.y(miS.get_feature_point(fp).getX(), miS.get_feature_point(fp).getY());
			xSrc = iaR.xprime(xBase, yBase);
			ySrc = iaR.yprime(xBase, yBase);
			miS.set_feature_point(fp, xSrc - aSize[2], ySrc - aSize[3]);
		}

		translation[0] = (int) Math.round(-aSize[2] / miS.pixelWidth());
		translation[1] = (int) Math.round(-aSize[3] / miS.pixelHeight());
		return translation;
	}

	public static double[] getAdaptedSize(microscopyImage miS, microscopyImage miR, imageAlignment iaS, imageAlignment iaR, double sizeLimit) {
		double minx = Double.MAX_VALUE, miny = Double.MAX_VALUE;
		double maxx = Double.MIN_VALUE, maxy = Double.MIN_VALUE;
		double xBase, yBase;
		double xRef, yRef;

		// Only use image corners for size determination
		ArrayList<Point.Double> c = new ArrayList<>();
		c.add(new Point.Double(0, 0));
		c.add(new Point.Double(miS.imgWidth(), 0));
		c.add(new Point.Double(miS.imgWidth(), miS.imgHeight()));
		c.add(new Point.Double(0, miS.imgHeight()));

		for (Point.Double p : c) {
			xBase = iaS.x(p.getX(), p.getY());
			yBase = iaS.y(p.getX(), p.getY());
			xRef = iaR.xprime(xBase, yBase);
			yRef = iaR.yprime(xBase, yBase);
			maxx = Math.max(xRef, maxx);
			minx = Math.min(xRef, minx);
			maxy = Math.max(yRef, maxy);
			miny = Math.min(yRef, miny);
		}

		// Prohibit positive values, because the top-left corner of the result shall not be in the result area
		minx = Math.min(minx, 0);
		maxx = Math.max(maxx, miR.imgWidth());
		miny = Math.min(miny, 0);
		maxy = Math.max(maxy, miR.imgHeight());

		// Result size is at least that of the reference
		double w = maxx - minx;
		double h = maxy - miny;

		if (sizeLimit >= 1) {
			// Limited size shall not be greater that the unlimited
			double wn = Math.min(sizeLimit * miR.imgWidth(), w);
			double hn = Math.min(sizeLimit * miR.imgHeight(), h);
			if (w > wn) {
				maxx = miR.imgWidth() + (wn - miR.imgWidth()) / 2;
				minx = - (wn - miR.imgWidth()) / 2;
			}
			if (h > hn) {
				maxy = miR.imgHeight() + (hn - miR.imgHeight()) / 2;
				miny = - (hn - miR.imgHeight()) / 2;
			}
		}

		double res[] = new double[4];
		res[0] = maxx;
		res[1] = maxy;
		res[2] = minx;
		res[3] = miny;
		return res;
	}

	public static double[] getJointSize(microscopyImage[] mi, imageAlignment[] ia) {
		double minx = Double.MAX_VALUE, miny = Double.MAX_VALUE;
		double maxx = Double.MIN_VALUE, maxy = Double.MIN_VALUE;

		// Only use image corners for size determination
		ArrayList<ArrayList<Point.Double>> corner = new ArrayList<>();
		for (microscopyImage m : mi) {
			ArrayList<Point.Double> c = new ArrayList<>();
			c.add(new Point.Double(0, 0));
			c.add(new Point.Double(m.imgWidth(), 0));
			c.add(new Point.Double(m.imgWidth(), m.imgHeight()));
			c.add(new Point.Double(0, m.imgHeight()));
			corner.add(c);
		}

		for (int i = 0; i < mi.length; i++) {
			for (Point.Double p : corner.get(i)) {
				p.setLocation(ia[i].x(p.getX(), p.getY()), ia[i].y(p.getX(), p.getY()));
				maxx = Math.max(p.getX(), maxx);
				minx = Math.min(p.getX(), minx);
				maxy = Math.max(p.getY(), maxy);
				miny = Math.min(p.getY(), miny);
			}
		}

		double res[] = new double[4];
		res[0] = maxx;
		res[1] = maxy;
		res[2] = minx;
		res[3] = miny;
		return res;
	}

	public static microscopyImage convertToJointCoords(microscopyImage mi, imageAlignment ia, double[] jointSize, double pxWidth, double pxHeight) {
		double xBase, yBase;
		double xSrc, ySrc;

		int wPx = (int) Math.round((jointSize[0] - jointSize[2]) / pxWidth);
		int hPx = (int) Math.round((jointSize[1] - jointSize[3]) / pxHeight);
		ImageStack ims = new ImageStack(wPx, hPx, mi.getStackSize());

		microscopyImage miOut = new microscopyImage(mi);

		miOut.getCalibration().pixelWidth = pxWidth;
		miOut.getCalibration().pixelHeight = pxHeight;
		miOut.updateImgSizeFromCalibration();

		int oldSlice = mi.getCurrentSlice();
		for (int s = 0; s < ims.getSize(); s++) {
			mi.setSlice(s + 1);
			ImageProcessor ip;
			switch (mi.getType()) {
				case ImagePlus.GRAY8:
					ip = new ByteProcessor(ims.getWidth(), ims.getHeight());
					break;
				case ImagePlus.GRAY16:
					ip = new ShortProcessor(ims.getWidth(), ims.getHeight());
					break;
				case ImagePlus.GRAY32:
					ip = new FloatProcessor(ims.getWidth(), ims.getHeight());
					break;
				default:
				case ImagePlus.COLOR_RGB:
					ip = new ColorProcessor(ims.getWidth(), ims.getHeight());
					break;
			}
			for (int u = 0; u < ims.getWidth(); u++) {
				for (int v = 0; v < ims.getHeight(); v++) {
					xBase = ((double) u)*miOut.pixelWidth() + jointSize[2];
					yBase = ((double) v)*miOut.pixelHeight() + jointSize[3];
					xSrc = ia.xprime(xBase, yBase);
					ySrc = ia.yprime(xBase, yBase);
					if (ip instanceof  ColorProcessor) {
						ip.putPixel(u, v, doubleArrayToInt(mi.get_valueTuple(xSrc, ySrc)));
					} else {
						ip.setf(u, v, (float) mi.get_value(xSrc, ySrc, 0));
					}
				}
			}
			ip.resetMinAndMax();
			ims.setProcessor(ip, s+1);
		}

		miOut.setStack(ims);
		mi.setSlice(oldSlice);
		miOut.setSlice(oldSlice);

		for (int fp = 0; fp < miOut.number_of_feature_points(); fp++) {
			xBase = ia.x(miOut.get_feature_point(fp).getX(), miOut.get_feature_point(fp).getY());
			yBase = ia.y(miOut.get_feature_point(fp).getX(), miOut.get_feature_point(fp).getY());
			miOut.set_feature_point(fp, xBase - jointSize[2], yBase - jointSize[3]);
		}

		return miOut;
	}

	/**
	 * Interpolation methods
	 */

	public static microscopyImage interpolateImageFromSampleGrid(double[][] sampleGrid, double[] stepSize, int scalefactor) {
		double[][] points = new double[sampleGrid[0].length*sampleGrid.length][3];
		for (int v = 0; v < sampleGrid.length; v++) {
			for (int u = 0; u < sampleGrid[0].length; u++) {
				points[v*sampleGrid[0].length + u][0] = u;
				points[v*sampleGrid[0].length + u][1] = v;
				points[v*sampleGrid[0].length + u][2] = sampleGrid[v][u];
			}
		}
		int[] max = {sampleGrid[0].length, sampleGrid.length};
		double[][] coeff = getBSplineCoefficientsFromScatteredPoints(points, max, 1, 64);

		int width = scalefactor * sampleGrid[0].length;
		int height = scalefactor * sampleGrid.length;
		double[][] interpolatedData = interpolateMatrixFromCoefficients(coeff, width, height);

		microscopyImage mi = getMicroscopyImageFromPixelData(interpolatedData, sampleGrid[0].length * stepSize[0], sampleGrid.length * stepSize[1]);
		return mi;
	}

	public static double[][] interpolateMatrixFromCoefficients(double[][] coeff, int w, int h) {
		double[][] out = new double[h][w];
		BSplineModel bs = new BSplineModel(coeff);

		for (int v = 0; v < h; v++) {
			final double tv = (double)(v * (coeff.length - 3)) / (double)(h - 1) + 1.0D;
			for (int u = 0; u < w; u++) {
				final double tu = (double) (u * (coeff[0].length - 3)) / (double) (w - 1) + 1.0D;
				bs.prepareForInterpolation(tu, tv, false);
				out[v][u] = (float) bs.interpolateI();
			}
		}
		return out;
	}

	public static double[][] refineBSplineCoefficients(double[][] in) {
		double[][] out = new double[(in.length-3)*2+3][(in[0].length-3)*2+3];

		for (int jo = -1; jo < out.length - 1; jo++) {
			for (int io = -1; io < out[0].length - 1; io++) {
				int i = (int) Math.floor(io/2) + 1;
				int j = (int) Math.floor(jo/2) + 1;
				if (jo%2 == 0) {
					if (io%2 == 0) {
						out[jo+1][io+1] = (in[j-1][i-1] + in[j+1][i-1] + in[j-1][i+1] + in[j+1][i+1]
								+ 6 * (in[j][i-1] + in[j-1][i] + in[j+1][i] + in[j][i+1]) + 36 * in[j][i]) / 64;
					} else {
						out[jo+1][io+1] = (in[j-1][i] + in[j+1][i] + in[j-1][i+1] + in[j+1][i+1]
								+ 6 * (in[j][i] + in[j][i+1])) / 16;
					}
				} else {
					if (io%2 == 0) {
						out[jo+1][io+1] = (in[j][i-1] + in[j+1][i-1] + in[j][i+1] + in[j+1][i+1]
								+ 6 * (in[j][i] + in[j+1][i])) / 16;
					} else {
						out[jo+1][io+1] = (in[j][i] + in[j+1][i] + in[j][i+1] + in[j+1][i+1]) / 4;
					}
				}
			}
		}
		return out;
	}

	public static double[][] getLandmarkResidualError(double[][] Pin, int[] max, double[][] coeff) {
		BSplineModel bs = new BSplineModel(coeff);
		double[][] Pout = new double[Pin.length][3];

		for (int i = 0; i < Pin.length; i++) {
			Pout[i][0] = Pin[i][0];
			Pout[i][1] = Pin[i][1];
			double tu = Pin[i][0] * (coeff[0].length - 3) / max[0] + 1.0D;
			double tv = Pin[i][1] * (coeff.length - 3) / max[1] + 1.0D;
			Pout[i][2] = Pin[i][2] - bs.prepareForInterpolationAndInterpolateI(tu, tv, false, false);
		}
		return Pout;
	}

	public static ArrayList<deformationHandle> getLandmarkResidualError(ArrayList<deformationHandle> deformations, int[] max, double[][][] coeff) {
		ArrayList<deformationHandle> dHout = new ArrayList<>();
		BSplineModel bsX = new BSplineModel(coeff[0]);
		BSplineModel bsY = new BSplineModel(coeff[1]);

		for (deformationHandle dH : deformations) {
			deformationHandle dHdiff = new deformationHandle(dH);
			double tu = dHdiff.getX() * (coeff[0][0].length - 3) / max[0] + 1.0D;
			double tv = dHdiff.getY() * (coeff[0].length - 3) / max[1] + 1.0D;
			dHdiff.setDx(dHdiff.getDx() - bsX.prepareForInterpolationAndInterpolateI(tu, tv, false, false));
			dHdiff.setDy(dHdiff.getDy() - bsY.prepareForInterpolationAndInterpolateI(tu, tv, false, false));
			dHout.add(dHdiff);
		}
		return dHout;
	}


	public static void scatter2coefficients(double[][] points, int[] max, double[][] coeff) {
		class ScatteredPoint {
			public double x;
			public double y;
			public double z;

			ScatteredPoint(double x, double y, double z) {
				this.x = x;
				this.y = y;
				this.z = z;
			}
		}

		ArrayList<ScatteredPoint> SPs = new ArrayList<>();
		for (double[] p : points) {
			SPs.add(new ScatteredPoint(p[0]*(coeff[0].length-3)/max[0] + 1, p[1]*(coeff.length-3)/max[1] + 1, p[2]));
		}

		int width = max[0];
		int height = max[1];
		double[][] d = new double[coeff.length][coeff[0].length];
		double[][] w = new double[coeff.length][coeff[0].length];

		for (int v = 0; v < d.length; v++) {
			for (int u = 0; u < d[0].length; u++) {
				d[v][u] = 0;
				w[v][u] = 0;
			}
		}

		for (ScatteredPoint p : SPs) {
			if (p.x >= (coeff[0].length-2)) p.x = (coeff[0].length-2) - 0.001;
			if (p.y >= (coeff.length-2)) p.y = (coeff.length-2) - 0.001;
			int i = (int)p.x - 1;
			int j = (int)p.y - 1;
			double s = p.x - (int)p.x;
			double t = p.y - (int)p.y;
			double[][] BB = new double[4][4];
			double Sumwab2 = 0;

			for (int l = 0; l < 4; l++) {
				for (int k = 0; k < 4; k++) {
					BB[l][k] = calcBB(k, s, l, t);
					Sumwab2 += Math.pow(BB[l][k], 2);
				}
			}
			for (int l = 0; l < 4; l++) {
				for (int k = 0; k < 4; k++) {
					double cp = BB[l][k] * p.z / Sumwab2;
					d[j+l][i+k] += Math.pow(BB[l][k], 2) * cp;
					w[j+l][i+k] += Math.pow(BB[l][k], 2);
				}
			}
		}

		for (int v = 0; v < coeff.length; v++) {
			for (int u = 0; u < coeff[0].length; u++) {
				if (w[v][u] == 0) {
					coeff[v][u] = 0;
				} else {
					coeff[v][u] = d[v][u] / w[v][u];
				}
			}
		}
	}

	public static void scatter2coefficients(ArrayList<deformationHandle> deformations, int[] max, double[][][] coeff) {
		double[][][] d = new double[coeff.length][coeff[0].length][coeff[0][0].length];
		double[][][] w = new double[coeff.length][coeff[0].length][coeff[0][0].length];

		for (int dir = 0; dir < d.length; dir++) {
			for (int v = 0; v < d[0].length; v++) {
				for (int u = 0; u < d[0][0].length; u++) {
					d[dir][v][u] = 0;
					w[dir][v][u] = 0;
				}
			}
		}

		ArrayList<deformationHandle> dRel = new ArrayList<>();
		for (deformationHandle p : deformations) {
			deformationHandle dH = new deformationHandle(p);
			dH.setX(dH.getX()*(coeff[0][0].length-3)/max[0] + 1);
			dH.setY(dH.getY()*(coeff[0].length-3)/max[1] + 1);
			dRel.add(dH);
		}

		for (deformationHandle p : dRel) {
			if (p.getX() >= (coeff[0][0].length-2)) p.setX((coeff[0][0].length-2) - 0.001);
			if (p.getY() >= (coeff[0].length-2)) p.setY((coeff[0].length-2) - 0.001);
			int i = (int)p.getX() - 1;
			int j = (int)p.getY() - 1;
			double s = p.getX() - (int)p.getX();
			double t = p.getY() - (int)p.getY();
			double[][] BB = new double[4][4];
			double Sumwab2 = 0;

			for (int l = 0; l < 4; l++) {
				for (int k = 0; k < 4; k++) {
					BB[l][k] = calcBB(k, s, l, t);
					Sumwab2 += Math.pow(BB[l][k], 2);
				}
			}
			for (int l = 0; l < 4; l++) {
				for (int k = 0; k < 4; k++) {
					for (int dir = 0; dir < 2; dir++) {
						double cp = BB[l][k] * (dir==0?p.getDx():p.getDy()) / Sumwab2;
						double weight = Math.pow(BB[l][k] * p.getWeight(), 2);
						d[dir][j+l][i+k] += weight * cp;
						w[dir][j+l][i+k] += weight;
					}
				}
			}
		}

		for (int v = 0; v < coeff[0].length; v++) {
			for (int u = 0; u < coeff[0][0].length; u++) {
				for (int dir = 0; dir < 2; dir++) {
					if (w[dir][v][u] == 0) {
						coeff[dir][v][u] = 0;
					} else {
						coeff[dir][v][u] = d[dir][v][u] / w[dir][v][u];
					}
				}
			}
		}
	}

	public static double[][][] getBSplineCoefficientsFromScatteredPoints(ArrayList<deformationHandle> deformations, int[] max, int gridMin, int gridMax) {
		int width = gridMin;
		int height = gridMin;
		double[][][] CL = new double[2][height+3][width+3];
		double[][][] CLsum = new double[2][height+3][width+3];
		double[][][] d = new double[2][height+3][width+3];

		for (int dir = 0; dir < 2; dir++) {
			for (int v = 0; v < d.length; v++) {
				for (int u = 0; u < d[0].length; u++) {
					d[dir][v][u] = 0;
				}
			}
		}

		// Only allow points with coordinates above or equal to zero
		ArrayList<deformationHandle> dInRange = new ArrayList<>();
		ArrayList<deformationHandle> dInRangeOrig = new ArrayList<>();
		for (deformationHandle dH : deformations) {
			if (dH.getX() < 0 || dH.getY() < 0) continue;
			dInRange.add(new deformationHandle(dH));
			dInRangeOrig.add(new deformationHandle(dH));
		}
//		IJ.log("Original");
//		for (deformationHandle dH : dInRangeOrig) {
//			IJ.log("x="+dH.getX() + " y="+dH.getY() + " dx="+dH.getDx() + " dy="+dH.getDy());
//		}

		while (width <= gridMax) {
			MiscHelper.scatter2coefficients(dInRange, max, CL);
			CLsum[0] = MiscHelper.addArrays(d[0], CL[0]);
			CLsum[1] = MiscHelper.addArrays(d[1], CL[1]);

			dInRange = MiscHelper.getLandmarkResidualError(dInRangeOrig, max, CLsum);
//			IJ.log("post width="+width);
//			for (deformationHandle dH : dInRange) {
//				IJ.log("x="+dH.getX() + " y="+dH.getY() + " dx="+dH.getDx() + " dy="+dH.getDy());
//			}
//			MiscHelper.printdoubleArray(CL[0]);
//			MiscHelper.printdoubleArray(CL[1]);
//			MiscHelper.printdoubleArray(CLsum[0]);
//			MiscHelper.printdoubleArray(CLsum[1]);


			width *= 2;
			height *= 2;
			CL = new double[2][height+3][width+3];
			d[0] = MiscHelper.refineBSplineCoefficients(CLsum[0]);
			d[1] = MiscHelper.refineBSplineCoefficients(CLsum[1]);
//			MiscHelper.printdoubleArray(d);
		}

		return CLsum;
	}

	public static double[][] getBSplineCoefficientsFromScatteredPoints(ArrayList<deformationHandle> deformations, boolean xDir, int[] max, int gridMin, int gridMax) {
		double[][] points = new double[deformations.size()][3];
		for (int i = 0; i < points.length; i++) {
			points[i][0] = deformations.get(i).getX();
			points[i][1] = deformations.get(i).getY();
			points[i][2] = xDir? deformations.get(i).getDx() : deformations.get(i).getDy();
		}
		return getBSplineCoefficientsFromScatteredPoints(points, max, gridMin, gridMax);
	}

	public static double[][] getBSplineCoefficientsFromScatteredPoints(double[][] points, int[] max, int gridMin, int gridMax) {
		int width = gridMin;
		int height = gridMin;
		double[][] CL = new double[height+3][width+3];
		double[][] CLsum = new double[height+3][width+3];
		double[][] d = new double[height+3][width+3];

		for (int v = 0; v < d.length; v++) {
			for (int u = 0; u < d[0].length; u++) {
				d[v][u] = 0;
			}
		}

		// Only allow points with coordinates above or equal to zero
		ArrayList<Double[]> pointsInRange = new ArrayList<>();
		for (int i = 0; i < points.length; i++) {
			if (points[i][0] < 0 || points[i][1] < 0) continue;
			Double[] p = {points[i][0], points[i][1], points[i][2]};
			pointsInRange.add(p);
		}
		double[][] P = new double[pointsInRange.size()][3];
		double[][] Porig = new double[pointsInRange.size()][3];
		for (int i = 0; i < P.length; i++) {
			Porig[i][0] = P[i][0] = pointsInRange.get(i)[0];
			Porig[i][1] = P[i][1] = pointsInRange.get(i)[1];
			Porig[i][2] = P[i][2] = pointsInRange.get(i)[2];
		}
//		MiscHelper.printdoubleArray(P);

		while (width <= gridMax) {
			MiscHelper.scatter2coefficients(P, max, CL);
//			P = MiscHelper.getLandmarkResidualError(P, max, CL);
			CLsum = MiscHelper.addArrays(d, CL);
			P = MiscHelper.getLandmarkResidualError(Porig, max, CLsum);

//			IJ.log("width="+width);
//			MiscHelper.printdoubleArray(P);
//			MiscHelper.printdoubleArray(CL);
//			MiscHelper.printdoubleArray(CLsum);


			width *= 2;
			height *= 2;
			CL = new double[height+3][width+3];
			d = MiscHelper.refineBSplineCoefficients(CLsum);
//			MiscHelper.printdoubleArray(d);
		}

		return CLsum;
	}

	public static double calcBB(int k, double s, int l, double t) {
		return calcB(k, s) * calcB(l, t);
	}

	public static double calcB(int k, double s) {
		double res = 0;
		switch (k) {
			case 0:
				res = Math.pow(1 - s, 3) / 6;
				break;
			case 1:
				res = (3*Math.pow(s, 3) - 6*Math.pow(s, 2) + 4) / 6;
				break;
			case 2:
				res = (-3*Math.pow(s, 3) + 3*Math.pow(s, 2) + 3*s + 1) / 6;
				break;
			case 3:
				res = Math.pow(s, 3) / 6;
				break;
		}
		return res;
	}

	public static double calculateGiniCoefficient(double[] yDist, int[] order) {
		double[] xDist = new double[yDist.length];
		for (int i = 0; i < xDist.length; i++) {
			xDist[i] = 1;
		}
		return calculateGiniCoefficient(xDist, yDist, order);
	}

	public static double calculateGiniCoefficient(double[] xDist, double[] yDist, int[] order) {
		double gini = 0;
		double xDistSum = 0;
		double yDistSum = 0;
		double[][] sorted = new double[Math.min(xDist.length, yDist.length)][4];

		for (int i = 0; i < sorted.length; i++) {
			xDistSum += xDist[i];
			yDistSum += yDist[i];
		}
		for (int i = 0; i < sorted.length; i++) {
			sorted[i][0] = xDist[i] / xDistSum;
			sorted[i][1] = yDist[i] / yDistSum;
			sorted[i][2] = sorted[i][1] / sorted[i][0];
		}
		sorted = sortNaturally(sorted, 2, order);
		for (int i = 0; i < sorted.length; i++) {
			sorted[i][3] = (i>0)? sorted[i-1][3] + sorted[i][1] : sorted[i][1];
		}

		for (int i = 0; i < sorted.length; i++) {
			gini += (sorted[i][3] + ((i == 0)? 0 : sorted[i-1][3])) * sorted[i][0] / 2;
		}
		gini = 1 - 2 * gini;

		return gini;
	}

}
