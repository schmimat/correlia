/*
 * Calculate mutual information for two image processors
 * Author: Florens Rohde
 *
 * Part of:
 * Correlia plugin for ImageJ and FIJI
 */
package de.ufz.correlia;

import ij.IJ;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class mutualInformationFast {
	public static final int PLAIMI = 0;
	public static final int NMI1D = 1;
	public static final int NMI2D = 2;
	public static final int MIE = 3;
	public static final int JE = 4;

	private int type;
	public int nbins;
	private double logbase;
	private ImageProcessor[][] ip;
	private double[] min;
	private double[] max;
	double[][] pdf1D;
	double[] pdf2D;
	public double lastResult;

	mutualInformationFast(ImageProcessor[][] ip, int type, int nbins, double[] min, double[] max) {
		this(ip, type, nbins, min, max, 10);
	}

	mutualInformationFast(ImageProcessor[][] ip, int type, int nbins, double[] min, double[] max, double logbase) {
		this.type = type;
		this.nbins = nbins;
		this.logbase = logbase;
		setMinMax(min, max);
		lastResult = calcNMI(ip);
	}

	private double log(double arg) {
		return log(arg, logbase);
	}

	public void setMinMax(double[] min, double[] max) {
		this.min = min;
		this.max = max;
	}

	public void calcPDF() {
		calcPDF1D();
		calcPDF2D();
	}

	public void calcPDF1D() {
		if (ip[0] == null || ip[1] == null) {
			throw new ArithmeticException("ImageProcessor is null.");
		}
		int h = Math.min(ip[0][0].getHeight(), ip[1][0].getHeight());
		int w = Math.min(ip[0][0].getWidth(), ip[1][0].getWidth());
		long[][] pxCount = new long[2][nbins];
		double[] slope = new double[2];
		boolean[] fp = new boolean[2];
		for (int k = 0; k < 2; k++) {    // hoist these calculations
			slope[k] = (double) nbins / (max[k] - min[k]);
			fp[k] = ip[k][0] instanceof FloatProcessor;
		}

		int[] pxSum = {0, 0};
		for (int j = 0; j < h; j++) {
			int bin;
			for (int i = 0; i < w; i++) {
				if (ip[0][1].get(i, j) == 0 || ip[1][1].get(i, j) == 0) {
					continue;
				}
				for (int k = 0; k < 2; k++) {
					double f;
					if (fp[k]) {
						f = Float.intBitsToFloat(ip[k][0].get(i, j));
					} else {
						f = (double)ip[k][0].get(i, j);
					}
					bin = (int) (slope[k] * (f - min[k]));
					bin = Math.min(nbins - 1, bin); // Use out of range bins
					pxCount[k][bin]++;
					pxSum[k]++;
				}
			}
		}

		pdf1D = new double[2][nbins];
		for (int k = 0; k < 2; k++) {
			double invPxCount;
			if (pxSum[k] == 0) {
				IJ.log("Zero pixels");
				invPxCount = 0;
			} else {
				invPxCount = 1.0F / (double) pxSum[k];
			}
			for (int i = 0; i < nbins; i++) {
				pdf1D[k][i] = invPxCount * (double) pxCount[k][i];
			}
		}
	}

	public void calcPDF2D() {
		int nbinsSq = nbins * nbins;

		if (ip[0] == null || ip[1] == null) {
			throw new ArithmeticException("ImageProcessor is null.");
		}
		int h = Math.min(ip[0][0].getHeight(), ip[1][0].getHeight());
		int w = Math.min(ip[0][0].getWidth(), ip[1][0].getWidth());
		long[] pxCount = new long[nbinsSq];
		int pxSum = 0;
		double[] slope = new double[2];
		boolean[] fp = new boolean[2];
		for (int k = 0; k < 2; k++) {    // hoist these calculations
			slope[k] = (double) nbins / (max[k] - min[k]);
			fp[k] = ip[k][0] instanceof FloatProcessor;
		}

		int[] bin = {0, 0};
		for (int j = 0; j < h; j++) {
			for (int i = 0; i < w; i++) {
				if (ip[0][1].get(i, j) == 0 || ip[1][1].get(i, j) == 0) {
					continue;
				}
				for (int k = 0; k < 2; k++) {
					double f;
					if (fp[k]) {
						f = Float.intBitsToFloat(ip[k][0].get(i, j));
					} else {
						f = (double)ip[k][0].get(i, j);
					}
					bin[k] = (int) (slope[k] * (f - min[k]));
					bin[k] = Math.min(nbins - 1, bin[k]); // Use out of range bins
				}
				pxCount[bin[1] * nbins + bin[0]]++;
				pxSum++;
			}
		}

		double invPxSum;
		if (pxSum == 0) {
			IJ.log("Zero pixels");
			invPxSum = 0;
		} else {
			invPxSum = 1.0F / (double) pxSum;
		}
		pdf2D = new double[nbinsSq];
		for(int b = 0; b < nbinsSq; b++) {
			pdf2D[b] = invPxSum * (double) pxCount[b];
		}
	}

	public double calcNMI(ImageProcessor[][] ipIn) {
		return calcNMI(ipIn, false);
	}

	public double calcNMI(ImageProcessor[][] ipIn, boolean usePrecalculatedPDF) {
		ip = ipIn;
		assert ip.length == 2;
		double mutual = 0F, jentropy = 0F;

		if (!usePrecalculatedPDF) {
			calcPDF();
		}

		for(int j = 0; j < nbins; j++) {
			for (int i = 0; i < nbins; i++) {
				double prod = pdf1D[1][j] * pdf1D[0][i];
				if (prod == 0F) continue;
				int n = j * nbins + i;
				if (pdf2D[n] == 0) continue;
				mutual += pdf2D[n] * log(pdf2D[n] / prod);
				switch (type) {
					default:
					case PLAIMI:
						break;
					case NMI1D:
						jentropy -= (pdf1D[0][i] * log(pdf1D[0][i])) + (pdf1D[1][j] * log(pdf1D[1][j]));
						break;
					case JE:
					case NMI2D:
						jentropy -= pdf2D[n] * log(pdf2D[n]);
						break;
				}
				if (Double.isNaN(jentropy)) {
					IJ.log("jentropy is NaN for pdf1D[0]["+i+"]:" + pdf1D[0][i] + " pdf1D[1]["+j+"]:" + pdf1D[1][j]);
				}
			}
		}
		switch (type) {
			default:
				break;
			case PLAIMI:
				jentropy = 1;
				break;
			case NMI1D:
				jentropy = 2 * jentropy;
				break;
			case JE:
				jentropy = mutual / jentropy;
				break;
		}

		lastResult = mutual / jentropy;
		return lastResult;
	}

	public static double calcEntropy(ImageProcessor ip, int nbins, double min, double max, double logbase) {
		double entropy = 0;
		double[] pdf = calcPDF(ip, nbins, min, max);

		for(int i = 0; i < nbins; i++) {
			if (pdf[i] == 0) continue;
			entropy -= pdf[i] * log(pdf[i], logbase);
		}

		return entropy;
	}

	public static double[] calcPDF(ImageProcessor ip, int nbins, double min, double max) {
		int h = ip.getHeight();
		int w = ip.getWidth();
		int[] row = new int[w];
		long[] pxCount = new long[nbins];
		double slope = (double) nbins / (max - min);
		boolean fp = ip instanceof FloatProcessor;

		int pxSum = 0;
		for (int j = 0; j < h; j++) {
			int bin;
			ip.getRow(0, j, row, w);
			for (int i = 0; i < w; i++) {
				double f;
				if (fp)
					f = Float.intBitsToFloat(row[i]);
				else
					f = (double) row[i];
				bin = (int) Math.floor((slope * (f - min)));

				if (bin >= 0 && bin < nbins) {
					pxCount[bin]++;
					pxSum++;
				}
			}
		}

		double[] pdf1D = new double[nbins];
		double invPxSum;
		if (pxSum == 0) {
			IJ.log("Zero pixels");
			invPxSum = 0;
		} else {
			invPxSum = 1.0F / (double) pxSum;
		}
		for (int i = 0; i < nbins; i++) {
			pdf1D[i] = invPxSum * (double) pxCount[i];
		}

		return pdf1D;
	}

	public static double log(double arg, double base) {
		if (base == 10) {
			return Math.log10(arg);
		} else if (base >= 2.71 && base <= 2.72) {
			return Math.log(arg);
		} else {
			return Math.log(arg) / Math.log(base);
		}
	}
}
