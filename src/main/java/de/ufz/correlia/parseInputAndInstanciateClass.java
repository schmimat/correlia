package de.ufz.correlia;

import java.util.ArrayList;
import java.util.Arrays;
import java.lang.reflect.InvocationTargetException;
//import org.apache.commons.lang.StringEscapeUtils;

// DEBUG, take out later
import ij.*;
// 

public class parseInputAndInstanciateClass
{
	public parseInputAndInstanciateClass()
	{
		m_error = new String("");
		m_className = new String("");
		m_subString = new String("");
		m_remainderString = new String("");
	}
	
	public void parseString(String s)
	{ parseString(s,""); }
	
	public void parseString(String s, String path)
	{
		if ( s.isEmpty() ){ m_className = new String(""); m_remainderString = new String(""); return; }
	
		 m_className = new String(""); // to be save that a new class definition is gonna be set
	
		String delims = "[ \n\t]";
		
		// split string and remove empty elements, save everything in an array list
		ArrayList<String> words = new ArrayList<String>(Arrays.asList(modify_path_in_string(s.split(delims),path)));
		words.removeAll(Arrays.asList("", null));
		
		int beginFlagFound = 0;
		String w;
		
		int substrIdxStart = 0;
		int substrIdxEnd = 0;
		int remainderStringStart = words.size();

		m_subString = new String("");
		m_remainderString = new String("");
		
		// analyse subString

		for ( int i=0; i<words.size(); i++ )
		{
			w = words.get(i);
			// if "end xyz" is found stop
			if ( w.equals("end") && beginFlagFound == 2 && words.get(i+1).equals(words.get(substrIdxStart)) )
			{ 
				substrIdxEnd = i; remainderStringStart = i+1; beginFlagFound--;
				break;
			}

			if ( beginFlagFound == 1 && w.length()>0 )
			{ 
				substrIdxStart = i;
				beginFlagFound++;
			}

			if ( w.equals("begin") && beginFlagFound == 0 ) // first begin in string s
			{ beginFlagFound++; }
			
		}
		
		if ( substrIdxStart == 0 )
		{ 
			m_error = new String("Error in parseInputAndInstantiateClass: no class definition found");
			return;
		}
		
		if ( substrIdxEnd == 0 )		
		{ 
			m_error = new String("Error in parseInputAndInstantiateClass: "+words.get(substrIdxStart)+" cannot be instantiated: missing end "+words.get(substrIdxStart));
			return;
		}
		
		m_className = words.get(substrIdxStart);

		// build substring
		for ( int i=substrIdxStart+1; i<substrIdxEnd; i++ )
		{
			if ( words.get(i).length()>0 )
			m_subString += words.get(i) + " ";
		}
		
		// build remainder string
		for ( int i=remainderStringStart; i<words.size(); i++ )
		{ m_remainderString += words.get(i) + " "; }

		
	}
	
	String[] modify_path_in_string(String[] keywords, String path)
	{
		for ( int i=0; i<keywords.length; i++ )
		{
			if ( keywords[i].equals("path") )
			{
				keywords[i+1] = path.replaceAll(" ","\\\\s\\\\") + keywords[i+1];
				//keywords[i+1] = StringEscapeUtils.escapeJava(path) + keywords[i+1];
			}
		}
	
		return keywords;
	}
	
	
	
	
	// get parsing results and class
	
	public String getClassName()
	{ return m_className; }
	
	public String getSubstring()
	{ return m_subString;}
	
	public String getRemainderString()
	{ return m_remainderString; }
	
	public Object instantiateClass()
	{
		try
		{ 
			Class c = Class.forName(m_className);
			return c.getDeclaredConstructor(String.class).newInstance( m_subString );
		}
		catch (ClassNotFoundException e)
		{ 
			m_error = new String("Error in parseInputAndInstantiateClass: "+m_className+" cannot be instantiated: class definition not found");
			return null;
		}
		catch (InstantiationException e)
		{ 
			m_error = new String("Error in parseInputAndInstantiateClass: "+m_className+" cannot be instantiated: InstantiationException");
			return null;
		}
		catch (IllegalAccessException e)
		{ 
			m_error = new String("Error in parseInputAndInstantiateClass: "+m_className+" cannot be instantiated: IllegalAccessException");
			return null;
		}
		catch (NoSuchMethodException e)
		{ 
			m_error = new String("Error in parseInputAndInstantiateClass: "+m_className+" cannot be instantiated: NoSuchMethodException");
			return null;
		}
		catch (InvocationTargetException e)
		{ 
			m_error = new String("Error in parseInputAndInstantiateClass: "+m_className+" cannot be instantiated: InvocationTargetException");
			return null;
		}
	}
	
	public String get_errorMessage()
	{ return m_error; }

	static public String classID(){ return "parseInputAndInstanciateClass"; }
	static public String author(){ return "Matthias Schmidt"; }
	static public String version(){ return "May 15 2017"; }
	
	// members
	private String m_className;
	private Class m_class;
	private String m_subString;
	private String m_remainderString;
	
	private String m_error;
	
}