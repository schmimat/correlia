/*
 * Dialog for editing the properties of an image
 * that can be of class ImagePlus or microscopyImage
 * Author: Matthias Schmidt, Florens Rohde
 *
 * Part of:
 * Correlia plugin for ImageJ and FIJI
 */
package de.ufz.correlia;

import ij.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

class dlgEditMicroscopyImageProperties extends JDialog {
	private static final Integer TEXTFIELD_WIDTH = 40;
	private static final Integer TEXTAREA_HEIGHT = 6;
	private static final Integer NUMERICFIELD_WIDTH = 40;
	public static final DateTimeFormatter ACQUISITIONDATE_FORMAT = java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;

	private Boolean wasCancelled;
	private JTextField tfTitle;
	private JTextField tfAcquisitionDate;
	private JTextField tfExperimenter;
	private JTextField tfSetup;
	private JTextArea taAdditionalInfo;
	private ImagePlus image;
	private String title;
	private LocalDate acquisitionDate;
	private String setup = "";
	private String experimenter = "";
	private String additionalInfo = "";


	dlgEditMicroscopyImageProperties(ImagePlus img) {
		debug.put(" entered");
		title = img.getTitle();
		acquisitionDate = LocalDate.now();
		setup_dialog(null);
	}

	dlgEditMicroscopyImageProperties(microscopyImage mi, Component parent) {
		debug.put(" entered");
		title = mi.getTitle();
		acquisitionDate = mi.get_acquisitionDate();
		setup = mi.get_setup();
		experimenter = mi.get_experimenter();
		additionalInfo = mi.get_additionalInfo();

		setup_dialog(parent);
	}

	private boolean setup_dialog(Component parent) {
		this.setTitle("Edit Image Properties");
		this.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		this.setMinimumSize(new Dimension(400, 400));

		Panel p = new Panel();
		p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));

		Panel ppropcontainer = new Panel();
		ppropcontainer.setLayout(new BoxLayout(ppropcontainer, BoxLayout.X_AXIS));

		JPanel pprop = new JPanel();
		GridLayout grdlay = new GridLayout(0, 2, 10, 10);    // parameters: columns, rows, horizontal gap, vertical gap

		pprop.setLayout(grdlay);

		//setup property textfields

		pprop.add(new JLabel("Title:"));
		tfTitle = new JTextField(title, TEXTFIELD_WIDTH);
		pprop.add(tfTitle);

		pprop.add(new JLabel("Acquisition date:"));
		tfAcquisitionDate = new JTextField(acquisitionDate.format(ACQUISITIONDATE_FORMAT));
		pprop.add(tfAcquisitionDate);

		pprop.add(new JLabel("Setup:"));
		tfSetup = new JTextField(setup, TEXTFIELD_WIDTH);
		pprop.add(tfSetup);

		pprop.add(new JLabel("Experimenter:"));
		tfExperimenter = new JTextField(experimenter, TEXTFIELD_WIDTH);
		pprop.add(tfExperimenter);

		//add properties panel
		ppropcontainer.add(Box.createHorizontalStrut(15));
		ppropcontainer.add(pprop);
		ppropcontainer.add(Box.createHorizontalStrut(15));
		p.add(ppropcontainer);
		p.add(Box.createVerticalStrut(10));

		p.add(new JLabel("Additional information:"));
		taAdditionalInfo = new JTextArea(additionalInfo);
		JScrollPane spAdditionalInfo = new JScrollPane(taAdditionalInfo);
		spAdditionalInfo.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		spAdditionalInfo.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 14));
		spAdditionalInfo.setMinimumSize(new Dimension(180, 120));
		spAdditionalInfo.setMaximumSize(new Dimension(180, 120));
		p.add(taAdditionalInfo);
		p.add(Box.createVerticalStrut(10));

		// add and initialise buttons
		Panel pBtn = new Panel();
		pBtn.setLayout(new BoxLayout(pBtn, BoxLayout.X_AXIS));

		pBtn.add(Box.createHorizontalStrut(30));
		JButton btnOK = new JButton("OK");
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				userConfirm(true);
			}
		});
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(
				e -> userConfirm(false)
		);

		pBtn.add(Box.createHorizontalStrut(20));
		pBtn.add(btnOK);
		pBtn.add(Box.createHorizontalStrut(15));
		pBtn.add(btnCancel);
		pBtn.add(Box.createHorizontalStrut(20));

		p.add(pBtn);
		p.add(Box.createVerticalStrut(10));
		this.add(p);

		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		return true;
	}

	public String get_title() {
		return tfTitle.getText();
	}

//	public double get_width() {
//		return Double.parseDouble(tfWidth.getText());
//	}

//	public double get_heigth() {
//		return Double.parseDouble(tfHeight.getText());
//	}

	public LocalDate get_acquisitionDate() {
		LocalDate ld;
		try {
			ld = LocalDate.parse(tfAcquisitionDate.getText(), ACQUISITIONDATE_FORMAT);
		} catch (DateTimeParseException e) {
			ld = acquisitionDate;
		}
		return ld;
	}

	public String get_setup() {
		return tfSetup.getText();
	}

	public String get_experimenter() {
		return tfExperimenter.getText();
	}

	public String get_additionalInfo() {
		return taAdditionalInfo.getText();
	}

	private void userConfirm(Boolean OKCancel) { // true = OK, false = Cancel
		wasCancelled = !OKCancel;
		this.setVisible(false);
	}

	public Boolean was_cancelled() {
		return wasCancelled;
	}

	static public String classID(){ return "dlgEditMicroscopyImageProperties"; }
	static public String author(){ return "Matthias Schmidt, Florens Rohde"; }
	static public String version(){ return "June 29 2017"; }
}
