// xy-data class is used for the manipulation of xy data
// it includes methods to:
// read/write data files
// integrate the data
//
package de.ufz.correlia;

import java.util.*;
import java.awt.*;
import java.io.*;
import java.math.BigDecimal;	// rounding numbers
import java.math.MathContext;	// rounding numbers

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

class xydata
{
  public xydata()	// constructor, empty object
  {
	m_x = new Vector();
	m_y = new Vector();
	
	m_is_sorted = false;
	
	m_linreg_slope = 0.0;
	m_linreg_offset = 0.0;
	
	m_imin = 0;
	m_imax = 0;
  }
  
  public xydata( String s )	// constructor is called by parser when xydata is instanciated that way
  {
	m_x = new Vector();
	m_y = new Vector();
	
	m_is_sorted = false;
	
	m_linreg_slope = 0.0;
	m_linreg_offset = 0.0;
	
	m_imin = 0;
	m_imax = 0;
	
	parse_parameterString(s);
  }
  

  public Boolean parse_parameterString( String s )
  {
  	String words[] = s.split("[ \n\t]");
  	for ( int i=0; i<words.length; i++ )	// get parameters for this image
  	{
  		if ( words[i].equals("file") && i+3<words.length ) // read data from file
  		{
  			if ( !read_datafile( words[i+1], Integer.parseInt(words[i+2]), Integer.parseInt(words[i+3]) ) )
  			{ return false;}
  		}

  		if ( words[i].equals("data") && i+2<words.length ) // read data from string
  		{
  			double x=0.0;
  			double y=0.0;
  		
  			i++;
  			
			NumberFormat format = NumberFormat.getInstance(Locale.ENGLISH);
			try{
				while ( i+1<words.length && !words[i].equals("endData") )
				{
					x = format.parse(words[i].replaceAll(",",".")).doubleValue();
					y = format.parse(words[i+1].replaceAll(",",".")).doubleValue();
					push_back(x,y);
					i+=2;
				}
  			}
			catch( ParseException e ){return false;}
  		}
  		
  		if ( words[i].equals("isSorted")){ m_is_sorted = true; }
  	}
  	return true;
  }
  
  
  public void push_back( double x, double y )
  {
	m_x.addElement( x );
	m_y.addElement( y );
	
	m_is_sorted = false;
  }

  public Boolean read_datafile( String path_to_file, int xcol, int ycol)
  {	
	File file = new File(path_to_file);
	BufferedReader reader = null;
  
	try {
		reader = new BufferedReader(new FileReader(file));
		String line = null;
		String data = null;
		int col = 0;
		double x=0.0;
		double y=0.0;

		int check=0;	// ensure two columns were recognised
		
		while ((line = reader.readLine()) != null)	// read file
		{
			StringTokenizer st = new StringTokenizer(line);
			col = 0;
			x = 0.0;
			y = 0.0;
			check=0; 
			 
			try
			{ 
				while (st.hasMoreTokens())
				{
					data = st.nextToken();
					if ( col == xcol ){ x = Double.parseDouble(data.replaceAll(",",".")); check++; }
					if ( col == ycol ){ y = Double.parseDouble(data.replaceAll(",",".")); check++; }
					col++;
				}

			 }
			catch ( NumberFormatException e )
			{ System.out.println("skipping header"); } 
			if( check==2 ){ push_back(x,y); }
		}
		reader.close();
	}
	catch (FileNotFoundException e) { e.printStackTrace(); return false;}
	catch (IOException e) { e.printStackTrace(); return false;}
	finally
	{
		try
		{ if (reader != null) { reader.close(); } }
		catch (IOException e) { return false; }
	}

	m_imin=0;
	m_imax=size()-1;
	return true;
  }
  
  public void save_datafile( String path_to_file )
  {
	Writer writer = null;

	try {
		writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path_to_file), "utf-8"));
 		for ( int i=0; i<size(); i++ )
		{ writer.write(""+get_x(i)+"\t"+get_y(i)+"\n"); }
	}
	catch (IOException e){ e.printStackTrace(); }
	finally
	{
		try
		{ if ( writer != null ){ writer.close(); } }
		catch (Exception ex) {}
	}
  }
  
  
  public double get_x( int idx )
  {
	if ( idx>=size() || idx<0 ){ return 0.0; }
	return (Double) m_x.elementAt( idx );
  }

  public double get_y( int idx )
  {
	if ( idx>=size() || idx<0 ){ return 0.0; }
	return (Double) m_y.elementAt( idx );
  }
 
  public int get_idx( double x )	// returns the index of the closest data point
  {
	if ( !is_sorted() ){ sort(); }
	
	int imin = 0;
	double xmin = get_x(imin);
	int imax = size()-1;
	double xmax = get_x(imax);
	int imid = (imax+imin)/2;
	double xmid = get_x(imid);
	
	while ( imid-imin > 0 && imax-imid > 0 )
	{
		if ( x>xmid ){ imin=imid; xmin=xmid; }
		if ( x<xmid ){ imax=imid; xmax=xmid; }
		if ( x==xmid ){ return imid; }
		
		imid = (imax+imin)/2;
		xmid = get_x(imid);
	}
	return imid;
  }
 
  public int size( )  { return m_x.size(); }

  public xydata copy()
  {
	xydata c = new xydata();
	
	c.m_x = m_x;
	c.m_y = m_y;
	c.m_is_sorted = m_is_sorted;
	c.m_linreg_slope = m_linreg_slope;
	c.m_linreg_offset = m_linreg_offset;
	
	return c;
  }
  
  public Boolean is_sorted(){ return m_is_sorted; }
  public Boolean sort()
  {
	if ( size() < 2 ){ return false; }
  
	for ( int i=0; i<size(); i++ )
	{
		double xi = (Double) m_x.elementAt(i);
		double yi = (Double) m_y.elementAt(i);
		int imin = i;
		double xmin = (Double) m_x.elementAt(i);
		
		for ( int j=i; j<size(); j++ )
		{
			if ( (Double) m_x.elementAt(j) <= xmin )
			{ imin = j; xmin =(Double) m_x.elementAt(j); }
		}
		
		m_x.set( i, xmin );
		m_y.set( i, (Double) m_y.elementAt(imin) );
		
		m_x.set( imin, xi );
		m_y.set( imin, yi );
	}
	
	average_multiple_xdata(); // y1(x), y2(x), y3(x) -> [(y1+y2+y3)/3](x)
	m_is_sorted = true;
	return true;
  }
  
  private void average_multiple_xdata()
  {

	double yi=0;
	int j=1;
	
	Vector vx = new Vector();
	Vector vy = new Vector();
	
	for ( int i=0; i<size(); i+=j )
	{
		yi=0.0;
		
		for ( j=0; i+j<size() && ( ((Double) m_x.elementAt(i+j)).equals(((Double) m_x.elementAt(i))) ); j++ )
		{ yi += (Double) m_y.elementAt(i+j);}
		vx.addElement((Double) m_x.elementAt(i));	// reduce data points
		vy.addElement(yi/j);	//average
	}
	m_x = vx;
	m_y = vy;
  }
  
  // x-range & y-range
  
  public double xmin()
  {
	if ( size()==0 ){ return 0.0; }
	if ( size()==1 ){ return (Double) m_x.elementAt(0); }
	if ( !is_sorted() ){ sort(); }
	
	return (Double) m_x.elementAt(0); 
  }
  
  public double xmax()
  {
	if ( size()==0 ){ return 0.0; }
	if ( size()==1 ){ return (Double) m_x.elementAt(0); }
	if ( !is_sorted() ){ sort(); }
	
	return (Double) m_x.elementAt(size()-1); 
  }
  
  public double ymin()
  {
	if ( size()==0 ){ return 0.0; }
	if ( size()==1 ){ return (Double) m_y.elementAt(0); }
	if ( !is_sorted() ){ sort(); }
	double ym = (Double) m_y.elementAt(0);
	
	for ( int i=0; i<size(); i++ )
	{ if ( (Double) m_y.elementAt(i) < ym ){ ym = (Double) m_y.elementAt(i); } }
		
	return ym;
  }
  
  public double ymax()
  {
	if ( size()==0 ){ return 0.0; }
	if ( size()==1 ){ return (Double) m_y.elementAt(0); }
	if ( !is_sorted() ){ sort(); }
	double ym = (Double) m_y.elementAt(0);
	
	for ( int i=0; i<size(); i++ )
	{ if ( (Double) m_y.elementAt(i) > ym ){ ym = (Double) m_y.elementAt(i); } }
		
	return ym;
  }

  // x-range & y-range rounded for displaying purposes

  public double xmin_round(int digits)
  {
	BigDecimal bd = new BigDecimal(xmin());
	bd = bd.round(new MathContext(digits));

	return bd.doubleValue();
  }
  
  
  public double xmax_round(int digits)
  {
	BigDecimal bd = new BigDecimal(xmax());
	bd = bd.round(new MathContext(digits));

	return bd.doubleValue();
  }

  
  
  public double ymin_round(int digits)
  {
	BigDecimal bd = new BigDecimal(ymin());
	bd = bd.round(new MathContext(digits));

	return bd.doubleValue();
  }
  
  
  public double ymax_round(int digits)
  {
	BigDecimal bd = new BigDecimal(ymax());
	bd = bd.round(new MathContext(digits));

	return bd.doubleValue();
  }

  
  
  public xydata split( int imin, int imax )
  {
	xydata res = new xydata();

	if ( imin < 0 || imax >=size() || imin >= imax ){ return res; }	// check range
	
	for ( int i=imin; i<=imax; i++ )
	{ res.push_back( get_x(i), get_y(i) ); }
	
	if ( is_sorted() )	// if original data was sorted, sub stack is also sorted
	{ res.m_is_sorted = true; }
	
	m_imin = imin;
	m_imax = imax;
	
	return res;
  }
  
  public xydata split( double xmin, double xmax )
  {
	xydata res = new xydata();

	if ( !is_sorted() ) { sort(); }
	
	if ( xmin >= xmax ) { return res; }	// check range

	if ( xmin < xmin() ){ xmin = xmin(); }
	if ( xmax > xmax() ){ xmax = xmax(); }
	
	int imin = find_closest_data_point( xmin );
	int imax = find_closest_data_point( xmax );
	
	return split( imin, imax );
  }
  
  public xydata unchecked_add( xydata xyd )
  {
	xydata sum = new xydata();
	
	for ( int i=0; i<size() && i<xyd.size(); i++ )
	{ xyd.push_back( get_x(i), get_y(i)+xyd.get_y(i) ); }
  
	return sum;
  }

  public xydata unchecked_sub( xydata xyd )
  {
	xydata diff = new xydata();
	
	for ( int i=0; i<size() && i<xyd.size(); i++ )
	{ xyd.push_back( get_x(i), get_y(i)-xyd.get_y(i) ); }
  
	return diff;
  }

  public xydata unchecked_mul( xydata xyd )
  {
	xydata prod = new xydata();
	
	for ( int i=0; i<size() && i<xyd.size(); i++ )
	{ xyd.push_back( get_x(i), get_y(i)*xyd.get_y(i) ); }
  
	return prod;
  }
  
  
  
  
  public xydata add( xydata xyd, int number_of_points )	// add (x,y1) (x,y2) -> (x,y1+y2)
  {
	xydata sum = new xydata();

	// check ranges, if no overlap -> no result
	
	double min = ( xmin() < xyd.xmin() ) ? xyd.xmin() : xmin();
	double max = ( xmax() > xyd.xmax() ) ? xyd.xmax() : xmax();
	if ( min>=max ){ return sum; }
	
	double dx = (max-min) / ( (double) number_of_points );
	
	for ( double x=min; x<=max; x+=dx )
	{ sum.push_back( x, y_lip(x)+xyd.y_lip(x) ); }
  
	return sum;
  }
  
  
  
  
  public int find_closest_data_point( double x )	// will return closed index i for which x(i)<x
  {
	if ( size()==0 ){ return -1; }
	if ( size()==1 || size()==2 ){ return 0; }
	if ( !is_sorted() ){ sort(); }	// guarantee the data points are sorted for ascending x and no (x,y1), (x,y2) pairs occur
	
	// check that x is in range
	if ( x<xmin() || x>xmax() ){ return -1; }
	
	int imin = 0;
	int imax = size()-1;
	int imid = (imax+imin)/2;
	double xmin = (Double) m_x.elementAt(imin);
	double xmid = (Double) m_x.elementAt(imid);
	double xmax = (Double) m_x.elementAt(imax);

	while ( imin != imid && imid != imax )
	{	
		if ( x == xmin ){ imid = imin; break; }
		if ( x == xmid ){ break; }
		if ( x == xmax ){ imid = imax; break; }
		
		if ( xmin <= x && xmid > x )
		{ imax = imid; }
		
		if ( xmid <= x && xmax > x )
		{ imin = imid; }
		
		imid = (imax+imin)/2;
		xmin = (Double) m_x.elementAt(imin);
		xmid = (Double) m_x.elementAt(imid);
		xmax = (Double) m_x.elementAt(imax);
	}

	return imid;
  }
  
  // interpolation
  
  public double y_lip( double x )	// returns the linearily interpolated y(x) value for arbitray x (xmin<=x<=xmax)
  {
	if ( size()==0 ){ return 0.0; }
	if ( size()==1 ){ return (Double) m_y.elementAt(0); }
	if ( !is_sorted() ){ sort(); }
  
	if ( x<=xmin() ){ return (Double) m_y.elementAt(0); }
	if ( x>=xmax() ){ return (Double) m_y.elementAt(size()-1); }
	
	// find index i for which x(i)<x<x(i+1)
	int i=find_closest_data_point( x );
	
	double xi0 = (Double) m_x.elementAt(i);
	double xi1 = (Double) m_x.elementAt(i+1);
	double yi0 = (Double) m_y.elementAt(i);
	double yi1 = (Double) m_y.elementAt(i+1);
	
	// linear interpolation
	double m=(yi1-yi0)/(xi1-xi0);
	double n=yi0-m*xi0;
	
	return m*x+n;
  }
  
  public double dy_lip( double x )	// returns the linearily interpolated derivative of y(x) value for arbitray x (xmin<=x<=xmax)
  {
	if ( size()==0 ){ return 0.0; }
	if ( size()==1 ){ return 0.0; }
	if ( !is_sorted() ){ sort(); }
  
	if ( x<xmin() ){ return y_lip(xmin()); }
	if ( x>xmax() ){ return y_lip(xmax()); }
	
	// find index i for which x(i)<x<x(i+1)
	int i=find_closest_data_point( x );
	if ( i==size()-1 ){ i--; }
	
	double xi0 = (Double) m_x.elementAt(i);
	double xi1 = (Double) m_x.elementAt(i+1);
	double yi0 = (Double) m_y.elementAt(i);
	double yi1 = (Double) m_y.elementAt(i+1);
	
	// linear interpolation
	return (yi1-yi0)/(xi1-xi0);
  }
  
  public double inty_lip( double x )
  {
	if ( size()==0 ){ return 0.0; }
	if ( size()==1 ){ return 0.0; }
	if ( !is_sorted() ){ sort(); }

	if ( x<xmin() ){ return y_lip(xmin()); }
	if ( x>xmax() ){ return y_lip(xmax()); }
	
	// find index i for which x(i)<x<x(i+1)
	int i=find_closest_data_point( x );

	double sum = 0.0;
	double dx = 0.0;
	
	for (int j=0; j<i; j++ )
	{
		dx = get_x(j+1)-get_x(j);
		sum += (get_y(j)+get_y(j+1))*dx;
	}
	
	sum += (get_y(i)+y_lip(x))*(x-get_x(i));
	sum *= 0.5;
	
	return sum;
  }
  
 
  public xydata dy()	// return an xydata object which contains the derivative
  {
	xydata deriv = new xydata();
	
	if ( size() <= 2 ) { return deriv; }
	
	for ( int i=0; i<size(); i++ )
	{ deriv.push_back( get_x(i), dy_lip( get_x(i) ) ); }
	
	return deriv;
  }
  
  public xydata inty()	// return an xydata object which contains the integrated values
  {
	xydata integ = new xydata();
	
	if ( size() <2 ){ integ.push_back( get_x(0), 0.0 ); }
  
	for ( int i=0; i<size(); i++ )
	{ integ.push_back( get_x(i), inty_lip( get_x(i) ) ); }
	
	return integ;
  
  }
  
  public String export_data_as_ASCII()
  {
	String str = new String("x\ty\n");
	for ( int i=0; i<size(); i++ )
	{ str = str + String.valueOf(m_x.elementAt(i)) + "\t" + String.valueOf(m_y.elementAt(i)) + "\n"; }
  
	return str;  
  }
  
  public String export_interpol_data_as_ASCII(int points, double xmin_, double xmax_)
  {
	if ( xmin_ >= xmax_ || points < 2 ){ return new String("bad number of points or bad limits\n"); }
  
	String str = new String("x\ty\n");
	
	double dx = (xmax_-xmin_)/( (double) points );
	
	for ( double x=xmin_; x<=xmax_; x+=dx )
	{ str = str + String.valueOf(x) + "\t" + String.valueOf(y_lip(x))+"\n"; }
	return str;
  }
  
  
  public Vector zeros()	//checks for zero transitions in the data and returns a vector(int) with the indices before the zero
  {
	Vector zerolist = new Vector();
	
	for ( int i=0; i<size()-2; i++ )
	{
		if ( ((Double) m_y.elementAt(i)).equals(0.0) || ((Double) m_y.elementAt(i)) * ((Double) m_y.elementAt(i+1)) < 0.0 )
		{ zerolist.addElement(i); }
	}
  
	return zerolist;
  }
  
  public String zeros_string()	// export the maxima list as a string
  {
	Vector list = zeros();
	int idx = 0;
	String str = new String("i\tx\ty\n");
	
	for ( int i=0; i<list.size(); i++ )
	{
		idx = ((Integer) list.elementAt(i)).intValue();
		str = str + Integer.toString( idx ) + "\t" + String.valueOf(m_x.elementAt(idx)) + "\t" + String.valueOf(m_y.elementAt(idx)) + "\n";
	}
	return str;
  }
  
  
  
  public Vector zeros_interpol()	//return double values of zeros (interpolated data)
  {
	Vector zerolist_idx = zeros();
	Vector zerolist_interpol = null;

	// straight line interpolation: f(x) = m*x+n -> x0 = -n/m
	double n = 0.0;
	double m = 0.0;
	int j = 0;
	
	
	for ( int i=0; i<zerolist_idx.size(); i++ )
	{
		j = ((Integer) zerolist_idx.elementAt(i)).intValue();
		m = ( ((Double) m_y.elementAt(j+1)).doubleValue() - ((Double) m_y.elementAt(j)).doubleValue() ) / 
		        ( ((Double) m_x.elementAt(j+1)).doubleValue() - ((Double) m_x.elementAt(j)).doubleValue() );
		n = ((Double) m_y.elementAt(j)).doubleValue() - m*((Double) m_x.elementAt(j)).doubleValue();
		zerolist_interpol.add( -n/m );
	}

	return zerolist_interpol;	
  }
  
  public Vector maxima()	//checks for maxima in the data and returns a vector(int) with the indices of the maximum
  {
	Vector list = new Vector();
	
	for ( int i=1; i<size()-2; i++ )
	{
		if (  ( ((Double) m_y.elementAt(i)).doubleValue() > ((Double) m_y.elementAt(i-1)).doubleValue() ) &&
		      ( ((Double) m_y.elementAt(i)).doubleValue() > ((Double) m_y.elementAt(i+1)).doubleValue() ) )
		{ list.addElement(i); }
	}
	
	return list;
  }

  public String maxima_string()	// export the maxima list as a string
  {
	Vector list = maxima();
	int idx = 0;
	String str = new String("i\tx\ty\n");
	
	for ( int i=0; i<list.size(); i++ )
	{
		idx = ((Integer) list.elementAt(i)).intValue();
		str = str + Integer.toString( idx ) + "\t" + String.valueOf(m_x.elementAt(idx)) + "\t" + String.valueOf(m_y.elementAt(idx)) + "\n";
	}
	return str;
  }
  
  public Vector strict_maxima(int points, double threshold)	// this is only considered a maximum if it is the maximum in [max-points:max+points]
  {
	Vector allMax = maxima();	// get all maxima
	Vector strictMax = new Vector();
	
	int idx = 0;
	int j = 0;
	Boolean isMax;

	for ( int i=0; i<allMax.size(); i++ )	// check each maximum
	{
		idx = ((Integer) allMax.elementAt(i)).intValue();
		j = idx-points;
		
		isMax = true;
		
		while ( j<=idx+points && j<size() )
		{
			if ( j < 0 ){ j=0; }
			if ( ( (Double) m_y.elementAt(idx) ).doubleValue() < threshold || ( (Double) m_y.elementAt(j) ).doubleValue() > ( (Double) m_y.elementAt(idx) ).doubleValue() )
			{ isMax = false; }
			j++;
		}
		if ( isMax )
		{ strictMax.addElement(idx); }
	}
	return strictMax;	
  }

  public String strict_maxima_string(int points, double threshold)	// export the strict maxima list as a string
  {
	Vector list = strict_maxima(points, threshold);
	int idx = 0;
	String str = new String("i\tx\ty\n");
	
	for ( int i=0; i<list.size(); i++ )
	{
		idx = ((Integer) list.elementAt(i)).intValue();
		str = str + Integer.toString( idx ) + "\t" + String.valueOf(m_x.elementAt(idx)) + "\t" + String.valueOf(m_y.elementAt(idx)) + "\n";
	}
	return str;
  }
  
  public Vector minima()	//checks for minima in the data and returns a vector(int) with the indices of the minimum
  {
	Vector list = new Vector();
	
	for ( int i=1; i<size()-2; i++ )
	{
		if (  ( ((Double) m_y.elementAt(i)).doubleValue() < ((Double) m_y.elementAt(i-1)).doubleValue() ) &&
		      ( ((Double) m_y.elementAt(i)).doubleValue() < ((Double) m_y.elementAt(i+1)).doubleValue() ) )
		{ list.addElement(i); }
	}
	
	return list;
  }

  public String minima_string()	// export the maxima list as a string
  {
	Vector list = minima();
	int idx = 0;
	String str = new String("i\tx\ty\n");
	
	for ( int i=0; i<list.size(); i++ )
	{
		idx = ((Integer) list.elementAt(i)).intValue();
		str = str + Integer.toString( idx ) + "\t" + String.valueOf(m_x.elementAt(idx)) + "\t" + String.valueOf(m_y.elementAt(idx)) + "\n";
	}
	return str;
  }
  
  public Vector strict_minima(int points, double threshold)	// this is only considered a minimum if it is the minimum in [min-points:min+points]
  {
	Vector allMin = minima();	// get all maxima
	Vector strictMin = new Vector();
	
	int idx = 0;
	int j = 0;
	Boolean isMin;

	for ( int i=0; i<allMin.size(); i++ )	// check each maximum
	{
		idx = ((Integer) allMin.elementAt(i)).intValue();
		j = idx-points;
		
		isMin = true;
		
		while ( j<=idx+points && j<size() )
		{
			if ( j < 0 ){ j=0; }
			if ( ( (Double) m_y.elementAt(idx) ).doubleValue() > threshold || ( (Double) m_y.elementAt(j) ).doubleValue() < ( (Double) m_y.elementAt(idx) ).doubleValue() )
			{ isMin = false; }
			j++;
		}
		if ( isMin )
		{ strictMin.addElement(idx); }
	}
	return strictMin;	
  }

  public String strict_minima_string(int points, double threshold)	// export the strict minima list as a string
  {
	Vector list = strict_minima(points, threshold);
	int idx = 0;
	String str = new String("i\tx\ty\n");
	
	for ( int i=0; i<list.size(); i++ )
	{
		idx = ((Integer) list.elementAt(i)).intValue();
		str = str + Integer.toString( idx ) + "\t" + String.valueOf(m_x.elementAt(idx)) + "\t" + String.valueOf(m_y.elementAt(idx)) + "\n";
	}
	return str;
  }
  
  public double peak_area( double xpeak, double maxWidth )	// guess peak position, find maximum in xpeak-maxWidth/2:xpeak+maxWidth/2
                                                          	// calculate area integrating from xpeak-2*xmaxSlopeLeft:xpeak+xmaxSlopeRight
  {
	double maxWidth2 = 0.5*maxWidth;
	double dx = maxWidth/100;
	 
	double y,dy;
	double ymax = y_lip(xpeak-maxWidth2);
	double dymax = dy_lip(xpeak-maxWidth2);
	double dymin = dy_lip(xpeak-maxWidth2);
	double xmax = xpeak-maxWidth2;
	double xmaxd = xpeak-maxWidth2;	// maximum slope
	double xmind = xpeak-maxWidth2;	// minimum slope
	 
	for ( double x=xpeak-maxWidth2; x<=xpeak+maxWidth2; x+=dx )
	{
		y = y_lip(x);
		dy = dy_lip(x);
	 
		if ( y > ymax ){ xmax=x; ymax=y; }
		if ( dy > dymax ){ xmaxd=x; dymax=dy; }
		if ( dy < dymin ){ xmind=x; dymin=dy; } 
	}
  
	//if everything went right the integration must be carried out from xmax-2*(xmax-xmaxd) to xmax+2*(xmind-xmax)
	if ( xmaxd >= xpeak || xmaxd >= xmind || xmind <= xpeak ){ return 0.0; }
	
	double area = integrate( xmax-2*(xmax-xmaxd), xmax+2*(xmind-xmax) );	// integral
	
	// subtract the offset/background under the peak
	
	area -= (y_lip(xmax+2*(xmind-xmax))+y_lip(xmax-2*(xmax-xmaxd)))*(xmaxd-xmind);
	
	return area;
  }
  
  public String peak_areas_string( int points, double threshold, double maxPeakWidth )	// uses strict_maxima to find the peaks and peak_area to calculate the area of the peaks
  {
	Vector maxlist = strict_maxima(points, threshold);
	
	int ipeak = 0;
	double area = 0.0;
	
	String str = new String("xpeak\tpeak-height\tpeak-area\n");
	
	for ( int i=0; i<maxlist.size(); i++ )
	{
		ipeak = (Integer) maxlist.elementAt(i);
		area = peak_area( get_x(ipeak), maxPeakWidth );
		str = str + get_x(ipeak) + "\t" + get_y(ipeak) + "\t" + area + "\n";
	}
	return str;
  }
  
  
  
  
  public double integrate( double xmin_, double xmax_ )
  {
	if ( size()==0 ){ return 0.0; }
	if ( size()==1 ){ return 0.0; }
	if ( !is_sorted() ){ sort(); }

	// TODO checks
	if ( xmin_>= xmax_ ){ return 0.0; }
	
	// find index i for which x(i)<x<x(i+1)
	int istart=find_closest_data_point( xmin_ );
	int iend=find_closest_data_point( xmax_ );

	double sum = 0.0;
	double dx = 0.0;
	
	// area between data points
	for (int i=istart; i<iend; i++ )
	{
		dx = get_x(i+1)-get_x(i);
		sum += (get_y(i)+get_y(i+1))*dx;
	}
	// area before first data point
	sum += (get_y(istart)+y_lip(xmin_))*(get_x(istart)-xmin_);
	// area after last data point
	sum += (get_y(iend)+y_lip(xmax_))*(xmax_-get_x(iend));
	sum *= 0.5;
	
	m_imin = istart;
	m_imax = iend;
	
	return sum;
  
  }
  
  // linear regression (fitting)
  public Boolean linreg( int imin, int imax )	// imin and imax are the boundaries for the fit
  {
	if ( !is_sorted()){sort();}
	if (  imin <0 || imin >=imax || imax>size()-1  ) { return false; }
  
	double xmean=0;
	double ymean=0;
	
	double nom=0; double denom=0;
	
	//calculate xmean, ymean
	
	for ( int i=imin; i<=imax; i++ )
	{xmean+=(Double) m_x.elementAt(i); ymean+=(Double) m_y.elementAt(i);}
	xmean/=( (double) (imax-imin+1) ); 
	ymean/=( (double) (imax-imin+1) );	

	//calculate vectors xi-xmean
	double xixmean, yiymean;

	for ( int i=imin; i<=imax; i++ )
	{
		xixmean = (Double) m_x.elementAt(i)-xmean;
		yiymean = (Double) m_y.elementAt(i)-ymean;		
		nom += xixmean*yiymean;
		denom += xixmean*xixmean;
	}
	
	m_linreg_slope = nom/denom;
	m_linreg_offset = ymean - xmean*m_linreg_slope;
	
	m_imin = imin;
	m_imax = imax;

	return true;
  
  
  }
  
  public Boolean linreg( double xmin, double xmax )	// uses linreg( imin, imax ) and find_closest_data_point( i )
  {
	if ( !is_sorted()){sort();}
	if ( xmin < (Double) m_x.elementAt(0) || xmin >= xmax || xmax > (Double) m_x.elementAt(size()-1) )
	{ return false; }
  
	m_imin = find_closest_data_point( xmin );
	m_imax = find_closest_data_point( xmax )+1;
  
	return linreg( m_imin, m_imax );
  }
  
  public double linreg_slope(){ return m_linreg_slope; }
  public double linreg_offset(){ return m_linreg_offset; }
  
  
  
  // smooth data
  
  public xydata smooth(int linreg_points)
  {
	xydata retval = new xydata();

	// checks
	if ( !is_sorted() ){ sort(); }
	if( size() < linreg_points || linreg_points<3 ){ return retval; }
	// variables to calculate the smoothed data points
	double y;	// value
	int imin, imax;
	
	for ( int i=0; i<size(); i++ )	//run through the data points
	{

		imin = i-linreg_points/2;
		imax = i+linreg_points/2;
		if ( imin<0 ){imin = 0; imax = linreg_points-1;}
		if ( imax>=size() ){imin = size()-linreg_points; imax = size()-1;}
		linreg( imin, imax);
		y = linreg_slope()*( (Double) m_x.elementAt(i) )+linreg_offset();
		retval.push_back( (Double) m_x.elementAt(i),y);
	}

	return retval;
  }
  
  static public String classID(){ return "xydata"; }
  static public String author(){ return "Matthias Schmidt"; }
  static public String version(){ return "September 4 2015"; }
  
  
  
  // member variables

  protected Vector m_x;	// x values (double)
  protected Vector m_y;	// y values (double)
  
  
  protected Boolean m_is_sorted;	// is the data sorted in ascending x order
  
  protected double m_linreg_slope;
  protected double m_linreg_offset;
  
  // ranges written by integrate, split etc...
  public int min_idx(){ return m_imin; }
  public int max_idx(){ return m_imax; }
  protected int m_imin;
  protected int m_imax;
  

}



