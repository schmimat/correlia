package de.ufz.correlia;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class imageElementPanel extends listElementPanel {
	final ImageIcon icoHide = new ImageIcon(this.getClass().getResource("/icons/eye_hide.png"));
	final ImageIcon icoShow = new ImageIcon(this.getClass().getResource("/icons/eye_show.png"));
	final ImageIcon icoExclusive = new ImageIcon(this.getClass().getResource("/icons/eye_exclusive.png"));
	final ImageIcon icoEnhanced = new ImageIcon(this.getClass().getResource("/icons/eye_enhanced.png"));
	protected static final Dimension COLOR_DIM = new Dimension(20, 20);

	private imageColour ic;
	private Color color;
	private JButton btnColor;
	private JLabel lbSlice = new JLabel();

	public imageElementPanel(correlia_ui cUI, int imgNo) {
		super(cUI,
				imgNo,
				(imgNo == cUI.get_selected_image()),
				new String[] {imageColour.HIDE, imageColour.SHOW, imageColour.EXCLUSIVE, imageColour.ENHANCED}
		);

		ic = ui.get_prj().get_imageColour(imgNo);
		this.color = ic.get_color();
		this.state.set(ic.get_visiblity());

		if (number == 0) {
			upperPan.remove(spMove);
			upperPan.add(Box.createRigidArea(MOVE_DIM), 1);
		}
//		} else if (number == 1) {
//			spMove.setModel(new SpinnerNumberModel(0, -1, 0, 1));
//		} else if (number == cUI.get_prj().size() - 1) {
//			spMove.setModel(new SpinnerNumberModel(0, 0, 1, 1));
//		}

		spMove.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				if (((SpinnerNumberModel) spMove.getModel()).getNumber().intValue() > 0) {
					if (number > 1) {
						ui.get_prj().swapImages(number, number - 1);
						if (selected) {
							ui.set_selected_image(number - 1);
						} else if (ui.get_selected_image() == number - 1) {
							ui.set_selected_image(number);
						}
						ui.update_gui();
					}
				} else {
					if (number < ui.get_prj().size() - 1) {
						ui.get_prj().swapImages(number, number + 1);
						if (selected) {
							ui.set_selected_image(number + 1);
						} else if (ui.get_selected_image() == number + 1) {
							ui.set_selected_image(number);
						}
						ui.update_gui();
					}
				}
			}
		});

		btnState.setIcon(get_state_icon());

		lbTitle.setText(ui.get_prj().get_imageNames()[number]);
		lbTitle.setForeground(state.is(imageColour.HIDE)? COLOR_INACTIVE : COLOR_ACTIVE);


		btnColor = new JButton();
		btnColor.addActionListener(
				e -> {
					Color newColor = JColorChooser.showDialog(this,"Choose color", color);
					if (newColor != null) {
						color = newColor;
						btnColor.setBackground(color);
						ic.set_color(color);
						ui.update_output();
					}
				}
		);
		btnColor.setBackground(color);
		btnColor.setMinimumSize(COLOR_DIM);
		btnColor.setMaximumSize(COLOR_DIM);

		upperPan.add(btnColor);
		upperPan.add(Box.createRigidArea(new Dimension(DEFAULT_MARGIN, 0)));

		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		if (ui.get_prj().get_image(imgNo).getNDimensions() > 2) {
			microscopyImage mi = ui.get_prj().get_image(imgNo);

			lbSlice = new JLabel(mi.getSliceLabel());
			lbSlice.setForeground(state.is(imageColour.HIDE)? COLOR_INACTIVE : COLOR_ACTIVE);

			JSpinner spSlice = new JSpinner();
			spSlice.setModel(new SpinnerNumberModel(mi.getSlice(), 1, mi.getNSlices(), 1));
			JLabel spacer = new JLabel("");
			spacer.setPreferredSize(new Dimension(0, PANEL_HEIGHT));
			spSlice.setMaximumSize(new Dimension(0, PANEL_HEIGHT));
			spSlice.setEditor(spacer);
			spSlice.setBorder(null);
			spSlice.addChangeListener(new ChangeListener() {
				@Override
				public void stateChanged(ChangeEvent e) {
					ui.get_prj().get_image(number).setSlice(((SpinnerNumberModel) spSlice.getModel()).getNumber().intValue());
					lbSlice.setText(ui.get_prj().get_image(number).getSliceLabel());
					lbSlice.setForeground(state.is(imageColour.HIDE)? COLOR_INACTIVE : COLOR_ACTIVE);
					ui.update_output();
					if (selected) {
						ui.update_image_props_pane();
					}
				}
			});
			JPanel lowerPan = new JPanel();
			lowerPan.setLayout(new BoxLayout(lowerPan, BoxLayout.X_AXIS));
			lowerPan.add(Box.createRigidArea(MOVE_DIM));
			lowerPan.add(Box.createRigidArea(new Dimension(DEFAULT_MARGIN, 0)));
			lowerPan.add(Box.createRigidArea(STATE_DIM));
			lowerPan.add(Box.createRigidArea(new Dimension(DEFAULT_MARGIN, 0)));
			lowerPan.add(lbSlice);
			lowerPan.add(Box.createRigidArea(new Dimension(DEFAULT_MARGIN, 0)));
			lowerPan.add(Box.createHorizontalGlue());
			lowerPan.add(spSlice);
			lowerPan.add(Box.createRigidArea(new Dimension(DEFAULT_MARGIN, 0)));
			this.add(lowerPan);
		}
	}

	public void select() {
		super.select();
		ui.get_imageListElement(ui.get_selected_image()).deselect();
		ui.set_selected_image(number);
		ui.update_image_props_pane();
		ui.update_overlay();
	}

	protected ImageIcon get_state_icon() {
		ImageIcon ico;
		switch (state.get()) {
			default:
			case imageColour.HIDE:
				ico = icoHide;
				break;
			case imageColour.SHOW:
				ico = icoShow;
				break;
			case imageColour.EXCLUSIVE:
				ico = icoExclusive;
				break;
			case imageColour.ENHANCED:
				ico = icoEnhanced;
				break;
		}
		return ico;
//		return icoImage.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH)
	}

	protected void state_changed() {
		ic.set_visibility(state.get());
		if (state.get().equals(imageColour.HIDE)) {
			lbTitle.setForeground(COLOR_INACTIVE);
			lbSlice.setForeground(COLOR_INACTIVE);
		} else {
			lbTitle.setForeground(COLOR_ACTIVE);
			lbSlice.setForeground(COLOR_ACTIVE);
			if (ui.get_prj().get_image(number) instanceof microscopyImageWarped) {
				((microscopyImageWarped)ui.get_prj().get_image(number)).update();
				ui.update_image_props_pane();
			}
		}
		ui.update_output();
	}
}