/*
 * Extension of the microscopyImage class that enabled nonlinear registration
 * by modifying the orignal image data in a transformation cascade
 * Author: Florens Rohde
 *
 * Part of:
 * Correlia plugin for ImageJ and FIJI
 */
package de.ufz.correlia;

import ij.IJ;
import ij.ImageListener;
import ij.ImagePlus;
import ij.ImageStack;
import ij.io.SaveDialog;
import org.w3c.dom.Element;

import javax.swing.*;
import java.awt.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;

public class microscopyImageWarped extends microscopyImage implements ImageListener {

	/** ImageStack containing the original image data */
	private ImageStack rawStack;
	/** contains the feature points in the original coordinate system */
	private ArrayList<Point.Double> rawFeaturePoints;
	/** contains the transformation instances of the cascade */
	private ArrayList<transformation> transformations;
	private int selectedTransformation = -1;
	private boolean changedSinceLastUpdate = true;

	/**
	 * Construct from XML element
	 * @param xml xmlHandler instance
	 * @param root xml root element
	 * @param prjPath Path of the correlia project
	 * @param prj correlia instance
	 */
	microscopyImageWarped(xmlHandler xml, Element root, String prjPath, correlia prj) {
		super(xml, root, prjPath);
		rawStack = this.getStack().duplicate();
		rawFeaturePoints = new ArrayList<>();
		rawFeaturePoints.addAll(this.get_feature_points());
		ImagePlus.addImageListener(this);

		transformations = new ArrayList<>();
		loadTransformations(xml, root, prj);
	}

	/**
	 * Construct from ImagePlus
	 * @param imp ImagePlus instance
	 */
	microscopyImageWarped(ImagePlus imp) {
		super(imp);
		rawStack = this.getStack().duplicate();
		rawFeaturePoints = new ArrayList<>();
		transformations = new ArrayList<>();
		ImagePlus.addImageListener(this);
	}

	/**
	 * Construct from microscopyImage
	 * @param mi microscopyImage instance
	 */
	microscopyImageWarped(microscopyImage mi) {
		super(mi);
		rawStack = this.getStack().duplicate();
		rawFeaturePoints = MiscHelper.copyFeatures(mi.get_feature_points());
		transformations = new ArrayList<>();
		ImagePlus.addImageListener(this);
	}

	/**
	 * Copy constructor
	 * @param miw microscopyImageWarped instance
	 */
	microscopyImageWarped(microscopyImageWarped miw) {
		super(miw);
		rawStack = miw.get_rawStack().duplicate();
		rawFeaturePoints = MiscHelper.copyFeatures(miw.get_feature_points());
		transformations = new ArrayList<>();
		transformation prevT = null;
		for (transformation t : miw.getTransformations()) {
			transformation tCopy = null;
			switch (t.getName()) {
				case transformationTileMatching.NAME:
					tCopy = new transformationTileMatching((transformationTileMatching)t);
					break;
				case transformationGlobalMI.NAME:
					tCopy = new transformationGlobalMI((transformationGlobalMI)t);
					break;
				case transformationLandmarks.NAME:
				case "Manual landmarks":
					tCopy = new transformationLandmarks((transformationLandmarks)t);
					break;
				case transformationBUnwarpJ.NAME:
				case "bUnwarpJ-Registration":
					tCopy = new transformationBUnwarpJ((transformationBUnwarpJ)t);
					break;
				default:
					debug.put("Unknown transformation: " + t.getName());
					continue;
			}
			tCopy.previousTransformation = prevT;
			prevT = tCopy;
			transformations.add(tCopy);
		}
		ImagePlus.addImageListener(this);
	}

	/**
	 * Load transformations from XML element
	 * @param xml xmlHandler instance
	 * @param root xml root element
	 * @param prj correlia project
	 */
	public void loadTransformations(xmlHandler xml, Element root, correlia prj) {
		int pos = 0;
		Element e;
		transformation prevT = null;
		do {
			e = xml.getElementByAttribute(root, "transformation", "pos", pos);
			if (e == null) break;
			transformation t = null;
			switch (e.getAttribute("type")) {
				case transformationTileMatching.NAME:
					t = new transformationTileMatching(xml, e, prj, prevT, this);
					break;
				case transformationGlobalMI.NAME:
					t = new transformationGlobalMI(xml, e, prj, prevT, this);
					break;
				case transformationLandmarks.NAME:
				case "Manual landmarks":
					t = new transformationLandmarks(xml, e, prj, prevT, this);
					break;
				case transformationBUnwarpJ.NAME:
				case "bUnwarpJ-Registration":
					t = new transformationBUnwarpJ(xml, e, prj, prevT, this);
					break;
			}
			addTransformation(t);
			if (t.active()) prevT = t;
			pos++;
		} while (pos > 0);
		updatePreviousTransformations();
	}

	// Update transformed image when opened
	public void imageOpened(ImagePlus imp) {
		debug.put(this.getTitle() + " image opened");
		if (imp == this) {
			update();
		}
	}
	public void imageUpdated(ImagePlus imp) {
//		debug.put(this.getTitle() + " image updated");
//		update();
	}
	public void imageClosed(ImagePlus imp) {
//		debug.put(this.getTitle() + " image closed");
	}

	@Override
	public Element buildXML(xmlHandler xml, String path) {
		ArrayList<Point.Double> feature_points_tmp = new ArrayList<>(feature_points);
		feature_points = new ArrayList<>(rawFeaturePoints);

		Element root = super.buildXML(xml, path);

		xml.addAttribute(root, "type", "warped");
		for (int t = 0; t < transformationsSize(); t++) {
			xml.addElement(root, xml.addAttribute(getTransformation(t).buildXML(xml), "pos", t));
		}

		feature_points = new ArrayList<>(feature_points_tmp);
		return root;
	}

	/**
	 * Create xml element for recipe exporting
	 * @param xml xmlHandler instance
	 * @return xml element
	 */
	public Element buildRecipe(xmlHandler xml) {
		Element root = xml.createElement("transformationRecipe");
		for (int t = 0; t < transformationsSize(); t++) {
			xml.addElement(root, xml.addAttribute(getTransformation(t).buildXML(xml, true), "pos", t));
		}
		return root;
	}

	/**
	 * Create transformations from xml element containing the recipe
	 * @param xml xmlHandler instance
	 * @param root xml root element
	 * @param prj correlia project
	 * @return success
	 */
	public boolean loadRecipe(xmlHandler xml, Element root, correlia prj) {
		if (transformationsSize() > 0) {
			int ans = JOptionPane.showConfirmDialog(null,
					"Do you want to replace the existing transformations?","", JOptionPane.YES_NO_CANCEL_OPTION);
			if (ans == JOptionPane.YES_OPTION) {
				transformations.clear();
				selectedTransformation = -1;
			} else if (ans == JOptionPane.CANCEL_OPTION) {
				return true;
			}
		}

		int pos = 0;
		Element e;
		transformation prevT = null;
		for (int i = transformationsSize() - 1; i >= 0; i--) {
			if (getTransformation(i).active()) {
				prevT = getTransformation(i);
			}
		}

		do {
			e = xml.getElementByAttribute(root, "transformation", "pos", pos);
			if (e == null) break;
			transformation t = null;
			switch (e.getAttribute("type")) {
				case transformationTileMatching.NAME:
					t = new transformationTileMatching(xml, e, prj, prevT, this);
					break;
				case transformationLandmarks.NAME:
					t = new transformationLandmarks(xml, e, prj, prevT, this);
					break;
				case transformationBUnwarpJ.NAME:
					t = new transformationBUnwarpJ(xml, e, prj, prevT, this);
					break;
				case transformationGlobalMI.NAME:
					t = new transformationGlobalMI(xml, e, prj, prevT, this);
					break;
			}
			if (t == null) return false;
			addTransformation(t);
			if (t.active()) prevT = t;
			pos++;
		} while (pos > 0);
		return true;
	}

	@Override
	public void save_image(String path) {
		path = SaveDialog.setExtension(path, FILE_EXTENSION_IMAGE);
		set_filePath(Paths.get(path).getFileName().toString());
		ImagePlus saveImp = this.createImagePlus();
		saveImp.setStack(this.get_rawStack());
		IJ.save(saveImp, path);
	}

	/**
	 * Convert coordinates from the transformed image to the raw image
	 * @param p transformed coordinates
	 * @return raw coordinates
	 */
	public Point.Double convertToRawCoords(Point.Double p) {
		Point.Double pPix = len2pix(p);
		int tag = lastActiveTransformation();
		while (tag >= 0) {
			transformation curT = getTransformation(tag);
			pPix = curT.coordTransformationT2O(pPix);
			tag = transformation.getTag(curT.dxyReference);
		}
		return pix2len(pPix);
	}

	@Override
	public void add_feature_point(Point.Double p) {
		super.add_feature_point(p);
		rawFeaturePoints.add(convertToRawCoords(p));
	}

	@Override
	public void set_feature_point(int pntIdx, double x, double y) {
		super.set_feature_point(pntIdx, x, y);
		try {
			rawFeaturePoints.set(pntIdx, convertToRawCoords(new Point.Double(x, y)));
		} catch (IndexOutOfBoundsException e) {
			debug.put(" index "+pntIdx+" out of bounds");
		}
	}

	@Override
	public void remove_feature_point(int pntIdx) {
		super.remove_feature_point(pntIdx);
		try {
			rawFeaturePoints.remove(pntIdx);
		} catch (IndexOutOfBoundsException e) {debug.put(" index "+pntIdx+" out of bounds");}
	}

	@Override
	public void clear_feature_points() {
		super.clear_feature_points();
		rawFeaturePoints.clear();
	}

	public void update() {
		debug.put(" entered (" + this.getTitle() + ")");
		boolean update = false;
		updatePreviousTransformations();

		if (changedSinceLastUpdate) {
			update = true;
		} else {
			for (transformation t : transformations) {
				if (t.changed()) {
					update = true;
					break;
				}
			}
		}
		if (update) {
			debug.put(" updating Imagestack");
			transformation curT;
			int lastT = -1;
			for (int t = 0; t < transformationsSize(); t++) {
				curT = transformations.get(t);
				if (!curT.active()) {
					continue;
				}
				lastT = t;
				if (!curT.update(false)) {
					return;
				}
			}
			if (lastT >= 0) {
				microscopyImage result = transformations.get(lastT).getResult();
				if (result != null) {
					this.setStack(result.getStack());
					this.set_feature_points(result.get_feature_points());
				} else {
					return;
				}
			} else {
				this.setStack(get_rawStack().duplicate());
				this.set_feature_points(MiscHelper.copyFeatures(this.get_rawFeaturePoints()));
			}
			changedSinceLastUpdate = false;
		}
	}

	/**
	 * Get transformed coordinates from raw coordinates
	 * @param fp raw coordinates
	 * @return transformed coordinates
	 */
	public Point.Double getTransformedCoords(Point.Double fp) {
//		Point.Double fpT = new Point.Double(fp.getX(), fp.getY());
		Point.Double fpT = len2pix(fp);
		transformation curT;
		for (int t = 0; t < transformationsSize(); t++) {
			curT = transformations.get(t);
			if (!curT.active()) {
				continue;
			}
			fpT = curT.coordTransformationO2T(fpT);
		}
		return pix2len(fpT);
	}

	/**
	 * Get index of the last transformation in the cascade that is activated
	 * @return transformation number
	 */
	public int lastActiveTransformation() {
		int lastT = -1;
		for (int t = 0; t < transformationsSize(); t++) {
			if (getTransformation(t).active()) {
				lastT = t;
			}
		}
		return lastT;
	}

	/**
	 * Append a transformation to the cascade
	 * @param t transformation
	 */
	public void addTransformation(transformation t) {
		transformations.add(t);
		selectedTransformation = transformations.size() - 1;
		changedSinceLastUpdate = true;
	}

	/**
	 * Remove a transformation from the cascade
	 * @param i transformation number
	 * @return success
	 */
	public boolean removeTransformation(int i) {
		if (transformationOutOfRange(i)) {
			return false;
		}
		transformations.remove(i);
		selectedTransformation = Math.min(selectedTransformation, transformationsSize() - 1);
		changedSinceLastUpdate = true;
		return true;
	}

	/**
	 * Get transformation by number
	 * @param i transformation number
	 * @return transformation
	 */
	public transformation getTransformation(int i) {
		if (transformationOutOfRange(i)) {
			return null;
		}
		return transformations.get(i);
	}

	public int getTransformationNr(transformation t) {
		return transformations.indexOf(t);
	}

	public transformation getSelectedTransformation() {
		return getTransformation(selectedTransformation);
	}

	public int getSelectedTransformationNr() {
		return selectedTransformation;
	}

	public boolean setSelectedTransformation(int i) {
		if (transformationOutOfRange(i)) {
			return false;
		}
		selectedTransformation = i;
		return true;
	}

	private boolean transformationOutOfRange(int i) {
		return i < 0 || i >= transformationsSize();
	}

	public ArrayList<transformation> getTransformations() {
		return transformations;
	}

	public int transformationsSize() {
		return transformations.size();
	}

	/**
	 * Swap two transformations in stack
	 * @param pos1 transformation position 1
	 * @param pos2 transformation position 2
	 * @return false, if one of the transformations does not exist
	 */
	public boolean swapTransformations(int pos1, int pos2) {
		if (pos1 < 0 || pos2 < 0 || pos1 >= transformationsSize() || pos2 >= transformationsSize()) {
			return false;
		}
		Collections.swap(transformations, pos1, pos2);
		return true;
	}

	public void activateTransformation(int i) {
		if (transformationOutOfRange(i)) {
			return;
		}
		getTransformation(i).activate();
	}

	public void deactivateTransformation(int i) {
		if (transformationOutOfRange(i)) {
			return;
		}
		getTransformation(i).deactivate();
		changedSinceLastUpdate = true;
	}

	/**
	 * Go through the cascade and update the previous transformation pointers
	 * by skipping deactivated transformations
	 */
	private void updatePreviousTransformations() {
		transformation prevT = null;
		for (int t = 0; t < transformationsSize(); t++) {
			if (getTransformation(t).active()) {
				transformations.get(t).setPreviousTransformation(prevT);
				prevT = transformations.get(t);
			}
		}
	}

	public ImageStack get_rawStack() {
		return rawStack;
	}
	public void set_rawStack(ImageStack imgStack) {
		rawStack = imgStack;
		if (transformationsSize() > 0) {
			getTransformation(0).dxyReference.setStack(rawStack);
		}
	}

	public ArrayList<Point.Double> get_rawFeaturePoints() {
		return rawFeaturePoints;
	}
}
