/*
 * Correlia project containing informations about images, alignments...
 * Author: Matthias Schmidt, Florens Rohde
 *
 * Part of:
 * Correlia plugin for ImageJ and FIJI
 */
package de.ufz.correlia;

import ij.*;
import ij.io.SaveDialog;
import ij.plugin.filter.GaussianBlur;
import ij.process.*;
import ij.process.ImageProcessor;
import ij.gui.NewImage;
import org.w3c.dom.Element;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Collections;
import java.util.ArrayList;
import java.io.File;

import java.util.concurrent.Semaphore;
import java.util.Random;

import java.awt.geom.*;


public class correlia {
	static final int IMAGEMATH_ADD      = 1;
	static final int IMAGEMATH_MULTIPLY = 2;
	static final int IMAGEMATH_SUBTRACT = 3;
	static final int IMAGEMATH_DIVIDE   = 4;

	static final String PROJECT_FILE_EXTENSION = ".correlia";
	static final double DEFAULT_IMAGECOLOUR_RED = 0.6;
	static final double DEFAULT_IMAGECOLOUR_GREEN = 0.6;
	static final double DEFAULT_IMAGECOLOUR_BLUE = 0.6;
	static final int HTML_IMAGE_SIZE = 600;
	static final String HTML_IMAGE_FORMAT = "png";

	static final int IMAGENAME_MAX_DISPLAY_LENGTH = 25;  // Limit the length of imagenames in the ui selection box

	private Instant start;

	/**
	 * Parse project properties from XML element
	 */
	public correlia(xmlHandler xml, Element root) {	  //
		images = new ArrayList<>();
		imageColourChannels = new ArrayList<>();
		imageAlignments = new ArrayList<>();

		projectPath = (new File(xml.getSavepath())).getParent() + File.separator;
		projectTitle = xml.getTextByElementName(root, "projectTitle");
		additionalInfo = xml.getTextByElementName(root, "additionalInfo");

		int pos = 0;
		Element mi;
		do {
			mi = xml.getElementByAttribute(root, "microscopyImage", "pos", pos);
			Element ic = xml.getElementByAttribute(root, "imageColour", "pos", pos);
			if (mi == null || ic == null) {
				break;
			}
			if (pos == 0) {
				baseImage = new microscopyImage(xml, mi, projectPath);
				baseImage_colour = new imageColour(xml, ic);
			} else {
				Element ia = xml.getElementByAttribute(root, "imageAlignment", "pos", pos);
				if (ia == null) {
					break;
				}
				imageAlignment iaNew = new imageAlignment(xml, ia);
				if (!ia.getAttribute("duplicateOf").isEmpty()) {
					int dupPos = get_imagePositionByID(ia.getAttribute("duplicateOf"));
					if (dupPos >= 0 && dupPos < size()) iaNew = this.get_imageAlignment(dupPos);
				}
				if (!mi.getAttribute("type").isEmpty() & mi.getAttribute("type").equals("warped")) {
					importImage(new microscopyImageWarped(xml, mi, projectPath, this), new imageColour(xml, ic), iaNew);
					if (!get_imageColour(pos).get_visiblity().equals(imageColour.HIDE)) {
						((microscopyImageWarped)get_image(pos)).update();
					}
				} else {
					importImage(new microscopyImage(xml, mi, projectPath), new imageColour(xml, ic), iaNew);
				}
			}
			pos++;
		} while (pos > 0);
	}

	/**
	 * Create a new correlia project using a base image
	 * @param imp Base ImagePlus image
	 */
	public correlia(ImagePlus imp) {
		projectTitle = "new correlia project";
		baseImage = new microscopyImage(imp);
		baseImage_colour = new imageColour(DEFAULT_IMAGECOLOUR_RED, DEFAULT_IMAGECOLOUR_GREEN, DEFAULT_IMAGECOLOUR_BLUE);
		images = new ArrayList<>();
		imageAlignments = new ArrayList<>();
		imageColourChannels = new ArrayList<>();
	}

	/**
	 * Create a new correlia project using a microscopyImage as base image
	 * @param mi Base microscopyImage image
	 */
	public correlia(microscopyImage mi) {
		projectTitle = "new correlia project";
		baseImage = mi;
		baseImage_colour = new imageColour(DEFAULT_IMAGECOLOUR_RED, DEFAULT_IMAGECOLOUR_GREEN, DEFAULT_IMAGECOLOUR_BLUE);
		images = new ArrayList<>();
		imageAlignments = new ArrayList<>();
		imageColourChannels = new ArrayList<>();
	}

	/**
	 * Check project and display the problem, if something is wrong
	 * @return false, if an error occurs
	 */
	public Boolean check() {
		Boolean cf = true;
	
		if (baseImage == null) {
			IJ.log("ERROR: correlia.check: no baseImage");
			cf = false;
		}
		
		if (imageColourChannels.size() != images.size()) {
			IJ.log("ERROR: correlia.check: number of imageColourChannels in project ("+imageColourChannels.size()+") is not equal to number of images ("+images.size()+").");
			cf = false;
		}
	
		if (imageAlignments.size() != images.size()) {
			IJ.log("ERROR: correlia.check: number of imageAlignments in project ("+ imageAlignments.size()+") is not equal to number of overlaid images ("+images.size()+"). ");
			cf = false;
		}
		
		// check base
		if (!baseImage.check()) {
			IJ.log("ERROR: correlia.check: base image check failed due to previous errors.");
			cf = false;
		}
		
		// check all images
		for (int i=0; i<images.size(); i++)	{
			if (!(images.get(i)).check()) {
				IJ.log("ERROR: correlia.check: image "+i+" check failed due to previous errors.");
				cf = false;
			}
		}

		return cf;
	}
	
	// add and remove images

	public void importImage(microscopyImage mi, imageColour icol, imageAlignment icoord) {
		if (icoord == null) {
			icoord = new imageAlignment(0, 0, 0, mi.imgWidth() / 2, mi.imgHeight() / 2);
		}
		images.add(mi);
		imageColourChannels.add(icol);
		imageAlignments.add(icoord);
	}

	/**
	 * Remove an image with its colour and coordinate data
	 * @param i image number
	 * @return false, if its the base image or not existing
	 */
	public Boolean removeImage(int i) {
		if (i<=0 || i>images.size()) {return false;}
		
		images.remove(i-1);
		imageColourChannels.remove(i-1);
		imageAlignments.remove(i-1);
		return true;
	}

	/**
	 * Swap two images in images stack
	 * @param pos1 image position 1
	 * @param pos2 image position 2
	 * @return false, if one the images is the base image or does not exist
	 */
	public Boolean swapImages(int pos1, int pos2) {
		if (pos1 <= 0 || pos2 <= 0 || pos1 >= size() || pos2 >= size())	{return false;}
	
		Collections.swap(images, pos1-1, pos2-1);
		Collections.swap(imageAlignments, pos1-1, pos2-1);
		Collections.swap(imageColourChannels, pos1-1, pos2-1);
		
		return true;
	}

	/**
	 * Move an image in images stack
	 * @param newPos desired position
	 * @param oldPos current position
	 * @return false, if one the images is the base image or does not exist
	 */
	public boolean moveImage(int newPos, int oldPos) {
		if (newPos <= 0 || oldPos <= 0 || newPos >= size() || oldPos >= size())	{return false;}
		if (newPos == oldPos) return true;

		boolean up = (newPos < oldPos);
		int diff = Math.abs(newPos - oldPos);
		while (diff != 0) {
			int swapPos = up?oldPos-1:oldPos+1;
			swapImages(oldPos, swapPos);
			oldPos = swapPos;
			diff--;
		}
		return true;
	}

	/**
	 * Replace an image in images stack
	 * if an argument is null, than the old data will be used
	 * @param pos position
	 * @param mi new microscopyImage
	 * @param ic new imageColour
	 * @param ia new imageAlignment
	 * @return false, if the image is the base image or does not exist
	 */
	public boolean replace(int pos, microscopyImage mi, imageColour ic, imageAlignment ia) {
		if (pos <= 0 || pos >= size())	{return false;}

		importImage(mi==null?get_image(pos):mi, ic==null?get_imageColour(pos):ic, ia==null?get_imageAlignment(pos):ia);
		removeImage(pos);
		moveImage(pos, size() - 1);
		return true;
	}

	/**
	 * Set project title
	 * @param s title
	 */
	public void set_projectTitle(String s) {
		projectTitle = s;
	}

	/**
	 * Get project title
	 * @return title
	 */
	public String get_projectTitle() {
		return projectTitle;
	}

	/**
	 * Set project path
	 * @param s path
	 */
	public void set_projectPath(String s) {
		projectPath = s;
	}

	/**
	 * Get project path
	 * @return path
	 */
	public String get_projectPath() {
		return projectPath;
	}

	/**
	 * Set additional Info
	 * @param ai additional Info
	 */
	public void set_additionalInfo(String ai) {
		additionalInfo = ai;
	}

	/**
	 * Get additional Info
	 * @return additiona info
	 */
	public String get_additionalInfo() {
		return additionalInfo;
	}


	/**
	 * Get number of images (including the base)
	 * @return image number
	 */
	public int size() {
		return images.size() + 1;
	}

	/**
	 * Get an image from the project
	 * @param i image number (0: base, >0: overlay)
	 * @return null, if the image does not exist
	 */
	public microscopyImage get_image(int i) {
		if (i<0 || i>images.size()) {return null;}
		if (i==0) {
			return baseImage;
		} else {
			return images.get(i-1);
		}
	}

	/**
	 * Get an image from the project
	 * @param ID identification string
	 * @return null, if the ID does not exist
	 */
	public microscopyImage get_imageByID(String ID) {
		for (int i = 0; i < size(); i++) {
			microscopyImage mi = get_image(i);
			if (mi.get_ID().equals(ID)) {
				return mi;
			}
		}
		return null;
	}

	/**
	 * Get the position of an image in the project by its ID
	 * @param ID identification string
	 * @return -1, if the ID does not exist
	 */
	public int get_imagePositionByID(String ID) {
		for (int i = 0; i < size(); i++) {
			microscopyImage mi = get_image(i);
			if (mi.get_ID().equals(ID)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Get the colour of an image from the project
	 * @param i image number (0: base, >0: overlay)
	 * @return null, if the image does not exist
	 */
	public imageColour get_imageColour(int i) {
		if (i<0 || i>images.size()) {return null;}
		if (i==0) {
			return baseImage_colour;
		} else {
			return imageColourChannels.get(i-1);
		}
	}

	/**
	 * Get image colours as space separated string of RGB factors (0 ... x)
	 * @param i image number (0: base, >0: overlay)
	 * @return space separated string
	 */
	public String get_colourString(int i) {
		return get_imageColour(i).get_colourString();
	}

	/**
	 * Get the coordinates of an image from the project
	 * @param i image number (0: base, >0: overlay)
	 * @return null, if the image does not exist
	 */
	public imageAlignment get_imageAlignment(int i)	{
		if (i<0 || i>images.size()) {return null;}
		if (i==0) {  // base image
			return new imageAlignment();
		} else {
			return imageAlignments.get(i-1);
		}
	}

	/**
	 *
	 * @return string of image names
	 */
	public String[] get_imageNames() {
		String[] s = new String[images.size()+1];
		
		s[0] = get_image(0).getTitle() + " (base)";
		
		if (s[0].length() > IMAGENAME_MAX_DISPLAY_LENGTH) {
			s[0] = s[0].substring(0, IMAGENAME_MAX_DISPLAY_LENGTH - 4) + "...";
		}

		for (int i=1; i<=images.size(); i++) {
			s[i] = get_image(i).getTitle();
			if (s[i].length() > IMAGENAME_MAX_DISPLAY_LENGTH) {
				s[i] = s[i].substring(0,IMAGENAME_MAX_DISPLAY_LENGTH - 4) + "...";
			}
		}
		return s;
	}

	/**
	 * Get width of the base image in physical units
	 * @return width
	 */
	public double width() {
		return baseImage.imgWidth();
	}

	/**
	 * Get height of the base image in physical units
	 * @return height
	 */
	public double height() {
		return baseImage.imgHeight();
	}

	/**
	 * Get width-height-ratio of the base image
	 * @return ratio
	 */
	public double aspectRatio()	{
		return width() / height();
	}

	/**
	 * Set coordinates for an image
	 * @param i image number
	 * @param coord coordinates
	 * @return false, if it is the base image or does not exist
	 */
	public boolean set_imageAlignment(int i, imageAlignment coord, boolean replace) {
		if (i<1 || i>images.size()) {return false;}
		if (replace) {
			imageAlignments.set(i - 1, coord);
		} else {
			imageAlignment ia = get_imageAlignment(i);
			ia.set_center(coord.xcenter(), coord.ycenter());
			ia.set_x0y0(coord.x0(), coord.y0());
			ia.set_rotAngle(coord.rotAngle());
			ia.set_scale(coord.xscale(), coord.yscale());
			ia.set_xyshear(coord.xshear(), coord.yshear());
		}
		return true;
	}

	/**
	 * Get IDs of images in the correlia project that use the same imageAlignment
	 * @param i image number
	 * @return ID strings
	 */
	public String[] getIDsOfLinkedImages(int i) {
		imageAlignment ia = get_imageAlignment(i);
		ArrayList<String> IDs = new ArrayList<>();
		for (int j = 1; j < size(); j++) {
			if (get_imageAlignment(j) == ia) {
				IDs.add(get_image(j).get_ID());
			}
		}
		String[] out = new String[IDs.size()];
		for (int o = 0; o < out.length; o++) {
			out[o] = IDs.get(o);
		}
		return out;
	}

	/**
	 * Check whether this image is linked to another
	 * @param i image number
	 * @return boolean
	 */
	public boolean imageIsLinked(int i) {
		imageAlignment ia = get_imageAlignment(i);
		for (int j = 1; j < size(); j++) {
			if (j == i) continue;
			if (get_imageAlignment(j) == ia) return true;
		}
		return false;
	}

	/**
	 * Create XML element for saving the project
	 * @param xml xmlHandler instance
	 * @return XML element
	 */
	public Element buildXML(xmlHandler xml) {
		Element root = xml.createElement("correlia");

		xml.addTextElement(root, "projectTitle", get_projectTitle());
		xml.addTextElement(root, "additionalInfo", get_additionalInfo());

		xml.addElement(root, xml.addAttribute(baseImage.buildXML(xml, buildImageName(0, true)), "pos", 0));
		xml.addElement(root, xml.addAttribute(baseImage_colour.buildXML(xml), "pos", 0));

		for (int i = 1; i < images.size() + 1; i++) {
			xml.addElement(root, xml.addAttribute(get_image(i).buildXML(xml, buildImageName(i, true)), "pos", i));
			xml.addElement(root, xml.addAttribute(get_imageColour(i).buildXML(xml), "pos", i));
			if (imageIsLinked(i)) {
				String[] IDs = getIDsOfLinkedImages(i);
				if (!IDs[0].equals(get_image(i).get_ID())) {
					ArrayList<String> params = new ArrayList<>();
					params.add("pos");
					params.add(Integer.toString(i));
					params.add("duplicateOf");
					params.add(IDs[0]);
					xml.addTextElementWithAttributes(root, "imageAlignment", "", params);
				}
			}
			xml.addElement(root, xml.addAttribute(get_imageAlignment(i).buildXML(xml), "pos", i));
		}

		return root;
	}

	/**
	 * Create filename for an image
	 * @param i image number
	 * @param withExtension append extension?
	 * @return filename string
	 */
	public String buildImageName(int i, boolean withExtension) {
		String iPath = projectTitle.replaceAll("_", " ");
		String ext = withExtension? microscopyImage.FILE_EXTENSION_IMAGE : "";
		if (i == 0) {
			return iPath + "_base" + ext;
		} else {
			return iPath + "_ovl" + Integer.toString(i-1) + ext;
		}
	}

	/**
	 * Save project at a user defined location with a timestamp
	 * Spaces are automatically replaced by underscores
	 * @return false, if user does input a path or due to insufficient permissions
	 */
	public boolean save_project(boolean saveAs) {
		if (saveAs || projectPath == null) {
			SaveDialog sd = new SaveDialog("Save project", projectTitle.replaceAll(" ", "_"), "");

			if (sd.getDirectory() == null) {
				return false;
			}

			String dir = sd.getDirectory();
			String fname = sd.getFileName().replaceAll(" ", "_");

			projectPath = dir + fname + File.separator; // + "_" + df.format(dt);

			// try to create sub-directory for backing up the raw images
			File f = new File(projectPath);

			if(!f.exists())	{
				if(!f.mkdir()) {
					return false;
				}
			}
		}

		// copy images
		if (!save_all_images(projectPath) || !save_all_preview_images(projectPath)) {
			IJ.showMessage("Cannot save raw images.\nMaybe insufficient permissions to write files?\nOr no disk space left?");
			return false;
		}

		xmlHandler xml = new xmlHandler();
		xml.setSavepath(projectPath + projectTitle.replaceAll(" ", "_") + PROJECT_FILE_EXTENSION);
		xml.addToDoc(buildXML(xml));
		xml.save();

		return true;
	}

	/**
	 * Save base and overlay images with names based on the project title
	 * @param path project path with trailing /
	 * @return true
	 */
	public boolean save_all_images(String path)	{
		for (int i=0; i<size(); i++) {
			get_image(i).save_image(path + buildImageName(i, true));
			
			// save spectra and images attached to ROIs in that particular image
			for (int j=0; j<(get_image(i)).number_of_rois(); j++) {	// run through ROIs
				for (int k=0; k<(get_image(i)).get_roi(j).get_number_of_spectra(); k++) {	// run through spectra in that ROI
					//TODO save spectra
				}
				for (int k=0; k<(get_image(i)).get_roi(j).get_number_of_images(); k++) {	// run through images attached to that ROI
					//TODO save images
				}
			}
		}
		return true;
	}

	/**
	 * Save base and overlay images as defined by HTML_IMAGE_FORMAT and HTML_IMAGE_SIZE
	 * @param path project path
	 * @return true
	 */
	public boolean save_all_preview_images(String path) {
		ImageProcessor ip;
		for (int i=0; i<size(); i++) {
			ip = (get_image(i)).getProcessor().resize(HTML_IMAGE_SIZE);
			IJ.saveAs(new ImagePlus(get_image(i).getTitle(),ip), HTML_IMAGE_FORMAT, path + buildImageName(i, false) + "_html");
		}
		return true;
	}

	/**
	 * Create HTML Code describing the project and its images
	 * @return html string
	 */
	public String build_saveString_html() {
		StringBuilder builder = new StringBuilder("<!DOCTYPE html>\n<html>\n  <head>\n    <title>" + projectTitle + "</title>\n");
		builder.append("  </head>\n  <style>body{font-family: helvetica, arial, sans-serif;}</style>\n  <body>\n");
	
		String imgWidth = String.format("%.3f", baseImage.imgWidth());
		String imgHeight = String.format("%.3f", baseImage.imgHeight());

		// base
		builder.append("  <h2>Base: ").append(baseImage.getTitle()).append("</h2>\n");
		builder.append("  <IMG SRC=\"").append(projectTitle).append("_base_html.png\" ALT=\"Base image\" WIDTH=600><br><br>\n");
		builder.append("  acquired on: ").append(baseImage.get_acquisitionDate()).append("<br>\n");
		builder.append("  set-up: ").append(baseImage.get_setup()).append("<br>\n");
		builder.append("  experimenter: ").append(baseImage.get_experimenter()).append("<br>\n");
		builder.append("  image width: ").append(imgWidth).append(" (").append(baseImage.getWidth()).append("px)<br>\n");
		builder.append("  image height: ").append(imgHeight).append(" (").append(baseImage.getHeight()).append("px)<br>\n");
		builder.append("  info:<br> ").append(baseImage.get_additionalInfoHTML()).append("<br>\n");
		builder.append("\n");
	
		// overlayed images
		for (int i=0; i<images.size(); i++)	{
			imgWidth = String.format("%.3f", (images.get(i)).getCalibration().pixelWidth*(images.get(i)).getWidth());
			imgHeight = String.format("%.3f", (images.get(i)).getCalibration().pixelHeight*(images.get(i)).getHeight());

			builder.append("  <h2>").append((images.get(i)).getTitle()).append("</h2>\n");
			builder.append("  <IMG SRC=\"").append(projectTitle).append("_ovl").append(Integer.toString(i)).append("_html.png\" ALT=\"Ovl image ").append(Integer.toString(i)).append("\" WIDTH=600><br><br>\n");
			builder.append("  acquired on: ").append((images.get(i)).get_acquisitionDate()).append("<br>\n");
			builder.append("  set-up: ").append((images.get(i)).get_setup()).append("<br>\n");
			builder.append("  experimenter: ").append((images.get(i)).get_experimenter()).append("<br>\n");
			builder.append("  image width: ").append(imgWidth).append(" (").append((images.get(i)).getWidth()).append("px)<br>\n");
			builder.append("  image height: ").append(imgHeight).append(" (").append((images.get(i)).getHeight()).append("px)<br>\n");
			builder.append("  info:<br> ").append((images.get(i)).get_additionalInfoHTML()).append("<br>\n");
			builder.append("\n");
		}
	
		builder.append("  </body>\n</html>");	// html string ends here
		return builder.toString();
	}
	
	/**
	 * Calculation of the projected image
	 * @param x measured in physical units with reference to the base coordinate system
	 * @param y measured in physical units with reference to the base coordinate system
	 * @param sph protection semaphore
	 * @return RGB values
	 */
	public int[] calc_pixelValue(double x, double y, Semaphore sph)	{
		double[] rgb = new double[] {0.0, 0.0, 0.0};
		int val;
		
		try {
			sph.acquire();	// protected code
			if (!get_imageColour(0).get_visiblity().equals(imageColour.HIDE)) {
				rgb = MiscHelper.addArrays(// multiply rgb values with factors and add the offset values (brightness & contrast...)
					MiscHelper.multiplyArrays(get_imageColour(0).RGB(), get_image(0).get_valueTupleNormalised(x,y)),
					get_imageColour(0).RGBoff()
				);
			}
			sph.release();	// end of protected code
		} catch (InterruptedException e) {}

		// run through the images and merge their pixel values
		double ximg, yimg;
		imageColour imgc;
		
		for (int imgNo=1; imgNo<=images.size(); imgNo++) {
			// calculate the coordinates in the coordinate system of the overlay image
			imgc = get_imageColour(imgNo);

			if (!imgc.get_visiblity().equals(imageColour.HIDE)) {
				ximg = get_imageAlignment(imgNo).xprime(x, y);
				yimg = get_imageAlignment(imgNo).yprime(x, y);

				if (get_image(imgNo).xy_inRange(ximg,yimg))	{// only calculate points where the image is
					switch (imgc.get_visiblity()) {
						case imageColour.ENHANCED:
							double bc = 0.0;
							try {
								sph.acquire();	// protected code
								bc = get_image(imgNo).get_valueBrightestChannelNormalised(ximg, yimg);
								sph.release();
							} catch (InterruptedException e) {}

							rgb[0] = (1.0-bc)*rgb[0];
							rgb[1] = (1.0-bc)*rgb[1];
							rgb[2] = (1.0-bc)*rgb[2];
							break;
						case imageColour.EXCLUSIVE:
							rgb[0] = 0; rgb[1] = 0; rgb[2] = 0;
							break;
						default:
							break;
					}

					try{
						sph.acquire();	// protected code
						rgb = MiscHelper.addArrays(rgb, // add contribution of this particular image to the final pixel colour
							MiscHelper.addArrays(// multiply rgb values with factors and add the offset values (brightness & contrast...)
								MiscHelper.multiplyArrays(get_imageColour(imgNo).RGB(), get_image(imgNo).get_valueTupleNormalised(ximg,yimg)),
								get_imageColour(imgNo).RGBoff()
							)
						);
						sph.release();
					} catch (InterruptedException e) {}
				}
			}
		}

		// check that no colour value is greater than 1
		for (int idxrgb=0; idxrgb<3; idxrgb++) {
			if (rgb[idxrgb]>1) {
				rgb[idxrgb]=1.0;
			}
		}

		// convert to integer and return value
		//TODO Why use 255 instead of the full integer value range?
		return new int[] {(int) (255*rgb[0]), (int) (255*rgb[1]), (int) (255*rgb[2])};
	}
	
	////////////////////////////////////////////////////
	// DATA PROCESSING FUNCTIONS ///////////////////////
	////////////////////////////////////////////////////

	/**
	 * Create new multiplication correlation image
	 * @param userROI user selection where the correlation shall be calculated
	 * @param imgs vector of image-indices that shall be considered in the correlation
	 * @param width pixel width of the resulting image
	 * @return false, if creation fails
	 */
	public Boolean dataProc_calcMultiplicationCorrelationImage(Rectangle2D userROI, int[] imgs, int width) {
		StringBuilder builder = new StringBuilder("ProdCorr-imgs");
		int imgCount = 0;	// counts how many images are to be correlated
		for (int i : imgs) {
			if (i>=0 && i<size()) {
				imgCount++;
				builder.append("-").append(i);
			}
		}
		
		// create new microscopyImage
		int imgIdx = create_imageFromROI(userROI, width, builder.toString());
		if (imgIdx<1) {return false;}

		double x  = userROI.getX(); // * get_image(imgIdx).getCalibration().pixelWidth;
		double dx = userROI.getWidth()/((double) width);
		double y0 = userROI.getY(); //* get_image(imgIdx).getCalibration().pixelHeight;
		double dy = userROI.getHeight()/((double) get_image(imgIdx).getHeight());
		
		FloatProcessor p = (FloatProcessor) get_image(imgIdx).getProcessor();
		double ximg,yimg;	// coordinates in image
		
		for (int i = 0; i<width; i++) {
			double y = y0;
			for (int j = 0; j<get_image(imgIdx).getHeight(); j++) {
				double corr = 1.0;
				for (int img: imgs) {
					if (img >= 0) {	// image shall be used for correlation
						ximg = get_imageAlignment(img).xprime(x, y);
						yimg = get_imageAlignment(img).yprime(x, y);
						corr *= get_image(img).get_valueBrightestChannelNormalised(ximg, yimg);
					}
				}
				corr = Math.pow(corr, 1/((double) imgCount));
				p.putPixelValue(i,j,corr);
				y += dy;
			}
			x += dx;
		}
	
		// write additional data to derived image
		builder = new StringBuilder("image derived by product-correlation method from images:\n");
		for (int i : imgs) {
			if (i>=0 && i<size()) {
				builder.append(get_image(i).getTitle()).append("\n");
			}
		}
		get_image(size()-1).set_additionalInfo(builder.toString());
		return true;
	}
	
	public double dataProc_calcPearsonCorrelationOfImages(Rectangle2D userROI, int img1, int img2, int number_of_points, int averages) {
		double avgPearson = 0.0;
		for (int i=0; i<averages; i++) {
			avgPearson += dataProc_calcPearsonCorrelationOfImages(userROI, img1, img2, number_of_points);
		}
		return avgPearson / averages;
	}
	
	public double dataProc_calcPearsonCorrelationOfImages(Rectangle2D userROI, int img1, int img2, int number_of_points) {
		debug.put("arg(img1="+img1+",img2="+img2+"number_of_points "+number_of_points+" entered");
		// generate number_of_points random points inside the ROI that shall be correlated
		Point2D.Double[] pts = generateRandomPointsList(userROI, number_of_points, 0);	// seed for random generator is the system time
		return dataProc_calcPearsonCorrelationOfImages(img1, img2, pts);
	}
	
	public double dataProc_calcPearsonCorrelationOfImages(int img1, int img2, Point2D.Double[] pts)	{// pts are the points which are used to create the data-sets for the Pearson correlation
		debug.put("arg(img1="+img1+",img2="+img2+" entered");
	
		// create the data-sets
		debug.putMicroDebug("create ArrayLists");
		double ximg1,yimg1,ximg2,yimg2;	// points in image coordinates 
		
		ArrayList<Double> dataImg1 = new ArrayList<>();	// intensity of image 1 at (x,y)
		ArrayList<Double> dataImg2 = new ArrayList<>();	// intensity of image 2 at (x,y)
		
		debug.putMicroDebug("create data-sets");
		for (Point2D.Double p : pts) {
			ximg1 = get_imageAlignment(img1).xprime(p.x, p.y);
			yimg1 = get_imageAlignment(img1).yprime(p.x, p.y);
			ximg2 = get_imageAlignment(img2).xprime(p.x, p.y);
			yimg2 = get_imageAlignment(img2).yprime(p.x, p.y);
			
			dataImg1.add(get_image(img1).get_valueBrightestChannelNormalised(ximg1, yimg1));
			dataImg2.add(get_image(img2).get_valueBrightestChannelNormalised(ximg2, yimg2));
		}
		
		// calculate Pearson-correlation
		debug.putMicroDebug("calculate Pearson-correlation");
		xystatistics xystat = new xystatistics(dataImg1, dataImg2);
		double pcc = xystat.calc_pearsonCorrelationCoefficiant();
		IJ.log("PCC: "+pcc);
		return pcc;
	}

	/**
	 * Pearson correlation can be used for image alignment
	 * idea: follow the gradient and maximize the Pearson correlation of two images (or minimize it in case they are anti-correlated)
	 * @param img1 coordinates of this image are varied and image is translated
	 * @param img2 static image to which img1 shall be aligned
	 * @param deltaVector (dx, dy, dRot, dscale)
	 * @param pts points which are used to create the data-sets for the Pearson correlation
	 * @return gradient
	 */
	public double[] dataProc_calcPearsonCorrelationOfImages_gradient(int img1, int img2, double[] deltaVector, Point2D.Double[] pts) {
		debug.put("entered");
		double PC   = dataProc_calcPearsonCorrelationOfImages(img1,img2,pts);
		double x0   = get_imageAlignment(img1).x0();
		double y0   = get_imageAlignment(img1).y0();
		double rot0 = get_imageAlignment(img1).rotAngle();
		double scl0 = get_imageAlignment(img1).xscale();

		double dx   = deltaVector[0];
		double dy   = deltaVector[1];
		double drot = deltaVector[2];
		double dscl = deltaVector[3];
		
		// compute PC(x+dx) 1.) x -> x+dx 2.) PC(x+dx)
		debug.putMicroDebug("compute PC(x+dx)");
		get_imageAlignment(img1).set_x0y0(x0+dx,y0);
		double PCdx = dataProc_calcPearsonCorrelationOfImages(img1,img2,pts);
		get_imageAlignment(img1).set_x0y0(x0,y0);
		
		// compute PC(y+dy) 1.) y -> y+dy 2.) PC(y+dy)
		debug.putMicroDebug("compute PC(y+dy)");
		get_imageAlignment(img1).set_x0y0(x0,y0+dx);
		double PCdy = dataProc_calcPearsonCorrelationOfImages(img1,img2,pts);
		get_imageAlignment(img1).set_x0y0(x0,y0);
		
		// compute PC(rotagl+drotangl)
		debug.putMicroDebug("compute PC(rot+drot)");
		get_imageAlignment(img1).set_rotAngle(rot0+drot);
		double PCdrot = dataProc_calcPearsonCorrelationOfImages(img1,img2,pts);
		get_imageAlignment(img1).set_rotAngle(rot0);
		
		// compute PC(scale+dscale)
		debug.putMicroDebug("compute PC(scl+dscl)");
		get_imageAlignment(img1).set_scale(scl0+dscl);
		double PCdscl = dataProc_calcPearsonCorrelationOfImages(img1,img2,pts);
		get_imageAlignment(img1).set_scale(scl0);
		
		// building the gradient
		debug.putMicroDebug("building the gradient");
		double[] grad = new double[4];
		
		grad[0] = (PCdx -PC)  / dx;
		grad[1] = (PCdy -PC)  / dy;
		grad[2] = (PCdrot-PC) / drot;
		grad[3] = (PCdscl-PC) / dscl;
	
		IJ.log("DEBUG: grad=("+grad[0]+" , "+grad[1]+" , "+grad[2]+" , "+grad[3]+")");
		return grad;
	}
	
	public Boolean dataProc_imageMath_addMul(Rectangle2D userROI, int[] imgs, int width, int add_mul) {
		debug.put("entered");
		
		if (add_mul != IMAGEMATH_ADD && add_mul != IMAGEMATH_MULTIPLY) {return false;}
		
		// build string to label resulting image
		StringBuilder builder = new StringBuilder();
		if (add_mul == IMAGEMATH_ADD) {
			builder.append("Add-imgs");
		}
		if (add_mul == IMAGEMATH_MULTIPLY) {
			builder.append("Mul-imgs");
		}
		for (int i : imgs) {
			if (i>=0 && i<size()) {
				builder.append("-").append(i);
			}
		}

		// create new microscopyImage
		debug.putMicroDebug("create new image from ROI");
		int imgIdx = create_imageFromROI(userROI, width, builder.toString());
		if (imgIdx<1) {return false;}
	
		double x  = userROI.getX();
		double dx = userROI.getWidth()/((double) width);
		double y0 = userROI.getY();
		double dy = userROI.getHeight()/((double) get_image(imgIdx).getHeight());
		
		FloatProcessor p = (FloatProcessor) get_image(imgIdx).getProcessor();
		double ximg,yimg;	// coordinates in image
		
		debug.putMicroDebug("calculate pixels");
		for (int i = 0; i<width; i++) {
			double y = y0;
			for (int j = 0; j<get_image(imgIdx).getHeight(); j++) {
				double res = 0.0;
				if (add_mul == IMAGEMATH_MULTIPLY) {
					res = 1.0;
				}
				for (int img: imgs) {
					if (img >= 0)	{  // image shall be used for correlation
						ximg = get_imageAlignment(img).xprime(x, y);
						yimg = get_imageAlignment(img).yprime(x, y);
						if (add_mul == IMAGEMATH_ADD) {
							res += get_image(img).get_valueBrightestChannelNormalised(ximg, yimg);
						}
						if (add_mul == IMAGEMATH_MULTIPLY) {
							res *= get_image(img).get_valueBrightestChannelNormalised(ximg, yimg);
						}
					}
				}
				p.putPixelValue(i,j,res);
				y+=dy;
			}
			x += dx;
		}
	
		// write additional data to derived image
		builder = new StringBuilder("image derived by");
		if (add_mul == IMAGEMATH_ADD) {
			builder.append(" add-images method from images:\n");
		}
		if (add_mul == IMAGEMATH_MULTIPLY) {
			builder.append(" multiply-images method from images:\n");
		}
		for (int i : imgs) {
			if (i>=0 && i<size()) {
				builder.append(get_image(i).getTitle()).append("\n");
			}
		}
		get_image(size()-1).set_additionalInfo(builder.toString());
		return true;
	}

	/**
	 * Create an empty image that aligns with ROI (coordinates of base image!)
	 * add image to project, add coordinates and colour to project
	 * result is a GRAY32 image (FloatProcessor)
	 * @param userROI
	 * @param width desired width in pixels (pixel resolution)
	 * @param title image title
	 * @return index of the new image in the project, -1 if not successful
	 */
	public int create_imageFromROI(Rectangle2D userROI, int width, String title) {
		if (width<1) {return -1;}
		int height = (int) (((double) width)*userROI.getHeight()/userROI.getWidth());
		imageAlignment icoord = new imageAlignment(userROI.getX(), userROI.getY());
		ImagePlus imp = NewImage.createFloatImage(title, width, height, 1,NewImage.FILL_RAMP);
		imp.getCalibration().setUnit(baseImage.getCalibration().getUnit());
		imp.getCalibration().pixelWidth  = userROI.getWidth()  / ((double) width);
		imp.getCalibration().pixelHeight = userROI.getHeight() / ((double) height);

		microscopyImage img = new microscopyImage(imp);
		img.set_experimenter("created by correlia");
		img.set_acquisitionDate(LocalDate.now());
		img.set_setup("correlia");
		imageColour icol = new imageColour(1.0,1.0,1.0);
		importImage(img, icol, icoord);
		return size()-1;
	}
	
	/**
	 * only allow points inside the ROI
	 * @param userROI
	 * @param size number of points to be generated
	 * @param seed for random generator, if seed<=0 use system time
	 * @return
	 */
	public Point2D.Double[] generateRandomPointsList(Rectangle2D userROI, int size, int seed) {
		Point2D.Double[] pts = new Point2D.Double[size];
		Random r; // random generator

		if (seed<= 0) {	// initialise with system time
			r = new Random(System.currentTimeMillis());
		} else {
			r = new Random(seed);
		}
		for (int i=0; i<size; i++) {
			Point2D.Double p = new Point2D.Double(	// create a random point inside ROI
				userROI.getX() + r.nextDouble()*userROI.getWidth(),
				userROI.getY() + r.nextDouble()*userROI.getHeight());
			pts[i] = p;
		}
		return pts;
	}

	/**
	 * Convert a microscopyImage to a microscopyImageWarped
	 * @param i image number
	 */
	public void convertMi2Miw(int i) {
		if (i == 0) {
			baseImage = new microscopyImageWarped(baseImage);
		} else {
			images.set(i-1, new microscopyImageWarped(images.get(i-1)));
		}
	}

	/**
	 * Get the source image in the reference coordinate system
	 * @param src source image number
	 * @param ref reference image number
	 * @return transformed image
	 */
	public microscopyImage getAdaptedImage(int src,  int ref) {
		microscopyImage miS = get_image(src);
		microscopyImage miR = get_image(ref);
		imageAlignment iaS = get_imageAlignment(src);
		imageAlignment iaR = get_imageAlignment(ref);

		return MiscHelper.getAdaptedImage(miS, miR, iaS, iaR);
	}

	/**
	 *  Automatically register the source image to the reference image with an affine Transformation
	 *  with a global search strategy
	 * @param reference reference image number
	 * @param source source image number
	 * @param translate include translation
	 * @param rotate include rotation
	 * @param scale include scale
	 * @param shear include shear
	 * @param miMatrix matrix containing the Mutual Information results for all positions
	 * @param stepSize distances of the global search grid
	 * @param minWidth minimal image size for downscaling
	 * @param verbosity debug information verbosity
	 * @return success
	 */
	public boolean affineRegistrationGlobal(
			int reference,
			int source,
			boolean translate,
			boolean rotate,
			boolean scale,
			boolean shear,
			double[][][][][][] miMatrix,
			double[] stepSize,
			int minWidth,
			int verbosity
	) {
		double[][] borders = new double[4][2];
		double[] min = new double[2];
		double[] max = new double[2];
		int nbins = 256;
		int blur = 2;
		ImageStack ims;
		double[] steps = new double[6];
		steps[0] = miMatrix[0][0][0][0][0].length;
		steps[1] = miMatrix[0][0][0][0].length;
		steps[2] = miMatrix[0][0][0].length;
		steps[3] = miMatrix[0][0].length;
		steps[4] = miMatrix[0].length;
		steps[5] = miMatrix.length;

		microscopyImage miR = new microscopyImage(get_image(reference));
		microscopyImage miS = new microscopyImage(get_image(source));
		microscopyImage[] mi = {miS, miR};

		for (int i = 0; i < mi.length; i++) {
			ImageStack newIms = new ImageStack(mi[i].getWidth(), mi[i].getHeight());
			int currentSlice = mi[i].getCurrentSlice();
			ImageProcessor ipImg = mi[i].getStack().getProcessor(currentSlice);
			ipImg = ipImg.convertToFloat();
			GaussianBlur gBlur = new GaussianBlur();
			gBlur.blurGaussian(ipImg, blur);
			newIms.addSlice(mi[i].getStack().getSliceLabel(currentSlice), ipImg);
			ImageProcessor ipMask = MiscHelper.createMask(ipImg);
			newIms.addSlice("Mask", ipMask);
			mi[i].setStack(newIms);
			if (verbosity > 1) mi[i].show();
		}

		mi = MiscHelper.resizeImagesToSamePixelWidth(mi, minWidth);

		imageAlignment iaR = get_imageAlignment(reference);
		imageAlignment iaS = get_imageAlignment(source);
		imageAlignment[] ia = {iaS, iaR};
		imageAlignment iaBest = new imageAlignment(iaS);

		min[0] = 0;
		min[1] = 0;
		max[0] = mi[0].getProcessor().getMax();
		max[1] = mi[1].getProcessor().getMax();
		debug.put("min 0:" + min[0] + " 1:" + min[1]);
		debug.put("max 0:" + max[0] + " 1:" + max[1]);

		ImageProcessor[][] ip;
		double miTmp;
		double miBest;

		borders[0][0] = -miS.imgWidth() / 8;
		borders[0][1] = miS.imgWidth() / 8;
		stepSize[0] = (borders[0][1] - borders[0][0]) / steps[0];
		borders[1][0] = -miS.imgHeight() / 8;
		borders[1][1] = miS.imgHeight() / 8;
		stepSize[1] = (borders[1][1] - borders[1][0]) / steps[1];
		borders[2][0] = -90;
		borders[2][1] = 90;
		stepSize[2] = (borders[2][1] - borders[2][0]) / steps[2];

		ip = MiscHelper.getAdaptedIPs(mi, ia);
		ims = new ImageStack(ip[1][0].getWidth(), ip[1][0].getHeight());
		for (int i = 0; i < mi.length; i++) {
			ims.addSlice(mi[i].getTitle(), ip[i]);
		}
		if (verbosity > 1) new ImagePlus("Images", ims).show();

		mutualInformationFast MI = new mutualInformationFast(ip, mutualInformationFast.PLAIMI, nbins, min, max);
		miBest = MI.lastResult;
		if (debug.is_debugging() && verbosity > 1) {
			new ImagePlus("Hist of initial alignment", MiscHelper.doubleArray2DToBP(MI.pdf2D, MI.nbins, MI.nbins)).show();
		}
		debug.put("initial registered MI:" + miBest);

		ims = new ImageStack(ip[0][0].getWidth(), ip[0][0].getHeight());
		for (int r = 0; r < steps[2]; r++) {
			for (int ty = 0; ty < steps[1]; ty++) {
				for (int tx = 0; tx < steps[0]; tx++) {
					ia[0] = new imageAlignment(iaS);
					if (translate) ia[0].set_x0y0(iaS.m_x0 + (borders[0][0] + tx * stepSize[0]), iaS.m_y0 + (borders[1][0] + ty * stepSize[1]));
					if (rotate) ia[0].set_rotAngle(iaS.m_rotAngle + (borders[2][0] + r * stepSize[2]));
					ip = MiscHelper.getAdaptedIPs(mi, ia);
					miTmp = MI.calcNMI(ip);
					if (verbosity > 1) ims.addSlice(miTmp + "::" + ty + ":" + tx + ":" + r, ip[0]);
					miMatrix[0][0][0][r][ty][tx] = miTmp;
					if (miTmp > miBest) {
						iaBest = new imageAlignment(ia[0]);
						miBest = miTmp;
					}
				}
			}
		}
		if (verbosity > 1) new ImagePlus("Transformed Source", ims).show();

		ia[0] = new imageAlignment(iaBest);
		return true;
	}

	/**
	 *  Automatically register the source image to the reference image with an affine Transformation
	 *  with a gradient following search strategy
	 * @param reference reference image number
	 * @param source source image number
	 * @param multiScale search in multiple resolution levels
	 * @param translate include translation
	 * @param rotate include rotation
	 * @param scale include scale
	 * @param shear include shear
	 * @param stepRes contains the affine parameters for all steps in the iterative process
	 * @param minWidth minimal image size for downscaling
	 * @param verbosity debug information verbosity
	 * @return success
	 */
	public boolean affineRegistrationGradient(
			int reference,
			int source,
			boolean multiScale,
			boolean translate,
			boolean rotate,
			boolean scale,
			boolean shear,
			double[][] stepRes,
			int minWidth,
			int verbosity
	) {
		double[][] borders = new double[6][2];
		double[] stepSize = new double[6];
		int stepLimit = stepRes.length;
		int curMinWidth = minWidth;
		double[] min = new double[2];
		double[] max = new double[2];
		int nbins = 256;
		ImageStack ims;
		int blur = 2;

		microscopyImage miR = new microscopyImage(get_image(reference));
		microscopyImage miS = new microscopyImage(get_image(source));
		microscopyImage[] miOrig = {miS, miR};

		// Convert RGB images to grayscale and keep only selected slice (if there are multiple in the stack)
		for (int i = 0; i < miOrig.length; i++) {
			ImageStack newIms = new ImageStack(miOrig[i].getWidth(), miOrig[i].getHeight());
			int currentSlice = miOrig[i].getCurrentSlice();
			ImageProcessor ipImg = miOrig[i].getStack().getProcessor(currentSlice);
			ipImg = ipImg.convertToFloat();
			GaussianBlur gBlur = new GaussianBlur();
			gBlur.blurGaussian(ipImg, blur);
			newIms.addSlice(miOrig[i].getStack().getSliceLabel(currentSlice), ipImg);
			ImageProcessor ipMask = MiscHelper.createMask(ipImg);
			newIms.addSlice("Mask", ipMask);
			miOrig[i].setStack(newIms);
			if (verbosity > 1) miOrig[i].show();
		}

		microscopyImage[] mi = MiscHelper.resizeImagesToSamePixelWidth(miOrig, curMinWidth);
		debug.put("Resized images to "+mi[0].getWidth()+"x"+mi[0].getHeight() + ", " + mi[1].getWidth()+"x"+mi[1].getHeight());

		imageAlignment iaR = new imageAlignment(get_imageAlignment(reference));
		imageAlignment iaS = new imageAlignment(get_imageAlignment(source));
		imageAlignment[] ia = {iaS, iaR};
		imageAlignment iaBest;

		min[0] = 0;
		min[1] = 0;
		max[0] = mi[0].getProcessor().getMax();
		max[1] = mi[1].getProcessor().getMax();
		debug.put("min 0:" + min[0] + " 1:" + min[1]);
		debug.put("max 0:" + max[0] + " 1:" + max[1]);

		ImageProcessor[][] ip;
		double miTmp;
		double miBest;

		debug.put("S-pxW:"+mi[0].pixelWidth());
		// Initial step sizes
		stepSize[0] = 8 * mi[0].pixelWidth();
		stepSize[1] = 8 * mi[0].pixelHeight();
		stepSize[2] = 2;
		stepSize[3] = 0.02;
		stepSize[4] = 0;
		stepSize[5] = 0;

		double[] stepSizeMin = new double[stepSize.length];
		stepSizeMin[0] = mi[0].pixelWidth();
		stepSizeMin[1] = mi[0].pixelHeight();
		stepSizeMin[2] = 0.25;
		stepSizeMin[3] = 0.005;
		stepSizeMin[4] = 1;
		stepSizeMin[5] = 1;


		iaBest = new imageAlignment(ia[0]);
		ip = MiscHelper.getAdaptedIPs(mi, ia);
		mutualInformationFast MI = new mutualInformationFast(ip, mutualInformationFast.PLAIMI, nbins, min, max);
		ims = new ImageStack(ip[1][0].getWidth(), ip[1][0].getHeight());
		ImageStack imsHist = new ImageStack(MI.nbins, MI.nbins);

		for (int s = 0; s < stepLimit; s++) {
			double[][][][][][] grad = new double[shear?3:1][shear?3:1][scale?3:1][rotate?3:1][translate?3:1][translate?3:1];

			ia[0] = new imageAlignment(iaBest);
			ip = MiscHelper.getAdaptedIPs(mi, ia);
			ims.addSlice("Reference("+s+") " + ia[1].m_x0 + ":" + ia[1].m_y0, ip[1][0]);
			ims.addSlice("Source("+s+") " + ia[0].m_x0 + ":" + ia[0].m_y0, ip[0][0]);

			miBest = MI.calcNMI(ip);
			if (debug.is_debugging() && verbosity > 1) {
				imsHist.addSlice("Hist(" + s + ")", MiscHelper.doubleArray2DToBP(MI.pdf2D, MI.nbins, MI.nbins));
			}
			debug.put("initial registered MI:" + miBest);

			boolean foundBetterMI = false;
			imageAlignment iaRef = new imageAlignment(iaBest);
			for (int sc = 0; sc < grad[0][0].length; sc++) {
				for (int r = 0; r < grad[0][0][0].length; r++) {
					for (int ty = 0; ty < grad[0][0][0][0].length; ty++) {
						for (int tx = 0; tx < grad[0][0][0][0][0].length; tx++) {
							ia[0] = new imageAlignment(iaRef);
							if (translate) ia[0].set_x0y0(ia[0].m_x0 + ((tx - 1) * stepSize[0]), ia[0].m_y0 + ((ty - 1) * stepSize[1]));
							if (rotate) ia[0].set_rotAngle(ia[0].m_rotAngle + ((r - 1) * stepSize[2]));
							if (scale) ia[0].set_scale(ia[0].m_xscale + ((sc - 1) * stepSize[3]));
							ip = MiscHelper.getAdaptedIPs(mi, ia);
							miTmp = MI.calcNMI(ip);
							grad[0][0][sc][r][ty][tx] = miTmp;
							if (miTmp > miBest) {
								iaBest = new imageAlignment(ia[0]);
								miBest = miTmp;
								foundBetterMI = true;
							}
						}
					}
				}
			}

			stepRes[s][0] = iaBest.m_x0;
			stepRes[s][1] = iaBest.m_y0;
			stepRes[s][2] = iaBest.m_rotAngle;
			stepRes[s][3] = iaBest.m_xscale;
			stepRes[s][4] = iaBest.m_xshear;
			stepRes[s][5] = iaBest.m_yshear;
			debug.put(s + ": " + iaBest.m_x0 + ":" + iaBest.m_y0 + ":" + iaBest.m_rotAngle + ":" + iaBest.m_xscale);
			ia[0] = new imageAlignment(iaBest);
			if (!foundBetterMI) {
				int limitReached = 0;
				for (int i = 0; i < stepSize.length; i++) {
					stepSize[i] *= 0.67;
					if (stepSize[i] <= stepSizeMin[i]) {
						stepSize[i] = stepSizeMin[i];
						limitReached++;
					}
				}
				if (limitReached >= stepSize.length) {
					if (!multiScale || curMinWidth > minWidth) {
						break;
					}
					if (verbosity > 0) {
						ImagePlus imp = new ImagePlus("Images ("+curMinWidth+")", ims);
						imp.setCalibration(mi[0].getCalibration());
						imp.show();
					}
					curMinWidth *= 2;

					mi = MiscHelper.resizeImagesToSamePixelWidth(miOrig, curMinWidth);
					debug.put("Resized images to "+mi[0].getWidth()+"x"+mi[0].getHeight() + ", " + mi[1].getWidth()+"x"+mi[1].getHeight());
					debug.put("S-pxW:"+mi[0].pixelWidth());
					stepSize[0] = 4 * mi[0].pixelWidth();
					stepSize[1] = 4 * mi[0].pixelHeight();
					stepSize[2] = 1;
					stepSize[3] = 0.01;
					stepSizeMin[0] = mi[0].pixelWidth();
					stepSizeMin[1] = mi[0].pixelHeight();
					stepSizeMin[2] = 0.5;
					stepSizeMin[3] = 0.005;

					ip = MiscHelper.getAdaptedIPs(mi, ia);
					ims = new ImageStack(ip[1][0].getWidth(), ip[1][0].getHeight());
				}
				s--;
				debug.put("new stepssizes:" + stepSize[0] + " : " + stepSize[1] + " : " + stepSize[2]);
			}
		}
		if (verbosity > 0) new ImagePlus("Images ("+curMinWidth+")", ims).show();
		if (verbosity > 1) new ImagePlus("Hists", imsHist).show();
		set_imageAlignment(source, iaBest, false);
		return true;
	}

	/**
	 * Wrapper for automatic affine registration with global oder iterative gradient search strategy
	 * @param reference reference image number
	 * @param source source image number
	 * @param multiScale search in multiple resolution levels
	 * @param translate include translation
	 * @param rotate include rotation
	 * @param scale include scale
	 * @param shear include shear
	 * @param verbosity debug information verbosity
	 * @return success
	 */
	public boolean affineRegistration(int reference, int source, int searchType, boolean multiScale, boolean translate, boolean rotate, boolean scale, boolean shear, int verbosity) {
		int steps;
		double[][][][][][] miMatrix;
		double[] stepSize = new double[6];
		double[][] stepRes;
		imageAlignment iaSold = new imageAlignment(get_imageAlignment(source));
		imageAlignment iaSnew;
		microscopyImage mi = null;

		if (searchType == 0) {
			// Global search
			steps = 20;
			miMatrix = new double[shear?steps:1][shear?steps:1][scale?steps:1][rotate?steps:1][translate?steps:1][translate?steps+10:1];
			affineRegistrationGlobal(reference, source, translate, rotate, scale, shear, miMatrix, stepSize, 256, verbosity);

			if (debug.is_debugging() && verbosity > 0) {
				(new ImagePlus("MI Matrices", MiscHelper.doubleArray2DToBP(miMatrix[0][0][0][0]))).show();
			}

			mi = MiscHelper.interpolateImageFromSampleGrid(miMatrix[0][0][0][0], stepSize, 30);
			if (debug.is_debugging()) {
				iaSnew = new imageAlignment(get_imageAlignment(source));
				mi.add_feature_point(mi.imgWidth() / 2, mi.imgHeight() / 2);
				mi.add_feature_point((iaSnew.m_x0 - iaSold.m_x0) + mi.imgWidth() / 2, (iaSnew.m_y0 - iaSold.m_y0) + mi.imgHeight() / 2);
				mi.show();
			}

		} else if (searchType == 1) {
			// Gradient search
			if (verbosity > 1) {
				iaSold = new imageAlignment(get_imageAlignment(source));
				steps = 15;
				miMatrix = new double[1][1][1][1][translate?steps:1][translate?steps:1];
				affineRegistrationGlobal(reference, source, true, false, false, false, miMatrix, stepSize, 256, 0);
				mi = MiscHelper.interpolateImageFromSampleGrid(miMatrix[0][0][0][0], stepSize, 30);
				mi.add_feature_point(mi.imgWidth()/2, mi.imgHeight()/2);
				set_imageAlignment(source, iaSold, false);
			}
			steps = 20;
			stepRes = new double[steps][6];
			affineRegistrationGradient(reference, source, multiScale, translate, rotate, scale, shear, stepRes, 256, verbosity);
			if (debug.is_debugging()) {
				int stepCount = 0;
				for (double[] s : stepRes) {
					if (s[0] != 0 && s[1] != 0) stepCount++;
				}
				debug.put("Affine Registration done in " + stepCount + " steps");
			}
			if (mi != null) {
				for (double[] step : stepRes) {
					if (step[0] != 0 && step[1] != 0) {
						mi.add_feature_point((step[0] - iaSold.m_x0) + mi.imgWidth() / 2, (step[1] - iaSold.m_y0) + mi.imgHeight() / 2);
					}
				}
				mi.show();
			}
		}

		imageAlignment ia = get_imageAlignment(source);
		debug.put("final iA with tx=" + ia.m_x0 + " ty=" + ia.m_y0 + " r=" + ia.m_rotAngle + " sx=" + ia.m_xscale);

		return true;
	}

	public boolean applyTransformationsToLinkedImages(int selectedImage) {
		if (!(get_image(selectedImage) instanceof microscopyImageWarped)) {
			return false;
		}

		String[] IDs = getIDsOfLinkedImages(selectedImage);
		for (String id : IDs) {
			int pos = get_imagePositionByID(id);
			if (pos == selectedImage) continue;
			if (get_image(pos).getClass() != microscopyImageWarped.class) {
				convertMi2Miw(pos);
			} else {
				((microscopyImageWarped)get_image(pos)).getTransformations().clear();
			}
			xmlHandler xml = new xmlHandler();
			microscopyImageWarped miDup = new microscopyImageWarped((microscopyImageWarped)get_image(selectedImage));
			for (transformation trans : miDup.getTransformations()) {
				trans.getSrcID().set(0, id);
				trans.updateDxyReference();
				((microscopyImageWarped)get_image(pos)).addTransformation(trans);
			}
			if (get_imageColour(pos).get_visiblity() != imageColour.HIDE) {
				((microscopyImageWarped)get_image(pos)).update();
			}
		}
		return true;
	}

	// class identification
	static public String classID(){return "correlia";}
	static public String author(){return "Matthias Schmidt";}
	static public String version(){return "June 09 2017";}

	// data
	private String projectTitle;
	private String projectPath;
	private String additionalInfo = "";
	microscopyImage baseImage;
	imageColour baseImage_colour;	// colour settings for the base
	final ArrayList<microscopyImage> images;	// contains the images (microscopyImage instances)
	final ArrayList<imageAlignment> imageAlignments;	// contains the coordinates of the images (x0,y0,angle) and exhibits coordinate transformation methods (imageAlignment instances)
	final ArrayList<imageColour> imageColourChannels;	// here the selected colour channels for the images are saved
}