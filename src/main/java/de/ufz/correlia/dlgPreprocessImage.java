/*
 * User dialog for preprocessing images
 * Author: Florens Rohde
 *
 * Part of:
 * Correlia plugin for ImageJ and FIJI
 */

package de.ufz.correlia;

import de.ufz.correlia.slider.RangeSlider;
import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.HistogramWindow;
import ij.plugin.filter.GaussianBlur;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;

public class dlgPreprocessImage extends JDialog {
	private static final Integer DLG_INNER_PADDING = 10;
	private static final Integer DLG_OUTER_PADDING = 10;
	public static final Color SRC_FP = Color.MAGENTA;
	public static final Color REF_FP = Color.CYAN;
	public static final Color MATCHLINE = Color.YELLOW;

	// flags
	boolean wasCancelled;
	Component parent = null;

	// data
	microscopyImage src;
	microscopyImage ref;
	microscopyImage srcWork;
	microscopyImage refWork;
	double[] smoothOrig;
	double[] smooth;
	double[][] histRangeOrig;
	double[][] histRange;

	// widgets
	JPanel pan = new JPanel();
	JSlider[] slSmooth = new JSlider[2];
	RangeSlider[] rslHist = new RangeSlider[2];

	dlgPreprocessImage(microscopyImage src, microscopyImage ref,
					   double[] smooth, double[][] histRange) {

		wasCancelled = false;

		this.src = new microscopyImage(src);
		this.ref = new microscopyImage(ref);

		if (smooth == null) {
			this.smoothOrig = new double[2];
			this.smooth = new double[2];
			this.smooth[0] = this.smoothOrig[0] = 0;
			this.smooth[1] = this.smoothOrig[1] = 0;
		} else {
			this.smoothOrig = smooth;
			this.smooth = Arrays.copyOf(this.smoothOrig, this.smoothOrig.length);
		}
		if (histRange == null) {
			this.histRange = new double[2][2];
		} else {
			this.histRange = histRange;
		}
		this.histRangeOrig = new double[2][2];
		for (int i = 0; i < this.histRange.length; i++) {
			if (this.histRange[i] == null) {
				this.histRange[i] = new double[2];
				this.histRange[i][0] = this.histRangeOrig[i][0] = (i==0?this.src:this.ref).getProcessor().getMin();
				this.histRange[i][1] = this.histRangeOrig[i][1] = (i==0?this.src:this.ref).getProcessor().getMax();
			} else {
				this.histRangeOrig[i] = Arrays.copyOf(this.histRange[i], this.histRange[i].length);
			}
			debug.put(i + ": " + this.histRange[i][0] + ":" + this.histRange[i][1]);
		}

		srcWork = src;
		applyPreprocessing(srcWork, 1, smooth[0], histRange[0]);
		refWork = ref;
		applyPreprocessing(refWork, 1, smooth[1], histRange[1]);
		srcWork.setTitle("Source: " + this.srcWork.getTitle());
		refWork.setTitle("Reference: " + this.refWork.getTitle());
		srcWork.show();
		refWork.show();

		setup_dialog();
	}

	private boolean setup_dialog() {
		this.setTitle("Preprocess images");
		this.setModalityType(ModalityType.APPLICATION_MODAL);
		this.setMinimumSize(new Dimension(700, 350));

		generateDialogContent();

		this.add(pan);
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		return true;
	}

	private void generateDialogContent() {
		pan.removeAll();
//		pan.setLayout(new BorderLayout(10, 10));
//		pan.setLayout(new GridLayout(0,1));
		pan.setBorder(BorderFactory.createEmptyBorder(DLG_OUTER_PADDING, DLG_OUTER_PADDING, DLG_OUTER_PADDING, DLG_OUTER_PADDING));
//		pan.add(new JLabel("Image processing with the controllers below is revertable." +
//				"You can also use other ImageJ tools, but these changes are volatile.", JLabel.LEADING));//, BorderLayout.PAGE_START);

//		JPanel pSelect = new JPanel();
		pan.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.anchor = GridBagConstraints.CENTER;
		c.ipadx = DLG_INNER_PADDING;
		c.ipady = DLG_INNER_PADDING;

		c.gridx = 0;
		c.gridy = 0;
//		c.gridwidth = 3;
//		pan.add(new JLabel("Image processing with the controllers below is revertable." +
//				"You can also use other ImageJ tools by using the \"Edit externally\" button," +
//				"but these changes are volatile.", JLabel.LEADING));
//
//		c.gridx = 0;
//		c.gridy++;
		c.gridwidth = 1;
		pan.add(Box.createHorizontalGlue(), c);
		c.gridx++;
		pan.add(new JLabel("Source", SwingConstants.CENTER), c);
		c.gridx++;
		pan.add(new JLabel("Reference", SwingConstants.CENTER), c);

		c.gridx = 0;
		c.gridy++;
		pan.add(new JLabel("Histogramm"), c);

		for (int i = 0; i < 2; i++) {
			HistogramWindow histWin = new HistogramWindow(i==0?src:ref);
			ImagePlus imp = histWin.getImagePlus();
			int w = 255;
			int h = 128;
			imp.setRoi(21,11, w, h);
			IJ.run(imp, "Crop", "");
			w += 3;
			IJ.run(imp, "Canvas Size...", "width="+w+" height="+h+" position=Center");
			imagePanel imgPan = new imagePanel(imp, w,h, false);
			imgPan.setLayout(new BorderLayout(5, 5));
//			imgPan.setBorder(null);
			JPanel imgPanWrap = new JPanel();
			imgPanWrap.setLayout(new BoxLayout(imgPanWrap, BoxLayout.PAGE_AXIS));
//			imgPanWrap.setBorder(null);
//			imgPanWrap.add(Box.createRigidArea(new Dimension(40,0)));
			imgPanWrap.add(imgPan);
			imgPanWrap.add(Box.createRigidArea(new Dimension(24,0)));
			imp.changes = false;
			imp.close();
			c.gridx++;
			pan.add(imgPanWrap, c);
		}

		c.gridx = 0;
		c.gridy++;
		pan.add(Box.createHorizontalGlue(), c);
		for (int i = 0; i < rslHist.length; i++) {
			rslHist[i] = new RangeSlider(0, 99,
					hist2slider((i==0?src:ref), histRange[i][0]),
					hist2slider((i==0?src:ref), histRange[i][1]));
			rslHist[i].setMajorTickSpacing(20);
			rslHist[i].setMinorTickSpacing(5);
			rslHist[i].setPaintTicks(true);
			rslHist[i].setBorder(BorderFactory.createEmptyBorder());
			rslHist[i].setEnabled(true);
			c.gridx++;
			pan.add(rslHist[i], c);
		}

		c.gridx = 0;
		c.gridy++;
		c.ipady = DLG_INNER_PADDING;
		pan.add(new JLabel("Smooth"), c);
		for (int i = 0; i < slSmooth.length; i++) {
			slSmooth[i] = new JSlider(SwingConstants.HORIZONTAL, 0, 8, (int) smooth[i]);
			slSmooth[i].setMajorTickSpacing(1);
			slSmooth[i].setPaintTicks(true);
			slSmooth[i].setPaintLabels(true);
			slSmooth[i].setSnapToTicks(true);
			slSmooth[i].setEnabled(true);
			c.gridx++;
			pan.add(slSmooth[i], c);
		}

		JButton btnApply = new JButton("Apply");
		btnApply.addActionListener(e -> {
			for (int i = 0; i < histRange.length; i++) {
				histRange[i][0] = slider2hist((i==0?src:ref), rslHist[i].getValue());
				histRange[i][1] = slider2hist((i==0?src:ref), rslHist[i].getUpperValue());
			}
			for (int i = 0; i < smooth.length; i++) {
				smooth[i] = slSmooth[i].getValue();
			}

//			srcWork.getStack().setProcessor(applyPreprocessing(srcWork.getStack().getProcessor( 1), smooth[0], histRange[0]), 1);
//			srcWork.repaintWindow();
//			refWork.getStack().setProcessor(applyPreprocessing(refWork.getStack().getProcessor(1), smooth[1], histRange[1]), 1);
//			refWork.repaintWindow();
			srcWork.setStack(applyPreprocessing(src.getStack(), 1, smooth[0], histRange[0]));
			refWork.setStack(applyPreprocessing(ref.getStack(), 1, smooth[1], histRange[1]));
		});

		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(e -> {
			srcWork.setStack(src.duplicate().getStack());
			refWork.setStack(ref.duplicate().getStack());
			for (int i = 0; i < smooth.length; i++) {
				smooth[i] = smoothOrig[i];
				slSmooth[i].setValue((int) smooth[i]);
			}
			for (int i = 0; i < histRange.length; i++) {
				histRange[i] = Arrays.copyOf(histRangeOrig[i], histRange.length);
				rslHist[i].setLowerAndUpperValue(
						(int)(100*histRange[i][0]/(i==0?src:ref).getProcessor().getMin()),
						(int)(100*histRange[i][1]/(i==0?src:ref).getProcessor().getMax())
				);
			}
		});

		JButton btnEditExternally = new JButton("Edit externally");
		btnEditExternally.addActionListener(e -> {
			userConfirm(true);
		});

		JButton btnLeave = new JButton("Quit");
		btnLeave.addActionListener(e -> {
			srcWork.setTitle(src.getTitle());
			refWork.setTitle(ref.getTitle());
			srcWork.close();
			refWork.close();
			userConfirm(true);
		});

//		c.ipady = 10;
//		c.gridx = 0;
//		c.gridy++;
//		pan.add(btnApply, c);
//		c.gridx++;
//		pan.add(btnReset, c);
//		c.gridx++;
//		pan.add(btnEditExternally, c);
//		c.gridx++;
//		pan.add(btnLeave, c);

		JPanel pButtons = new JPanel();
		GridLayout grdlayBtn = new GridLayout(1, 0, DLG_INNER_PADDING, DLG_INNER_PADDING);    // parameters: columns, rows, horizontal gap, vertical gap
		pButtons.setLayout(grdlayBtn);
		pButtons.add(btnApply);
		pButtons.add(btnReset);
		pButtons.add(btnEditExternally);
		pButtons.add(btnLeave);
		c.ipady = 4 * DLG_INNER_PADDING;
		c.gridx = 0;
		c.gridy++;
		c.gridwidth = 3;
		pan.add(pButtons, c);

//		pan.removeAll();
////		pan.setLayout(new BorderLayout(10, 10));
//		pan.setLayout(new GridLayout(0,1));
//		pan.setBorder(BorderFactory.createEmptyBorder(DLG_OUTER_PADDING, DLG_OUTER_PADDING, DLG_OUTER_PADDING, DLG_OUTER_PADDING));
//		pan.add(new JLabel("Image processing with the controllers below is revertable." +
//				"You can also use other ImageJ tools, but these changes are volatile.", JLabel.LEADING));//, BorderLayout.PAGE_START);
//		pan.add(pSelect);//, BorderLayout.CENTER);
//		pan.add(pButtons);//, BorderLayout.PAGE_END);
		pan.revalidate();
		pan.repaint();
		SwingUtilities.updateComponentTreeUI(this);
	}

	private int hist2slider(ImagePlus imp, double hist) {
		ImageProcessor ip = imp.getProcessor();
		return (int)Math.round((hist - ip.getMin()) * 100 / (ip.getMax() - ip.getMin()));
	}
	private double slider2hist(ImagePlus imp, int slider) {
		ImageProcessor ip = imp.getProcessor();
		return ip.getMin() + slider * (ip.getMax() - ip.getMin()) / 100;
	}

	public static void applyPreprocessing(microscopyImage mi, int slice, double smooth, double[] histRange) {
		mi.setStack(applyPreprocessing(mi.getStack(), slice, smooth, histRange));
	}

	public static ImageStack applyPreprocessing(ImageStack in, int slice, double smooth, double[] histRange) {
		ImageStack out = new ImageStack(in.getWidth(), in.getHeight());

		for (int s = 1; s <= in.getSize(); s++) {
			ImageProcessor ip = in.getProcessor(s).convertToFloatProcessor();
			if (s == slice || slice < 1) {
				ip = applyPreprocessing(ip, smooth, histRange);
			}
			out.addSlice(in.getSliceLabel(s), ip);
		}

		return out;
	}

	public static ImageProcessor applyPreprocessing(ImageProcessor in, double smooth, double[] histRange) {
		FloatProcessor ip = in.convertToFloatProcessor();
		if (histRange != null) {
			double[] oldPixels = MiscHelper.toDouble((float[]) ip.getPixels());
			ip.setPixels(MiscHelper.toFloat(MiscHelper.normalizeArray(oldPixels, histRange[0], histRange[1], 0, 1)));
			ip.resetMinAndMax();
		}

		if (smooth > 0) {
			GaussianBlur gBlur = new GaussianBlur();
			gBlur.blurGaussian(ip, smooth);
		}

		return ip;
	}


	public microscopyImage getSrcWork() {
		return srcWork;
	}

	public microscopyImage getRefWork() {
		return refWork;
	}

	public double[] getSmooth() {
		return smooth;
	}

	public double[][] getHistRange() {
		return histRange;
	}

	private void userConfirm(boolean OKCancel) { // true = OK, false = Cancel
		wasCancelled = !OKCancel;
		this.setVisible(false);
	}

	public boolean wasCancelled() {
		return wasCancelled;
	}

	static public String classID() {
		return "dlgPreprocessImage";
	}

	static public String author() {
		return "Florens Rohde";
	}

	static public String version() {
		return "November 21 2017";
	}
}