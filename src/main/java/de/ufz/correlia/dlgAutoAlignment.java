/*
 * Dialog for setting the properities for automatic affine image alignment
 * Author: Florens Rohde
 *
 * Part of:
 * Correlia plugin for ImageJ and FIJI
 */

package de.ufz.correlia;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

class dlgAutoAlignment extends JDialog {
	private static final Integer TEXTFIELD_WIDTH = 40;
	private static final Integer TEXTAREA_HEIGHT = 6;
	private static final Integer DLG_MINIMUM_WIDTH = 300;
	private static final Integer DLG_MINIMUM_HEIGHT = 250;
	private static final Integer DLG_INNER_PADDING = 20;
	private static final Integer DLG_OUTER_PADDING = 20;
	private static final Integer DLG_FONT_SIZE = 14;
	public static final String SEARCH_GLOBAL = "global";
	public static final String SEARCH_HILL = "hillclimbing";

	private boolean wasCancelled;
	private JComboBox cbSearchType;
	private JComboBox cbVerbosity = null;
	private boolean doTranslation;
	private boolean doRotation;
	private boolean doShear;
	private boolean doScale;
	private boolean dryRun;
	private boolean multiScale;

	dlgAutoAlignment() {
		debug.put(" entered");
		doTranslation = true;
		doRotation = false;
		doScale = false;
		doShear = false;
		dryRun = false;
		multiScale = false;
		setup_dialog();
		cbSearchType.setSelectedItem(SEARCH_HILL);
		if (cbVerbosity != null) {
			cbVerbosity.setSelectedIndex(1);
		}
		this.setVisible(true);
	}

	private boolean setup_dialog() {
		String[] opts;

		this.setTitle("Choose parameters for automatic alignment");
		this.setModalityType(ModalityType.APPLICATION_MODAL);
		this.setMinimumSize(new Dimension(DLG_MINIMUM_WIDTH, DLG_MINIMUM_HEIGHT));

		JPanel topPane = new JPanel();
		topPane.setLayout(new BoxLayout(topPane, BoxLayout.PAGE_AXIS));
		topPane.setBorder(BorderFactory.createEmptyBorder(0, 0, DLG_INNER_PADDING, 0));


		JCheckBox chkMultiScale = new JCheckBox("Apply multiscale search (slower)", multiScale);
		chkMultiScale.addActionListener(e -> multiScale = !multiScale);
		topPane.add(chkMultiScale);

		// Pane with two columns for labels and textfields
		JPanel twoColPane = new JPanel();
		twoColPane.setLayout(new GridLayout(0, 2));
		twoColPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
				"Include parameters", TitledBorder.LEFT, TitledBorder.TOP));

		opts = new String[] {SEARCH_GLOBAL, SEARCH_HILL};
		cbSearchType = new JComboBox<>(opts);
//		twoColPane.add(new JLabel("Type of search: "));
//		twoColPane.add(cbSearchType);

		JCheckBox chkTranslation = new JCheckBox("Translation", doTranslation);
		chkTranslation.addActionListener(e -> doTranslation = !doTranslation);
		twoColPane.add(chkTranslation);
		JCheckBox chkRotation = new JCheckBox("Rotation", doRotation);
		chkRotation.addActionListener(e -> doRotation = !doRotation);
		twoColPane.add(chkRotation);

		JCheckBox chkScale = new JCheckBox("Scale", doScale);
		chkScale.addActionListener(e -> doScale = !doScale);
		twoColPane.add(chkScale);
		twoColPane.add(Box.createHorizontalGlue());
//		JCheckBox chkShear = new JCheckBox("Shear", doShear);
//		chkShear.addActionListener(e -> doShear = !doShear);
//		twoColPane.add(chkShear);

		if (debug.is_debugging()) {
			JCheckBox chkDryRun = new JCheckBox("Dry Run", doScale);
			chkDryRun.addActionListener(e -> dryRun = !dryRun);
			twoColPane.add(chkDryRun);
			twoColPane.add(Box.createHorizontalGlue());

			twoColPane.add(new JLabel("Intermediate step verbosity:"));
			opts = new String[] {"Nothing", "A little bit", "Everything"};
			cbVerbosity = new JComboBox<>(opts);
			twoColPane.add(cbVerbosity);
		}

		// Button Pane
		JPanel buttomPane = new JPanel();
		buttomPane.setLayout(new BoxLayout(buttomPane, BoxLayout.X_AXIS));

		JButton btnDone = new JButton("OK");
		btnDone.addActionListener(
				e -> userConfirm(true)
		);
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(
				e -> userConfirm(false)
		);

		buttomPane.add(Box.createHorizontalGlue());
		buttomPane.add(btnDone);
		buttomPane.add(Box.createRigidArea(new Dimension(DLG_INNER_PADDING, 0)));
		buttomPane.add(btnCancel);
		buttomPane.add(Box.createHorizontalGlue());

		JPanel container = new JPanel(new BorderLayout(0, 5));
		container.setBorder(BorderFactory.createEmptyBorder(DLG_OUTER_PADDING, DLG_OUTER_PADDING, DLG_OUTER_PADDING, DLG_OUTER_PADDING));
		container.add(topPane, BorderLayout.PAGE_START);
		container.add(twoColPane, BorderLayout.CENTER);
		container.add(buttomPane, BorderLayout.PAGE_END);

		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(container, BorderLayout.CENTER);

		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		return true;
	}

	private void userConfirm(Boolean OKCancel) { // true = OK, false = Cancel
		wasCancelled = !OKCancel;
		this.setVisible(false);
	}

	public boolean was_cancelled() {
		return wasCancelled;
	}

	public String get_searchType() {
		return (String) cbSearchType.getSelectedItem();
	}

	public boolean get_translation() {
		return doTranslation;
	}

	public boolean get_rotation() {
		return doRotation;
	}

	public boolean get_scale() {
		return doScale;
	}

	public boolean get_shear() {
		return doShear;
	}

	public boolean get_dryRun() {
		return dryRun;
	}

	public boolean get_multiScale() {
		return multiScale;
	}

	public int get_verbosity() {
		return cbVerbosity.getSelectedIndex();
	}

	static public String classID() {
		return "dlgAutoAlignment";
	}

	static public String author() {
		return "Florens Rohde";
	}

	static public String version() {
		return "Sept 18 2017";
	}
}
