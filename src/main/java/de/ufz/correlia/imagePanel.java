package de.ufz.correlia;

import ij.ImagePlus;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

import static javax.swing.SwingUtilities.isLeftMouseButton;
import static javax.swing.SwingUtilities.isRightMouseButton;

public class imagePanel extends JPanel{

	private ImagePlus im;
	private BufferedImage image;
	private MouseAdapter mouseListener;
	private int maxWidth;
	private int maxHeight;
	private int width;
	private int height;

	public imagePanel(ImagePlus imp, int maxW, int maxH, boolean openOnClick) {
		maxWidth = maxW;
		maxHeight = maxH;
		Dimension size = new Dimension(maxWidth, maxHeight);
		setPreferredSize(size);
		setMinimumSize(size);
		setMaximumSize(size);
		setSize(size);
		setLayout(null);

		if (openOnClick) {
			mouseListener = createMouseListener();
			addMouseListener(mouseListener);
		}
		loadImage(imp);
	}

	public void loadImage(ImagePlus imp) {
		if (imp == null) {
			im = null;
			image = new BufferedImage(maxWidth, maxHeight, BufferedImage.TYPE_BYTE_BINARY);
			width = maxWidth;
			height = maxHeight;
		} else {
			im = imp;

			width = maxWidth;
			height = (im.getHeight() * maxWidth) / im.getWidth();

			if (height > maxHeight) {
				height = maxHeight;
				width = (im.getWidth() * maxHeight) / im.getHeight();
			}
//			IJ.log(im.getTitle() + " w: " + Integer.toString(width) + " h: " + Integer.toString(height));
			image = im.getBufferedImage();
		}
	}

	private MouseAdapter createMouseListener() {
		return new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (im != null) {
					if (isLeftMouseButton(e)) {
						if (im instanceof microscopyImage) {
							((microscopyImage)im).show(microscopyImage.MODE_OVL);
						} else {
							im.show();
						}
					} else if (isRightMouseButton(e)){
						if (im instanceof microscopyImage) {
							((microscopyImage)im).show(microscopyImage.MODE_EDIT_FP);
						}
					}
				}
			}
		};
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(image, 0, 0, width, height,this); // see javadoc for more info on the parameters
	}

}