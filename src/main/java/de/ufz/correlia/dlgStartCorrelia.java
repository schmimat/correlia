/*
Dialogue checkboxes takes an array of strings and creates a dialogue with according
checkboxes. It provides a method returning an array of Boolean representing the
states of the checkbox.

author: Matthias Schmidt
date: March 20, 2015
*/

package de.ufz.correlia;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

class dlgStartCorrelia extends JDialog {
	dlgStartCorrelia() {
		buttons = new ArrayList<>();
		setup_dialogue();
		this.setTitle("Start Correlia");
		pack();

		m_debugging = false;
		m_wasCancelled = true;
	}

	private void setup_dialogue() {
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(0,1,10,10));
		p.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		// create and add radiobuttons
		ButtonGroup btnGroup = new ButtonGroup();
		JRadioButton rbnew = new JRadioButton("new project");
		btnGroup.add(rbnew);
		buttons.add(rbnew);
		p.add(rbnew);

		JRadioButton rbload = new JRadioButton("load project");
		btnGroup.add(rbload);
		buttons.add(rbload);
		p.add(rbload);

		// add checkbox for debugging mode
		JCheckBox cbdbg = new JCheckBox("debugging");
		cbdbg.setSelected(false);
		cbdbg.addActionListener(e -> userDebugging());
		p.add(cbdbg);

		// add and initialise buttons
		JPanel pBtn = new JPanel();
		pBtn.setLayout(new BoxLayout(pBtn, BoxLayout.X_AXIS));

		JButton btnOK = new JButton("OK");
		btnOK.addActionListener(e -> userConfirm(true));
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(e -> userConfirm(false));

		pBtn.add(btnOK);
		pBtn.add(Box.createHorizontalStrut(15));
		pBtn.add(btnCancel);

		p.add(pBtn);
		this.add(p);
	}

	public int get_selected_button() {
		if (m_wasCancelled) {
			return -1;
		}

		for (int i = 0; i < buttons.size(); i++) {
			if ((buttons.get(i)).isSelected()) {
				return i;
			}
		}

		return -1;
	}


	public Boolean was_cancelled() {
		return m_wasCancelled;
	}

	public Boolean debugging() {
		return m_debugging;
	}

	private void userConfirm(Boolean OKCancel) {// true = OK, false = Cancel
		m_wasCancelled = !OKCancel;
		this.setVisible(false);
	}

	private void userDebugging() {
		m_debugging = !m_debugging;
	}


	static public String classID() {
		return "dlgStartCorrelia";
	}

	static public String author() {
		return "Matthias Schmidt";
	}

	static public String version() {
		return "January 19 2017";
	}

	// widgets
	private ArrayList<JRadioButton> buttons;

	// flags
	private Boolean m_wasCancelled;
	private Boolean m_debugging;

}

