/*
correliaScreen class provides the window on which correlia_ui displays the project data

author: Matthias Schmidt
*/
package de.ufz.correlia;

import ij.ImagePlus;


public class correliaScreen extends ImagePlus {
	correliaScreen (ImagePlus imp) {
		super( imp.getTitle(), imp.getStack() );
		
		x0 = 0.0;
		y0 = 0.0;
		screenFitFactor = 1;
	}

	correliaScreen(microscopyImage mi) {
		super(mi.getTitle(), mi.getStack());

		x0 = 0.0;
		y0 = 0.0;
		screenFitFactor = 1;
	}

	public void close()	{} // override method in order not to set image pointers to zero

	double x0, y0;	// coordinate offset (with respect to the base image)
	double screenFitFactor;	// if 1 -> fits to screen, if 2 fits to screen if zoom is 50% etc. ...

	static public String classID(){ return "correliaScreen"; }
	static public String author(){ return "Matthias Schmidt"; }
	static public String version(){ return "November 28 2015"; }
}