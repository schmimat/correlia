package de.ufz.correlia;

import ij.*;
import ij.gui.Roi;
import ij.gui.PolygonRoi;

import java.util.Vector;
import java.awt.Polygon;

import ij.io.OpenDialog;


public class microscopyImageRoiHandler
{
	microscopyImageRoiHandler( Roi pr, String title )
	{
		m_roi = pr;
		m_roi.setName(title);
		m_info = new String("");
		m_spec = new Vector();	// initialise vector that bears the spectra attached to this ROI
		m_img  = new Vector();	// initialise vector that bears the images attached to this ROI
	}
	
	microscopyImageRoiHandler( String defStr ) // this constructor is called by parser
	{
		m_roi = new Roi(0,0,1,1);	// create a one-pixel Roi at 0,0 

		m_info = new String("");
		m_spec = new Vector();	// initialise vector that bears the spectra attached to this ROI
		m_img  = new Vector();	// initialise vector that bears the images attached to this ROI
	
		parse_parameterString( defStr );
	}

	// set, get, edit
	
	public void set_title( String t ){ if ( t!=null ){ m_roi.setName(t); } }
	public String get_title(){ return m_roi.getName(); }
	
	public void set_info( String a ){ if( a!=null ){ m_info = a; } }
	public void add_info( String a ){ if( a!=null ){ m_info = m_info + a; } }
	public String get_info(){ return m_info; }
	
	public Roi get_roi(){ return m_roi; }
	
	public int get_number_of_spectra()
	{ return m_spec.size(); }
	
	public int get_number_of_images()
	{ return m_img.size(); }
	
	
	
	public Boolean  add_spec()
	{
		OpenDialog od = new OpenDialog("Open spectrum and attribute it to ROI");
		if ( od.getPath() == null ){ return false; }	// was dialogue cancelled?
		
		String xyInitString = new String("file "+od.getPath()+" 0 1");
		
// 		String xyInitString = new String("data\n0 0.0\n1 0.2\n2 0.5 endData");
		
		xydata xy = new xydata(xyInitString);
		xydata_plot xydp = new xydata_plot(xy);
		
		xydp.plot();	// plot spectrum
		
		m_spec.addElement(xydp);

		return true;
	}
	 
	 
	// public ... get_spec( int i )

	// public Boolean add_image( int i )
	
	// public ... get_image( int i )
	
	
	
	
	
	
	// save and load parameters
	public String build_saveString()
	{
		String s = new String("\tbegin microscopyImageRoiHandler\n");
		s += build_saveStringContent();
		s += "\tend microscopyImageRoiHandler\n";
		return s;
	
	}
	
	protected String build_saveStringContent()
	{
		String s = new String("");

		// save ROI
		Polygon pg = m_roi.getPolygon();
		s += "\t      roiPolygon";
		
		for ( int i=0; i<pg.xpoints.length; i++ )
		{ s += " " + pg.xpoints[i] + " " + pg.ypoints[i]; }
	
		s += " endRoiPolygon\n";

		s += "\t      title " + get_title() + " endTitle\n";
		s += "\t      info " + get_title() + " endInfo\n";
		
		s += "\n\n";
		
		for ( int i=0; i<m_spec.size(); i++ )	// save spectrum data
		{
			// save the spectrum
		// TODO
		
			s += "\t      spectrum\n";
// 			s += "\t        file
			
			
			s += "\t      endSpectrum\n";
		}
		
		
		return s;
	}
	
	public Boolean parse_parameterString( String s )
	{
		if ( s.isEmpty() ){ return false; }
		String words[] = s.split("[ \n\t]");
			
		for ( int i=0; i<words.length; i++ )	// get parameters for this image
		{
			if ( words[i].equals("title") ) // read title
			{
				String t = new String("");
				while ( i<words.length-1 && !words[i+1].equals("endTitle") )
				{ i++; t += words[i] + " "; }
				set_title(t);
			}
			
			if ( words[i].equals("info") ) // read additional info
			{
				String t = new String("");
				while ( i<words.length-1 && !words[i+1].equals("endInfo") )
				{ i++; t += words[i] + " "; }
				t = t.replaceAll("<br>","\n");
				add_info(t);
			}
			
			if ( words[i].equals("roiPolygon") ) // read additional info
			{
				Vector vxp = new Vector();	// x coordinates of polygon
				Vector vyp = new Vector();	// y coordinates of polygon
			
				i++;
			
				while ( i<words.length-2 && !words[i].equals("endRoiPolygon") && !words[i+1].equals("endRoiPolygon") )
				{
					float x,y;
					try {	// try to read two numbers for (x,y) coordinates
						x = Float.parseFloat( words[i] );
						y = Float.parseFloat( words[i+1] );
					}
					catch ( NumberFormatException e )
					{ return false; }
					vxp.addElement(x);
					vyp.addElement(y);
					i+=2;
				}
				
				float[] xp = new float[vxp.size()];
				float[] yp = new float[vyp.size()];
				for ( int j=0; j<vxp.size() && j<vyp.size(); j++ )
				{ xp[j] = (Float) vxp.elementAt(j); yp[j] = (Float) vyp.elementAt(j); }
				
				m_roi = new PolygonRoi( xp, yp, Roi.POLYGON );
			}
			
			if ( words[i].equals("spectrum") ) // read spectrum data
			{
				String str = new String( words[i] );
				while ( !words[i].equals("endSpectrum") )
				{ 
					str += " "+words[i];
					i++;
					if ( i>=words.length ){ return false; }
				}
				
				xydata spec = new xydata(str);
				m_spec.add(spec);
			}
			
		}
		
		return true;
	}

	static public String classID(){ return "microscopyImageRoiHandler"; }
	static public String author(){ return "Matthias Schmidt"; }
	static public String version(){ return "November 21 2016"; }
	
	
	// members
	
	String m_info;	// add information on the ROI

	private Roi m_roi;
	private Vector m_spec;	// spectra attached to this ROI

	private Vector m_img;	// images attached to this ROI
	

}