package de.ufz.correlia;

import ij.gui.GenericDialog;
import ij.*;

public class dlgMicroImageTitleAndCalibrate
{
	dlgMicroImageTitleAndCalibrate(String title, double imageWidth)
	{
		GenericDialog gd = new GenericDialog("Calibrate microscopy image");
//		gd.addStringField("Image title: ", title, 40);
		gd.addStringField("Width  (µm): ", (String.format( "%.4f", imageWidth)).replaceAll(",","."), 40);
		gd.showDialog();
		if (gd.wasCanceled()){ m_width = 0.0; return; }

//		m_title = gd.getNextString();
		m_width = Double.parseDouble(gd.getNextString().replaceAll(",","."));
		return;
	}

	
//	public String get_title(){ return m_title; }
	public double get_width(){ return m_width; }
	
	static public String classID(){ return "dlgMicroImageTitleAndCalibrate"; }
	static public String author(){ return "Matthias Schmidt"; }
	static public String version(){ return "September 1 2015";}
	
	
	private String m_title;
	private double m_width;
}
