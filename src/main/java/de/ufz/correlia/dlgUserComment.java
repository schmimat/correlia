/*
Dialogue user comment opens a dialogue with textfield to enter a comment.
It provides a method returning the comment.

author: Matthias Schmidt
*/

package de.ufz.correlia;

import java.util.*;
import java.util.Vector;
import java.awt.*;
import java.io.*;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.Box;
import javax.swing.BoxLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

// DEBUG, can be removed
// import ij.*;
////////////////////////

class dlgUserComment extends JDialog
{
	dlgUserComment( String dlgTitle, Boolean hideOKCancel )
	{
		initialise(dlgTitle, hideOKCancel);
	}

	dlgUserComment( JFrame owner, String dlgTitle, Boolean hideOKCancel )
	{
		super(owner,true);	// true -> modal dialog
		initialise(dlgTitle, hideOKCancel);
	}

	dlgUserComment( Dialog owner, String dlgTitle, Boolean hideOKCancel )
	{
		super(owner,true);	// true -> modal dialog
		initialise(dlgTitle, hideOKCancel);
	}
	
	protected void initialise(String dlgTitle, Boolean hideOKCancel)
	{
		m_wasCancelled = true;
		m_waitingForUser = true;
		m_fontSize = 14;
		
		setup_dialogue(hideOKCancel);
		this.setTitle(dlgTitle);
		pack();
		setVisible(true);
	}
	
	
	private void setup_dialogue(Boolean hideOKCancel)
	{
		Panel p = new Panel();
		setMinimumSize(new Dimension(300,300));
		p.setLayout(new BoxLayout(p,BoxLayout.Y_AXIS));

		
		// add text area
		TAinput = new JTextArea("");
		JScrollPane scrpn = new JScrollPane(TAinput);
		scrpn.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrpn.setFont(new Font(Font.MONOSPACED, Font.PLAIN, m_fontSize));

		
		p.add( Box.createVerticalStrut( 10 ) );
		p.add( TAinput );
		
		p.add( Box.createVerticalStrut( 10 ) );
		
		// add and initialise buttons
		Panel pBtn = new Panel();
		pBtn.setLayout(new BoxLayout(pBtn,BoxLayout.X_AXIS));

		pBtn.add( Box.createHorizontalStrut( 30 ) );
		btnOK = new JButton("OK");
		btnOK.addActionListener(new ActionListener(){ public void actionPerformed(ActionEvent e){ userConfirm( true ); }});
		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener(){ public void actionPerformed(ActionEvent e){ userConfirm( false ); }});

		
		pBtn.add( Box.createHorizontalStrut( 20 ) );
		pBtn.add(btnOK);
		pBtn.add( Box.createHorizontalStrut( 15 ) );
		pBtn.add(btnCancel);
		pBtn.add( Box.createHorizontalStrut( 20 ) );
		
		if (!hideOKCancel){p.add(pBtn);}
		p.add( Box.createVerticalStrut( 10 ) );
		this.add(p);
	}
	
	public String get_comment(){ return TAinput.getText(); }
	
	public Boolean was_cancelled(){ return m_wasCancelled; }
	
	public void setFontSize( int fs ){ if ( fs>0 ) { m_fontSize = fs; } return; }
	
	private void userConfirm( Boolean OKCancel ) // true = OK, false = Cancel
	{ m_wasCancelled = !OKCancel; m_waitingForUser = false; this.setVisible(false); }
	
	public Boolean waitingForUser() { return m_waitingForUser; }
	
	static public String classID(){ return "dlgUserComment"; }
	static public String author(){ return "Matthias Schmidt"; }
	static public String version(){ return "February 22 2016"; }
	
	
	// widgets
	
	private JButton btnOK;
	private JButton btnCancel;
	
	
	private JTextArea TAinput;
	
	// font size
	private int m_fontSize;
	
	// flags
	private Boolean m_waitingForUser;
	private Boolean m_wasCancelled;
	
}

