/*
The microscopyImage class extends the ImagePlus. It contains information
about the image source and the image calibration. Pixel values are accessed
using physical length units (microns). Therefore double numbers are used.
Values between the pixels are interpolated.

The class was developped at the Helmholtz Centre for Environmental Research Leipzig
(UFZ) within the framework of ProVIS - Centre for Chemical Microscopy.

author : Matthias Schmidt
*/
package de.ufz.correlia;

import ij.*;
import ij.io.*;
import ij.ImagePlus;

import ij.gui.*;
import ij.gui.ImageCanvas;
import ij.process.FloatPolygon;
import ij.process.LUT;
import org.w3c.dom.Element;

import java.io.*;
import java.nio.file.*;
import java.lang.Double;
import java.text.NumberFormat;
import java.text.ParseException;

import java.awt.*;

import java.awt.Font;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Locale;

import java.awt.event.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JPanel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JButton;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;


public class microscopyImage extends ImagePlus implements MouseListener, KeyListener {//, WindowListener
	public static final String FILE_EXTENSION_IMAGE = ".tif";
	public static final String FILE_EXTENSION_DEFSTR = ".mimg";
	public static final DateTimeFormatter ACQUISITIONDATE_FORMAT = java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
	public static final int MODE_SHOW = 0;
	public static final int MODE_OVL = 1;
	public static final int MODE_EDIT_FP = 2;

	microscopyImage(xmlHandler xml, Element root, String prjPath) {
		super(prjPath + xml.getTextByElementName(root, "path"));
		debug.put("entered constructor(xml)");

		this.setTitle(xml.getTextByElementName(root, "title"));
		m_filePath = prjPath + xml.getTextByElementName(root, "path");
		m_additionalInfo = xml.getTextByElementName(root, "additionalInfo");
		try {
			m_acquisitionDate = LocalDate.parse(xml.getTextByElementName(root, "acquisitionDate"), ACQUISITIONDATE_FORMAT);
		} catch (DateTimeParseException e) {
			debug.put("acquisitionDate not parseable: use YYYY-MM-DD instead of " + xml.getTextByElementName(root, "acquisitionDate"));
			m_acquisitionDate = LocalDate.now();
		}

		m_setup = xml.getTextByElementName(root, "setup");
		m_experimenter = xml.getTextByElementName(root, "experimenter");
		this.setSlice(xml.getIntByElementName(root, "slice"));

		m_ID = root.getAttribute("id");
		if (m_ID.isEmpty()) m_ID = MiscHelper.generateID();

		m_width = xml.getDoubleAttributeByElementName(root, "size", "width");
		m_height = xml.getDoubleAttributeByElementName(root, "size",  "height");
		calibrateFromSize();

		feature_points = new ArrayList<>();
		for (Element fp : xml.getElementsByName(root, "feature")) {
			if (fp.getAttribute("type").equals("point")) {
				feature_points.add(new Point.Double(xml.getDoubleAttribute(fp, "x"), xml.getDoubleAttribute(fp, "y")));
			}
		}

		microImg_rois = new ArrayList<>();
		ovl = new Overlay();
		m_selectedRoi = -1;
		EF_dialogue_active = false;
		EF_show_roi_dlg = false;
	}

	/**
	 * constructor for interactive calibration
	 * @param imp Image Plus
	 */
	microscopyImage(ImagePlus imp) {
		this(imp, false);
	}

	microscopyImage(ImagePlus imp, boolean quiet) {
		super(imp.getTitle(), imp.getStack());
		
		if (!quiet) debug.put("entered constructor(ImagePlus)");
		
		m_filePath = "noPath";

		if (!quiet) {
			dlgEditMicroscopyImageProperties dlgProp = new dlgEditMicroscopyImageProperties(imp);
			if (dlgProp.was_cancelled()) {
				this.setTitle(imp.getTitle());
				m_acquisitionDate = LocalDate.now();
				m_setup = "";
				m_experimenter = "";
				m_additionalInfo = "";
			} else {
				this.setTitle(dlgProp.get_title());
				//			this.getCalibration().pixelWidth = dlgProp.get_width() / ((double) imp.getWidth());
				m_acquisitionDate = dlgProp.get_acquisitionDate();
				m_setup = dlgProp.get_setup();
				m_experimenter = dlgProp.get_experimenter();
				m_additionalInfo = dlgProp.get_additionalInfo();
			}
		} else {
			this.setTitle(imp.getTitle());
			m_acquisitionDate = LocalDate.now();
			m_setup = "";
			m_experimenter = "";
			m_additionalInfo = "";
		}

		// if image is not calibrated, ask user to calibrate
		if (!quiet && imp.getCalibration().pixelWidth == 1) {
			dlgMicroImageTitleAndCalibrate mical;
			do {
				mical = new dlgMicroImageTitleAndCalibrate(imp.getTitle(), imp.getCalibration().pixelWidth * this.getWidth());
				m_width = mical.get_width();
				m_height = ((double) this.getHeight()) / ((double) this.getWidth()) * m_width;
				if (m_width <= 0.0) {
					IJ.showMessage("Please calibrate the image properly");
				}
			} while (mical.get_width() <= 0.0);

			this.getCalibration().setUnit("µm");
			this.getCalibration().pixelWidth = m_width / ((double) this.getWidth());
			this.getCalibration().pixelHeight = this.getCalibration().pixelWidth;
		} else {
			double convFac;
			switch (imp.getCalibration().getUnit()) {
				case "cm":
					convFac = 1E6;
					break;
				case "mm":
					convFac = 1E3;
					break;
				case "inch":
					convFac = 2540.0;
					break;
				case "µm":
				case "um":
				default:
					convFac = 1.0;
			}
			this.getCalibration().setUnit("µm");
			this.getCalibration().pixelWidth = convFac * imp.getCalibration().pixelWidth;
			this.getCalibration().pixelHeight = convFac * imp.getCalibration().pixelHeight;
		}

		m_ID = MiscHelper.generateID();

		m_width  = this.getCalibration().pixelWidth * imp.getWidth();
		m_height  = this.getCalibration().pixelHeight * imp.getHeight();

		feature_points = new ArrayList<>();
		microImg_rois = new ArrayList<>();
		ovl = new Overlay();
		m_selectedRoi = -1;
		EF_dialogue_active = false;
		EF_show_roi_dlg = false;
//		gd = setup_dialogue();
	}
	
	microscopyImage(microscopyImage mi)	{  // copy constructor
		super(mi.getTitle(), mi.getStack().duplicate());
		
//		debug.put("entered constructor(microscopyImage)");
		
		// set active slice
		this.setPosition(mi.getChannel(), mi.getSlice(), mi.getFrame());
		
		m_filePath = mi.m_filePath;
		
		// calibration
		m_width = mi.m_width;
		m_height = mi.m_height;		

		calibrateFromSize();

		m_ID = mi.m_ID;

		// info
		m_acquisitionDate = mi.m_acquisitionDate;
		m_setup = mi.m_setup;
		m_experimenter = mi.m_experimenter;
		m_additionalInfo = mi.m_additionalInfo;
		feature_points = MiscHelper.copyFeatures(mi.get_feature_points());
		microImg_rois = new ArrayList<>(mi.microImg_rois);
		m_selectedRoi = -1;
		
		// flags
		EF_dialogue_active = false;
		EF_show_roi_dlg = false;

		// overlay & canvas
		ovl = new Overlay();
		gd = setup_dialogue();
	}

	public void calibrateFromSize() {
		calibrateFromSize(m_width, m_height);
	}

	public void calibrateFromSize(double w, double h) {
		m_width = w;
		m_height = h;
		this.getCalibration().setUnit("µm");
		this.getCalibration().pixelWidth = m_width / ((double) (getWidth()));
		this.getCalibration().pixelHeight = m_height / ((double) (getHeight()));
	}

	public void updateImgSizeFromCalibration() {
		m_width  = this.getCalibration().pixelWidth * this.getWidth();
		m_height  = this.getCalibration().pixelHeight * this.getHeight();
	}

	@Override
	public void show() {
		show(MODE_SHOW);
	}

	public void show(int mode) {
		debug.put("("+this.getTitle()+", mode="+mode+") entered");
		showMode = mode;

		if (showMode == MODE_SHOW) {
			this.setHideOverlay(true);
		} else if (showMode == MODE_OVL) {
			draw_overlay();
			this.setOverlay(ovl);
			this.deleteRoi();
			IJ.setTool("rectangle");
		} else if (showMode == MODE_EDIT_FP) {
			IJ.setTool("multipoint");
			IJ.run(this, "Point Tool...", "type=Crosshair color=Red size=Medium label show counter=0");
			if (number_of_feature_points() > 0) {
				float[] ox = new float[number_of_feature_points()];
				float[] oy = new float[number_of_feature_points()];
				for (int i = 0; i < number_of_feature_points(); i++) {
					Point.Double fp = len2pix(get_feature_point(i));
					ox[i] = (float) fp.getX();
					oy[i] = (float) fp.getY();
				}
				PointRoi r = new PointRoi(ox, oy, number_of_feature_points());
				r.setPointType(1); // 1=Crosshair
				r.setShowLabels(true);
				this.setRoi(r);
			}
//			this.setOverlay(null);
			this.setHideOverlay(true);
		}
		super.show();

 		canv = this.getWindow().getCanvas();
		canv.addMouseListener(this);
		canv.addKeyListener(this);
	}

	public void close()	{  // override method in order not to set image pointers to zero
		debug.put("entered");
		if (showMode == MODE_EDIT_FP) {
			Roi r = getRoi();
			if (r != null && r instanceof PointRoi) {
				FloatPolygon pts = r.getFloatPolygon();
				clear_feature_points();
				for (int i = 0; i < pts.npoints; i++) {
					Point.Double pt = new Point.Double(pts.xpoints[i], pts.ypoints[i]);
					add_feature_point(pix2len(pt));
				}
			}
			deleteRoi();
			showMode = MODE_SHOW;
		}
		hide();
	}
	
	public Boolean check() {
		 Boolean cf = true;
	
		 // check the pixel dimensions of the image
		 if (getWidth()<=0) {
		 	IJ.log("ERROR: in microscopyImage.check: image width is "+getWidth()+" pixels.");
		 	cf = false;
		 }
		 if (getHeight()<=0) {
		 	IJ.log("ERROR: in microscopyImage.check: image height is "+getHeight()+" pixels.");
		 	cf = false;
		 }
		 return cf;
	}

	public Element buildXML(xmlHandler xml, String path) {
		ArrayList<String> params;
		Element root = xml.createElement("microscopyImage");
		xml.addAttribute(root, "type", "plain");
		xml.addAttribute(root, "id", get_ID());
		xml.addTextElement(root, "path", path);
		xml.addTextElement(root, "title", getTitle());
		xml.addTextElement(root, "additionalInfo", get_additionalInfo());
		xml.addTextElement(root, "currentSlice", getZ());
		xml.addTextElement(root, "acquisitionDate", get_acquisitionDate().format(ACQUISITIONDATE_FORMAT));
		xml.addTextElement(root, "setup", get_setup());
		xml.addTextElement(root, "experimenter", get_experimenter());

		params = new ArrayList<>();
		params.add("unit");
		params.add("um");
		params.add("width");
		params.add(Double.toString(m_width));
		params.add("height");
		params.add(Double.toString(m_height));
		xml.addTextElementWithAttributes(root, "size", "", params);

		for (Point.Double fp : feature_points) {
			params = new ArrayList<>();
			params.add("type");
			params.add("point");
			params.add("x");
			params.add(Double.toString(fp.getX()));
			params.add("y");
			params.add(Double.toString(fp.getY()));
			xml.addTextElementWithAttributes(root, "feature", "", params);
		}
		return root;
	}

	public int x2pix(double x) {	// calculates the x-pixel coordinate from an x coordinate
		return ((int) (x/pixelWidth()));
	}
	
	public int y2pix(double y) {	// calculates the y-pixel coordinate from an y coordinate
		return ((int) (y/pixelHeight()));
	}

	public double get_pixelValue(int x, int y, int channel) {
		if (x<0 || x>=getWidth() || y<0 || y>=getHeight()) {
			return 0;
		}
		if (this.getType()==ImagePlus.GRAY8) {
//			LUT lut = this.getLuts()[0];
//			switch (channel) {
//				case 0:
//					return lut.getRed(y*width + x);
//				case 1:
//					return lut.getGreen(y*width + x);
//				case 2:
//					return lut.getBlue(y*width + x);
//			}
//			return lut.get(getPixel(x,y)[0])[channel];
			return getPixel(x,y)[0];
		}	// 8 bit grey scale
		if (this.getType()==ImagePlus.GRAY16 || this.getType()==ImagePlus.GRAY32) {
			return (double) this.getProcessor().getPixelValue(x,y);
		}
		return getPixel(x,y)[channel];
	}
	
	public double get_value(double x, double y, int channel) {	// continous function to get pixel values (interpolates linearily), channel: [R,G,B]
							// (x,y) in physical units (µm)
		if (!xy_inRange(x,y)) {
			return 0.0;
		}

		double xPix = x/pixelWidth();	// x in pixel units
		double yPix = y/pixelHeight();	// y in pixel units

		int x0 = (int)(xPix);
		int x1 = x0+1;
		int y0 = (int)(yPix);
		int y1 = y0+1;

		if (x1>=getWidth()) {x1=x0;}
		if (y1>=getHeight()) {y1=y0;}

		return get_pixelValue(x0, y0, channel)*(x1-xPix)*(y1-yPix) + 
			get_pixelValue(x1, y0, channel)*(xPix-x0)*(y1-yPix) +
			get_pixelValue(x1, y1, channel)*(xPix-x0)*(yPix-y0) +
			get_pixelValue(x0, y1, channel)*(x1-xPix)*(yPix-y0);
	}
	
	public double get_valueNormalised(double x, double y, int channel) {	// similar to get_value(x,y,channel) but returns values in between 0 and 1
		return (get_value(x, y, channel) - getProcessor().getMin())/(getProcessor().getMax()-getProcessor().getMin());
	}

	public double[] get_valueTuple(double x, double y) {	// continous function to get pixel values (interpolates linearily) for the three colour channels
		if (!xy_inRange(x,y)) {
			return new double[] {0.0,0.0,0.0};
		}

		double xPix = x/pixelWidth();	// x in pixel units
		double yPix = y/pixelHeight();	// y in pixel units

		int x0 = (int)(xPix);
		int x1 = x0+1;
		int y0 = (int)(yPix);
		int y1 = y0+1;

		double A = (x1-xPix)*(y1-yPix);
		double B = (xPix-x0)*(y1-yPix);
		double C = (xPix-x0)*(yPix-y0);
		double D = (x1-xPix)*(yPix-y0);
		
		if (x1>=getWidth()) {x1=x0;}
		if (y1>=getHeight()) {y1=y0;}

		double[] res;
		
		// is it a single colour channel?
		if (this.getType()==ImagePlus.GRAY8 || this.getType()==ImagePlus.GRAY16 || this.getType()==ImagePlus.GRAY32) {
			double val = get_pixelValue(x0, y0, 0)*A + 
				get_pixelValue(x1, y0, 0)*B +
				get_pixelValue(x1, y1, 0)*C +
				get_pixelValue(x0, y1, 0)*D;
			res = new double[] {val,val,val};
		} else {  // three colour channels
			res = new double[]{
			get_pixelValue(x0, y0, 0)*A + 
			get_pixelValue(x1, y0, 0)*B +
			get_pixelValue(x1, y1, 0)*C +
			get_pixelValue(x0, y1, 0)*D,
			get_pixelValue(x0, y0, 1)*A + 
			get_pixelValue(x1, y0, 1)*B +
			get_pixelValue(x1, y1, 1)*C +
			get_pixelValue(x0, y1, 1)*D,
			get_pixelValue(x0, y0, 2)*A + 
			get_pixelValue(x1, y0, 2)*B +
			get_pixelValue(x1, y1, 2)*C +
			get_pixelValue(x0, y1, 2)*D};
		}
		return res;
	}

	public double[] get_valueTupleNormalised(double x, double y) {	// similar to get_tuple(x,y) but values are normalised to range [0...1]
		double[] rgb = get_valueTuple(x,y);
		for (int i=0; i<3; i++) {
			rgb[i] = (rgb[i]-getProcessor().getMin()) / (getProcessor().getMax()-getProcessor().getMin());
		}
		return rgb;
	}
	
	public double get_valueBrightestChannel(double x, double y) {	// continous function to get pixel values (interpolates linearily) for the three colour channels
		// if this image is a single channel image, return the channel value
		if (this.getType()==ImagePlus.GRAY8 || this.getType()==ImagePlus.GRAY16 || this.getType()==ImagePlus.GRAY32) {
			return get_value(x,y,0);
		}
		if (!xy_inRange(x,y)) {
			return 0.0;
		}

		double[] res = get_valueTuple(x, y);
			
		// return brightest channel value
		if (res[0] > res[1] && res[0] > res[2]) {
			return res[0];	// return red
		}
		if (res[1] > res[0] && res[1] > res[2]) {
			return res[1];	// return green
		}
		return res[2];	// return blue
	}
	
	public double get_valueBrightestChannelNormalised(double x, double y) {	// similar to get_valueBrightestChannel(x,y) but values normalised to [0...1]
		return (get_valueBrightestChannel(x,y)-getProcessor().getMin()) / (getProcessor().getMax()-getProcessor().getMin());
	}
	
	public Boolean xy_inRange(double x, double y) {
		if (x<0.0 || x>imgWidth() || y<0.0 || y>imgHeight()) {return false;}
		return true;
	}
	
	public double imgWidth() {	// returns the width of the image in physical units (e.g. microns)
		return m_width;
	}
	
	public double imgHeight() {	// returns the height of the image in physical units (e.g. microns)
		return m_height;
	}
	
	public double pixelWidth() {	// return the width of one pixel in physical units (microns)
		return this.getCalibration().pixelWidth;
	}
	
	public double pixelHeight() {	// return the width of one pixel in physical units (microns)
		return this.getCalibration().pixelHeight;
	}

	public String get_ID() {
		return m_ID;
	}

	public void set_ID(String s) {
		m_ID = s;
	}

	public void generateNewID() {
		m_ID = MiscHelper.generateID();
	}

	public LocalDate get_acquisitionDate() {return m_acquisitionDate;}
	public void set_acquisitionDate(LocalDate ld) {m_acquisitionDate = ld;}

	public String get_setup() {return m_setup;}
	public void set_setup(String sp) {m_setup = sp;}
	
	public String get_experimenter() {return m_experimenter;}
	public void set_experimenter(String ex) {m_experimenter = ex;}
	
	public String get_additionalInfo() {return m_additionalInfo;}
	public void set_additionalInfo(String ai) {m_additionalInfo = ai;}
	public String get_additionalInfoHTML() {
		String s = m_additionalInfo;
		s = s.replaceAll("\n","<br>");
		return s;
	}
	
	public void setSliceLabel(String sl) {	// set label of selected slice
		getStack().setSliceLabel(sl,getSlice());
	}
	
	public String getSliceLabel() {	// return label of selected slice
		try {
			String label = getStack().getSliceLabel(getSlice());
			if (label == null || label.isEmpty()) {
				return "Slice " + Integer.toString(getSlice());
			} else {
				return label;
			}
		} catch (IllegalArgumentException e) {}
		return "-";
	}
	
	public String[] getSliceLabels() {
		String[] sl = new String[getStack().getSize()];
	
		for (int i=1; i<=getStack().getSize(); i++) {
			sl[i-1] = getStack().getSliceLabel(i);
		}
		return sl;
	}

	public Point.Double pix2len(Point.Double pix) {
		return new Point.Double(pix.getX() * pixelWidth(), pix.getY() * pixelHeight());
	}

	public Point.Double len2pix(Point.Double len) {
		return new Point.Double(len.getX() / pixelWidth(), len.getY() / pixelHeight());
	}

	// functions to define features for correlative microscopy
	public ArrayList<Point.Double> get_feature_points() {return feature_points;}
	public int number_of_feature_points() {return feature_points.size();}
	public void add_feature_point(double x, double y) {
		add_feature_point(new Point.Double(x, y));
	}
	public void add_feature_point(Point.Double p) {
		debug.put(" (entered ("+this.getTitle()+")");
		feature_points.add(p);
	}
	public Point.Double get_feature_point(int pntIdx) {
		try {
			return feature_points.get(pntIdx);
		} catch (IndexOutOfBoundsException e) {
			debug.put(" index "+pntIdx+" out of bounds");
			return null;
		}
	}
	public void set_feature_point(int pntIdx, double x, double y) {
		try {
			feature_points.set(pntIdx, new Point.Double(x, y));
		} catch (IndexOutOfBoundsException e) {
			debug.put(" index "+pntIdx+" out of bounds");
		}
	}
	public void set_feature_points(ArrayList<Point.Double> fps) {
		clear_feature_points();
		for (Point.Double fp: fps) {
			add_feature_point(fp);
		}
	}

	public void remove_feature_point(int pntIdx) {
		try {
			feature_points.remove(pntIdx);
		} catch (IndexOutOfBoundsException e) {
			debug.put(" index " + pntIdx + " out of bounds");
		}
	}

	public void clear_feature_points() {
		feature_points.clear();
	}

	private void draw_overlay() {
		ovl.clear();	// remove all objects from overlay
		Font fo = new Font(this.getProcessor().getFont().getName(),// name
				this.getProcessor().getFont().getStyle(),	// style
				this.getWidth()/64	// size
		);

		for(int i=0; i<number_of_feature_points(); i++) {	// run through all points
			Point.Double p = get_feature_point(i);
			
			int ix = (int) (p.x/this.getCalibration().pixelWidth);
			int iy = (int) (p.y/this.getCalibration().pixelHeight); //this.pixelHeight());
			
			Roi circle = new OvalRoi(ix-this.getWidth()/200,
					iy-this.getWidth()/200,
					this.getWidth()/100,
					this.getWidth()/100
			);
			Roi circleCentre = new OvalRoi(ix, iy, 0, 0);
			circle.setStrokeColor(Color.red);
			circle.setStrokeWidth(1);
			circleCentre.setStrokeColor(Color.red);
			circleCentre.setStrokeWidth(2);
			TextRoi txt = new TextRoi(ix+this.getWidth()/70,iy-this.getWidth()/70, ""+i,fo);
			txt.setStrokeColor(Color.red);
			ovl.add(circle);
			ovl.add(circleCentre);
			ovl.add(txt);
		}

		for (int i=0; i<microImg_rois.size(); i++) {	// run through the rois
			Roi r = (microImg_rois.get(i)).get_roi();
			TextRoi txt;
			Color col;
			
			if (i!= m_selectedRoi) {
				col = new Color(140,140,255);
				r.setStrokeColor(col); r.setStrokeWidth(this.getWidth()/500);
				txt = new TextRoi(
					((int) r.getPolygon().getBounds().getX()) + ((int) r.getPolygon().getBounds().getWidth())/3,
					((int) r.getPolygon().getBounds().getY()) + ((int) (r.getPolygon().getBounds().getHeight())) + this.getWidth()/64,
					r.getName(),fo);
				txt.setStrokeColor(col);
				ovl.add(txt);
			} else {
				col = new Color(230,180,0);
				r.setStrokeColor(col); r.setStrokeWidth(this.getWidth()/200);
				txt = new TextRoi(
					((int) r.getPolygon().getBounds().getX()) + ((int) r.getPolygon().getBounds().getWidth())/3,
					((int) r.getPolygon().getBounds().getY()) + ((int) (r.getPolygon().getBounds().getHeight())) + this.getWidth()/64,
					r.getName(),fo);
				txt.setStrokeColor(col);
				ovl.add(txt);
			}

			ovl.add(r);
		}
	}

	protected NonBlockingGenericDialog setup_dialogue() {
		gd = new NonBlockingGenericDialog("Edit image: "+this.getTitle()); 
		
		Panel pan = new Panel();
		pan.setLayout(new BoxLayout(pan,BoxLayout.Y_AXIS));
		
		// add menu bar
// 		pan.add(Box.createVerticalStrut(10));
		pan.add(setup_menuBar());
		pan.add(Box.createVerticalStrut(20));
		// add panels
		pan.add(setup_roiPanel());
		pan.add(Box.createVerticalStrut(20));
		pan.add(setup_specPanel());

		gd.add(pan);
		gd.setOKLabel("Close");
		gd.hideCancelButton();

		gd.setMinimumSize(new Dimension(300, 300));
		gd.setMaximumSize(new Dimension(300, 300));
		
		return gd;
	}
	
	private JPanel setup_menuBar() {
		// panel and menu bar
		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p,BoxLayout.X_AXIS));
		JMenuBar mb = new JMenuBar();
		
		// File menu
		JMenu menuFile = new JMenu("File");
		menuFile.setMnemonic(KeyEvent.VK_F);
		JMenuItem menuItemSave = new JMenuItem("Save image", KeyEvent.VK_S);
		menuItemSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				save();
			}
		});
		menuItemSave.setToolTipText("<html>save image in microscopy-image format:<br>TIFF image + text file</html>");
		JMenuItem menuItemClose = new JMenuItem("Close", KeyEvent.VK_Q);
		menuItemClose.setToolTipText("close dialogue");
		menuItemClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gd.hide(); EF_dialogue_active=false;
			}
		});
		
		menuFile.add(menuItemSave);
		menuFile.add(menuItemClose);

		// ROI and spectra menu
		JMenu menuROISpec = new JMenu("ROIs and spectra");
		menuROISpec.setMnemonic(KeyEvent.VK_R);
		menuROISpec.setToolTipText("add, delete, modify ROIs and attributed spectra");
		
		JMenuItem menuItemAddRoi = new JMenuItem("Add ROI", KeyEvent.VK_A);
		menuItemAddRoi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				add_roi();
			}
		});
		menuItemAddRoi.setToolTipText("<html>add a ROI to image:<br><li>draw ROI using ImageJ tools first</li><li>use this function and add it to image</li></html>");
		JMenuItem menuItemDeleteRoi = new JMenuItem("Delete ROI", KeyEvent.VK_A);
		menuItemDeleteRoi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				delete_roi();
			}
		});
		menuItemDeleteRoi.setToolTipText("delete selected ROI from image");

		menuROISpec.add(menuItemAddRoi);
		menuROISpec.add(menuItemDeleteRoi);
		
		// add menus to menu bar
		mb.add(menuFile);
		
		p.add(mb);
		p.setMinimumSize(new Dimension(50, 40));
		
		return p;
	}
	
	private JPanel setup_roiPanel() {
		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p,BoxLayout.X_AXIS));
		
		// button select roi
		ImageIcon icoSelectRoi = new ImageIcon(this.getClass().getResource("/icons/button_ico_select_ROI.png"));
		JButton Btn_select_roi = new JButton(icoSelectRoi);
		Btn_select_roi.setToolTipText("select ROI");
		Btn_select_roi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {select_roi();
			}
		});

		// button roi information
		ImageIcon icoInfoRoi = new ImageIcon(this.getClass().getResource("/icons/button_ico_image_info.png"));
		JButton Btn_roi_info = new JButton(icoInfoRoi);
		Btn_roi_info.setToolTipText("get information on selected ROI");
		Btn_roi_info.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {display_roi_info();
			}
		});

		// button add roi
		ImageIcon icoAddRoi = new ImageIcon(this.getClass().getResource("/icons/button_ico_define_features.png"));
		JButton Btn_add_roi = new JButton(icoAddRoi);
		Btn_add_roi.setToolTipText("add ROI to image");
		Btn_add_roi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {add_roi();
			}
		});

// 		// button delete roi
// 		ImageIcon icoDelRoi = new ImageIcon(this.getClass().getResource("/icons/button_ico_define_features.png"));
// 		JButton Btn_delete_roi = new JButton(icoDelRoi);
// 		Btn_delete_roi.setToolTipText("delete ROI");
// 		Btn_delete_roi.addActionListener(new ActionListener() {public void actionPerformed(ActionEvent e) {delete_roi();}});
		
		p.add(Box.createHorizontalStrut(10));
		p.add(Btn_select_roi);
		p.add(Box.createHorizontalStrut(5));
		p.add(Btn_roi_info);
		p.add(Box.createHorizontalStrut(5));
		p.add(Btn_add_roi);
// 		p.add(Box.createHorizontalStrut(5));
// 		p.add(Btn_delete_roi);
		p.add(Box.createHorizontalStrut(10));
		
		return p;
	}
	
	private JPanel setup_specPanel() {
		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p,BoxLayout.X_AXIS));
		
		// button select spectrum
		ImageIcon icoSelectSpec = new ImageIcon(this.getClass().getResource("/icons/button_ico_selectSpectrum.png"));
		JButton Btn_select_spectrum = new JButton(icoSelectSpec);
		Btn_select_spectrum.setToolTipText("select spectrum attributed to this ROI and plot");
		Btn_select_spectrum.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				select_spectrum();
			}
		});

		// button spectrum info
		ImageIcon icoInfoSpec = new ImageIcon(this.getClass().getResource("/icons/button_ico_image_info.png"));
		JButton Btn_spectrum_info = new JButton(icoInfoSpec);
		Btn_spectrum_info.setToolTipText("get information on selected spectrum");
		Btn_spectrum_info.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				display_spectrum_info();
			}
		});
		
		// button add spectrum
		ImageIcon icoAddSpec = new ImageIcon(this.getClass().getResource("/icons/button_ico_addSpectrum.png"));
		JButton Btn_add_spectrum = new JButton(icoAddSpec);
		Btn_add_spectrum.setToolTipText("attribute a spectrum to ROI");
		Btn_add_spectrum.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				add_spectrum();
			}
		});
		
		// button delete spectrum
		ImageIcon icoDelSpec = new ImageIcon(this.getClass().getResource("/icons/button_ico_delSpectrum.png"));
		JButton Btn_delete_spectrum = new JButton(icoDelSpec);
		Btn_delete_spectrum.setToolTipText("delete selectred spectrum from ROI");
		Btn_delete_spectrum.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				delete_spectrum();
			}
		});

		p.add(Box.createHorizontalStrut(10));
		p.add(Btn_select_spectrum);
		p.add(Box.createHorizontalStrut(5));
		p.add(Btn_spectrum_info);
		p.add(Box.createHorizontalStrut(5));
		p.add(Btn_add_spectrum);
		p.add(Box.createHorizontalStrut(5));
		p.add(Btn_delete_spectrum);
		p.add(Box.createHorizontalStrut(10));
		
		return p;
	}
	
	// save, ROI and spectrum functions called by the dialogue
	
	public int number_of_rois() {return microImg_rois.size();}
	
	public microscopyImageRoiHandler get_roi(int i) {
		if (i<0 || i>=number_of_rois()) {return null;}
		return microImg_rois.get(i);
	}

	public void select_roi() {
		String[] roiTitles = new String[microImg_rois.size()];
		
		for (int i=0; i<microImg_rois.size(); i++) {
			roiTitles[i] = (microImg_rois.get(i)).get_title();
		}
		
		dlgRadiobuttons dlg = new dlgRadiobuttons(gd, "Select ROI", roiTitles, false);
		dlg.setLocationRelativeTo(null);	// centre dialogue
		select_roi(dlg.get_selected_button());
	}
	
	public void select_roi(int roiIdx) {
		if (roiIdx < 0 || roiIdx > microImg_rois.size()-1) {return;}
	
		m_selectedRoi = roiIdx;
		
		draw_overlay();
		this.setOverlay(ovl);
		this.show();
		this.repaintWindow();
	}
	
	public void display_roi_info() {
		IJ.log("DEBUG: display roi info, not implemented yet");
	}

	public void add_roi() {
		dlgUserComment duc = new dlgUserComment(gd,"ROI title and description", false);	// modal dialogue
		duc.setLocationRelativeTo(null);
		
		if (duc.was_cancelled()) {return;}	// if user pressed cancel button stop here 
		
		String ROItitle = duc.get_comment().split("\n")[0];
		Roi r = this.getRoi();

		microscopyImageRoiHandler mrh = new microscopyImageRoiHandler(r, ROItitle);
		mrh.set_info(duc.get_comment());
		microImg_rois.add(mrh);
		
		m_selectedRoi = microImg_rois.size()-1;
		
		draw_overlay();
		this.setOverlay(ovl);
		this.show();
		this.repaintWindow();
	}

	public void delete_roi() {
		if (microImg_rois.size() > 0) {
			microImg_rois.remove(m_selectedRoi);
		}
		
		m_selectedRoi = microImg_rois.size()-1;
		
		draw_overlay();
		this.setOverlay(ovl);
		this.show();
		this.repaintWindow();
	}

	public void select_spectrum() {
		IJ.log("DEBUG: select spectrum, not implemented yet");
	}
	
	public void display_spectrum_info() {
		IJ.log("DEBUG: display spectrum info, not implemented yet");
	}
	
	public void add_spectrum() {
		if (m_selectedRoi < 0) {IJ.showMessage("Cannot attribute spectrum to ROI: No ROI selected."); return;}
		if (!(microImg_rois.get(m_selectedRoi)).add_spec())	// attribute spectrum to ROI
		{IJ.showMessage("Cannot attribute spectrum to ROI: No ROI or no spectrum selected."); return;}
	}
	
	public void delete_spectrum() {
		IJ.log("DEBUG: delete spectrum, not implemented yet");
	}

	public boolean save() {
		return save("");
	}

	public boolean save(String path) {	// save image, rois, spectrum, annotation
		IJ.log("DEBUG: save microscopyImage is not fully implemented yet");
		Path p;
		if (!path.equals("noPath") && !path.equals("")) {
			p = Paths.get(path);
		} else if (get_filePath().equals("noPath") || get_filePath().equals("")) {
			SaveDialog sd = new SaveDialog("Target microscopy image location", getTitle(), "");
			if (sd.getDirectory() == null) {
				IJ.showMessage("Save dialog was cancelled");
				return false;
			}
			p = Paths.get(sd.getDirectory() + sd.getFileName());
		} else {
			p = Paths.get(get_filePath());
		}
		String pathBase = p.toString().replaceAll(" ","_");

		save_image(pathBase);
		return save_defstr(pathBase);
	}

	public void save_image(String path) {
		path = SaveDialog.setExtension(path, FILE_EXTENSION_IMAGE);
		set_filePath(Paths.get(path).getFileName().toString());
		IJ.save(this, path);
	}

	public boolean save_defstr(String path) {
		path = SaveDialog.setExtension(path, FILE_EXTENSION_DEFSTR);
//		try	{
//			FileWriter fw = new FileWriter(path,false);
//			BufferedWriter bw = new BufferedWriter(fw);
//			bw.write(build_saveString());
//			bw.close();
//		} catch(IOException e) {
//			IJ.showMessage("Cannot save microscopy image defstring.\nMaybe insufficient permissions to write files?");
//			return false;
//		}
		return true;
	}

	// override the getWidth / getHeight functions as they do not seem to work in ImageJ > 1.50
	
	public int getWidth() {return getProcessor().getWidth();}
	public int getHeight() {return getProcessor().getHeight();}
	
	static public String classID() {return "microscopyImage";}
	static public String author() {return "Matthias Schmidt";}
	static public String version() {return "May 15 2017";}
	
	protected String m_filePath;	// path to original image
	public void set_filePath(String p) {m_filePath = p;}
	public String get_filePath() {return m_filePath;}
	
	// mouse events

	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}
	public void mouseDragged(MouseEvent e) {}
	
	public void mouseExited(MouseEvent e) {}
	public void mouseClicked(MouseEvent e) {
		// get coordinate in image (respects zoom)
		double xpix = canv.offScreenXD(e.getX());
		double ypix = canv.offScreenYD(e.getY());

		if (showMode == MODE_OVL) {
			if (e.isControlDown()) {
				double x = xpix;
				double y = ypix;
				if (this.getRoi() != null) {
					Roi r = this.getRoi();
					if (r.isArea()) {
						x = r.getXBase() + r.getFloatWidth() / 2;
						y = r.getYBase() + r.getFloatHeight() / 2;
					}
					this.deleteRoi();
				}
				x *= pixelWidth();
				y *= pixelHeight();

				this.add_feature_point(x, y);    // add this point to the list of features in base image which can be used for correlative microscopy
			} else if (e.isShiftDown()) {
				this.remove_feature_point(this.number_of_feature_points() - 1);
			}
		}

		if (EF_show_roi_dlg) {	// true if ALT is pressed
			Boolean[] roisAtCurserPos = new Boolean[microImg_rois.size()];
			int numberOfRoisAtCurserPos = 0;
		
			// check rois are available for the given mouse curser position
			for (int i=0; i<microImg_rois.size(); i++) {
				if ((microImg_rois.get(i)).get_roi().contains((int) xpix, (int) ypix)) {
					roisAtCurserPos[i] = true;
					numberOfRoisAtCurserPos++;
				} else {
					roisAtCurserPos[i] = false;
				}
			}
			
			if (numberOfRoisAtCurserPos == 1) {	// if there is only one ROI select this one
				int i=0;
				while (!roisAtCurserPos[i]) {
					i++;
				}
				select_roi(i);
				EF_show_roi_dlg=false;
				return;
			}
			
			if (!(numberOfRoisAtCurserPos == 0)) {
				String[] rois = new String[numberOfRoisAtCurserPos];
				int[] roiIdx = new int[numberOfRoisAtCurserPos];
				int stringPos = 0;
				for (int i=0; i<microImg_rois.size(); i++) {
					if (roisAtCurserPos[i]) {	// this ROI contains the cursor position
						rois[stringPos] = (microImg_rois.get(i)).get_title();
						roiIdx[stringPos] = i;
						stringPos++;
					}
				}
			
				dlgRadiobuttons dlg = new dlgRadiobuttons(gd, "Select ROI", rois, false);
				dlg.setLocationRelativeTo(null);	// centre dialogue
				select_roi(roiIdx[dlg.get_selected_button()]);
			} else {
				IJ.showMessage("No ROIs are defined at this curser position.");
			}
		}
		
// 		if (EF_modifyPixelCathegoryMap) {	// can be set true from dialogue
// 			modifyPixelCathegoryMap(e.getX(), e.getY(), m_modifyPixelCathegoryMap_diameter, m_modifyPixelCathegoryMap_threshold, m_modifyPixelCathegoryMap_protect, m_modifyPixelCathegoryMap_value );
// 		}

		if (showMode == MODE_OVL) {
			draw_overlay();
			this.setOverlay(ovl);
			this.repaintWindow();
		}
	}
	public void mouseEntered(MouseEvent e) {
//		if (showMode == MODE_SHOW) {
//			draw_overlay();
//			this.setOverlay(ovl);
//			this.repaintWindow();
//		} else {
//			this.setHideOverlay(true);
//		}
	}
	public void mouseMoved(MouseEvent e) {IJ.log("mouse moved");}

	// key events
	public void keyPressed(KeyEvent e) {
//		if (e.getKeyCode() == e.VK_ALT) {EF_show_roi_dlg = true; return;}	// "ALT" and mouse click will display avaiable rois and spectra in this point
// 		if (e.getKeyCode() == e.VK_TAB) {if(!EF_modifyPixelCathegoryMap) {modifyPixelCathegoryMap_enable(); return;} modifyPixelCathegoryMap_disable(); return;}
	}
	public void keyReleased(KeyEvent e) {
		if(e.getKeyCode() == e.VK_SPACE) {	// space opens and hides the dialogue
			if (!EF_dialogue_active) {
				EF_dialogue_active=true; gd.setLocationRelativeTo(null); gd.show();
			}
// 			else {gd.hide(); EF_dialogue_active=false;}
		}
	}
	public void keyTyped(KeyEvent e) {}

	// dialogue window
	NonBlockingGenericDialog gd;

	// event flags
	Boolean EF_show_roi_dlg;
	Boolean EF_dialogue_active;
// 	Boolean EF_modifyPixelCathegoryMap;

	Overlay ovl;	// use ImageJ overlay function to indicate feature points etc.
	ImageCanvas canv;	// zooming etc.
	ImageWindow win;

	private int showMode = MODE_SHOW;

	private double m_width, m_height;	// dimensions of the image in physical units
	private LocalDate m_acquisitionDate;
	private String m_setup;
	private String m_experimenter;
	private String m_additionalInfo;
	private String m_ID;

	// define prominent features in the image which are lateron used for correlative microscopy
	protected ArrayList<Point.Double> feature_points;
	
	// define ROIs (each saved in a microscopyImageRoiHandler class
	private ArrayList<microscopyImageRoiHandler> microImg_rois;
	private int m_selectedRoi;

// 	// second 8 bit image of equal size used to define different cathegories of pixels onto which different recipes for displaying can be applied
// 	private ImagePlus m_pixelCathegoryMap;
// 	int m_modifyPixelCathegoryMap_diameter;
// 	int m_modifyPixelCathegoryMap_threshold;
// 	int m_modifyPixelCathegoryMap_value;
// 	Boolean m_modifyPixelCathegoryMap_protect;
}
