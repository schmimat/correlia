/*
User dialogue which allows for assigning features in one microscopy image (base)
to those defined in the overlay image.
It calculates the transformation matrix for the transformation of the overlay
coordinate system to the coordinates of the base.

The class was developped at the Helmholtz Centre for Environmental Research Leipzig
(UFZ) within the framework of ProVIS - Centre for Chemical Microscopy.

author : Matthias Schmidt
version: June 05 2015
*/

package de.ufz.correlia;

import java.util.ArrayList;

import java.awt.*;
import java.awt.geom.*;

import ij.*;
import ij.gui.GenericDialog;


import javax.swing.JLabel;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;

public class dlgCorrelateFeatures {
	dlgCorrelateFeatures(ArrayList<Point.Double> baseFeatures, ArrayList<Point.Double> ovlFeatures) {
		m_baseFeatures = baseFeatures;
		m_ovlFeatures = ovlFeatures;
		m_assignment = new ArrayList<>();

		m_wasCanceled = false;

		choices = new ArrayList<>();

		GenericDialog gd = setup_dialogue();
		gd.showDialog();

		if (gd.wasCanceled()) {
			m_wasCanceled = true;
			return;
		} else {
			if (!get_user_assignment()) {
				m_wasCanceled = true;
				return;
			} // did the user assign more than three points?

			m_adjustmentFactor = calculate_adjustmentFactor();
			m_rotAngle = calculate_rotAngle();
			m_x0 = calculate_x0();

		}
	}

	private GenericDialog setup_dialogue() {
		GenericDialog gd = new GenericDialog("Correlate features");
		Panel pan = new Panel();
		pan.setLayout(new BoxLayout(pan, BoxLayout.Y_AXIS));

		// string of choices in combo box
		String[] ch = new String[m_ovlFeatures.size() + 1];
		ch[0] = "not assigned";
		for (int i = 0; i < m_ovlFeatures.size(); i++) {
			ch[i + 1] = "" + i;
		}

		Panel p = new Panel();
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.add(new JLabel("base"));
		p.add(Box.createHorizontalStrut(20));
		p.add(new JLabel("overlay"));
		pan.add(p);
		pan.add(Box.createVerticalStrut(20));

		for (int i = 0; i < m_baseFeatures.size(); i++) {
			p = new Panel();
			p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
			p.add(new JLabel("  " + i + "  "));
			p.add(Box.createHorizontalStrut(20));
			JComboBox cb = new JComboBox(ch);
			p.add(cb);

			pan.add(p);
			pan.add(Box.createVerticalStrut(5));
			choices.add(cb);
		}

		gd.addPanel(pan);
		return gd;
	}

	public Boolean wasCanceled() {
		return m_wasCanceled;
	}

	public imageAlignment get_imageAlignment(double rotCenterX, double rotCenterY) {
		return new imageAlignment(m_x0.getX(), m_x0.getY(), m_rotAngle, rotCenterX, rotCenterY);
//		return new imageAlignment(m_x0.getX(), m_x0.getY(), m_rotAngle);
	}

	public double get_adjustmentFactor() {
		return m_adjustmentFactor;
	}

	protected Boolean get_user_assignment() {
		int pointsAssigned = 0;
		int assignment;

		for (int i = 0; i < choices.size(); i++) {
			assignment = (choices.get(i)).getSelectedIndex() - 1;
			m_assignment.add(assignment);
			if (assignment >= 0)    // this feature is assigned to a feature in the base
			{
				pointsAssigned++;
			}
		}

		// check if double assignments were made
		int idxI, idxJ;
		Boolean doubleAssignment = false;

		for (int i = 0; i < m_assignment.size(); i++) {
			idxI = m_assignment.get(i);

			for (int j = 0; j < m_assignment.size(); j++) {
				idxJ = m_assignment.get(j);

				if (idxI != -1 && i != j && idxI == idxJ) // double assignment
				{
					doubleAssignment = true;
				}
			}
		}

		if (doubleAssignment) {
			IJ.showMessage("Error: A feature in the overlay was assigned to two or more features in the base.");
			return false;
		}

		if (pointsAssigned < 3) {
			IJ.showMessage("Cannot correlate images: less than three points were assigned.");
			return false;
		}
		return true;
	}


	protected Boolean calculate_transformation() {


		return true;
	}

	protected double calculate_adjustmentFactor()    // calculate distances between all features in base and overlay, sum their fractions up and calculate average
	{
		double sum = 0.0;
		int count = 0;
		double distBase = 0.0;
		double distOvl = 0.0;

		for (int i = 0; i < m_baseFeatures.size(); i++) {
			if (m_assignment.get(i) >= 0)    // this feature was assigned
			{
				Point2D.Double pBase = m_baseFeatures.get(i);

				for (int j = i + 1; j < m_baseFeatures.size(); j++) {
					if (m_assignment.get(j) >= 0)    // this feature was assigned
					{
						distBase = pBase.distance(m_baseFeatures.get(j));
						Point2D.Double pOvl = m_ovlFeatures.get(m_assignment.get(i));
						distOvl = pOvl.distance(m_ovlFeatures.get(m_assignment.get(j)));
						sum += distBase / distOvl;
						count++;
					}
				}
			}
		}

		return sum / count;
	}

	protected double calculate_rotAngle()    // calculate the angles of feature-connecting lines with respect to the x-axis in base and overlay and determine the rotation angle
	{
		// reference point is the first assigned feature in base
		Point2D.Double featRefBase = new Point2D.Double();
		int featRefBaseIdx = 0;

		Point2D.Double featRefOvl = new Point2D.Double();
		int featRefOvlIdx = 0;

		double angDiff = 0.0;
		int count = 0;

		for (int i = 0; i < m_baseFeatures.size(); i++) {
			if (m_assignment.get(i) >= 0) {   // this feature was assigned
				featRefBase = m_baseFeatures.get(i);    // feature
				featRefBaseIdx = i;
				featRefOvlIdx = m_assignment.get(i);
				featRefOvl = m_ovlFeatures.get(featRefOvlIdx);
				break;
			}
		}

		// now reference point is defined

		// calculate angles of the other features from this reference with respect to the x-axis
		for (int i = featRefBaseIdx + 1; i < m_baseFeatures.size(); i++) {
			if (m_assignment.get(i) >= 0)    // this feature was assigned
			{
				Point2D.Double featBase = m_baseFeatures.get(i);
				Point2D.Double featOvl = m_ovlFeatures.get(m_assignment.get(i));

				double ad = angleX(featRefBase, featBase) - angleX(featRefOvl, featOvl);

				if (ad > 180) {
					ad -= 360;
				}
				if (ad < -180) {
					ad += 360;
				}

				angDiff += ad;
				count++;
			}
		}
		return angDiff /= count;
	}

	protected double angleX(Point2D.Double p1, Point2D.Double p2)    // angle under which the line p1p2 intersects with x-axis, 0<ang<360
	{
		double alpha;

		double d = p1.distance(p2);    // distance
		double lx = p2.getX() - p1.getX();    // projection on x-axis
		double ly = p2.getY() - p1.getY();    // projection on y-axis

		alpha = Math.asin(ly / d);

		// get quadrant right
		if (lx < 0 && ly > 0) {
			alpha = Math.PI - alpha;
		}
		if (lx < 0 && ly < 0) {
			alpha = Math.PI - alpha;
		}
		if (lx > 0 && ly < 0) {
			alpha = 2 * Math.PI + alpha;
		}

		return 180 * alpha / Math.PI;    // return angle in degrees
	}

	protected Point2D.Double calculate_x0() {
		Point2D.Double p = new Point2D.Double();

		Point2D.Double featBase = new Point2D.Double();
		Point2D.Double featOvl = new Point2D.Double();

		// find first feature
		for (int i = 0; i < m_baseFeatures.size(); i++) {
			if (m_assignment.get(i) >= 0)    // this feature was assigned
			{
				featBase = m_baseFeatures.get(i);    // feature
				featOvl = m_ovlFeatures.get(m_assignment.get(i));
				break;
			}
		}

		double alpha = m_rotAngle / 180 * Math.PI;
		double beta = Math.atan(featOvl.getY() / featOvl.getX());
		double dist = featOvl.distance(0.0, 0.0);

		double dx = Math.cos(alpha + beta) * dist * m_adjustmentFactor;
		double dy = Math.sin(alpha + beta) * dist * m_adjustmentFactor;

		p.setLocation(featBase.getX() - dx, featBase.getY() - dy);
		return p;
	}


	static public String classID() {
		return "dlgCorrelateFeatures";
	}

	static public String author() {
		return "Matthias Schmidt";
	}

	static public String version() {
		return "July 1 2015";
	}


	// flags
	Boolean m_wasCanceled;

	// data
	ArrayList<Point.Double> m_baseFeatures;
	ArrayList<Point.Double> m_ovlFeatures;
	ArrayList<Integer> m_assignment;

	// transformation
	double m_adjustmentFactor;
	double m_rotAngle;    // degrees
	Point2D.Double m_x0;    // offset point

	// widgets
	ArrayList<JComboBox> choices;


}