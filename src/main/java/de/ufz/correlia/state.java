package de.ufz.correlia;
import java.util.ArrayList;
import java.util.Arrays;

public class state {
	private ArrayList<String> states;
	private ArrayList<Integer> is;
	private int cur;
	state(ArrayList<String> states) {
		this.states = states;
		this.cur = 0;
	}
	state(String[] states) {
		this.states = new ArrayList<>(Arrays.asList(states));
		this.cur = 0;
	}

	public void add(String s) {
		states.add(s);
	}

	public void next() {
		cur = (cur + 1) % states.size();
	}

	public void prev() {
		cur = (cur > 0)? (cur - 1) : states.size() - 1;
	}

	public void set(String s) {
		cur = states.indexOf(s);
	}

	public String get() {
		return states.get(cur);
	}

	public boolean is(String s) {
		return get().equals(s);
	}
}