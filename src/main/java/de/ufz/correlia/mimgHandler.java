/*
mimgHandler handles mimg/tiff images in ImageJ which is the unified ProVIS image format 
developed by Matthias Schmidt
*/
package de.ufz.correlia;

import java.io.*;
import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.*;

public class mimgHandler {
	mimgHandler() {

	}

	public microscopyImage open(String mimgPath) {
		Path p = Paths.get(mimgPath);
		return open(p.getParent().toString() + File.separator, p.getFileName().toString());
	}

	public microscopyImage open(String mimgFileDirectory, String mimgFileName) {
		String defStr = "";
		microscopyImage img = null;

		// read file content as definition of the microscopy image
		try {
			FileReader fileReader = new FileReader(mimgFileDirectory + mimgFileName);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				defStr += line;
				defStr += "\n";
			}
		} catch (IOException e) {
			return img;
		}

		// set up parser
		parseInputAndInstanciateClass parser = new parseInputAndInstanciateClass();
		parser.parseString(defStr, mimgFileDirectory);    // parse string and set the path for the image

//		if (parser.getClassName().equals("microscopyImage")) {
//			img = new microscopyImage(parser.getSubstring());
//		} else if (parser.getClassName().equals("microscopyImageWarped")) {
//			img = new microscopyImageWarped(parser.getSubstring());
//		}

		return img;
	}

	public Boolean save(microscopyImage mi) {
		return true;
	}


	static public String classID() {
		return "mimgHandler";
	}

	static public String author() {
		return "Matthias Schmidt";
	}

	static public String version() {
		return "November 21 2016";
	}
}