/*
User dialogue which allows for assigning features in one microscopy image (base)
to those defined in the overlay image.
It calculates the transformation matrix for the transformation of the overlay
coordinate system to the coordinates of the base.

 * Author: Matthias Schmidt and Florens Rohde
 * Part of:
 * Correlia plugin for ImageJ and FIJI
*/

package de.ufz.correlia;
import ij.IJ;
import ij.ImagePlus;
import ij.gui.*;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.ArrayList;

public class dlgMatchFeatures extends JDialog {
	private static final Integer DLG_INNER_PADDING = 20;
	private static final Integer DLG_OUTER_PADDING = 20;
	private static final int MATCHES_PADDING = 5;
	private static final int MATCHES_PER_ROW = 3;
	private static final Dimension DIM_MATCH = new Dimension(80, 30);
	public static final Color SRC_FP = Color.RED;
	public static final Color REF_FP = Color.GREEN;
	public static final Color MATCHLINE = Color.CYAN;

	// flags
	boolean wasCancelled;
	Component parent = null;

	// data
	private boolean useAffineModel;
	private microscopyImage src;
	private microscopyImage srcWork;
	private microscopyImage refWork;
	private imageAlignment srcIA;
	private imageAlignment refIA;
	private ImagePlus out;
	private ArrayList<Point.Double> srcFeatures;
	private ArrayList<Point.Double> refFeatures;
	private ArrayList<Integer[]> matches;
	double[] offsetRealLength = new double[2];

	// widgets
	JPanel pan = new JPanel();
	ArrayList<JComboBox<String>> cbSrcFp = new ArrayList<>();
	ArrayList<JComboBox<String>> cbRefFp = new ArrayList<>();
	JComboBox<String> cbSrcAdd;
	JComboBox<String> cbRefAdd;

	dlgMatchFeatures(microscopyImage src, microscopyImage ref, imageAlignment srcImageAlignment, imageAlignment refImageAlignment,
					 boolean affine, ImagePlus outputScreen, ArrayList<Integer[]> preMatches) {
		srcFeatures = src.get_feature_points();
		refFeatures = ref.get_feature_points();
		matches = new ArrayList<>();

		if (preMatches != null) {
			for (Integer[] m : preMatches) {
				if (m[0] < 0 | m[1] < 0) break;
				try {
					srcFeatures.get(m[0]);
					refFeatures.get(m[1]);
				} catch (IndexOutOfBoundsException e) {
					continue;
				}
				matches.add(m);
			}
		}

		wasCancelled = false;
		useAffineModel = affine;

		this.src = src;
		refIA = refImageAlignment;
		srcIA = srcImageAlignment;
		if (MiscHelper.unmodifiedImageAlignment(srcIA)) {
			srcIA = new imageAlignment(
					refIA.m_x0 + (ref.imgWidth()-src.imgWidth()) / 2,
					refIA.m_y0 + (ref.imgHeight()-src.imgHeight()) / 2,
					refIA.m_rotAngle,
					src.imgWidth() / 2, src.imgHeight() / 2
			);
		}

		microscopyImage[] mi = {src, ref};
		imageAlignment[] ia = {srcIA, refIA};
		double[] jS = MiscHelper.getJointSize(mi, ia);
		double[] pxSize = new double[2];
		pxSize[0] = Math.max(src.pixelWidth(), ref.pixelWidth());
		pxSize[1] = Math.max(src.pixelHeight(), ref.pixelHeight());
		srcWork = MiscHelper.convertToJointCoords(src, srcIA, jS, pxSize[0], pxSize[1]);
		refWork = MiscHelper.convertToJointCoords(ref, refIA, jS, pxSize[0], pxSize[1]);

		out = outputScreen;
		ColorProcessor fp = new ColorProcessor(srcWork.getWidth(), srcWork.getHeight());
		ImageProcessor srcIP = (srcWork.getStack().getProcessor(srcWork.getSlice())).convertToByteProcessor();
		ImageProcessor refIP = (refWork.getStack().getProcessor(refWork.getSlice())).convertToByteProcessor();
		byte[] emptyPixels = new byte[srcIP.getWidth()*srcIP.getHeight()];
		for (int i = 0; i < emptyPixels.length; i++) emptyPixels[i] = 0;
		fp.setRGB((byte[]) srcIP.getPixels(), (byte[]) refIP.getPixels(), emptyPixels);
		out.setStack(new ImagePlus("", fp).getStack());
		updateOverlay();
		setup_dialog();
	}

	private boolean getGuiMatches() {
		for (int i = 0; i < cbSrcFp.size(); i++) {
			try {
				Integer[] m = new Integer[2];
				m[0] = Integer.parseInt((String) cbSrcFp.get(i).getSelectedItem());
				m[1] = Integer.parseInt((String) cbRefFp.get(i).getSelectedItem());
				if (i < matches.size()) {
					matches.set(i, m);
				} else {
					matches.add(m);
				}
			} catch (NumberFormatException e) {
				continue;
			}
		}
		return true;
	}

	public static AffineTransform calcAffineTransformFromThreeMatches(double[][] srcFixP, double[][] refFixP) {
		double m00, m01, m02, m10, m11, m12;

		double d = refFixP[0][0] * (refFixP[2][1] - refFixP[1][1])
				+ refFixP[1][0] * (refFixP[0][1] - refFixP[2][1])
				+ refFixP[2][0] * (refFixP[1][1] - refFixP[0][1]);
		m00 = (1 / d) * (refFixP[0][1] * (srcFixP[1][0] - srcFixP[2][0])
				+ refFixP[1][1] * (srcFixP[2][0] - srcFixP[0][0])
				+ refFixP[2][1] * (srcFixP[0][0] - srcFixP[1][0]));
		m01 = (1 / d) * (refFixP[0][0] * (srcFixP[2][0] - srcFixP[1][0])
				+ refFixP[1][0] * (srcFixP[0][0] - srcFixP[2][0])
				+ refFixP[2][0] * (srcFixP[1][0] - srcFixP[0][0]));
		m10 = (1 / d) * (refFixP[0][1] * (srcFixP[1][1] - srcFixP[2][1])
				+ refFixP[1][1] * (srcFixP[2][1] - srcFixP[0][1])
				+ refFixP[2][1] * (srcFixP[0][1] - srcFixP[1][1]));
		m11 = (1 / d) * (refFixP[0][0] * (srcFixP[2][1] - srcFixP[1][1])
				+ refFixP[1][0] * (srcFixP[0][1] - srcFixP[2][1])
				+ refFixP[2][0] * (srcFixP[1][1] - srcFixP[0][1]));
		m02 = (1 / d) * (refFixP[0][0] * (refFixP[2][1] * srcFixP[1][0] - refFixP[1][1] * srcFixP[2][0])
				+ refFixP[1][0] * (refFixP[0][1] * srcFixP[2][0] - refFixP[2][1] * srcFixP[0][0])
				+ refFixP[2][0] * (refFixP[1][1] * srcFixP[0][0] - refFixP[0][1] * srcFixP[1][0]));
		m12 = (1 / d) * (refFixP[0][0] * (refFixP[2][1] * srcFixP[1][1] - refFixP[1][1] * srcFixP[2][1])
				+ refFixP[1][0] * (refFixP[0][1] * srcFixP[2][1] - refFixP[2][1] * srcFixP[0][1])
				+ refFixP[2][0] * (refFixP[1][1] * srcFixP[0][1] - refFixP[0][1] * srcFixP[1][1]));

		double[] resultParameters = new double[6];
		resultParameters[0] = m00;
		resultParameters[1] = m10;
		resultParameters[2] = m01;
		resultParameters[3] = m11;
		resultParameters[4] = m02;
		resultParameters[5] = m12;

		AffineTransform affine = new AffineTransform(resultParameters);
		return affine;
	}

	public static boolean guessInitialMatches(
			ArrayList<Point.Double> srcFeatures,
			ArrayList<Point.Double> refFeatures,
			ArrayList<Integer[]> matches) {

		if (srcFeatures.size() < 3 || refFeatures.size() < 3) {
			return false;
		}

		ArrayList<Integer> matchedSrcFp = new ArrayList<>();
		ArrayList<Integer> matchedRefFp = new ArrayList<>();
		for (Integer[] m : matches) {
			matchedSrcFp.add(m[0]);
			matchedRefFp.add(m[1]);
		}

		while (matches.size() < 3) {
			double[] dist = new double[srcFeatures.size() - matches.size()];
			int[] srcFp = new int[dist.length];
			int[] nextRefFp = new int[dist.length];
			int c = 0;

			for (int s = 0; s < srcFeatures.size(); s++) {
				if (matchedSrcFp.contains(s)) continue;
				int nextRefNr = -1;
				double nextRefDist = Double.MAX_VALUE;
				for (int r = 0; r < refFeatures.size(); r++) {
					if (matchedRefFp.contains(r)) continue;
					double d = refFeatures.get(r).distance(srcFeatures.get(s));
					if (d < nextRefDist) {
						nextRefNr = r;
						nextRefDist = d;
					}
				}
				srcFp[c] = s;
				nextRefFp[c] = nextRefNr;
				dist[c] = nextRefDist;
//				IJ.log(c+": "+ s+"->"+nextRefNr+ " : "+nextRefDist);
				c++;
			}

			int nearest = MiscHelper.findMinPos(dist);
//			IJ.log("nearest: "+nearest);
			Integer[] m = new Integer[2];
			m[0] = srcFp[nearest];
			m[1] = nextRefFp[nearest];
			matches.add(m);
			matchedSrcFp.add(m[0]);
			matchedRefFp.add(m[1]);
		}

		return true;
	}

	public static boolean guessMatches(ArrayList<Point.Double> srcFeatures,
									   ArrayList<Point.Double> refFeatures,
									   ArrayList<Point.Double> refFeaturesInSrcCoords,
									   ArrayList<Integer[]> matches,
									   boolean useAffineModel) {

		ArrayList<Integer> matchedSrcFp = new ArrayList<>();
		ArrayList<Integer> matchedRefFp = new ArrayList<>();

		if (refFeaturesInSrcCoords == null) {
			refFeaturesInSrcCoords = new ArrayList<>();
		}

		for (Integer[] m : matches) {
			matchedSrcFp.add(m[0]);
			matchedRefFp.add(m[1]);
		}

		if (useAffineModel) {
			double[][] srcFixP = new double[3][2];
			double[][] refFixP = new double[3][2];

			for (int i = 0; i < srcFixP.length; i++) {
				srcFixP[i][0] = srcFeatures.get(matches.get(i)[0]).getX();
				srcFixP[i][1] = srcFeatures.get(matches.get(i)[0]).getY();
				refFixP[i][0] = refFeatures.get(matches.get(i)[1]).getX();
				refFixP[i][1] = refFeatures.get(matches.get(i)[1]).getY();
			}

			AffineTransform affine = calcAffineTransformFromThreeMatches(srcFixP, refFixP);

			refFeaturesInSrcCoords.clear();
			for (int r = 0; r < refFeatures.size(); r++) {
				Point2D pRefInSrcCoords = affine.transform(refFeatures.get(r), null);
				refFeaturesInSrcCoords.add((Point2D.Double)pRefInSrcCoords);
			}
		}

		ArrayList<Double> distOfMatches = new ArrayList<>();
		while (matches.size() < Math.min(srcFeatures.size(), refFeatures.size())) {
			double[] dist = new double[srcFeatures.size() - matches.size()];
			int[] srcFp = new int[dist.length];
			int[] nextRefFp = new int[dist.length];
			int c = 0;

			for (int s = 0; s < srcFeatures.size(); s++) {
				if (matchedSrcFp.contains(s)) continue;
				int nextRefNr = -1;
				double nextRefDist = Double.MAX_VALUE;
				for (int r = 0; r < refFeatures.size(); r++) {
					if (matchedRefFp.contains(r)) continue;
					double d = 0;
					if (useAffineModel) {
						d = srcFeatures.get(s).distance(refFeaturesInSrcCoords.get(r));
					}
					if (d < nextRefDist) {
						nextRefNr = r;
						nextRefDist = d;
					}
				}
				srcFp[c] = s;
				nextRefFp[c] = nextRefNr;
				dist[c] = nextRefDist;
				c++;
			}

			int nearest = MiscHelper.findMinPos(dist);
			Integer[] m = new Integer[2];
			m[0] = srcFp[nearest];
			m[1] = nextRefFp[nearest];
			matches.add(m);
			matchedSrcFp.add(m[0]);
			matchedRefFp.add(m[1]);

			distOfMatches.add(dist[nearest]);

			if (distOfMatches.size() > 2) {
				double[] distMatches = new double[distOfMatches.size()];
				for (int i = 0; i < distMatches.length; i++) {
					distMatches[i] = distOfMatches.get(i);
				}
				int[] order = new int[distMatches.length];
				double gini = MiscHelper.calculateGiniCoefficient(distMatches, order);
				if (gini > 0.3) {
					matches.remove(matches.size()-1);
					matchedSrcFp.remove(matchedSrcFp.size()-1);
					matchedRefFp.remove(matchedRefFp.size()-1);
					break;
				}
			}
		}

		return true;
	}

	public void updateOverlay() {
		out.setOverlay(getMatchVisualisation(srcWork, refWork.get_feature_points(), arrListInt2int(matches)));
	}

	public static Overlay getMatchVisualisation(microscopyImage src, ArrayList<Point.Double> refFeaturesInSrcCoords, int[][] matches) {
		ArrayList<ArrayList<Point.Double>> features = new ArrayList<>(new ArrayList<>());
		features.add(src.get_feature_points());
		features.add(refFeaturesInSrcCoords);

		ArrayList<Color> colors = new ArrayList<>();
		colors.add(SRC_FP);
		colors.add(REF_FP);
		colors.add(MATCHLINE);

//		src.setTitle("Found matches");

		Overlay ovl = new Overlay();

		Font fo = new Font(src.getProcessor().getFont().getName(),// name
				src.getProcessor().getFont().getStyle(),	// style
				src.getWidth()/64
		);	// size

		for (int i = 0; i < features.size(); i++) {
			for (int fp = 0; fp < features.get(i).size(); fp++) {	// run through all points
				Point.Double p = features.get(i).get(fp);

				int ix = (int) (p.x/src.getCalibration().pixelWidth);
				int iy = (int) (p.y/src.getCalibration().pixelHeight);

				Roi circle = new OvalRoi(ix-src.getWidth()/200,
						iy-src.getWidth()/200,
						src.getWidth()/100,
						src.getWidth()/100
				);
				Roi circleCentre = new OvalRoi(ix, iy, 0, 0);
				circle.setStrokeColor(colors.get(i));
				circle.setStrokeWidth(1);
				circleCentre.setStrokeColor(colors.get(i));
				circleCentre.setStrokeWidth(2);
				TextRoi txt = new TextRoi(ix+src.getWidth()/70,
						iy-src.getWidth()/70,
						""+fp,
						fo
				);
				txt.setStrokeColor(colors.get(i));
//				Color bg = new Color(0, 0, 0, 128);
//				txt.setFillColor(bg);
				ovl.add(circle);
				ovl.add(circleCentre);
				ovl.add(txt);
			}
		}

		if (matches != null) {
			for (int m = 0; m < matches.length; m++) {
				if (matches[m][0] < 0 | matches[m][1] < 0) continue;
				Point.Double pSrc = features.get(0).get(matches[m][0]);
				Point.Double pRef = features.get(1).get(matches[m][1]);

				int ixSrc = (int) (pSrc.x / src.getCalibration().pixelWidth);
				int iySrc = (int) (pSrc.y / src.getCalibration().pixelHeight);
				int ixRef = (int) (pRef.x / src.getCalibration().pixelWidth);
				int iyRef = (int) (pRef.y / src.getCalibration().pixelHeight);

				Roi line = new Line(ixSrc, iySrc, ixRef, iyRef);
				line.setStrokeColor(colors.get(2));
				line.setStrokeWidth(2);
				ovl.add(line);
			}
		}
//		src.setOverlay(ovl);
		return ovl;
	}

	private boolean setup_dialog() {
		this.setTitle("Match features");
		this.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		this.setMinimumSize(new Dimension(350, 500));

		generateDialogContent();

		this.add(pan);
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(parent);
		this.setVisible(true);
		return true;
	}

	private void generateDialogContent() {
		// string of choices in combo box
		String[] chSrc = new String[srcFeatures.size() + 1];
		chSrc[0] = "--";
		for (int i = 0; i < srcFeatures.size(); i++) {
			chSrc[i + 1] = "" + i;
		}
		String[] chRef = new String[refFeatures.size() + 1];
		chRef[0] = "--";
		for (int i = 0; i < refFeatures.size(); i++) {
			chRef[i + 1] = "" + i;
		}

		JButton btnGuess = new JButton("Guess");
		btnGuess.setBorder(BorderFactory.createEmptyBorder(0, DLG_INNER_PADDING, 0, 0));
		btnGuess.addActionListener(e -> {
			getGuiMatches();
			if (matches.size() < 3) {
				if (!guessInitialMatches(srcWork.get_feature_points(), refWork.get_feature_points(), matches)) {
					IJ.showMessage("Cannot guess from less than three matches");
					return;
				}
			} else {
				ArrayList<Point.Double> refFeaturesInSrcCoords = new ArrayList<>();
				guessMatches(srcFeatures, refFeatures, refFeaturesInSrcCoords, matches, true);
			}
			generateDialogContent();
			updateOverlay();
		});
		JButton btnAccept = new JButton("Accept");
		btnAccept.setBorder(BorderFactory.createEmptyBorder(0, DLG_INNER_PADDING, 0, 0));
		btnAccept.addActionListener(e -> {
			getGuiMatches();
			userConfirm(true);
		});
		JButton btnCancel = new JButton("Cancel");
		btnCancel.setBorder(BorderFactory.createEmptyBorder(0, DLG_INNER_PADDING, 0, 0));
		btnCancel.addActionListener(e -> {
			userConfirm(false);
		});

		cbSrcFp.clear();
		cbRefFp.clear();
		cbSrcFp.add(new JComboBox<>(chSrc));
		cbSrcFp.add(new JComboBox<>(chSrc));
		cbSrcFp.add(new JComboBox<>(chSrc));
		cbRefFp.add(new JComboBox<>(chRef));
		cbRefFp.add(new JComboBox<>(chRef));
		cbRefFp.add(new JComboBox<>(chRef));

		for (int i = 0; i < cbSrcFp.size(); i++) {
			int src;
			int ref;
			if (matches.size() <= i) {
				src = -1;
				ref = -1;
			} else if (matches.get(i)[0] >= srcFeatures.size() | matches.get(i)[1] >= refFeatures.size()) {
				src = -1;
				ref = -1;
			} else {
				src = matches.get(i)[0];
				ref = matches.get(i)[1];
			}
			cbSrcFp.get(i).setSelectedItem(chSrc[src + 1]);
			cbRefFp.get(i).setSelectedItem(chRef[ref + 1]);
		}

		cbSrcAdd = new JComboBox<>(chSrc);
		cbRefAdd = new JComboBox<>(chRef);
		JButton btnAddFp = new JButton("Add");
		btnAddFp.addActionListener(a -> {
			Integer[] m = new Integer[2];
			try {
				m[0] = Integer.parseInt((String) cbSrcAdd.getSelectedItem());
				m[1] = Integer.parseInt((String) cbRefAdd.getSelectedItem());
			} catch (NumberFormatException e) {
				return;
			}
			matches.add(m);
			generateDialogContent();
			updateOverlay();
		});

		JPanel pSelect = new JPanel();
		GridLayout grdlay = new GridLayout(0, 2, 10, 10);    // parameters: columns, rows, horizontal gap, vertical gap
		pSelect.setLayout(grdlay);

		JLabel lblSrc = new JLabel("Source");
		lblSrc.setForeground(SRC_FP);
		JLabel lblRef = new JLabel("Reference");
		lblRef.setForeground(REF_FP);
		pSelect.add(lblSrc);
		pSelect.add(lblRef);
		for (int i = 0; i < cbSrcFp.size(); i++) {
			pSelect.add(cbSrcFp.get(i));
			pSelect.add(cbRefFp.get(i));
		}

		JPanel pButtons = new JPanel();
		GridLayout grdlayBtn = new GridLayout(0, 1, 10, 10);    // parameters: columns, rows, horizontal gap, vertical gap
		pButtons.setLayout(grdlayBtn);

		pButtons.add(btnGuess);
		pButtons.add(btnAccept);
		pButtons.add(btnCancel);

		Dimension dim = new Dimension(
				MATCHES_PER_ROW*(DIM_MATCH.width+2*MATCHES_PADDING),
				(int)Math.ceil((matches.size()-3)/MATCHES_PER_ROW)*(DIM_MATCH.height+MATCHES_PADDING));
		JPanel lstMatches = new JPanel();
		FlowLayout fl = new FlowLayout(FlowLayout.LEFT, 5, 5);
		lstMatches.setLayout(fl);
		lstMatches.setPreferredSize(dim);
		lstMatches.setMinimumSize(dim);
		lstMatches.setMaximumSize(dim);

		for (int i = 3; i < matches.size(); i++) {
			lstMatches.add(new match(matches, i));
		}

		JScrollPane spMatches = new JScrollPane(lstMatches);
		dim = new Dimension(250, 100);
		spMatches.setMinimumSize(dim);
		spMatches.setMinimumSize(dim);
		spMatches.setMaximumSize(dim);
		spMatches.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		JPanel pAddMatch = new JPanel();
		pAddMatch.setLayout(new GridLayout(1,3));
		pAddMatch.add(cbSrcAdd);
		pAddMatch.add(cbRefAdd);
		pAddMatch.add(btnAddFp);

		JPanel pMatches = new JPanel();
		pMatches.setLayout(new BorderLayout());
		pMatches.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
				"Additional matches (click to delete)"));
		pMatches.add(pAddMatch, BorderLayout.PAGE_START);
		pMatches.add(spMatches, BorderLayout.CENTER);

		JPanel panTop = new JPanel();
		panTop.setLayout(new BoxLayout(panTop, BoxLayout.LINE_AXIS));
		panTop.setBorder(BorderFactory.createEmptyBorder(0, 0, DLG_INNER_PADDING, 0));
		panTop.add(pSelect);
		panTop.add(Box.createRigidArea(new Dimension(DLG_INNER_PADDING, 0)));
		panTop.add(pButtons);


		pan.removeAll();
		pan.setLayout(new GridLayout(0, 1));
		pan.setBorder(BorderFactory.createEmptyBorder(DLG_OUTER_PADDING, DLG_OUTER_PADDING, DLG_OUTER_PADDING, DLG_OUTER_PADDING));
		pan.add(new JLabel("<html>You can use the \"Guess\" button to estimate landmark correspondencies.<br />" +
				"If you did not match at least three landmarks manually, these will be guessed first.<br />" +
				"Furster guesses (click again) will be based on a model " +
				"defined by these three correspondencies.<br /></html>", JLabel.LEADING));
		pan.add(panTop);
		pan.add(pMatches);
		pan.revalidate();
		pan.repaint();

		SwingUtilities.updateComponentTreeUI(this);
	}

	public static int[][] arrListInt2int(ArrayList<Integer[]> arrList) {
		if (arrList == null) return null;
		if (arrList.size() < 1) return null;
		int[][] prim = new int[arrList.size()][arrList.get(0).length];
		for (int i = 0; i < arrList.size(); i++) {
			for (int j = 0; j < arrList.get(0).length; j++) {
				prim[i][j] = arrList.get(i)[j];
			}
		}
		return prim;
	}

	public ArrayList<Integer[]> getMatches() {
		return matches;
	}

	protected static double angleX(Point2D.Double p1, Point2D.Double p2) {   // angle under which the line p1p2 intersects with x-axis, 0<ang<360
		double alpha;

		double d = p1.distance(p2);    // distance
		double lx = p2.getX() - p1.getX();    // projection on x-axis
		double ly = p2.getY() - p1.getY();    // projection on y-axis

		alpha = Math.asin(ly / d);

		// get quadrant right
		if (lx < 0 && ly > 0) {
			alpha = Math.PI - alpha;
		}
		if (lx < 0 && ly < 0) {
			alpha = Math.PI - alpha;
		}
		if (lx > 0 && ly < 0) {
			alpha = 2 * Math.PI + alpha;
		}

		return 180 * alpha / Math.PI;    // return angle in degrees
	}

	protected static imageAlignment calcImageAlignment(ArrayList<Point.Double> srcFP, ArrayList<Point.Double> refFP,
													   ArrayList<Integer[]> matches,
													   double rotCenterX, double rotCenterY) {
		// Calculate center
		double xSrc = 0;
		double ySrc = 0;
		double xRef = 0;
		double yRef = 0;
		for (int i = 0; i < matches.size(); i++) {
			xSrc += srcFP.get(matches.get(i)[0]).getX();
			ySrc += srcFP.get(matches.get(i)[0]).getY();
			xRef += refFP.get(matches.get(i)[1]).getX();
			yRef += refFP.get(matches.get(i)[1]).getY();
		}
		Point2D.Double srcCenter = new Point2D.Double(xSrc/matches.size(),ySrc/matches.size());
		Point2D.Double refCenter = new Point2D.Double(xRef/matches.size(),yRef/matches.size());


		// Scale
		double sumSrc = 0;
		double sumRef = 0;
		for (int i = 0; i < matches.size(); i++) {
			Point2D.Double pSrc = srcFP.get(matches.get(i)[0]);
			Point2D.Double pRef = refFP.get(matches.get(i)[1]);
			double distSrc = pSrc.distance(srcCenter);
			double distRef = pRef.distance(refCenter);
			sumSrc += distSrc;
			sumRef += distRef;
		}
		double scale = sumRef / sumSrc;


		// Rotation angle
		double sumAngDiff = 0;
		for (int i = 0; i < matches.size(); i++) {
			Point2D.Double pSrc = srcFP.get(matches.get(i)[0]);
			Point2D.Double pRef = refFP.get(matches.get(i)[1]);

			double ad = angleX(pRef, refCenter) - angleX(pSrc, srcCenter);
			if (ad > 180) {
				ad -= 360;
			}
			if (ad < -180) {
				ad += 360;
			}
			sumAngDiff += ad;
		}
		double rotAngle = sumAngDiff / matches.size();

		imageAlignment ia = new imageAlignment(0, 0,
				rotAngle, 0, 0,
				scale);

		// Translation
		double txSum = 0;
		double tySum = 0;
		for (int i = 0; i < matches.size(); i++) {
			Point2D.Double pSrc = srcFP.get(matches.get(i)[0]);
			Point2D.Double pRef = refFP.get(matches.get(i)[1]);

			txSum += pRef.getX() - ia.xprime(pSrc.getX(), pSrc.getY());
			tySum += pRef.getY() - ia.yprime(pSrc.getX(), pSrc.getY());
		}
		ia.set_x0y0(txSum / matches.size(), tySum / matches.size());

		return ia;
	}

	protected double calcFPError(ArrayList<Point.Double> srcFP, ArrayList<Point.Double> refFP, ArrayList<Integer[]> matches, imageAlignment ia) {
		double errorSum = 0;
		for (int i = 0; i < matches.size(); i++) {
			Point2D.Double pSrc = srcFP.get(matches.get(i)[0]);
			Point2D.Double pRef = refFP.get(matches.get(i)[1]);
			Point2D.Double pSrcInRef = new Point2D.Double(ia.x(pSrc.getX(), pSrc.getY()), ia.y(pSrc.getX(), pSrc.getY()));
			double dist = pRef.distance(pSrcInRef);
			errorSum += dist;
		}
		return errorSum;
	}

	public imageAlignment getImageAlignment() {
		if (useAffineModel) {
			// Transpose reference features into base coordinates
			ArrayList<Point2D.Double> refFeaturesBased = new ArrayList<>();
			for (Point2D.Double pRef : refFeatures) {
				double xRef = refIA.x(pRef.getX(), pRef.getY());
				double yRef = refIA.y(pRef.getX(), pRef.getY());
				refFeaturesBased.add(new Point2D.Double(xRef, yRef));
			}


			// Calculate center
			double xSrc = 0;
			double ySrc = 0;
			double xRef = 0;
			double yRef = 0;
			for (int i = 0; i < matches.size(); i++) {
				xSrc += srcFeatures.get(matches.get(i)[0]).getX();
				ySrc += srcFeatures.get(matches.get(i)[0]).getY();
				xRef += refFeaturesBased.get(matches.get(i)[1]).getX();
				yRef += refFeaturesBased.get(matches.get(i)[1]).getY();
			}
			Point2D.Double srcCenter = new Point2D.Double(xSrc/matches.size(),ySrc/matches.size());
			Point2D.Double refCenter = new Point2D.Double(xRef/matches.size(),yRef/matches.size());


			// Scale
			double sumSrc = 0;
			double sumRef = 0;
			for (int i = 0; i < matches.size(); i++) {
				Point2D.Double pSrc = srcFeatures.get(matches.get(i)[0]);
				Point2D.Double pRef = refFeaturesBased.get(matches.get(i)[1]);
				double distSrc = pSrc.distance(srcCenter);
				double distRef = pRef.distance(refCenter);
				sumSrc += distSrc;
				sumRef += distRef;
			}
			double scale = sumRef / sumSrc;


			// Rotation angle
			double sumAngDiff = 0;
			for (int i = 0; i < matches.size(); i++) {
				Point2D.Double pSrc = srcFeatures.get(matches.get(i)[0]);
				Point2D.Double pRef = refFeaturesBased.get(matches.get(i)[1]);

				double ad = angleX(pRef, refCenter) - angleX(pSrc, srcCenter);
				if (ad > 180) {
					ad -= 360;
				}
				if (ad < -180) {
					ad += 360;
				}
				sumAngDiff += ad;
			}
			double rotAngle = sumAngDiff / matches.size();

			imageAlignment ia = new imageAlignment(0, 0,
					rotAngle, src.imgWidth() / 2, src.imgHeight() / 2,
					scale);

			// Translation
			double txSum = 0;
			double tySum = 0;
			for (int i = 0; i < matches.size(); i++) {
				Point2D.Double pSrc = srcFeatures.get(matches.get(i)[0]);
				Point2D.Double pRef = refFeaturesBased.get(matches.get(i)[1]);

				txSum += pRef.getX() - ia.x(pSrc.getX(), pSrc.getY());
				tySum += pRef.getY() - ia.y(pSrc.getX(), pSrc.getY());
			}
			ia.set_x0y0(txSum / matches.size(), tySum / matches.size());

			return ia;
		} else {
			return null;
		}
	}

	private void userConfirm(boolean OKCancel) { // true = OK, false = Cancel
		wasCancelled = !OKCancel;
		this.setVisible(false);
	}

	public boolean wasCancelled() {
		return wasCancelled;
	}

	static public String classID() {
		return "dlgMatchFeatures";
	}

	static public String author() {
		return "Florens Rohde, Matthias Schmidt";
	}

	static public String version() {
		return "November 14 2017";
	}

	private class match extends JLabel {
		private ArrayList<Integer[]> matches;
		private int item;
		match(ArrayList<Integer[]> matches, int i) {
			super("<html>" +
					"<font color=\""+String.format("#%06X", (0xFFFFFF & SRC_FP.getRGB()))+"\">" + matches.get(i)[0] + "</font>" +
					" -> " +
					"<font color=\""+String.format("#%06X", (0xFFFFFF & REF_FP.getRGB()))+"\">" + matches.get(i)[1] + "</font>" +
					"</html>", SwingConstants.CENTER);
			this.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
			this.setPreferredSize(DIM_MATCH);
			this.setMaximumSize(DIM_MATCH);
			this.setMinimumSize(DIM_MATCH);
			this.matches = matches;
			item = i;

			this.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					matches.remove(item);
					generateDialogContent();
					updateOverlay();
				}
			});
		}
	}
}