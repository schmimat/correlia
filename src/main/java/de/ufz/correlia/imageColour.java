/*
imageColour class is part of the image overlay project
The class provides information on the extent to which the overlay image
uses the RGB channels of the base image.
The values are factors in the range of [0 ... 2]

author: Matthias Schmidt
*/
package de.ufz.correlia;

import org.w3c.dom.Element;

import java.awt.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Locale;


public class imageColour {
	public static final int COLOR_MAX_VALUE = 255;
	public static final String HIDE = "Hide";
	public static final String SHOW = "Show";
	public static final String EXCLUSIVE = "Exclusive";
	public static final String ENHANCED = "Enhanced";

	private String visibility;

	imageColour(double red, double green, double blue) {
		set_red(red, 0.0);
		set_green(green, 0.0);
		set_blue(blue, 0.0);

		visibility = SHOW;
	}

	imageColour(xmlHandler xml, Element root) {
		Element rgb = xml.getElementByName(root, "rgb");
		set_RGB(xml.getDoubleAttribute(rgb, "r"), xml.getDoubleAttribute(rgb, "g"), xml.getDoubleAttribute(rgb, "b"));
		Element rgbOff = xml.getElementByName(root, "rgbOff");
		set_RGB_Off(xml.getDoubleAttribute(rgbOff, "ro"), xml.getDoubleAttribute(rgbOff, "go"), xml.getDoubleAttribute(rgbOff, "bo"));

		visibility = xml.getTextByElementName(root, "show");

	}

	imageColour(imageColour ic) {
		set_red(ic.R(), ic.Roff());
		set_green(ic.G(), ic.Goff());
		set_blue(ic.B(), ic.Boff());

		visibility = ic.get_visiblity();
	}

	public double R() {
		return m_R;
	}

	public double G() {
		return m_G;
	}

	public double B() {
		return m_B;
	}

	public double[] RGB() {
		return new double[]{m_R, m_G, m_B};
	}

	public double Roff() {
		return m_Roff;
	}

	public double Goff() {
		return m_Goff;
	}

	public double Boff() {
		return m_Boff;
	}

	public double[] RGBoff() {
		return new double[]{m_Roff, m_Goff, m_Boff};
	}

	public int[] iRGBoff() {
		return new int[]{(int) m_Roff, (int) m_Goff, (int) m_Boff};
	}

	public Color get_color() {
		return new Color((int) (COLOR_MAX_VALUE * R()), (int) (COLOR_MAX_VALUE * G()), (int) (COLOR_MAX_VALUE * B()));
	}

	public void set_color(Color c) {
		set_red((double) c.getRed() / COLOR_MAX_VALUE);
		set_green((double) c.getGreen() / COLOR_MAX_VALUE);
		set_blue((double) c.getBlue() / COLOR_MAX_VALUE);
	}

	public String get_colourString() {
		String s = new String("");

		s += String.format("%.2f", R());
		s += " ";
		s += String.format("%.2f", G());
		s += " ";
		s += String.format("%.2f", B());

		return s;
	}


	public Boolean set_red(double r) {
		if (r >= 0) {
			m_R = r;
			return true;
		} else {
			m_R = 1;
			return false;
		}
	}

	public Boolean set_red(double r, double ro) {
		if (r >= 0) {
			m_R = r;
			m_Roff = ro;
			return true;
		} else {
			m_R = 1;
			m_Roff = 0;
			return false;
		}
	}

	public Boolean set_green(double g) {
		if (g >= 0) {
			m_G = g;
			return true;
		} else {
			m_G = 1;
			return false;
		}
	}

	public Boolean set_green(double g, double go) {
		if (g >= 0) {
			m_G = g;
			m_Goff = go;
			return true;
		} else {
			m_G = 1;
			m_Goff = 0;
			return false;
		}
	}

	public Boolean set_blue(double b) {
		if (b >= 0) {
			m_B = b;
			return true;
		} else {
			m_B = 1;
			return false;
		}
	}

	public Boolean set_blue(double b, double bo) {
		if (b >= 0) {
			m_B = b;
			m_Boff = bo;
			return true;
		} else {
			m_B = 1;
			m_Boff = 0;
			return false;
		}
	}

	public Boolean set_RGB(double r, double g, double b) {
		r = Math.min(1.0, r);
		g = Math.min(1.0, g);
		b = Math.min(1.0, b);
		return set_red(r) && set_green(g) && set_blue(b);
	}

	public Boolean set_RGB_Off(double ro, double go, double bo) {
		return set_red(R(), ro) && set_green(G(), go) && set_blue(B(), bo);
	}

	public void set_visibility(String s) {
		visibility = s;
	}

	public String get_visiblity() {
		return visibility;
	}

	// save and load parameters
	public String build_saveString() {
		String s = new String("begin imageColour\n");

		if (Roff() == 0.0 && Goff() == 0.0 && Boff() == 0.0) {
			s += "  RGB ";
			s += (Double.toString(m_R)).replaceAll(",", ".") + " ";
			s += (Double.toString(m_G)).replaceAll(",", ".") + " ";
			s += (Double.toString(m_B)).replaceAll(",", ".") + "\n";
		} else {
			s += "  Red " + (Double.toString(m_R)).replaceAll(",", ".") + " " + (Double.toString(m_Roff)).replaceAll(",", ".") + "\n";
			s += "  Green " + (Double.toString(m_G)).replaceAll(",", ".") + " " + (Double.toString(m_Goff)).replaceAll(",", ".") + "\n";
			s += "  Blue " + (Double.toString(m_B)).replaceAll(",", ".") + " " + (Double.toString(m_Boff)).replaceAll(",", ".") + "\n";
		}
		s += "end imageColour\n";

		return s;
	}

	public Element buildXML(xmlHandler xml) {
		Element root = xml.createElement("imageColour");
		ArrayList<String> params;

		params = new ArrayList<>();
		params.add("r");
		params.add(Double.toString(m_R).replaceAll(",", "."));
		params.add("g");
		params.add(Double.toString(m_G).replaceAll(",", "."));
		params.add("b");
		params.add(Double.toString(m_B).replaceAll(",", "."));
		xml.addTextElementWithAttributes(root, "rgb", "", params);

		params = new ArrayList<>();
		params.add("ro");
		params.add(Double.toString(m_Roff).replaceAll(",", "."));
		params.add("go");
		params.add(Double.toString(m_Goff).replaceAll(",", "."));
		params.add("bo");
		params.add(Double.toString(m_Boff).replaceAll(",", "."));
		xml.addTextElementWithAttributes(root, "rgbOff", "", params);

		xml.addTextElement(root, "show", visibility);
		return root;
	}

	// colour function: col: origCol -> newCol; col(x) = a*x - b , a = m_RGB, b = m_RGBoff

	private double m_R, m_G, m_B;
	private double m_Roff, m_Goff, m_Boff;

	static public String classID() {
		return "imageColour";
	}

	static public String author() {
		return "Matthias Schmidt";
	}

	static public String version() {
		return "December 1 2016";
	}
}
