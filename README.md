Correlia is an open-source ImageJ/FIJI plug-in for the registration of 2D multi-modal microscopy data-sets. The software is developed at ProVIS - Centre for Correlative Microscopy and is specifically designed for the needs of chemical microscopy involving various micrographs as well as chemical maps at different resolutions and field-of-views. 

[Website](https://www.ufz.de/correlia)

Please note, that this repository is no longer actively worked on.
We are currently working on a complete rewrite with the following improvements:
* ImageJ2 Plugin
* Refactoring to use dev standards for class names etc
* Unit tests
* Clear separation of functionality and user interface